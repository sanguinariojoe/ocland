/*
 *  This file is part of ocland, a free cloud OpenCL interface.
 *  Copyright (C) 2012-2019  Jose Luis Cercos Pita <jlcercos@gmail.com>
 *
 *  ocland is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  ocland is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with ocland.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef ARCH_H_INCLUDED
#define ARCH_H_INCLUDED

/** @brief Return a string representing the architecture string
 *
 * The architecture string contains a first character defining whether
 * big endian ('<') or little endian is applied ('>'), followed by an arbitrary
 * number of characters defining the size (in bytes) of several types, like
 * pointers and sizes
 *
 * @return Architecture string
 */
char* architecture_string();

#endif  // ARCH_H_INCLUDED
