/*
 *  This file is part of ocland, a free cloud OpenCL interface.
 *  Copyright (C) 2012-2019  Jose Luis Cercos Pita <jlcercos@gmail.com>
 *
 *  ocland is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  ocland is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with ocland.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <stdbool.h>

#ifndef LIST_H_INCLUDED
#define LIST_H_INCLUDED

/** @brief Generic list node
 *
 * A list is nothing else but a list of nodes linked between them, such that
 * each node knows the previous and next elements on the list.
 * On top of that, the #List will store the starting and ending nodes, as well
 * as the number of elements
 */
typedef struct _Node
{
    /** @brief Generic data.
     *
     * #List will be responsible of allocating/deallocating memory for the inner
     * object.
     */
    void  *data;
    /// Size of the stored data
    size_t size;
    /// Next chain node
    struct _Node *next;
    /// Former chain node
    struct _Node *prev;
    /// A special field to can return null objects (to easily traverse lists)
    bool is_null;
} Node;

/** @brief Generic list
 *
 * This list implements a generic list, i.e. an agnostic list which is able to
 * store any kind of data. Even though this list can be used in a straight way,
 * it is however strongly recommended to use typed lists
 * @see typed_list.h
 */
typedef struct _List List;

/** @brief Create a new list
 *
 * The lists shall be deleted at some point calling delete_list()
 * @return The new list
 */
List* list_new();

/** @brief Add a new node at the beginning of the list
 * @param list Considered list
 * @param data Node data
 * @param data_size Node data size
 * @return The same added node
 */
Node list_push_front(List* list, void* data, size_t data_size);

/** @brief Add a new node at the end of the list
 * @param data Node data
 * @param data_size Node data size
 * @return The same appended node
 */
Node list_push_back(List* list, void* data, size_t data_size);

/** @brief Get the first node of the list
 * @param list Considered list
 * @return First node of the list
 */
Node list_front(List* list);

/** @brief Get the last node of the list
 * @param list Considered list
 * @return Last node of the list
 */
Node list_back(List* list);

/** @brief Drop the first node of the list
 * @param list Considered list
 * @return The drop node
 * @warning Remember to manually free the inner data of the returned node
 */
Node list_pop_front(List* list);

/** @brief Drop the last node of the list
 * @param list Considered list
 * @return The drop node
 * @warning Remember to manually free the inner data of the returned node
 */
Node list_pop_back(List* list);

/** @brief Get the number of nodes inside the list
 * @param list Considered list
 * @return List length
 */
size_t list_len(List* list);

/** @brief Delete a whole list, including all the inner nodes
 * @param list Considered list
 * @remarks After calling this method, the list pointer shall be considered
 * invalid
 */
void list_delete(List* list);

/** @brief Get the next node in the list
 * @param node Current node
 * @return Next node
 */
Node list_next(Node node);

/** @brief Get the previous node in the list
 * @param node Current node
 * @return Previous node
 */
Node list_prev(Node node);

/** @brief Get the node data
 * @param node Node
 * @return Node data
 */
void* list_data(Node node);

/** @brief Check whether the node is the first one or not
 * @param node Node
 * @return 1 if it is the first node, 0 otherwise
 */
bool list_is_first(Node node);

/** @brief Check whether the node is the last one or not
 * @param node Node
 * @return 1 if it is the last node, 0 otherwise
 */
bool list_is_last(Node node);

/** @brief Get if it is the null node (end of a list)
 * @param node Node
 * @return false for regular nodes, true when the next to the last node or the
 * previous of the first one are queried
 */
bool list_is_null(Node node);

/** @brief Get a node by its index
 * @param list Considered list
 * @param index Node to get extracted
 * @warning Pretty inneficient method. Use front() and back() when
 * possible
 * @return The node
 */
Node list_at(List* list, size_t index);

/** @brief Insert a node into the list
 * @param list Considered list
 * @param data Node data
 * @param data_size Node data size
 * @param index Insertion point
 * @warning Pretty inneficient method. Use push_front() and push_back() when
 * possible
 * @return The same inserted node
 */
Node list_insert(List* list, void* data, size_t data_size, size_t index);

/** @brief Remove a node from the list
 * @param list Considered list
 * @param index Removal point
 * @warning Pretty inneficient method. Use pop_front() and pop_back() when
 * possible
 * @return The removed node
 * @warning Remember to manually free the inner data of the returned node
 */
Node list_remove_at(List* list, size_t index);

/** @brief Find the data inside the list
 * @param list Considered list
 * @param data Data to look for
 * @param data_size Size of the data
 * @return The appropriate node index, or the length of the list if the node
 * cannot be found.
 */
size_t list_find(List* list, void* data, size_t data_size);

#endif // LIST_H_INCLUDED
