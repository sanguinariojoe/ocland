/*
 *  This file is part of ocland, a free cloud OpenCL interface.
 *  Copyright (C) 2012-2019  Jose Luis Cercos Pita <jlcercos@gmail.com>
 *
 *  ocland is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  ocland is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with ocland.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <stdbool.h>

#ifndef NOTIFIER_H_INCLUDED
#define NOTIFIER_H_INCLUDED

/**
 * @brief Notifier instance
 *
 * Notifiers are instances which keeps listening from the remote peer, which
 * would send some data associated to an identifier
 */
typedef struct _notifier* notifier;

/** @brief Create a new notifier
 *
 * The notifier shall be deleted at some point calling notifier_delete()
 * @param socket Already connected socket where the notifier will be listening
 * @return The new notifier
 */
notifier notifier_new(int socket);

/** @brief Delete a notifier
 * @param n Notifier to be deleted
 * @remarks After calling this method, the list pointer shall be considered
 * invalid
 */
void notifier_delete(notifier n);

/**
 * Append a notification endpoint
 * @param n          Notifier
 * @param identifier Identifier which shall be received from remote peer to
 *                   trigger the launch of fn
 * @param fn         Function to be triggered, which receives 4 arguments: The
 *                   identifier, the size of the data provided by the remote
 *                   peer, the message from the remote peer, and the user data
 *                   (which might be NULL)
 * @param user_data  NULL, or a pointer to memory allocated data to be passed
 *                   to the function
 * @param once       Wether the notification shall ran just once or not
 */
 void notifier_append(notifier n,
                      void* id,
                      void (*fn) (void *, size_t, void *, void*),
                      void* user_data,
                      bool once);

/**
 * Remove a notification endpoint
 * @param n          Notifier
 * @param identifier Identifier of the notification endpoint to be removed
 */
void notifier_remove(notifier n, void* identifier);

/**
 * Set a notification endpoint which is called whenever no valid endpoint is
 * found for the received identifier
 * @param n          Notifier
 * @param fn         Function to be triggered, which receives 4 arguments: The
 *                   identifier, the size of the data provided by the remote
 *                   peer, the message from the remote peer, and the user data
 *                   (which might be NULL)
 * @param user_data  NULL, or a pointer to memory allocated data to be passed
 *                   to the function
 */
void notifier_set_default(notifier n,
                          void (*fn) (void *, size_t, void *, void*),
                          void* user_data);

/**
 * Remove the default notification endpoint
 * @param n          Notifier
 * @see notifier_set_default()
 */
void notifier_unset_default(notifier n);

#endif // NOTIFIER_H_INCLUDED
