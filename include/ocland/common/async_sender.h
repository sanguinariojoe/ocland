/*
 *  This file is part of ocland, a free cloud OpenCL interface.
 *  Copyright (C) 2012-2019  Jose Luis Cercos Pita <jlcercos@gmail.com>
 *
 *  ocland is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  ocland is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with ocland.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef ASYNCSENDER_H_INCLUDED
#define ASYNCSENDER_H_INCLUDED

/**
 * @brief Thread safe asynchronous Sender instance
 *
 * Asynchronous Senders are instances which may send data to the counterpart
 * peer in an asynchronous way
 */
typedef struct _async_sender* async_sender;

/** @brief Create a new async_sender
 *
 * The async_sender shall be deleted at some point calling async_sender_delete()
 * @param socket Already connected socket for the async_sender
 * @return The new async_sender
 */
async_sender async_sender_new(int socket);

/** @brief Delete a async_sender
 * @param sender Asynchronous Sender to be deleted
 */
void async_sender_delete(async_sender sender);

/**
 * Send data to the counterpart peer
 * @param sender Asynchronous sender
 * @param id     Identifier to be prefixed to the package to let the peer know
 *               which package is it
 * @param size   Size of the data to be sent
 * @param data   Data to be sent
 * @return       The amount of sent data
 */
ssize_t async_send(async_sender sender, void* id, size_t size, void* data);

#endif // ASYNCSENDER_H_INCLUDED
