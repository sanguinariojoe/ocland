/*
 *  This file is part of ocland, a free cloud OpenCL interface.
 *  Copyright (C) 2012-2019  Jose Luis Cercos Pita <jlcercos@gmail.com>
 *
 *  ocland is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  ocland is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with ocland.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef ASYNCRECEIVER_H_INCLUDED
#define ASYNCRECEIVER_H_INCLUDED

#include <ocland/common/dataExchange.h>

/**
 * @brief Thread safe asynchronous Receiver instance
 *
 * Asynchronous Receivers are instances which may send data to the counterpart
 * peer in an asynchronous way
 */
typedef struct _async_receiver* async_receiver;

/** @brief Create a new async_receiver
 *
 * The async_receiver shall be deleted at some point calling
 * async_receiver_delete()
 * @param socket Already connected socket for the async_receiver
 * @return The new async_receiver
 */
async_receiver async_receiver_new(int socket);

/** @brief Delete a async_receiver
 * @param receiver Asynchronous Receiver to be deleted
 */
void async_receiver_delete(async_receiver receiver);

/**
 * Non-blocking operation to know if a particular message is ready
 * @param receiver Asynchronous receiver
 * @param id       Identifier agreed with the counterpart peer
 * @return         true if the message can be obtained, false otherwise
 */
bool async_has_msg(async_receiver receiver, void* id);

/**
 * Receive data from the counterpart peer
 *
 * This is a thread safe blocking operation which is waiting for the data to be
 * ready
 * @param receiver Asynchronous receiver
 * @param id       Identifier agreed with the counterpart peer
 * @return         The data, with NULL inner data member if an error happened.
 *                 Remember to free the inner data member
 */
recv_pending async_recv(async_receiver receiver, void* id);

#endif // ASYNCRECEIVER_H_INCLUDED
