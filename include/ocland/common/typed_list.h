/*
 *  This file is part of ocland, a free cloud OpenCL interface.
 *  Copyright (C) 2012-2019  Jose Luis Cercos Pita <jlcercos@gmail.com>
 *
 *  ocland is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  ocland is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with ocland.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef TYPED_LIST_H_INCLUDED
#define TYPED_LIST_H_INCLUDED

// Concatenate 2 strings
#ifdef PASTER
#undef PASTER
#endif
#define PASTER(x, y) x ## _ ## y
#ifdef CAT
#undef CAT
#endif
#define CAT(x,y)  PASTER(x,y)

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <ocland/common/list.h>

/**
 * Carry out a typed list declaration
 *
 * This operation can be carried out many times, provided that DEFINE_LIST is
 * called once
 *
 * @param  TYPE   Typed list type
 * @param  PREFIX Typed list name prefix. All the methods available in list.h
 *                will be available now, with the selected prefix
 */
#define DECLARE_LIST(TYPE, PREFIX)                                             \
List* CAT(PREFIX,new) ();                                                      \
CAT(PREFIX,node) CAT(PREFIX,push_front) (List* list, TYPE data);               \
CAT(PREFIX,node) CAT(PREFIX,push_back) (List* list, TYPE data);                \
CAT(PREFIX,node) CAT(PREFIX,front) (List* list);                               \
CAT(PREFIX,node) CAT(PREFIX,back) (List* list);                                \
CAT(PREFIX,node) CAT(PREFIX,pop_front) (List* list);                           \
CAT(PREFIX,node) CAT(PREFIX,pop_back) (List* list);                            \
size_t CAT(PREFIX,len) (List* list);                                           \
void CAT(PREFIX,delete) (List* list);                                          \
CAT(PREFIX,node) CAT(PREFIX,next) (CAT(PREFIX,node) node);                     \
CAT(PREFIX,node) CAT(PREFIX,prev) (CAT(PREFIX,node) node);                     \
TYPE CAT(PREFIX,data) (CAT(PREFIX,node) node);                                 \
bool CAT(PREFIX,is_first) (CAT(PREFIX,node) node);                             \
bool CAT(PREFIX,is_last) (CAT(PREFIX,node) node);                              \
bool CAT(PREFIX,is_null) (CAT(PREFIX,node) node);                              \
CAT(PREFIX,node) CAT(PREFIX,at) (List* list, size_t index);                    \
CAT(PREFIX,node) CAT(PREFIX,insert) (List* list, TYPE data, size_t index);     \
CAT(PREFIX,node) CAT(PREFIX,remove_at) (List* list, size_t index);             \
size_t CAT(PREFIX,find) (List* list, TYPE data);

/**
 * Helper macro to reimplement functions that only requires to get translated
 * from generic data to typed one
 * @param  TYPE   Typed list type
 * @param  PREFIX Typed list name prefix
 * @param  FUNC   Function name
 */
#define SIMPLE_TYPED_LIST_REIMPLEMENTATION(TYPE, PREFIX, FUNC)                 \
CAT(PREFIX,node) CAT(PREFIX,FUNC) (List* list, TYPE data)                      \
{                                                                              \
    return CAT(PREFIX,from_list_node) (                                        \
        CAT(list,FUNC)(list, &data, sizeof(TYPE)));                            \
}

/**
 * Carry out a typed list definition/implementation
 *
 * This operation shall be carried out just once
 *
 * @param  TYPE   Typed list type
 * @param  PREFIX Typed list name prefix. All the methods available in list.h
 *                will be available now, with the selected prefix
 */
#define DEFINE_LIST(TYPE, PREFIX)                                              \
struct CAT(PREFIX,NodeStruct)                                                  \
{                                                                              \
    TYPE data;                                                                 \
    Node *next;                                                                \
    Node *prev;                                                                \
    bool is_null;                                                              \
};                                                                             \
typedef struct CAT(PREFIX,NodeStruct) CAT(PREFIX,node);                        \
CAT(PREFIX,node) CAT(PREFIX,null_node)()                                       \
{                                                                              \
    CAT(PREFIX,node) node;                                                     \
    node.next = NULL;                                                          \
    node.prev = NULL;                                                          \
    node.is_null = true;                                                       \
    return node;                                                               \
}                                                                              \
List* CAT(PREFIX,new) ()                                                       \
{                                                                              \
    return list_new();                                                         \
}                                                                              \
CAT(PREFIX,node) CAT(PREFIX,from_list_node) (Node node)                        \
{                                                                              \
    CAT(PREFIX,node) new_node;                                                 \
    new_node.next = node.next;                                                 \
    new_node.prev = node.prev;                                                 \
    new_node.is_null = node.is_null;                                           \
    if(!node.is_null)                                                          \
        memcpy(&(new_node.data), node.data, sizeof(TYPE));                     \
    return new_node;                                                           \
}                                                                              \
CAT(PREFIX,node) CAT(PREFIX,push_front) (List* list, TYPE data)                \
{                                                                              \
    return CAT(PREFIX,from_list_node) (                                        \
        CAT(list,push_front)(list, &data, sizeof(TYPE)));                      \
}                                                                              \
CAT(PREFIX,node) CAT(PREFIX,push_back) (List* list, TYPE data)                 \
{                                                                              \
    return CAT(PREFIX,from_list_node) (                                        \
        CAT(list,push_back)(list, &data, sizeof(TYPE)));                       \
}                                                                              \
CAT(PREFIX,node) CAT(PREFIX,front) (List* list)                                \
{                                                                              \
    return CAT(PREFIX,from_list_node) ( CAT(list,front)(list) );               \
}                                                                              \
CAT(PREFIX,node) CAT(PREFIX,back) (List* list)                                 \
{                                                                              \
    return CAT(PREFIX,from_list_node) ( CAT(list,back)(list) );                \
}                                                                              \
CAT(PREFIX,node) CAT(PREFIX,pop_front) (List* list)                            \
{                                                                              \
    Node generic_node = list_pop_front(list);                                  \
    CAT(PREFIX,node) node = CAT(PREFIX,from_list_node) (generic_node);         \
    free(generic_node.data);                                                   \
    return node;                                                               \
}                                                                              \
CAT(PREFIX,node) CAT(PREFIX,pop_back) (List* list)                             \
{                                                                              \
    Node generic_node = list_pop_back(list);                                   \
    CAT(PREFIX,node) node = CAT(PREFIX,from_list_node) (generic_node);         \
    free(generic_node.data);                                                   \
    return node;                                                               \
}                                                                              \
size_t CAT(PREFIX,len) (List* list) { return list_len(list); }                 \
void CAT(PREFIX,delete) (List* list) { list_delete(list); }                    \
CAT(PREFIX,node) CAT(PREFIX,next) (CAT(PREFIX,node) node)                      \
{                                                                              \
    if(!node.next)                                                             \
        return CAT(PREFIX,null_node)();                                        \
    return CAT(PREFIX,from_list_node) (*(node.next));                          \
}                                                                              \
CAT(PREFIX,node) CAT(PREFIX,prev) (CAT(PREFIX,node) node)                      \
{                                                                              \
    if(!node.prev)                                                             \
        return CAT(PREFIX,null_node)();                                        \
    return CAT(PREFIX,from_list_node) (*(node.prev));                          \
}                                                                              \
TYPE CAT(PREFIX,data) (CAT(PREFIX,node) node) { return node.data; }            \
bool CAT(PREFIX,is_first) (CAT(PREFIX,node) node)                              \
{                                                                              \
    return !node.is_null && (node.prev == NULL);                               \
}                                                                              \
bool CAT(PREFIX,is_last) (CAT(PREFIX,node) node)                               \
{                                                                              \
    return !node.is_null && (node.next == NULL);                               \
}                                                                              \
bool CAT(PREFIX,is_null) (CAT(PREFIX,node) node) { return node.is_null; }      \
CAT(PREFIX,node) CAT(PREFIX,at) (List* list, size_t index)                     \
{                                                                              \
    return CAT(PREFIX,from_list_node) (list_at(list, index));                  \
}                                                                              \
CAT(PREFIX,node) CAT(PREFIX,insert) (List* list, TYPE data, size_t index)      \
{                                                                              \
    return CAT(PREFIX,from_list_node) (                                        \
        list_insert(list, &data, sizeof(TYPE), index));                        \
}                                                                              \
CAT(PREFIX,node) CAT(PREFIX,remove_at) (List* list, size_t index)              \
{                                                                              \
    Node generic_node = list_remove_at(list, index);                           \
    CAT(PREFIX,node) node = CAT(PREFIX,from_list_node) (generic_node);         \
    free(generic_node.data);                                                   \
    return node;                                                               \
}                                                                              \
size_t CAT(PREFIX,find) (List* list, TYPE data)                                \
{                                                                              \
    return list_find(list, &data, sizeof(TYPE));                               \
}

#endif // TYPED_LIST_H_INCLUDED
