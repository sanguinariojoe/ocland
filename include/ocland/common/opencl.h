/*
 *  This file is part of ocland, a free cloud OpenCL interface.
 *  Copyright (C) 2012-2019  Jose Luis Cercos Pita <jlcercos@gmail.com>
 *
 *  ocland is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  ocland is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with ocland.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef OCLAND_OPENCL_H_INCLUDED
#define OCLAND_OPENCL_H_INCLUDED

#define CL_USE_DEPRECATED_OPENCL_1_0_APIS
#define CL_USE_DEPRECATED_OPENCL_1_1_APIS
#define CL_USE_DEPRECATED_OPENCL_1_2_APIS
#define CL_USE_DEPRECATED_OPENCL_2_0_APIS
#define CL_USE_DEPRECATED_OPENCL_2_1_APIS
#ifndef CL_TARGET_OPENCL_VERSION
#define CL_TARGET_OPENCL_VERSION 220
#endif
#include <CL/cl.h>
#include <CL/cl_gl.h>
#include <CL/cl_gl_ext.h>
#include <CL/cl_egl.h>
#include <CL/cl_ext.h>
// #include <CL/cl_d3d10.h>
// #include <CL/cl_d3d11.h>
// #include <CL/cl_dx9_media_sharing.h>
#ifndef CL_EXT_PREFIX__VERSION_1_1_DEPRECATED
    #define CL_EXT_PREFIX__VERSION_1_1_DEPRECATED
#endif
#ifndef CL_EXT_SUFFIX__VERSION_1_1_DEPRECATED
    #define CL_EXT_SUFFIX__VERSION_1_1_DEPRECATED
#endif

const char* get_opencl_error_name(cl_int error);

#endif  // OCLAND_OPENCL_H_INCLUDED
