/*
 *  This file is part of ocland, a free cloud OpenCL interface.
 *  Copyright (C) 2012  Jose Luis Cercos Pita <jl.cercos@upm.es>
 *
 *  ocland is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  ocland is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with ocland.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef DATAEXCHANGE_H_INCLUDED
#define DATAEXCHANGE_H_INCLUDED

#include <stddef.h>
#include <sys/types.h>
#include <stdio.h>
#include <stdarg.h>

#define OCLAND_NULL_PTR_STR "OCLAND_NULL_PTR_AS_STRING\0"
#define OCLAND_NULL_PTR_STR_SIZE 26

/** @brief Disassemble a binary message to a set of arguments
 * @param msg Message to disassembly
 * @param VARARGS The assembling objects. Each entry of an object is actually
 * composed by 2 arguments, its size and the pointer of the memory address where
 * data shall be copy. The list of objects shall end with a 0 size and NULL
 * pointer
 * @return Pointer to the remaining message, NULL if errors are detected.
 */
char* disassemble_msg(char* msg, ...);

/** Returns the last socket error detected
 * @return Error detected.
 */
char* SocketsError();

/** recv method reimplementation. This reimplementation will expect that received data matchs
 * with request data on length, if not (errors or mismatch) connection will closed (and socket
 * conviniently set to -1).
 * @param socket Specifies the socket file descriptor.
 * @param buffer Points to a buffer where the message should be stored.
 * @param length Specifies the length in bytes of the buffer pointed to by the buffer argument.
 * @param flags Specifies the type of message reception.
 * @return Upon successful completion, Recv() shall return the length of the message in bytes.
 * If no messages are available to be received and the peer has performed an orderly shutdown,
 * Recv() shall return 0. Otherwise, -1 shall be returned and errno set to indicate the error.
 */
ssize_t Recv(int *socket, void *buffer, size_t length, int flags);

/** send method reimplementation. This reimplementation will expect that sent data matchs
 * with request data on length, if not (errors or mismatch) connection will closed (and socket
 * conviniently set to -1).
 * @param socket Specifies the socket file descriptor.
 * @param buffer Points to the buffer containing the message to send.
 * @param length Specifies the length of the message in bytes.
 * @param flags Specifies the type of message transmission.
 * @return Upon successful completion, Send() shall return the number of bytes sent. Otherwise,
 * -1 shall be returned and errno set to indicate the error.
 */
ssize_t Send(int *socket, const void *buffer, size_t length, int flags);

/** @brief Assemble and send to the counterpart
 *
 * Sending data is always easy, since the amount of data to send is always
 * known. Along this time, this method is taking care on assembling all the
 * information and sending it to the remote peer.
 * @param socket Open socket with the remote peer
 * @param VARARGS The assembling objects. This is composed by 2 lists of objects,
 * first a list of sizes, which should end with a 0 size, followed by a list of
 * pointers to the objects memory addresses, ending up with a NULL pointer.
 * @return 0 if everything went right, 1 otherwise
 * @remarks The number of sizes and pointers shall match
 */
int send_comm(int socket, ...);

/** @brief Backend for send_comm()
 *
 * This method is conveniently provided to allow other methods to forward the
 * variable list of arguments to sender
 * @param socket Open socket with the remote peer
 * @param args The assembling objects, formatted in the same way than
 * send_comm()
 * @return 0 if everything went right, 1 otherwise
 * @remarks The number of sizes and pointers shall match
 */
int vsend_comm(int socket, va_list args);

/// Remaining message after disassembly
typedef struct _recv_pending {
    /// Size of the remaining message
    size_t size;
    /// Remaining message
    char* data;
} recv_pending;

/** @brief Receive and disassembly from the counterpart
 *
 * Sometimes receiving is also easy, sometimes is not, since the size of the
 * received data depends on some chunks of the received data itself.
 *
 * For this reason this method is slighly more complex, becoming able to
 * dissasembly the received data, giving back as well the remaining information
 * in a recv_pending structure.
 * @param socket Open socket with the remote peer
 * @param msg Remaining message afgter dissasembly. The method will allocate all
 * the required memory, just remeber to free the data attribute afterwards. NULL
 * can be sent if the remaining message can be safely discarded
 * @param VARARGS The disassembling objects. This is composed by 2 lists of objects,
 * first a list of sizes, which should end with a 0 size, followed by a list of
 * pointers to the objects memory addreses, ending up with a NULL pointer.
 * @return 0 if everything went right, 1 otherwise
 * @remarks The number of sizes and pointers shall match
 */
int recv_comm(int socket, recv_pending *msg, ...);

#endif // DATAEXCHANGE_H_INCLUDED
