/*
 *  This file is part of ocland, a free cloud OpenCL interface.
 *  Copyright (C) 2012-2019  Jose Luis Cercos Pita <jlcercos@gmail.com>
 *
 *  ocland is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  ocland is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with ocland.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef OCLAND_DEVICE_H_INCLUDED
#define OCLAND_DEVICE_H_INCLUDED

#include <ocland/common/dataExchange.h>
#include <ocland/server/validator.h>
#include <ocland/server/get_info.h>

/** clGetDeviceIDs ocland abstraction
 *
 * In ocland server only num_entries parameter is expected in the incoming
 * message, assuming that if num_entries > 0 then the platforms array is
 * requested
 * @param clientfd Client connection socket.
 * @param buffer Buffer to exchange data.
 * @param v Validator.
 * @param msg Data received by the client.
 */
void ocland_clGetDeviceIDs(int* clientfd, validator v, recv_pending msg);

DECLARE_INFO_GETTER(clGetDeviceInfo)

// ----------------------------------
// OpenCL 1.2
// ----------------------------------
/** clCreateSubDevices ocland abstraction
 * @param clientfd Client connection socket.
 * @param buffer Buffer to exchange data.
 * @param v Validator.
 * @param msg Data received by the client.
 */
void ocland_clCreateSubDevices(int* clientfd, validator v, recv_pending msg);

/** clRetainDevice ocland abstraction
 * @param clientfd Client connection socket.
 * @param buffer Buffer to exchange data.
 * @param v Validator.
 * @param msg Data received by the client.
 */
void ocland_clRetainDevice(int* clientfd, validator v, recv_pending msg);

/** clReleaseDevice ocland abstraction
 * @param clientfd Client connection socket.
 * @param buffer Buffer to exchange data.
 * @param v Validator.
 * @param msg Data received by the client.
 */
void ocland_clReleaseDevice(int* clientfd, validator v, recv_pending msg);

#endif  // OCLAND_DEVICE_H_INCLUDED
