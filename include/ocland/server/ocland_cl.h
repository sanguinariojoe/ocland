/*
 *  This file is part of ocland, a free cloud OpenCL interface.
 *  Copyright (C) 2012  Jose Luis Cercos Pita <jl.cercos@upm.es>
 *
 *  ocland is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  ocland is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with ocland.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <ocland/server/validator.h>
#include <ocland/server/ocland_version.h>

#ifndef OCLAND_CL_H_INCLUDED
#define OCLAND_CL_H_INCLUDED


/** clEnqueueReadBuffer ocland abstraction
 * @param clientfd Client connection socket
 * @param buffer Buffer to exchange data
 * @param v Validator
 * @param data Data received by the client
 * @return 0 if message can't be dispatched, 1 otherwise
 */
void ocland_clEnqueueReadBuffer(int* clientfd, validator v, recv_pending msg);

/** clEnqueueWriteBuffer ocland abstraction
 * @param clientfd Client connection socket
 * @param buffer Buffer to exchange data
 * @param v Validator
 * @param data Data received by the client
 * @return 0 if message can't be dispatched, 1 otherwise
 */
void ocland_clEnqueueWriteBuffer(int* clientfd, validator v, recv_pending msg);

/** clEnqueueCopyBuffer ocland abstraction.
 * @param clientfd Client connection socket.
 * @param buffer Buffer to exchange data.
 * @param v Validator.
 * @param data Data received by the client.
 * @return 0 if message can't be dispatched, 1 otherwise.
 */
int ocland_clEnqueueCopyBuffer(int* clientfd, char* buffer, validator v, void* data);

/** clEnqueueCopyImage ocland abstraction.
 * @param clientfd Client connection socket.
 * @param buffer Buffer to exchange data.
 * @param v Validator.
 * @param data Data received by the client.
 * @return 0 if message can't be dispatched, 1 otherwise.
 */
int ocland_clEnqueueCopyImage(int* clientfd, char* buffer, validator v, void* data);

/** clEnqueueCopyImageToBuffer ocland abstraction.
 * @param clientfd Client connection socket.
 * @param buffer Buffer to exchange data.
 * @param v Validator.
 * @param data Data received by the client.
 * @return 0 if message can't be dispatched, 1 otherwise.
 */
int ocland_clEnqueueCopyImageToBuffer(int* clientfd, char* buffer, validator v, void* data);

/** clEnqueueCopyBufferToImage ocland abstraction.
 * @param clientfd Client connection socket.
 * @param buffer Buffer to exchange data.
 * @param v Validator.
 * @param data Data received by the client.
 * @return 0 if message can't be dispatched, 1 otherwise.
 */
int ocland_clEnqueueCopyBufferToImage(int* clientfd, char* buffer, validator v, void* data);

/** clEnqueueNDRangeKernel ocland abstraction.
 * @param clientfd Client connection socket.
 * @param buffer Buffer to exchange data.
 * @param v Validator.
 * @param data Data received by the client.
 * @return 0 if message can't be dispatched, 1 otherwise.
 */
int ocland_clEnqueueNDRangeKernel(int* clientfd, char* buffer, validator v, void* data);

/** clEnqueueReadImage ocland abstraction.
 * @param clientfd Client connection socket.
 * @param buffer Buffer to exchange data.
 * @param v Validator.
 * @param data Data received by the client.
 * @return 0 if message can't be dispatched, 1 otherwise.
 */
int ocland_clEnqueueReadImage(int* clientfd, char* buffer, validator v, void* data);

/** clEnqueueWriteImage ocland abstraction.
 * @param clientfd Client connection socket.
 * @param buffer Buffer to exchange data.
 * @param v Validator.
 * @param data Data received by the client.
 * @return 0 if message can't be dispatched, 1 otherwise.
 */
int ocland_clEnqueueWriteImage(int* clientfd, char* buffer, validator v, void* data);

// ----------------------------------
// OpenCL 1.1
// ----------------------------------

/** clEnqueueReadBufferRect ocland abstraction.
 * @param clientfd Client connection socket.
 * @param buffer Buffer to exchange data.
 * @param v Validator.
 * @return 0 if message can't be dispatched, 1 otherwise.
 */
int ocland_clEnqueueReadBufferRect(int* clientfd, char* buffer, validator v);

/** clEnqueueWriteBufferRect ocland abstraction.
 * @param clientfd Client connection socket.
 * @param buffer Buffer to exchange data.
 * @param v Validator.
 * @return 0 if message can't be dispatched, 1 otherwise.
 */
int ocland_clEnqueueWriteBufferRect(int* clientfd, char* buffer, validator v);

/** clEnqueueCopyBufferRect ocland abstraction.
 * @param clientfd Client connection socket.
 * @param buffer Buffer to exchange data.
 * @param v Validator.
 * @return 0 if message can't be dispatched, 1 otherwise.
 */
int ocland_clEnqueueCopyBufferRect(int* clientfd, char* buffer, validator v);

/** clEnqueueFillBuffer ocland abstraction.
 * @param clientfd Client connection socket.
 * @param buffer Buffer to exchange data.
 * @param v Validator.
 * @return 0 if message can't be dispatched, 1 otherwise.
 */
int ocland_clEnqueueFillBuffer(int* clientfd, char* buffer, validator v);

/** clEnqueueFillImage ocland abstraction.
 * @param clientfd Client connection socket.
 * @param buffer Buffer to exchange data.
 * @param v Validator.
 * @return 0 if message can't be dispatched, 1 otherwise.
 */
int ocland_clEnqueueFillImage(int* clientfd, char* buffer, validator v);

/** clEnqueueMigrateMemObjects ocland abstraction.
 * @param clientfd Client connection socket.
 * @param buffer Buffer to exchange data.
 * @param v Validator.
 * @return 0 if message can't be dispatched, 1 otherwise.
 */
int ocland_clEnqueueMigrateMemObjects(int* clientfd, char* buffer, validator v);

/** clEnqueueMarkerWithWaitList ocland abstraction.
 * @param clientfd Client connection socket.
 * @param buffer Buffer to exchange data.
 * @param v Validator.
 * @return 0 if message can't be dispatched, 1 otherwise.
 */
int ocland_clEnqueueMarkerWithWaitList(int* clientfd, char* buffer, validator v);

/** clEnqueueBarrierWithWaitList ocland abstraction.
 * @param clientfd Client connection socket.
 * @param buffer Buffer to exchange data.
 * @param v Validator.
 * @return 0 if message can't be dispatched, 1 otherwise.
 */
int ocland_clEnqueueBarrierWithWaitList(int* clientfd, char* buffer, validator v);

#endif // OCLAND_CL_H_INCLUDED
