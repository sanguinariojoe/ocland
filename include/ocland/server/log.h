/*
 *  This file is part of ocland, a free cloud OpenCL interface.
 *  Copyright (C) 2012  Jose Luis Cercos Pita <jl.cercos@upm.es>
 *
 *  ocland is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  ocland is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with ocland.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef LOG_H_INCLUDED
#define LOG_H_INCLUDED

#ifdef OCLAND_SERVER_VERBOSE
    #define VERBOSE_IN() {printf("[OCLAND %s:%d]: %s...\n", \
                          __FILE__, __LINE__, __func__);    \
                          fflush(stdout);}
    #define VERBOSE_OUT(flag) {printf("\t%s (:%d) -> %s (%d)\n",     \
                               __func__, __LINE__,                   \
                               get_opencl_error_name(flag), flag);   \
                               fflush(stdout);}
#else
    #define VERBOSE_IN()
    #define VERBOSE_OUT(flag)
#endif

#define eprintf(...) {                              \
    fprintf(stderr, "[OCLAND ERROR %s:%d]: %s\n",   \
                    __FILE__, __LINE__, __func__);  \
    fprintf(stderr, __VA_ARGS__);                   \
    fflush(stderr);                                 \
}

/** Set log file destination.
 * @param path file path.
 * @return 1 if file could opened,
 * 0 otherwise.
 */
int setLogFile(const char* path);

#endif // LOG_H_INCLUDED
