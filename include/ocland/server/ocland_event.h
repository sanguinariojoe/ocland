/*
 *  This file is part of ocland, a free cloud OpenCL interface.
 *  Copyright (C) 2012  Jose Luis Cercos Pita <jl.cercos@upm.es>
 *
 *  ocland is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  ocland is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with ocland.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef OCLAND_EVENT_LINKER_H_INCLUDED
#define OCLAND_EVENT_LINKER_H_INCLUDED

#ifndef CL_TARGET_OPENCL_VERSION
#define CL_TARGET_OPENCL_VERSION 220
#endif

#include <CL/cl.h>
#include <CL/cl_ext.h>

#include <ocland/common/typed_list.h>

typedef List* ocland_events_linker;

/**
 * Create a new ocland events linker instance
 *
 * Events are a paradigm shift, for the sake of performance. In the rest of
 * OpenCL entities, the reference which is used to exchange data with the client
 * is the locally generated one. Conversely, in events such reference is the one
 * provided by the client.
 *
 * The main benefit is clients may ask for events operations (including
 * command enqueing) in a streaming way, that is, without waiting for an answer.
 *
 * @return The new ocland events linker
 */
ocland_events_linker new_ocland_events_linker();

/**
 * Removes an ocland events linker
 * @param linker Ocland events linker
 */
void del_ocland_events_linker(ocland_events_linker linker);

/**
 * Create an ocland event linker on top of the local and remote references
 * @param linker Ocland events linker
 * @param local  Locally generated event using the OpenCL API
 * @param peer   Event provided by the remote peer
 */
void add_ocland_event_linker(ocland_events_linker linker,
                             cl_event local,
                             cl_event peer);

/**
 * Deletes an events linker
 * @param linker Ocland events linker
 * @param local  Locally generated event using the OpenCL API
 */
void remove_ocland_event_linker(ocland_events_linker linker, cl_event local);

/**
 * Get the local event from the client reference
 * @param  linker Ocland events linker
 * @param  peer   Remote reference
 * @return        Local event
 */
cl_event event_peer2local(ocland_events_linker linker, cl_event peer);

/**
 * Get the client event reference from the local event
 * @param  linker Ocland events linker
 * @param  local  Local event
 * @return        Client reference
 */
cl_event event_local2peer(ocland_events_linker linker, cl_event local);

#endif // OCLAND_EVENT_LINKER_H_INCLUDED
