/*
 *  This file is part of ocland, a free cloud OpenCL interface.
 *  Copyright (C) 2012-2019  Jose Luis Cercos Pita <jlcercos@gmail.com>
 *
 *  ocland is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  ocland is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with ocland.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef OCLAND_CONTEXT_H_INCLUDED
#define OCLAND_CONTEXT_H_INCLUDED

#include <ocland/common/dataExchange.h>
#include <ocland/server/validator.h>
#include <ocland/server/get_info.h>

/**
 * Safe way to release a context, removing all the associated stuff
 * @param  context Context
 * @return CL_SUCCESS if the context is correctly removed, CL_INVALID_CONTEXT
 *         otherwise
 */
cl_int release_context(cl_context context);

/**
 * Send an error message to the client context notify callback
 * @param v       Validator associated to the client
 * @param context Affected context
 * @param errinfo Error string
 */
void report_context_error(validator v,
                          cl_context context,
                          const char *errinfo);

/** clCreateContext ocland abstraction
 * @param clientfd Client connection socket
 * @param buffer Buffer to exchange data
 * @param v Validator
 * @param data Data received by the client
 * @return 0 if message can't be dispatched, 1 otherwise
 */
void ocland_clCreateContext(int* clientfd, validator v, recv_pending msg);

/** clCreateContextFromType ocland abstraction
 * @param clientfd Client connection socket
 * @param buffer Buffer to exchange data
 * @param v Validator
 * @param data Data received by the client
 * @return 0 if message can't be dispatched, 1 otherwise
 */
void ocland_clCreateContextFromType(int* clientfd, validator v, recv_pending msg);

/** clRetainContext ocland abstraction
 * @param clientfd Client connection socket
 * @param buffer Buffer to exchange data
 * @param v Validator
 * @param data Data received by the client
 * @return 0 if message can't be dispatched, 1 otherwise
 */
void ocland_clRetainContext(int* clientfd, validator v, recv_pending msg);

/** clReleaseContext ocland abstraction
 * @param clientfd Client connection socket
 * @param buffer Buffer to exchange data
 * @param v Validator
 * @param data Data received by the client
 * @return 0 if message can't be dispatched, 1 otherwise
 */
void ocland_clReleaseContext(int* clientfd, validator v, recv_pending msg);

DECLARE_INFO_GETTER(clGetContextInfo)

#endif  // OCLAND_CONTEXT_H_INCLUDED
