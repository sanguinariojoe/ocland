/*
 *  This file is part of ocland, a free cloud OpenCL interface.
 *  Copyright (C) 2012-2019  Jose Luis Cercos Pita <jlcercos@gmail.com>
 *
 *  ocland is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  ocland is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with ocland.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef OCLAND_PLATFORM_H_INCLUDED
#define OCLAND_PLATFORM_H_INCLUDED

#include <ocland/common/dataExchange.h>
#include <ocland/server/validator.h>
#include <ocland/server/get_info.h>

/** clGetPlatformIDs ocland abstraction.
 *
 * In ocland server only num_entries parameter is expected in the incoming
 * message, assuming that if num_entries > 0 then the platforms array is
 * requested
 * @param clientfd Client connection socket.
 * @param v Validator.
 * @param msg Data received from the client.
 */
void ocland_clGetPlatformIDs(int* clientfd, validator v, recv_pending msg);

DECLARE_INFO_GETTER(clGetPlatformInfo)

#endif  // OCLAND_PLATFORM_H_INCLUDED
