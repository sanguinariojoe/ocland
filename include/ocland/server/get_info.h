/*
 *  This file is part of ocland, a free cloud OpenCL interface.
 *  Copyright (C) 2012-2019  Jose Luis Cercos Pita <jlcercos@gmail.com>
 *
 *  ocland is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  ocland is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with ocland.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef OCLAND_GET_INFO_H_INCLUDED
#define OCLAND_GET_INFO_H_INCLUDED

#include <ocland/common/dataExchange.h>
#include <ocland/server/validator.h>

// Concatenate 2 strings
#ifdef PASTER
#undef PASTER
#endif
#define PASTER(x, y) x ## _ ## y
#ifdef CAT
#undef CAT
#endif
#define CAT(x,y)  PASTER(x,y)

/**
 * Declare an OpenCL information getter ocland abstraction
 *
 * Almost every single OpenCL info getter has the same signature and
 * functionality, so a set of macros can be conveniently used to avoid spaghetti
 * code
 *
 * @param  FUNC   OpenCL function name
 * @param  PREFIX Typed list name prefix. All the methods available in list.h
 *                will be available now, with the selected prefix
 */
#define DECLARE_INFO_GETTER(FUNC)                                              \
void CAT(ocland,FUNC) (int* clientfd, validator v, recv_pending msg);

/**
 * Define/implement an OpenCL information getter ocland abstraction
 *
 * Almost every single OpenCL info getter has the same signature and
 * functionality, so a set of macros can be conveniently used to avoid spaghetti
 * code
 *
 * @param  FUNC      OpenCL function name
 * @param  TYPE      OpenCL object type
 * @param  INFO_TYPE OpenCL param name type
 * @param  NAME      Ocland variable name. This name is used both for the object
 *                   and for the helper functions, like is_NAME to check whether
 *                   the received object is a valid one
 */
#define DEFINE_INFO_GETTER(FUNC, TYPE, INFO_TYPE, NAME)                        \
void CAT(ocland,FUNC) (int* clientfd, validator v, recv_pending msg)           \
{                                                                              \
    VERBOSE_IN();                                                              \
    cl_int flag = CL_OUT_OF_HOST_MEMORY;                                       \
    TYPE NAME;                                                                 \
    INFO_TYPE param_name;                                                      \
    size_t param_value_size, param_value_size_ret=0;                           \
    void *param_value = NULL;                                                  \
    const size_t msg_size = sizeof(TYPE) +                                     \
                            sizeof(INFO_TYPE) +                                \
                            sizeof(size_t);                                    \
    if( (msg.size != msg_size) ||                                              \
        !disassemble_msg(msg.data, sizeof(TYPE), &NAME,                        \
                                   sizeof(INFO_TYPE), &param_name,             \
                                   sizeof(size_t), &param_value_size,          \
                                   0, NULL) )                                  \
    {                                                                          \
        send_comm(*clientfd, sizeof(cl_int), sizeof(size_t), 0,                \
                             &flag, &param_value_size_ret, NULL);              \
        VERBOSE_OUT(flag);                                                     \
        return;                                                                \
    }                                                                          \
    if(!CAT(is,NAME)(v, NAME)) {                                               \
        flag = CL_INVALID_CONTEXT;                                             \
        send_comm(*clientfd, sizeof(cl_int), sizeof(size_t), 0,                \
                             &flag, &param_value_size_ret, NULL);              \
        VERBOSE_OUT(flag);                                                     \
        return;                                                                \
    }                                                                          \
    if(param_value_size) {                                                     \
        param_value = (void*)malloc(param_value_size);                         \
        if(!param_value) {                                                     \
            send_comm(*clientfd, sizeof(cl_int), sizeof(size_t), 0,            \
                                 &flag, &param_value_size_ret, NULL);          \
            VERBOSE_OUT(flag);                                                 \
            return;                                                            \
        }                                                                      \
        memset(param_value, '\0', param_value_size);                           \
    }                                                                          \
    flag = FUNC(NAME,                                                          \
                param_name,                                                    \
                param_value_size,                                              \
                param_value,                                                   \
                &param_value_size_ret);                                        \
    if( (flag != CL_SUCCESS) || !param_value_size ) {                          \
        free(param_value);                                                     \
        send_comm(*clientfd, sizeof(cl_int), sizeof(size_t), 0,                \
                             &flag, &param_value_size_ret, NULL);              \
        VERBOSE_OUT(flag);                                                     \
        return;                                                                \
    }                                                                          \
    send_comm(*clientfd, sizeof(cl_int),                                       \
                         sizeof(size_t),                                       \
                         param_value_size_ret,                                 \
                         0,                                                    \
                         &flag,                                                \
                         &param_value_size_ret,                                \
                         param_value,                                          \
                         NULL);                                                \
    free(param_value);                                                         \
    VERBOSE_OUT(flag);                                                         \
}

#endif  // OCLAND_GET_INFO_H_INCLUDED
