/*
 *  This file is part of ocland, a free cloud OpenCL interface.
 *  Copyright (C) 2012-2019  Jose Luis Cercos Pita <jlcercos@gmail.com>
 *
 *  ocland is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  ocland is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with ocland.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef OCLAND_COMMAND_QUEUE_H_INCLUDED
#define OCLAND_COMMAND_QUEUE_H_INCLUDED

#include <ocland/common/dataExchange.h>
#include <ocland/server/validator.h>
#include <ocland/server/get_info.h>

/** clCreateCommandQueue ocland abstraction
 * @param clientfd Client connection socket
 * @param buffer Buffer to exchange data
 * @param v Validator
 * @param data Data received by the client
 * @return 0 if message can't be dispatched, 1 otherwise
 */
void ocland_clCreateCommandQueue(int* clientfd, validator v, recv_pending msg);

/** clRetainCommandQueue ocland abstraction
 * @param clientfd Client connection socket
 * @param buffer Buffer to exchange data
 * @param v Validator
 * @param data Data received by the client
 * @return 0 if message can't be dispatched, 1 otherwise
 */
void ocland_clRetainCommandQueue(int* clientfd, validator v, recv_pending msg);

/** clReleaseCommandQueue ocland abstraction
 * @param clientfd Client connection socket
 * @param buffer Buffer to exchange data
 * @param v Validator
 * @param data Data received by the client
 * @return 0 if message can't be dispatched, 1 otherwise
 */
void ocland_clReleaseCommandQueue(int* clientfd, validator v, recv_pending msg);

DECLARE_INFO_GETTER(clGetCommandQueueInfo)

/** clCreateCommandQueueWithProperties ocland abstraction
 * @param clientfd Client connection socket
 * @param buffer Buffer to exchange data
 * @param v Validator
 * @param data Data received by the client
 * @return 0 if message can't be dispatched, 1 otherwise
 */
void ocland_clCreateCommandQueueWithProperties(int* clientfd,
                                              validator v,
                                              recv_pending msg);

#endif  // OCLAND_COMMAND_QUEUE_H_INCLUDED
