/*
 *  This file is part of ocland, a free cloud OpenCL interface.
 *  Copyright (C) 2012-2019  Jose Luis Cercos Pita <jl.cercos@upm.es>
 *
 *  ocland is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  ocland is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with ocland.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef OCLAND_KERNEL_H_INCLUDED
#define OCLAND_KERNEL_H_INCLUDED

#include <stdbool.h>

#include <ocland/common/opencl.h>
#include <ocland/client/ocland.h>
#include <ocland/client/ocland_icd.h>
#include <ocland/common/typed_list.h>

struct _cl_kernel
{
    /// Dispatch table
    struct _cl_icd_dispatch *dispatch;
    /// Pointer of server instance
    cl_kernel ptr;
    /// Reference count to control when the object must be destroyed
    cl_uint rcount;
    /// Associated server
    ocland_server server;
    /// Parent program
    cl_program program;
    /// Function name
    char* function_name;
    /// List of arguments
    List* args;
};

/** @brief Get the local kernel from the remote identifier
 * @param kernel Remote kernel instance
 * @return Local kernel instance, NULL if no kernel associated with the
 * remote instance can be found
 */
cl_kernel get_kernel(cl_kernel kernel);

/**
 * Check for valid local kernel
 * @param  kernel The local kernel identifier, not a remote peer one
 * @return true if the kernel is valid, false otherwise
 */
bool is_kernel(cl_kernel kernel);

/** clCreateKernel ocland abstraction method.
 */
cl_kernel oclandCreateKernel(cl_program       program ,
                             const char *     kernel_name ,
                             cl_int *         errcode_ret);

/** clCreateKernelsInProgram ocland abstraction method.
 */
cl_int oclandCreateKernelsInProgram(cl_program      program ,
                                    cl_uint         num_kernels ,
                                    cl_kernel *     kernels ,
                                    cl_uint *       num_kernels_ret);

/** clRetainKernel ocland abstraction method.
 */
cl_int oclandRetainKernel(cl_kernel     kernel);

/** clReleaseKernel ocland abstraction method.
 */
cl_int oclandReleaseKernel(cl_kernel    kernel);

/** clSetKernelArg ocland abstraction method.
 */
cl_int oclandSetKernelArg(cl_kernel     kernel ,
                          cl_uint       arg_index ,
                          size_t        arg_size ,
                          const void *  arg_value);

/** clGetKernelInfo ocland abstraction method.
 */
cl_int oclandGetKernelInfo(cl_kernel        kernel ,
                           cl_kernel_info   param_name ,
                           size_t           param_value_size ,
                           void *           param_value ,
                           size_t *         param_value_size_ret);

/** clGetKernelWorkGroupInfo ocland abstraction method.
 */
cl_int oclandGetKernelWorkGroupInfo(cl_kernel                   kernel ,
                                    cl_device_id                device ,
                                    cl_kernel_work_group_info   param_name ,
                                    size_t                      param_value_size ,
                                    void *                      param_value ,
                                    size_t *                    param_value_size_ret);

// -------------------------------------------- //
//                                              //
// OpenCL 1.2 methods                           //
//                                              //
// -------------------------------------------- //

/** clUnloadPlatformCompiler ocland abstraction method.
 */
 cl_int oclandGetKernelArgInfo(cl_kernel           kernel ,
                               cl_uint             arg_index ,
                               cl_kernel_arg_info  param_name ,
                               size_t              param_value_size ,
                               void *              param_value ,
                               size_t *            param_value_size_ret)
;


#endif  // OCLAND_KERNEL_H_INCLUDED
