/*
 *  This file is part of ocland, a free cloud OpenCL interface.
 *  Copyright (C) 2012-2019  Jose Luis Cercos Pita <jl.cercos@upm.es>
 *
 *  ocland is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  ocland is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with ocland.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef OCLAND_SAMPLER_H_INCLUDED
#define OCLAND_SAMPLER_H_INCLUDED

#include <stdbool.h>

#include <ocland/common/opencl.h>
#include <ocland/client/ocland.h>
#include <ocland/client/ocland_icd.h>

struct _cl_sampler
{
    /// Dispatch table
    struct _cl_icd_dispatch *dispatch;
    /// Pointer of server instance
    cl_sampler ptr;
    /// Reference count to control when the object must be destroyed
    cl_uint rcount;
    /// Associated server
    ocland_server server;
    /// Owning context
    cl_context context;
    /// Have normalized coords
    cl_bool normalized_coords;
    /// Addressing mode
    cl_addressing_mode addressing_mode;
    /// Addressing mode
    cl_filter_mode filter_mode;
};

/** @brief Get the local sampler from the remote identifier
 * @param sampler Remote sampler instance
 * @return Local sampler instance, NULL if no sampler associated with the
 * remote instance can be found
 */
cl_sampler get_sampler(cl_sampler sampler);

/**
 * Check for valid local sampler
 * @param  sampler The local sampler identifier, not a remote peer one
 * @return true if the sampler is valid, false otherwise
 */
bool is_sampler(cl_sampler sampler);

/** clCreateSampler ocland abstraction method.
 */
cl_sampler oclandCreateSampler(cl_context           context ,
                               cl_bool              normalized_coords ,
                               cl_addressing_mode   addressing_mode ,
                               cl_filter_mode       filter_mode ,
                               cl_int *             errcode_ret);


/** clRetainSampler ocland abstraction method.
 */
cl_int oclandRetainSampler(cl_sampler sampler);

/** clReleaseSampler ocland abstraction method.
 */
cl_int oclandReleaseSampler(cl_sampler sampler);

/** clGetSamplerInfo ocland abstraction method.
 */
cl_int oclandGetSamplerInfo(cl_sampler          sampler ,
                            cl_sampler_info     param_name ,
                            size_t              param_value_size ,
                            void *              param_value ,
                            size_t *            param_value_size_ret);

// -------------------------------------------- //
//                                              //
// OpenCL 2.0 methods                           //
//                                              //
// -------------------------------------------- //



#endif  // OCLAND_SAMPLER_H_INCLUDED
