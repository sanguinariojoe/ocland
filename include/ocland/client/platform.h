/*
 *  This file is part of ocland, a free cloud OpenCL interface.
 *  Copyright (C) 2012-2019  Jose Luis Cercos Pita <jl.cercos@upm.es>
 *
 *  ocland is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  ocland is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with ocland.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef OCLAND_PLATFORM_H_INCLUDED
#define OCLAND_PLATFORM_H_INCLUDED

#include <stdbool.h>

#include <ocland/common/opencl.h>
#include <ocland/client/ocland.h>
#include <ocland/client/ocland_icd.h>

/// cl_platform_id implementation
struct _cl_platform_id {
    /// Dispatch table
    struct _cl_icd_dispatch *dispatch;
    /// Pointer of server instance
    cl_platform_id ptr;
    /// Associated server
    ocland_server server;
};

/** @brief Set the global dispatch table
 *
 * This method is called from the ICD to let know this platforms manager about
 * the used dispatch table, a required field in all the OpenCL objects
 * implementation.
 */
void populate_dispatch_table(struct _cl_icd_dispatch *dispatch);

/** @brief Add a new platform to the list of available platforms
 * @param platform Remote platform instance
 * @param Associated server
 * @param Dispatch table
 */
void add_platform(cl_platform_id platform, ocland_server server);

/** @brief Get the local platform from the remote identifier
 * @param platform Remote platform instance
 * @return Local platform instance, NULL if no platform associated with the
 * remote instance can be found
 */
cl_platform_id get_platform(cl_platform_id platform);

/**
 * Check for valid local platforms
 * @param  platform The local platform identifier, not a remote peer one
 * @return true if the platform is valid, false otherwise
 */
bool is_platform(cl_platform_id platform);

/** clGetPlatformIDs ocland abstraction method.
 */
cl_int oclandGetPlatformIDs(cl_uint         num_entries,
                            cl_platform_id* platforms,
                            cl_uint*        num_platforms);

/** clGetPlatformInfo ocland abstraction method
 */
cl_int oclandGetPlatformInfo(cl_platform_id    platform,
                             cl_platform_info  param_name,
                             size_t            param_value_size,
                             void *            param_value,
                             size_t *          param_value_size_ret);


#endif  // OCLAND_PLATFORM_H_INCLUDED
