/*
 *  This file is part of ocland, a free cloud OpenCL interface.
 *  Copyright (C) 2012  Jose Luis Cercos Pita <jl.cercos@upm.es>
 *
 *  ocland is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  ocland is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with ocland.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <sys/socket.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <pthread.h>

#include <ocland/common/notifier.h>
#include <ocland/common/async_sender.h>
#include <ocland/common/async_receiver.h>
#include <ocland/common/opencl.h>

#ifndef OCLAND_H_INCLUDED
#define OCLAND_H_INCLUDED

/** @brief Server data
 */
struct _ocland_server
{
    /// Address
    char* address;
    /// Open Socket
    int socket;
    /// Callbacks socket
    int cb_socket;
    /// Data exchange socket
    int data_socket;
    /// Callbacks notifier instance
    notifier cb_notifier;
    /// Asynchronous data sender instance
    async_sender data_sender;
    /// Asynchronous data sender instance
    async_receiver data_receiver;
    /// Server communication interface locker
    pthread_mutex_t lock;
};

typedef struct _ocland_server* ocland_server;

/** @brief Initializes ocland, loading server files and connecting to servers.
 * @return Number of active servers.
 */
size_t oclandInit();

/// Methods integer identifiers
enum {
    ocland_clGetPlatformIDs,
    ocland_clGetPlatformInfo,
    ocland_clGetDeviceIDs,
    ocland_clGetDeviceInfo,
    ocland_clCreateContext,
    ocland_clCreateContextFromType,
    ocland_clRetainContext,
    ocland_clReleaseContext,
    ocland_clGetContextInfo,
    ocland_clCreateCommandQueue,
    ocland_clRetainCommandQueue,
    ocland_clReleaseCommandQueue,
    ocland_clGetCommandQueueInfo,
    ocland_clSetCommandQueueProperty, // (NO ICD)
    ocland_clCreateBuffer,
    ocland_clCreateImage2D,
    ocland_clCreateImage3D,
    ocland_clRetainMemObject,
    ocland_clReleaseMemObject,
    ocland_clGetSupportedImageFormats,
    ocland_clGetMemObjectInfo,
    ocland_clGetImageInfo,
    ocland_clCreateSampler,
    ocland_clRetainSampler,
    ocland_clReleaseSampler,
    ocland_clGetSamplerInfo,
    ocland_clCreateProgramWithSource,
    ocland_clCreateProgramWithBinary,
    ocland_clRetainProgram,
    ocland_clReleaseProgram,
    ocland_clBuildProgram,
    ocland_clUnloadCompiler,
    ocland_clGetProgramInfo,
    ocland_clGetProgramBuildInfo,
    ocland_clCreateKernel,
    ocland_clCreateKernelsInProgram,
    ocland_clRetainKernel,
    ocland_clReleaseKernel,
    ocland_clSetKernelArg,
    ocland_clGetKernelInfo,
    ocland_clGetKernelWorkGroupInfo,
    ocland_clWaitForEvents,
    ocland_clGetEventInfo,
    ocland_clRetainEvent,
    ocland_clReleaseEvent,
    ocland_clGetEventProfilingInfo,
    ocland_clFlush,
    ocland_clFinish,
    ocland_clEnqueueReadBuffer,
    ocland_clEnqueueWriteBuffer,
    ocland_clEnqueueCopyBuffer,
    ocland_clEnqueueReadImage,
    ocland_clEnqueueWriteImage,
    ocland_clEnqueueCopyImage,
    ocland_clEnqueueCopyImageToBuffer,
    ocland_clEnqueueCopyBufferToImage,
    ocland_clEnqueueMapBuffer,
    ocland_clEnqueueMapImage,
    ocland_clEnqueueUnmapMemObject,
    ocland_clEnqueueNDRangeKernel,
    ocland_clEnqueueTask,
    ocland_clEnqueueNativeKernel,
    ocland_clEnqueueMarker,
    ocland_clEnqueueWaitForEvents,
    ocland_clEnqueueBarrier,
    ocland_clGetExtensionFunctionAddress, // (NO ICD)
    ocland_clCreateFromGLBuffer,
    ocland_clCreateFromGLTexture2D,
    ocland_clCreateFromGLTexture3D,
    ocland_clCreateFromGLRenderbuffer,
    ocland_clGetGLObjectInfo,
    ocland_clGetGLTextureInfo,
    ocland_clEnqueueAcquireGLObjects,
    ocland_clEnqueueReleaseGLObjects,
    ocland_clGetGLContextInfoKHR,
    ocland_clGetDeviceIDsFromD3D10KHR, // (NO ICD)
    ocland_clCreateFromD3D10BufferKHR, // (NO ICD)
    ocland_clCreateFromD3D10Texture2DKHR, // (NO ICD)
    ocland_clCreateFromD3D10Texture3DKHR, // (NO ICD)
    ocland_clEnqueueAcquireD3D10ObjectsKHR, // (NO ICD)
    ocland_clEnqueueReleaseD3D10ObjectsKHR, // (NO ICD)
    ocland_clSetEventCallback,
    ocland_clCreateSubBuffer,
    ocland_clSetMemObjectDestructorCallback,
    ocland_clCreateUserEvent,
    ocland_clSetUserEventStatus,
    ocland_clEnqueueReadBufferRect,
    ocland_clEnqueueWriteBufferRect,
    ocland_clEnqueueCopyBufferRect,
    ocland_clCreateSubDevicesEXT,
    ocland_clRetainDeviceEXT,
    ocland_clReleaseDeviceEXT,
    ocland_clCreateEventFromGLsyncKHR,
    ocland_clCreateSubDevices,
    ocland_clRetainDevice,
    ocland_clReleaseDevice,
    ocland_clCreateImage,
    ocland_clCreateProgramWithBuiltInKernels,
    ocland_clCompileProgram,
    ocland_clLinkProgram,
    ocland_clUnloadPlatformCompiler,
    ocland_clGetKernelArgInfo,
    ocland_clEnqueueFillBuffer,
    ocland_clEnqueueFillImage,
    ocland_clEnqueueMigrateMemObjects,
    ocland_clEnqueueMarkerWithWaitList,
    ocland_clEnqueueBarrierWithWaitList,
    ocland_clGetExtensionFunctionAddressForPlatform, // (NO ICD)
    ocland_clCreateFromGLTexture,
    ocland_clGetDeviceIDsFromD3D11KHR, // (NO ICD)
    ocland_clCreateFromD3D11BufferKHR, // (NO ICD)
    ocland_clCreateFromD3D11Texture2DKHR, // (NO ICD)
    ocland_clCreateFromD3D11Texture3DKHR, // (NO ICD)
    ocland_clCreateFromDX9MediaSurfaceKHR, // (NO ICD)
    ocland_clEnqueueAcquireD3D11ObjectsKHR, // (NO ICD)
    ocland_clEnqueueReleaseD3D11ObjectsKHR, // (NO ICD)
    ocland_clGetDeviceIDsFromDX9MediaAdapterKHR, // (NO ICD)
    ocland_clEnqueueAcquireDX9MediaSurfacesKHR, // (NO ICD)
    ocland_clEnqueueReleaseDX9MediaSurfacesKHR, // (NO ICD)
    ocland_clCreateFromEGLImageKHR,
    ocland_clEnqueueAcquireEGLObjectsKHR,
    ocland_clEnqueueReleaseEGLObjectsKHR,
    ocland_clCreateEventFromEGLSyncKHR,
    ocland_clCreateCommandQueueWithProperties,
    ocland_clCreatePipe,
    ocland_clGetPipeInfo,
    ocland_clSVMAlloc,
    ocland_clSVMFree,
    ocland_clEnqueueSVMFree,
    ocland_clEnqueueSVMMemcpy,
    ocland_clEnqueueSVMMemFill,
    ocland_clEnqueueSVMMap,
    ocland_clEnqueueSVMUnmap,
    ocland_clCreateSamplerWithProperties,
    ocland_clSetKernelArgSVMPointer,
    ocland_clSetKernelExecInfo,
    ocland_clGetKernelSubGroupInfoKHR,
    ocland_clCloneKernel,
    ocland_clCreateProgramWithIL,
    ocland_clEnqueueSVMMigrateMem,
    ocland_clGetDeviceAndHostTimer,
    ocland_clGetHostTimer,
    ocland_clGetKernelSubGroupInfo,
    ocland_clSetDefaultDeviceCommandQueue,
    ocland_clSetProgramReleaseCallback,
    ocland_clSetProgramSpecializationConstant
};

#include <ocland/client/platform.h>

/** @brief Waits until the server is locked, and then gives access and block for
 * other instances
 *
 * You may lock the servers in order to avoid parallel packages sent/received
 * are used for the wrong method
 * @param socket Server socket
 */
void lock(ocland_server server);

/** @brief Unlock the server for other instances
 *
 * You may lock the servers in order to avoid parallel packages sent/received
 * are processed by the wrong method
 * @param socket Server socket
 */
void unlock(ocland_server server);

/**
 * Ask the server for an OpenCL entity information
 *
 * All information queries are carried out in the same way, so this method is
 * just reducing the spaghetti code
 *
 * The variable arguments shall be formatted in the same way it is done for
 * send_comm()
 * @param  server               Involved server
 * @param  param_value_size     Size of memory allocated in #param_value
 * @param  param_value          Memory address where value shall be copy
 * @param  param_value_size_ret Returning value size
 * @param  VARARGS              The arguments to be setn to the server
 * @return                      OpenCL status code, which depends on the
 *                              specific information query
 * @see send_comm()
 */
cl_int oclandGetGenericInfo(ocland_server server,
                            size_t param_value_size,
                            void *param_value,
                            size_t *param_value_size_ret,
                            ...);

/** clEnqueueReadBuffer ocland abstraction method.
 */
cl_int oclandEnqueueReadBuffer(cl_command_queue     command_queue ,
                               cl_mem               buffer ,
                               cl_bool              blocking_read ,
                               size_t               offset ,
                               size_t               cb ,
                               void *               ptr ,
                               cl_uint              num_events_in_wait_list ,
                               const cl_event *     event_wait_list ,
                               cl_event *           event);

/** clEnqueueWriteBuffer ocland abstraction method.
 */
cl_int oclandEnqueueWriteBuffer(cl_command_queue    command_queue ,
                                cl_mem              buffer ,
                                cl_bool             blocking_write ,
                                size_t              offset ,
                                size_t              cb ,
                                const void *        ptr ,
                                cl_uint             num_events_in_wait_list ,
                                const cl_event *    event_wait_list ,
                                cl_event *          event);

/** clEnqueueCopyBuffer ocland abstraction method.
 */
cl_int oclandEnqueueCopyBuffer(cl_command_queue     command_queue ,
                               cl_mem               src_buffer ,
                               cl_mem               dst_buffer ,
                               size_t               src_offset ,
                               size_t               dst_offset ,
                               size_t               cb ,
                               cl_uint              num_events_in_wait_list ,
                               const cl_event *     event_wait_list ,
                               cl_event *           event);

/** clEnqueueReadImage ocland abstraction method.
 * @param element_size Size of each element.
 */
cl_int oclandEnqueueReadImage(cl_command_queue      command_queue ,
                              cl_mem                image ,
                              cl_bool               blocking_read ,
                              const size_t *        origin ,
                              const size_t *        region ,
                              size_t                row_pitch ,
                              size_t                slice_pitch ,
                              size_t                element_size ,
                              void *                ptr ,
                              cl_uint               num_events_in_wait_list ,
                              const cl_event *      event_wait_list ,
                              cl_event *            event);

/** clEnqueueWriteImage ocland abstraction method.
 * @param element_size Size of each element.
 */
cl_int oclandEnqueueWriteImage(cl_command_queue     command_queue ,
                               cl_mem               image ,
                               cl_bool              blocking_write ,
                               const size_t *       origin ,
                               const size_t *       region ,
                               size_t               row_pitch ,
                               size_t               slice_pitch ,
                               size_t               element_size ,
                               const void *         ptr ,
                               cl_uint              num_events_in_wait_list ,
                               const cl_event *     event_wait_list ,
                               cl_event *           event);

/** clEnqueueCopyImage ocland abstraction method.
 */
cl_int oclandEnqueueCopyImage(cl_command_queue      command_queue ,
                              cl_mem                src_image ,
                              cl_mem                dst_image ,
                              const size_t *        src_origin ,
                              const size_t *        dst_origin ,
                              const size_t *        region ,
                              cl_uint               num_events_in_wait_list ,
                              const cl_event *      event_wait_list ,
                              cl_event *            event);

/** clEnqueueCopyImageToBuffer ocland abstraction method.
 */
cl_int oclandEnqueueCopyImageToBuffer(cl_command_queue  command_queue ,
                                      cl_mem            src_image ,
                                      cl_mem            dst_buffer ,
                                      const size_t *    src_origin ,
                                      const size_t *    region ,
                                      size_t            dst_offset ,
                                      cl_uint           num_events_in_wait_list ,
                                      const cl_event *  event_wait_list ,
                                      cl_event *        event);

/** clEnqueueCopyBufferToImage ocland abstraction method.
 */
cl_int oclandEnqueueCopyBufferToImage(cl_command_queue  command_queue ,
                                      cl_mem            src_buffer ,
                                      cl_mem            dst_image ,
                                      size_t            src_offset ,
                                      const size_t *    dst_origin ,
                                      const size_t *    region ,
                                      cl_uint           num_events_in_wait_list ,
                                      const cl_event *  event_wait_list ,
                                      cl_event *        event);

/** clEnqueueNDRangeKernel ocland abstraction method.
 */
cl_int oclandEnqueueNDRangeKernel(cl_command_queue  command_queue ,
                                  cl_kernel         kernel ,
                                  cl_uint           work_dim ,
                                  const size_t *    global_work_offset ,
                                  const size_t *    global_work_size ,
                                  const size_t *    local_work_size ,
                                  cl_uint           num_events_in_wait_list ,
                                  const cl_event *  event_wait_list ,
                                  cl_event *        event) CL_API_SUFFIX__VERSION_1_0;

// -------------------------------------------- //
//                                              //
// OpenCL 1.1 methods                           //
//                                              //
// -------------------------------------------- //

/** clEnqueueReadBufferRect ocland abstraction method.
 */
cl_int oclandEnqueueReadBufferRect(cl_command_queue     command_queue ,
                                   cl_mem               buffer ,
                                   cl_bool              blocking_read ,
                                   const size_t *       buffer_origin ,
                                   const size_t *       host_origin ,
                                   const size_t *       region ,
                                   size_t               buffer_row_pitch ,
                                   size_t               buffer_slice_pitch ,
                                   size_t               host_row_pitch ,
                                   size_t               host_slice_pitch ,
                                   void *               ptr ,
                                   cl_uint              num_events_in_wait_list ,
                                   const cl_event *     event_wait_list ,
                                   cl_event *           event);

/** clEnqueueWriteBufferRect ocland abstraction method.
 */
cl_int oclandEnqueueWriteBufferRect(cl_command_queue     command_queue ,
                                    cl_mem               buffer ,
                                    cl_bool              blocking_write ,
                                    const size_t *       buffer_origin ,
                                    const size_t *       host_origin ,
                                    const size_t *       region ,
                                    size_t               buffer_row_pitch ,
                                    size_t               buffer_slice_pitch ,
                                    size_t               host_row_pitch ,
                                    size_t               host_slice_pitch ,
                                    const void *         ptr ,
                                    cl_uint              num_events_in_wait_list ,
                                    const cl_event *     event_wait_list ,
                                    cl_event *           event);

/** clEnqueueCopyBufferRect ocland abstraction method.
 */
cl_int oclandEnqueueCopyBufferRect(cl_command_queue     command_queue ,
                                   cl_mem               src_buffer ,
                                   cl_mem               dst_buffer ,
                                   const size_t *       src_origin ,
                                   const size_t *       dst_origin ,
                                   const size_t *       region ,
                                   size_t               src_row_pitch ,
                                   size_t               src_slice_pitch ,
                                   size_t               dst_row_pitch ,
                                   size_t               dst_slice_pitch ,
                                   cl_uint              num_events_in_wait_list ,
                                   const cl_event *     event_wait_list ,
                                   cl_event *           event);

// -------------------------------------------- //
//                                              //
// OpenCL 1.2 methods                           //
//                                              //
// -------------------------------------------- //

/** clUnloadPlatformCompiler ocland abstraction method.
 */
cl_int oclandGetKernelArgInfo(cl_kernel        kernel ,
                              cl_uint          arg_index ,
                              cl_kernel_arg_info   param_name ,
                              size_t           param_value_size ,
                              void *           param_value ,
                              size_t *         param_value_size_ret);

/** clEnqueueFillBuffer ocland abstraction method.
 */
cl_int oclandEnqueueFillBuffer(cl_command_queue    command_queue ,
                               cl_mem              buffer ,
                               const void *        pattern ,
                               size_t              pattern_size ,
                               size_t              offset ,
                               size_t              cb ,
                               cl_uint             num_events_in_wait_list ,
                               const cl_event *    event_wait_list ,
                               cl_event *          event);

/** clEnqueueFillImage ocland abstraction method.
 * @param fill_color_size Size of fill_color following the
 * rules described in clEnqueueFillImage specification.
 */
cl_int oclandEnqueueFillImage(cl_command_queue    command_queue ,
                              cl_mem              image ,
                              size_t              fill_color_size ,
                              const void *        fill_color ,
                              const size_t *      origin ,
                              const size_t *      region ,
                              cl_uint             num_events_in_wait_list ,
                              const cl_event *    event_wait_list ,
                              cl_event *          event);

/** clEnqueueMigrateMemObjects ocland abstraction method.
 */
cl_int oclandEnqueueMigrateMemObjects(cl_command_queue        command_queue ,
                                      cl_uint                 num_mem_objects ,
                                      const cl_mem *          mem_objects ,
                                      cl_mem_migration_flags  flags ,
                                      cl_uint                 num_events_in_wait_list ,
                                      const cl_event *        event_wait_list ,
                                      cl_event *              event);

/** clEnqueueMarkerWithWaitList ocland abstraction method.
 */
cl_int oclandEnqueueMarkerWithWaitList(cl_command_queue  command_queue ,
                                       cl_uint            num_events_in_wait_list ,
                                       const cl_event *   event_wait_list ,
                                       cl_event *         event);

/** clEnqueueBarrierWithWaitList ocland abstraction method.
 */
cl_int oclandEnqueueBarrierWithWaitList(cl_command_queue  command_queue ,
                                        cl_uint            num_events_in_wait_list ,
                                        const cl_event *   event_wait_list ,
                                        cl_event *         event);

#endif // OCLAND_H_INCLUDED
