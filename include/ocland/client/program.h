/*
 *  This file is part of ocland, a free cloud OpenCL interface.
 *  Copyright (C) 2012-2019  Jose Luis Cercos Pita <jl.cercos@upm.es>
 *
 *  ocland is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  ocland is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with ocland.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef OCLAND_PROGRAM_H_INCLUDED
#define OCLAND_PROGRAM_H_INCLUDED

#include <stdbool.h>

#include <ocland/common/opencl.h>
#include <ocland/client/ocland.h>
#include <ocland/client/ocland_icd.h>

typedef struct _program_compile* program_compile;

struct _cl_program
{
    /// Dispatch table
    struct _cl_icd_dispatch *dispatch;
    /// Pointer of server instance
    cl_program ptr;
    /// Reference count to control when the object must be destroyed
    cl_uint rcount;
    /// Associated server
    ocland_server server;
    /// Owning context
    cl_context context;
    /// Number of devices
    cl_uint num_devices;
    /// Devices
    cl_device_id *devices;
    /// Program source code (if built with clCreateProgramWithSource() )
    char *src;
    /// Compilations (per device)
    program_compile *compilations;
    /// Number of kernels attached
    cl_uint num_kernels_attached;
};

/** @brief Get the local program from the remote identifier
 * @param program Remote program instance
 * @return Local program instance, NULL if no program associated with the
 * remote instance can be found
 */
cl_program get_program(cl_program program);

/**
 * Check for valid local program
 * @param  program The local program identifier, not a remote peer one
 * @return true if the program is valid, false otherwise
 */
bool is_program(cl_program program);

/** clGetSamplerInfo ocland abstraction method.
 */
cl_program oclandCreateProgramWithSource(cl_context         context ,
                                         cl_uint            count ,
                                         const char **      strings ,
                                         const size_t *     lengths ,
                                         cl_int *           errcode_ret);

/** clCreateProgramWithBinary ocland abstraction method.
 */
cl_program oclandCreateProgramWithBinary(cl_context              context ,
                                         cl_uint                 num_devices ,
                                         const cl_device_id *    device_list ,
                                         const size_t *          lengths ,
                                         const unsigned char **  binaries ,
                                         cl_int *                binary_status ,
                                         cl_int *                errcode_ret);

/** clRetainProgram ocland abstraction method.
 */
cl_int oclandRetainProgram(cl_program  program);

/** clReleaseProgram ocland abstraction method.
 */
cl_int oclandReleaseProgram(cl_program  program);

/** clBuildProgram ocland abstraction method.
 */
cl_int oclandBuildProgram(cl_program            program ,
                          cl_uint               num_devices ,
                          const cl_device_id *  device_list ,
                          const char *          options ,
                          void (CL_CALLBACK *   pfn_notify)(cl_program  program , void *  user_data),
                          void *                user_data);

/** clGetProgramInfo ocland abstraction method.
 */
cl_int oclandGetProgramInfo(cl_program          program ,
                            cl_program_info     param_name ,
                            size_t              param_value_size ,
                            void *              param_value ,
                            size_t *            param_value_size_ret);

/** clGetProgramBuildInfo ocland abstraction method.
 */
cl_int oclandGetProgramBuildInfo(cl_program             program ,
                                 cl_device_id           device ,
                                 cl_program_build_info  param_name ,
                                 size_t                 param_value_size ,
                                 void *                 param_value ,
                                 size_t *               param_value_size_ret);

// -------------------------------------------- //
//                                              //
// OpenCL 1.2 methods                           //
//                                              //
// -------------------------------------------- //

/** clCreateProgramWithBuiltInKernels ocland abstraction method.
*/
cl_program oclandCreateProgramWithBuiltInKernels(cl_context            context ,
                                                 cl_uint               num_devices ,
                                                 const cl_device_id *  device_list ,
                                                 const char *          kernel_names ,
                                                 cl_int *              errcode_ret);

/** clCompileProgram ocland abstraction method.
 */
cl_int oclandCompileProgram(cl_program            program ,
                            cl_uint               num_devices ,
                            const cl_device_id *  device_list ,
                            const char *          options ,
                            cl_uint               num_input_headers ,
                            const cl_program *    input_headers,
                            const char **         header_include_names ,
                            void (CL_CALLBACK *   pfn_notify)(cl_program  program , void *  user_data),
                            void *                user_data);

/** clLinkProgram ocland abstraction method.
 */
cl_program oclandLinkProgram(cl_context            context ,
                             cl_uint               num_devices ,
                             const cl_device_id *  device_list ,
                             const char *          options ,
                             cl_uint               num_input_programs ,
                             const cl_program *    input_programs ,
                             void (CL_CALLBACK *   pfn_notify)(cl_program  program , void *  user_data),
                             void *                user_data ,
                             cl_int *              errcode_ret);

/** clUnloadPlatformCompiler ocland abstraction method.
 */
cl_int oclandUnloadPlatformCompiler(cl_platform_id  platform);

#endif  // OCLAND_PROGRAM_H_INCLUDED
