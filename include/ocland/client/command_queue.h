/*
 *  This file is part of ocland, a free cloud OpenCL interface.
 *  Copyright (C) 2012-2019  Jose Luis Cercos Pita <jlcercos@gmail.com>
 *
 *  ocland is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  ocland is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with ocland.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef OCLAND_COMMAND_QUEUE_H_INCLUDED
#define OCLAND_COMMAND_QUEUE_H_INCLUDED

#include <stdbool.h>

#include <ocland/common/opencl.h>
#include <ocland/client/ocland.h>
#include <ocland/client/ocland_icd.h>

struct _cl_command_queue
{
    /// Dispatch table
    struct _cl_icd_dispatch *dispatch;
    /// Pointer of server instance
    cl_command_queue ptr;
    /// Reference count to control when the object must be destroyed
    cl_uint rcount;
    /// Associated server
    ocland_server server;
    /// Properties specified to create the context
    cl_command_queue_properties props;
    /// Owner context
    cl_context context;
    /// Associated device
    cl_device_id device;
};

/** @brief Get the local command queue from the remote identifier
 * @param queue Remote command queue instance
 * @return Local command queue instance, NULL if no command queue associated
 * with the remote instance can be found
 */
cl_command_queue get_queue(cl_command_queue queue);

/**
 * Check for valid local command queues
 * @param  queue The local command queue identifier, not a remote peer one
 * @return true if the command queue is valid, false otherwise
 */
bool is_queue(cl_command_queue queue);

/** clCreateCommandQueue ocland abstraction method.
 */
cl_command_queue oclandCreateCommandQueue(cl_context                     context,
                                          cl_device_id                   device,
                                          cl_command_queue_properties    properties,
                                          cl_int *                       errcode_ret);

/** clRetainCommandQueue ocland abstraction method.
 */
cl_int oclandRetainCommandQueue(cl_command_queue command_queue);

/** clReleaseCommandQueue ocland abstraction method.
 */
cl_int oclandReleaseCommandQueue(cl_command_queue command_queue);

/** clGetCommandQueueInfo ocland abstraction method.
 */
cl_int oclandGetCommandQueueInfo(cl_command_queue      command_queue,
                                 cl_command_queue_info param_name,
                                 size_t                param_value_size,
                                 void *                param_value,
                                 size_t *              param_value_size_ret);

/** clCreateCommandQueueWithProperties ocland abstraction method.
 */
cl_command_queue oclandCreateCommandQueueWithProperties(cl_context   context,
                                                        cl_device_id device,
                                                        const cl_queue_properties *properties,
                                                        cl_int *     errcode_ret);

#endif  // OCLAND_COMMAND_QUEUE_H_INCLUDED
