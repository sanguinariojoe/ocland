/*
 *  This file is part of ocland, a free cloud OpenCL interface.
 *  Copyright (C) 2012-2019  Jose Luis Cercos Pita <jl.cercos@upm.es>
 *
 *  ocland is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  ocland is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with ocland.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef OCLAND_CONTEXT_H_INCLUDED
#define OCLAND_CONTEXT_H_INCLUDED

#include <stdbool.h>

#include <ocland/common/opencl.h>
#include <ocland/client/ocland.h>
#include <ocland/client/ocland_icd.h>

struct _cl_context
{
    /// Dispatch table
    struct _cl_icd_dispatch *dispatch;
    /// Pointer of server instance
    cl_context ptr;
    /// Reference count to control when the object must be destroyed
    cl_uint rcount;
    /// Associated server
    ocland_server server;
    /// Number of properties specified to create the context
    cl_uint num_props;
    /// Properties specified to create the context
    cl_context_properties *props;
    /// Number of devices inside the context
    cl_uint num_devices;
    /// Devices in the context
    cl_device_id *devices;
    /// User function to be called in case of execution errors
    void (CL_CALLBACK * pfn_notify)(const char *, const void *, size_t, void *);
    /// User data to be passed to pfn_notify
    void* user_data;
};

/** @brief Get the local context from the remote identifier
 * @param context Remote context instance
 * @return Local context instance, NULL if no context associated with the
 * remote instance can be found
 */
cl_context get_context(cl_context context);

/**
 * Check for valid local context
 * @param  context The local context identifier, not a remote peer one
 * @return true if the context is valid, false otherwise
 */
bool is_context(cl_context context);

/**
 * Check if a device is in the context
 * @param  context The local context identifier, not a remote peer one
 * @param  device The local device identifier, not a remote peer one
 * @return true if the context is valid, false otherwise
 */
bool has_context_device(cl_context context, cl_device_id device);

/** clCreateContext ocland abstraction method
 */
cl_context oclandCreateContext(const cl_context_properties * properties,
                               cl_uint                       num_devices,
                               const cl_device_id *          devices,
                               void (CL_CALLBACK * pfn_notify)(const char *, const void *, size_t, void *),
                               void *                        user_data,
                               cl_int *                      errcode_ret);

/** clCreateContextFromType ocland abstraction method.
 */
cl_context oclandCreateContextFromType(const cl_context_properties * properties,
                                       cl_device_type                device_type,
                                       void (CL_CALLBACK * pfn_notify)(const char *, const void *, size_t, void *),
                                       void *                        user_data,
                                       cl_int *                      errcode_ret);

/** clRetainContext ocland abstraction method.
 */
cl_int oclandRetainContext(cl_context context);

/** clRetainContext ocland abstraction method.
 */
cl_int oclandReleaseContext(cl_context context);

/** clGetContextInfo ocland abstraction method.
 */
cl_int oclandGetContextInfo(cl_context         context,
                            cl_context_info    param_name,
                            size_t             param_value_size,
                            void *             param_value,
                            size_t *           param_value_size_ret);

#endif  // OCLAND_CONTEXT_H_INCLUDED
