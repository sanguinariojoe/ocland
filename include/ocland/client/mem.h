/*
 *  This file is part of ocland, a free cloud OpenCL interface.
 *  Copyright (C) 2012-2019  Jose Luis Cercos Pita <jl.cercos@upm.es>
 *
 *  ocland is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  ocland is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with ocland.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef OCLAND_MEM_H_INCLUDED
#define OCLAND_MEM_H_INCLUDED

#include <stdbool.h>

#include <ocland/common/opencl.h>
#include <ocland/client/ocland.h>
#include <ocland/client/ocland_icd.h>

/**
 * Sub-buffer memory objects extra data
 * @see clCreateSubBuffer()
 */
typedef struct _sub_mem_data
{
    /// Parent memory object
    cl_mem parent;
    /// Memory offset
    size_t offset;
} sub_mem_data;

/**
 * Image (2D/3D) memory objects extra data
 * @see clCreateImage()
 */
typedef struct _image_data
{
    /// Image format
    cl_image_format format;
    /// Element size
    size_t element_size;
    /// Image descriptor
    cl_image_desc desc;
} image_data;

struct _cl_mem
{
    /// Dispatch table
    struct _cl_icd_dispatch *dispatch;
    /// Pointer of server instance
    cl_mem ptr;
    /// Reference count to control when the object must be destroyed
    cl_uint rcount;
    /// Associated server
    ocland_server server;
    /// Type of object
    cl_mem_object_type type;
    /// Generation flags
    cl_mem_flags flags;
    /// Device allocated memory
    size_t size;
    /** Pinned host memory address (useless in ocland, it is just considered
     * to return appropriate errors
     */
    void* host_mem;
    /// Owning context
    cl_context context;
    /// Sub-buffer data
    sub_mem_data sub_mem;
    /// Sub-image data
    image_data image;
    /// Destructor callback, called when the memory object is no longer
    void (CL_CALLBACK  *pfn_notify) (cl_mem memobj,
                                     void *user_data);
    /// User data passed as argument to the destructor callback
    void *user_data;
};

/** @brief Get the local mem from the remote identifier
 * @param mem Remote mem instance
 * @return Local mem instance, NULL if no mem associated with the
 * remote instance can be found
 */
cl_mem get_mem(cl_mem mem);

/**
 * Check for valid local mem
 * @param  mem The local mem identifier, not a remote peer one
 * @return true if the mem is valid, false otherwise
 */
bool is_mem(cl_mem mem);

/** clCreateBuffer ocland abstraction method.
 */
cl_mem oclandCreateBuffer(cl_context    context ,
                          cl_mem_flags  flags ,
                          size_t        size ,
                          void *        host_ptr ,
                          cl_int *      errcode_ret);

/** clRetainMemObject ocland abstraction method.
 */
cl_int oclandRetainMemObject(cl_mem memobj);

/** clReleaseMemObject ocland abstraction method.
 */
cl_int oclandReleaseMemObject(cl_mem memobj);

/** clGetSupportedImageFormats ocland abstraction method.
 */
cl_int oclandGetSupportedImageFormats(cl_context           context,
                                      cl_mem_flags         flags,
                                      cl_mem_object_type   image_type ,
                                      cl_uint              num_entries ,
                                      cl_image_format *    image_formats ,
                                      cl_uint *            num_image_formats);

/** clGetMemObjectInfo ocland abstraction method.
 */
cl_int oclandGetMemObjectInfo(cl_mem            memobj ,
                              cl_mem_info       param_name ,
                              size_t            param_value_size ,
                              void *            param_value ,
                              size_t *          param_value_size_ret);

/** clGetImageInfo ocland abstraction method.
*/
cl_int oclandGetImageInfo(cl_mem            image ,
                          cl_image_info     param_name ,
                          size_t            param_value_size ,
                          void *            param_value ,
                          size_t *          param_value_size_ret);

// -------------------------------------------- //
//                                              //
// OpenCL 1.1 methods                           //
//                                              //
// -------------------------------------------- //

/** clCreateSubBuffer ocland abstraction method.
*/
cl_mem oclandCreateSubBuffer(cl_mem                    buffer ,
                             cl_mem_flags              flags ,
                             cl_buffer_create_type     buffer_create_type ,
                             const void *              buffer_create_info ,
                             cl_int *                  errcode_ret);

/** clSetMemObjectDestructorCallback ocland abstraction method.
 */
cl_int oclandSetMemObjectDestructorCallback(cl_mem  memobj ,
                                            void (CL_CALLBACK * pfn_notify)(cl_mem  memobj , void* user_data),
                                            void * user_data);

// -------------------------------------------- //
//                                              //
// OpenCL 1.2 methods                           //
//                                              //
// -------------------------------------------- //

/** clCreateImage ocland abstraction method.
 */
cl_mem oclandCreateImage(cl_context              context,
                         cl_mem_flags            flags,
                         const cl_image_format * image_format,
                         const cl_image_desc *   image_desc,
                         void *                  host_ptr,
                         cl_int *                errcode_ret);


#endif  // OCLAND_MEM_H_INCLUDED
