/*
 *  This file is part of ocland, a free cloud OpenCL interface.
 *  Copyright (C) 2012-2019  Jose Luis Cercos Pita <jl.cercos@upm.es>
 *
 *  ocland is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  ocland is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with ocland.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef OCLAND_EVENT_H_INCLUDED
#define OCLAND_EVENT_H_INCLUDED

#include <stdbool.h>

#include <ocland/common/opencl.h>
#include <ocland/common/typed_list.h>
#include <ocland/client/ocland.h>
#include <ocland/client/ocland_icd.h>
#include <pthread.h>

struct _cl_event
{
    /// Dispatch table
    struct _cl_icd_dispatch *dispatch;
    /** Pointer of server instance.
     *
     * Events imply a paradigm shift, since converselly to other ocland
     * entities, which uses a reference provided by the server, events uses
     * the local pointer as a reference, in such a way they can be used in a
     * streaming way, i.e. it is not required to wait for a server answer to
     * proceed
     */
    cl_event ptr;
    /// Reference count to control when the object must be destroyed
    cl_uint rcount;
    /// Associated server
    ocland_server server;
    /// Owning command queue
    cl_command_queue queue;
    /// Command that generated the event
    cl_command_type command_type;
    /// Owning context (User events)
    cl_context context;
    /// Command execution status
    cl_int execution_status;
    /// The event status callbacks list
    List* status_callbacks;
    /** Mutex to avoid several threads try to simultaneously access/edit the
     * event status, and the callbacks
     */
    pthread_mutex_t lock;
};

/**
 * Check for valid local event
 * @param  event The local event identifier, not a remote peer one
 * @return true if the event is valid, false otherwise
 */
bool is_event(cl_event event);

/**
 * Thread safe event status setting
 * @param event  Considered event
 * @param status New event execution status
 */
void set_event_status(cl_event event, cl_int status);

/**
 * Thread safe event status getting
 * @param event  Considered event
 * @return       Event execution status
 */
cl_int get_event_status(cl_event event);

/**
 * Create a new event, and append it to the events list
 * @param  context      Context where the event shall be created in
 * @param  queue        Command queue where the event was generated. Can be NULL
 *                      for events generated with clCreateUserEvent()
 * @return              The new generated event
 */
cl_event new_event(cl_context context, cl_command_queue queue);

/**
 * Safely remove a event, even if it was not correctly generated
 * @param event Event to remove
 */
void del_event(cl_event event);

/**
 * Get the list of events associated to a command queue. The returned events are
 * retained.
 * @param  command_queue Command queue
 * @param  num_events    Number of events
 * @return               Array of events
 */
cl_event* get_command_queue_events(cl_command_queue command_queue,
                                   cl_uint *num_events);

/**
 * Wait for the events to reach an specific status (or lower, i.e. more
 * advanced)
 *
 * This method is not checking for the validity of the events, neither their
 * procedence. It is responsibility of the caller to make all the required
 * checks before
 * @param  num_events       Number of events
 * @param  event_list       List of events
 * @param  execution_status Execution status to wait
 * @return                  Minimum execution state found
 */
cl_int wait_for_events(cl_uint num_events,
                       const cl_event *event_list,
                       cl_int execution_status);

/** clWaitForEvents ocland abstraction method.
 */
cl_int oclandWaitForEvents(cl_uint              num_events ,
                           const cl_event *     event_list);

/** clGetEventInfo ocland abstraction method.
 */
cl_int oclandGetEventInfo(cl_event          event ,
                          cl_event_info     param_name ,
                          size_t            param_value_size ,
                          void *            param_value ,
                          size_t *          param_value_size_ret);

/** clRetainEvent ocland abstraction method.
 */
cl_int oclandRetainEvent(cl_event  event);

/** clReleaseEvent ocland abstraction method.
 */
cl_int oclandReleaseEvent(cl_event  event);

/** clGetEventProfilingInfo ocland abstraction method.
 */
cl_int oclandGetEventProfilingInfo(cl_event             event ,
                                   cl_profiling_info    param_name ,
                                   size_t               param_value_size ,
                                   void *               param_value ,
                                   size_t *             param_value_size_ret);

/** clFlush ocland abstraction method.
 */
cl_int oclandFlush(cl_command_queue  command_queue);

/** clFinish ocland abstraction method.
 */
cl_int oclandFinish(cl_command_queue  command_queue);

// -------------------------------------------- //
//                                              //
// OpenCL 1.1 methods                           //
//                                              //
// -------------------------------------------- //

/** clCreateUserEvent ocland abstraction method.
 */
cl_event oclandCreateUserEvent(cl_context     context ,
                               cl_int *       errcode_ret);

/** clSetUserEventStatus ocland abstraction method.
 */
cl_int oclandSetUserEventStatus(cl_event    event ,
                                cl_int      execution_status);

/** clSetEventCallback ocland abstraction method.
 */
cl_int oclandSetEventCallback(cl_event event,
                              cl_int  command_exec_callback_type ,
                              void (CL_CALLBACK *pfn_notify) (cl_event event, cl_int event_command_exec_status, void *user_data),
                              void *user_data);

#endif  // OCLAND_EVENT_H_INCLUDED
