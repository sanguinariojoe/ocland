/*
 *  This file is part of ocland, a free cloud OpenCL interface.
 *  Copyright (C) 2012  Jose Luis Cercos Pita <jl.cercos@upm.es>
 *
 *  ocland is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  ocland is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with ocland.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef LOG_H_INCLUDED
#define LOG_H_INCLUDED

#include <stdio.h>
#include <string.h>
#include <ocland/common/opencl.h>

#ifdef OCLAND_CLIENT_VERBOSE
    #define VERBOSE_IN() {printf("[line %d]: %s...\n", __LINE__, __func__);  \
                          fflush(stdout);}
    #define VERBOSE_OUT(flag) {printf("\t%s -> %s (%d)\n",                   \
                               __func__, get_opencl_error_name(flag), flag); \
                               fflush(stdout);}
    #define VERBOSE_MSG(format, ...) {printf("[OCLAND %s:%d (%s)]: ", \
                                      __FILE__, __LINE__, __func__);  \
                                      printf(format, __VA_ARGS__);    \
                                      fflush(stdout);}
#else
    #define VERBOSE_IN()
    #define VERBOSE_OUT(flag)
    #define VERBOSE_MSG(format, ...)
#endif

#endif  // LOG_H_INCLUDED
