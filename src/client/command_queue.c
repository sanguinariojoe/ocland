/*
 *  This file is part of ocland, a free cloud OpenCL interface.
 *  Copyright (C) 2012-2019  Jose Luis Cercos Pita <jl.cercos@upm.es>
 *
 *  ocland is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  ocland is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with ocland.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <ocland/common/dataExchange.h>
#include <ocland/client/device.h>
#include <ocland/client/context.h>
#include <ocland/client/command_queue.h>

#include <ocland/common/typed_list.h>
DEFINE_LIST(cl_command_queue, queues)

/// List of command queues
static List* queues = NULL;

cl_command_queue get_queue(cl_command_queue remote_queue)
{
    if(!queues_len(queues))
        return NULL;
    for(queues_node queue_node = queues_front(queues);
        !queues_is_null(queue_node);
        queue_node = queues_next(queue_node))
    {
        cl_command_queue local_queue = queues_data(queue_node);
        if(local_queue->ptr == remote_queue)
            return local_queue;
    }

    return NULL;
}

bool is_queue(cl_command_queue queue)
{
    if(!queues)
        queues = queues_new();
    return queues_find(queues, queue) < queues_len(queues);
}

cl_command_queue oclandCreateCommandQueue(cl_context                   context,
                                          cl_device_id                 device,
                                          cl_command_queue_properties  properties,
                                          cl_int *                     errcode_ret)
{
    if(!queues)
        queues = queues_new();

    // Let's check the passed OpenCL instances
    if(!is_context(context)) {
        if(errcode_ret) *errcode_ret = CL_INVALID_CONTEXT;
        return NULL;
    }
    if(!has_context_device(context, device)) {
        if(errcode_ret) *errcode_ret = CL_INVALID_DEVICE;
        return NULL;
    }

    // Call the remote peer
    int err;
    const unsigned int method = ocland_clCreateCommandQueue;
    ocland_server server = context->server;
    lock(server);

    err = send_comm(server->socket, sizeof(unsigned int),
                                    sizeof(cl_context),
                                    sizeof(cl_device_id),
                                    sizeof(cl_command_queue_properties),
                                    0,
                                    &method,
                                    &(context->ptr),
                                    &(device->ptr),
                                    &properties,
                                    NULL);
    if(err) {
        unlock(server);
        if(errcode_ret) *errcode_ret = CL_OUT_OF_HOST_MEMORY;
        return NULL;
    }

    cl_int flag;
    cl_command_queue command_queue;
    err = recv_comm(server->socket, NULL, sizeof(cl_int),
                                          sizeof(cl_command_queue),
                                          0,
                                          &flag,
                                          &command_queue,
                                          NULL);
    unlock(server);

    if(err) {
        if(errcode_ret) *errcode_ret = CL_OUT_OF_HOST_MEMORY;
        return NULL;
    }

    if(errcode_ret) *errcode_ret = flag;

    if(flag != CL_SUCCESS) {
        return NULL;
    }

    // Register the new context
    cl_command_queue data = (cl_command_queue)malloc(
       sizeof(struct _cl_command_queue));
    data->dispatch = context->dispatch;
    data->ptr = command_queue;
    data->rcount = 1;
    data->server = context->server;
    data->props = properties;
    data->context = context;
    data->device = device;
    queues_push_back(queues, data);

    oclandRetainContext(data->context);

    return data;
}

cl_int oclandRetainCommandQueue(cl_command_queue command_queue)
{
    if(!is_queue(command_queue))
        return CL_INVALID_COMMAND_QUEUE;

    command_queue->rcount++;
    return CL_SUCCESS;
}

cl_int oclandReleaseCommandQueue(cl_command_queue command_queue)
{
    if(!is_queue(command_queue))
        return CL_INVALID_COMMAND_QUEUE;

    if(command_queue->rcount > 1) {
        command_queue->rcount--;
        return CL_SUCCESS;
    }

    // The context shall be destroyed, including from server
    int err;
    cl_int flag;
    const unsigned int method = ocland_clReleaseCommandQueue;
    ocland_server server = command_queue->server;
    lock(server);

    err = send_comm(server->socket, sizeof(unsigned int),
                                    sizeof(cl_command_queue),
                                    0,
                                    &method,
                                    &(command_queue->ptr),
                                    NULL);
    if(err) {
        unlock(server);
        return CL_OUT_OF_HOST_MEMORY;
    }

    err = recv_comm(server->socket, NULL, sizeof(cl_int),
                                          0,
                                          &flag,
                                          NULL);
    if(err) {
        unlock(server);
        return CL_OUT_OF_HOST_MEMORY;
    }

    unlock(server);

    if(flag != CL_SUCCESS){
        return flag;
    }

    // So we can remove the context
    oclandReleaseContext(command_queue->context);
    queues_remove_at(queues, queues_find(queues, command_queue));
    free(command_queue);

    return CL_SUCCESS;
}

/**
 * Get info from the context
 *
 * This method only returns data that shall not be queried to the server
 * @param  context Context
 * @param  param_name Parameter
 * @param  param_value_size Allocated memory inside #param_value
 * @param  param_value Allocated memory where the queried data shall be copy
 * @param  param_value_size_ret Returned size of the queried data
 * @return 1 if the queried data cannot be extracted from the
 * local instance, and shall be therefore queried to the server. CL_SUCCESS
 * if the query can be processed. CL_INVALID_VALUE if the query can be processed
 * but param_value_size is smaller than the size of the returned value.
 */
cl_int get_queue_info(cl_command_queue       command_queue,
                      cl_command_queue_info  param_name,
                      size_t                 param_value_size,
                      void *                 param_value,
                      size_t *               param_value_size_ret)
{
    size_t size_ret = 0;
    void* data_ret = NULL;
    switch(param_name) {
        case CL_QUEUE_CONTEXT:
            size_ret = sizeof(cl_context);
            data_ret = &(command_queue->context);
            break;
        case CL_QUEUE_DEVICE:
            size_ret = sizeof(cl_device_id);
            data_ret = &(command_queue->device);
            break;
        case  CL_QUEUE_REFERENCE_COUNT:
            size_ret = sizeof(cl_uint);
            data_ret = &(command_queue->rcount);
            break;
        case CL_QUEUE_PROPERTIES:
            size_ret = sizeof(cl_command_queue_properties);
            data_ret = &(command_queue->props);
            break;
        default:
            return 1;
    }

    if(param_value_size_ret)
        *param_value_size_ret = size_ret;
    if(param_value) {
        if(param_value_size < size_ret)
            return CL_INVALID_VALUE;
        memcpy(param_value, data_ret, size_ret);
    }

    return CL_SUCCESS;
}

cl_int oclandGetCommandQueueInfo(cl_command_queue      command_queue,
                                 cl_command_queue_info param_name,
                                 size_t                param_value_size,
                                 void *                param_value,
                                 size_t *              param_value_size_ret)
{
    if(!is_queue(command_queue))
        return CL_INVALID_COMMAND_QUEUE;

    cl_int flag;
    flag = get_queue_info(command_queue,
                          param_name,
                          param_value_size,
                          param_value,
                          param_value_size_ret);
    if(flag != 1)  // flag = 1 means we should call the server
        return flag;

    // Call the remote peer to try to get the missing info
    const unsigned int method = ocland_clGetCommandQueueInfo;
    return oclandGetGenericInfo(command_queue->server,
                                param_value_size,
                                param_value,
                                param_value_size_ret,
                                // Variable arguments
                                sizeof(unsigned int),
                                sizeof(cl_context),
                                sizeof(cl_context_info),
                                sizeof(size_t),
                                0,
                                &method,
                                &(command_queue->ptr),
                                &param_name,
                                &param_value_size,
                                NULL);
}

cl_command_queue oclandCreateCommandQueueWithProperties(cl_context   context,
                                                        cl_device_id device,
                                                        const cl_queue_properties *properties,
                                                        cl_int *     errcode_ret)
{
    if(!queues)
        queues = queues_new();

    // Let's check the passed OpenCL instances
    if(!is_context(context)) {
        if(errcode_ret) *errcode_ret = CL_INVALID_CONTEXT;
        return NULL;
    }
    if(!has_context_device(context, device)) {
        if(errcode_ret) *errcode_ret = CL_INVALID_DEVICE;
        return NULL;
    }

    // Let's try to get the applied properties (which are stored inside the
    // command queue instance, and can be queried by clGetCommandQueueInfo() )
    cl_command_queue_properties props = 0;
    cl_uint num_properties = 0;
    if(properties) {
        while(properties[num_properties] != 0) {
            if(properties[num_properties] == CL_QUEUE_PROPERTIES) {
                props = (cl_command_queue_properties)properties[num_properties + 1];
            }
            num_properties += 2;
        }
        num_properties++;  // Tailing 0
    }
    size_t props_size = num_properties * sizeof(cl_queue_properties);

    // Call the remote peer
    int err;
    const unsigned int method = ocland_clCreateCommandQueueWithProperties;
    ocland_server server = context->server;
    lock(server);

    if(properties) {
        err = send_comm(server->socket, sizeof(unsigned int),
                                        sizeof(cl_context),
                                        sizeof(cl_device_id),
                                        sizeof(size_t),
                                        props_size,
                                        0,
                                        &method,
                                        &(context->ptr),
                                        &(device->ptr),
                                        &props_size,
                                        properties,
                                        NULL);
    }
    else {
        err = send_comm(server->socket, sizeof(unsigned int),
                                        sizeof(cl_context),
                                        sizeof(cl_device_id),
                                        sizeof(size_t),
                                        0,
                                        &method,
                                        &(context->ptr),
                                        &(device->ptr),
                                        &props_size,
                                        NULL);
    }
    if(err) {
        unlock(server);
        if(errcode_ret) *errcode_ret = CL_OUT_OF_HOST_MEMORY;
        return NULL;
    }

    cl_int flag;
    cl_command_queue command_queue;
    err = recv_comm(server->socket, NULL, sizeof(cl_int),
                                          sizeof(cl_command_queue),
                                          0,
                                          &flag,
                                          &command_queue,
                                          NULL);
    unlock(server);

    if(err) {
        if(errcode_ret) *errcode_ret = CL_OUT_OF_HOST_MEMORY;
        return NULL;
    }

    if(errcode_ret) *errcode_ret = flag;

    if(flag != CL_SUCCESS) {
        return NULL;
    }

    // Register the new context
    cl_command_queue data = (cl_command_queue)malloc(
       sizeof(struct _cl_command_queue));
    data->dispatch = context->dispatch;
    data->ptr = command_queue;
    data->rcount = 1;
    data->server = context->server;
    data->props = props;
    data->context = context;
    data->device = device;
    queues_push_back(queues, data);

    oclandRetainContext(data->context);

    return data;
}
