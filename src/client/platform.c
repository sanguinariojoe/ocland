/*
 *  This file is part of ocland, a free cloud OpenCL interface.
 *  Copyright (C) 2012-2019  Jose Luis Cercos Pita <jl.cercos@upm.es>
 *
 *  ocland is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  ocland is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with ocland.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <ocland/common/dataExchange.h>
#include <ocland/client/platform.h>
#include <ocland/client/device.h>
#include <ocland/client/ocland.h>

#include <ocland/common/typed_list.h>
DEFINE_LIST(cl_platform_id, platforms)

/// List of platforms
static List* platforms = NULL;

/// Dispatching table coming from the ICD
static struct _cl_icd_dispatch *dispatch_table;

void populate_dispatch_table(struct _cl_icd_dispatch *dispatch)
{
    dispatch_table = dispatch;
}

void add_platform(cl_platform_id platform, ocland_server server)
{
    if(!platforms)
        platforms = platforms_new();
    cl_platform_id data = (cl_platform_id)malloc(
        sizeof(struct _cl_platform_id));
    data->dispatch = dispatch_table;
    data->ptr = platform;
    data->server = server;
    platforms_push_back(platforms, data);
    add_devices(data);
}

cl_platform_id get_platform(cl_platform_id remote_platform)
{
    if(!platforms_len(platforms))
        return NULL;
    for(platforms_node platform_node = platforms_front(platforms);
        !platforms_is_null(platform_node);
        platform_node = platforms_next(platform_node))
    {
        cl_platform_id local_platform = platforms_data(platform_node);
        if(local_platform->ptr == remote_platform)
            return local_platform;
    }

    return NULL;
}

bool is_platform(cl_platform_id platform)
{
    if(!platforms)
        platforms = platforms_new();
    return platforms_find(platforms, platform) < platforms_len(platforms);
}

cl_int oclandGetPlatformIDs(cl_uint         num_entries,
                            cl_platform_id* local_platforms,
                            cl_uint*        num_platforms)
{
    // If no platforms have been found, add_platform() was never called and
    // thus platforms_new()
    if(!platforms)
        platforms = platforms_new();

    if(num_platforms) *num_platforms = platforms_len(platforms);

    if(!num_entries)
        return CL_SUCCESS;

    if(num_entries > platforms_len(platforms))
        num_entries = platforms_len(platforms);
    platforms_node platform_node = platforms_front(platforms);
    for(cl_uint i = 0; i < num_entries; i++) {
        local_platforms[i] = platforms_data(platform_node);
        if(i < num_entries - 1)
            platform_node = platforms_next(platform_node);
    }

    return CL_SUCCESS;
}

/**
 * Get info from the platform
 *
 * This method only returns data that shall not be queried to the server
 * @param  platform Platform
 * @param  param_name Parameter
 * @param  param_value_size Allocated memory inside #param_value
 * @param  param_value Allocated memory where the queried data shall be copy
 * @param  param_value_size_ret Returned size of the queried data
 * @return 1 if the queried data cannot be extracted from the
 * local instance, and shall be therefore queried to the server. CL_SUCCESS
 * if the query can be processed. CL_INVALID_VALUE if the query can be processed
 * but param_value_size is smaller than the size of the returned value.
 */
cl_int get_platform_info(cl_platform_id   platform,
                         cl_context_info  param_name,
                         size_t           param_value_size,
                         void *           param_value,
                         size_t *         param_value_size_ret)
{
    size_t size_ret = 0;
    void* data_ret = NULL;
    switch(param_name) {
        case CL_PLATFORM_EXTENSIONS:
            size_ret = 11 * sizeof(char);
            data_ret = (void*)"cl_khr_icd\0";
            break;
        default:
            return 1;
    }

    if(param_value_size_ret)
        *param_value_size_ret = size_ret;
    if(param_value) {
        if(param_value_size < size_ret)
            return CL_INVALID_VALUE;
        memcpy(param_value, data_ret, size_ret);
    }

    return CL_SUCCESS;
}

cl_int oclandGetPlatformInfo(cl_platform_id    platform,
                             cl_platform_info  param_name,
                             size_t            param_value_size,
                             void *            param_value,
                             size_t *          param_value_size_ret)
{
    if(!is_platform(platform))
        return CL_INVALID_PLATFORM;

    cl_int flag;
    flag = get_platform_info(platform,
                             param_name,
                             param_value_size,
                             param_value,
                             param_value_size_ret);
    if(flag != 1)  // flag = 1 means we should call the server
        return flag;


    // Call the remote peer
    ocland_server server = platform->server;
    lock(server);

    int err;
    const unsigned int method = ocland_clGetPlatformInfo;
    err = send_comm(server->socket, sizeof(unsigned int),
                                    sizeof(cl_platform_id),
                                    sizeof(cl_platform_info),
                                    sizeof(size_t),
                                    0,
                                    &method,
                                    &(platform->ptr),
                                    &param_name,
                                    &param_value_size,
                                    NULL);
    if(err) {
        unlock(server);
        return CL_OUT_OF_HOST_MEMORY;
    }

    recv_pending rest;
    size_t size_ret;
    err = recv_comm(server->socket, &rest, sizeof(cl_int),
                                           sizeof(size_t),
                                           0,
                                           &flag,
                                           &size_ret,
                                           NULL);
    if(err) {
        unlock(server);
        free(rest.data);
        return CL_OUT_OF_HOST_MEMORY;
    }

    unlock(server);

    if(flag != CL_SUCCESS){
        free(rest.data);
        return flag;
    }
    if(param_value_size_ret) *param_value_size_ret = size_ret;

    // The remaining message is already the param_value
    if(param_value) {
        if(param_value_size < size_ret) {
            free(rest.data);
            return CL_INVALID_VALUE;
        }
        if(rest.size != size_ret) {
            free(rest.data);
            return CL_OUT_OF_HOST_MEMORY;
        }
        memcpy(param_value, rest.data, size_ret);
    }
    free(rest.data);

    return CL_SUCCESS;
}
