/*
 *  This file is part of ocland, a free cloud OpenCL interface.
 *  Copyright (C) 2012  Jose Luis Cercos Pita <jl.cercos@upm.es>
 *
 *  ocland is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  ocland is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with ocland.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <ocland/client/ocland.h>
#include <ocland/client/ocland_icd.h>
#include <ocland/client/log.h>

#include <string.h>

#ifndef MAX_N_EVENTS
    #define MAX_N_EVENTS 1<<20    // 1048576
#endif

#define SYMB(f) \
typeof(icd_##f) f __attribute__ ((alias ("icd_" #f), visibility("default")))

#pragma GCC visibility push(hidden)

cl_uint num_master_events = 0;
cl_event master_events[MAX_N_EVENTS];

// --------------------------------------------------------------
// Platforms
// --------------------------------------------------------------

static cl_int
__GetPlatformIDs(cl_uint num_entries,
                 cl_platform_id *platforms,
                 cl_uint *num_platforms)
{
    VERBOSE_IN();
    if(    ( !platforms   && !num_platforms )
        || (  num_entries && !platforms )
        || ( !num_entries &&  platforms )) {
        VERBOSE_OUT(CL_INVALID_VALUE);
        return CL_INVALID_VALUE;
    }

    populate_dispatch_table(&master_dispatch);
    if(!oclandInit()) {
        VERBOSE_OUT(CL_PLATFORM_NOT_FOUND_KHR);
        return CL_PLATFORM_NOT_FOUND_KHR;
    }

    // The OpenCL specification explicitelly ask to check the number of
    // available platforms, to report CL_PLATFORM_NOT_FOUND_KHR in case no
    // platforms are available. This shall be done regardless num_platforms
    // argument
    cl_uint ret_num_platforms;
    cl_int flag = oclandGetPlatformIDs(num_entries,
                                       platforms,
                                       &ret_num_platforms);
    if(flag != CL_SUCCESS){
        VERBOSE_OUT(flag);
        return flag;
    }
    if(!ret_num_platforms){
        VERBOSE_OUT(CL_PLATFORM_NOT_FOUND_KHR);
        return CL_PLATFORM_NOT_FOUND_KHR;
    }

    if(num_platforms) *num_platforms = ret_num_platforms;

    VERBOSE_OUT(CL_SUCCESS);
    return CL_SUCCESS;
}

CL_API_ENTRY cl_int CL_API_CALL
icd_clGetPlatformIDs(cl_uint           num_entries ,
                     cl_platform_id *  platforms ,
                     cl_uint *         num_platforms) CL_API_SUFFIX__VERSION_1_0
{
    VERBOSE_IN();
    cl_int flag = __GetPlatformIDs(num_entries, platforms, num_platforms);
    VERBOSE_OUT(flag);
    return flag;
}
SYMB(clGetPlatformIDs);

CL_API_ENTRY cl_int CL_API_CALL
icd_clIcdGetPlatformIDsKHR(cl_uint num_entries,
                           cl_platform_id *platforms,
                           cl_uint *num_platforms) CL_API_SUFFIX__VERSION_1_2
{
    VERBOSE_IN();
    cl_int flag = __GetPlatformIDs(num_entries, platforms, num_platforms);
    VERBOSE_OUT(flag);
    return flag;
}
SYMB(clIcdGetPlatformIDsKHR);

CL_API_ENTRY cl_int CL_API_CALL
icd_clGetPlatformInfo(cl_platform_id   platform,
                      cl_platform_info param_name,
                      size_t           param_value_size,
                      void *           param_value,
                      size_t *         param_value_size_ret) CL_API_SUFFIX__VERSION_1_0
{
    VERBOSE_IN();
    if (param_value_size == 0 && param_value != NULL) {
        VERBOSE_OUT(CL_INVALID_VALUE);
        return CL_INVALID_VALUE;
    }
    // Connect to servers to get info
    cl_int flag = oclandGetPlatformInfo(platform,
                                        param_name,
                                        param_value_size,
                                        param_value,
                                        param_value_size_ret);
    VERBOSE_OUT(flag);
    return flag;
}
SYMB(clGetPlatformInfo);

// --------------------------------------------------------------
// Devices
// --------------------------------------------------------------

CL_API_ENTRY cl_int CL_API_CALL
icd_clGetDeviceIDs(cl_platform_id platform,
                 cl_device_type   device_type,
                 cl_uint          num_entries,
                 cl_device_id *   devices,
                 cl_uint *        num_devices)
{
    VERBOSE_IN();
    if( (!num_entries && devices) || (!devices && !num_devices) ) {
        VERBOSE_OUT(CL_INVALID_VALUE);
        return CL_INVALID_VALUE;
    }

    cl_int flag;
    flag = oclandGetDeviceIDs(platform,
                              device_type,
                              num_entries,
                              devices,
                              num_devices);
    VERBOSE_OUT(flag);
    return flag;
}
SYMB(clGetDeviceIDs);

CL_API_ENTRY cl_int CL_API_CALL
icd_clGetDeviceInfo(cl_device_id    device,
                    cl_device_info  param_name,
                    size_t          param_value_size,
                    void *          param_value,
                    size_t *        param_value_size_ret) CL_API_SUFFIX__VERSION_1_0
{
    VERBOSE_IN();
    cl_int flag = oclandGetDeviceInfo(device,
                                      param_name,
                                      param_value_size,
                                      param_value,
                                      param_value_size_ret);
    VERBOSE_OUT(flag);
    return flag;
}
SYMB(clGetDeviceInfo);

CL_API_ENTRY cl_int CL_API_CALL
icd_clCreateSubDevices(cl_device_id                         in_device,
                       const cl_device_partition_property * properties,
                       cl_uint                              num_entries,
                       cl_device_id                       * out_devices,
                       cl_uint                            * num_devices) CL_API_SUFFIX__VERSION_1_2
{
    VERBOSE_IN();
    if(    ( !out_devices && !num_devices )
        || ( !out_devices &&  num_entries )
        || (  out_devices && !num_entries )
        || ( !properties ))
        return CL_INVALID_VALUE;
    cl_int flag = oclandCreateSubDevices(in_device,
                                         properties,
                                         num_entries,
                                         out_devices,
                                         num_devices);
    VERBOSE_OUT(flag);
    return flag;
}
SYMB(clCreateSubDevices);

CL_API_ENTRY cl_int CL_API_CALL
icd_clRetainDevice(cl_device_id device) CL_API_SUFFIX__VERSION_1_2
{
    VERBOSE_IN();
    cl_int flag = oclandRetainDevice(device);
    VERBOSE_OUT(flag);
    return flag;
}
SYMB(clRetainDevice);

CL_API_ENTRY cl_int CL_API_CALL
icd_clReleaseDevice(cl_device_id device) CL_API_SUFFIX__VERSION_1_2
{
    VERBOSE_IN();
    cl_int flag = oclandReleaseDevice(device);
    VERBOSE_OUT(flag);
    return flag;
}
SYMB(clReleaseDevice);

// --------------------------------------------------------------
// Context
// --------------------------------------------------------------

CL_API_ENTRY cl_context CL_API_CALL
icd_clCreateContext(const cl_context_properties * properties,
                    cl_uint                       num_devices ,
                    const cl_device_id *          devices,
                    void (CL_CALLBACK * pfn_notify)(const char *, const void *, size_t, void *),
                    void *                        user_data,
                    cl_int *                      errcode_ret) CL_API_SUFFIX__VERSION_1_0
{
    VERBOSE_IN();
    // validate the arguments
    if( !num_devices || !devices || (!pfn_notify && user_data) ){
        if(errcode_ret) *errcode_ret = CL_INVALID_VALUE;
        VERBOSE_OUT(CL_INVALID_VALUE);
        return NULL;
    }
    cl_int flag;
    cl_context context = oclandCreateContext(properties,
                                             num_devices,
                                             devices,
                                             pfn_notify,
                                             user_data,
                                             &flag);
    if(errcode_ret) *errcode_ret = flag;
    VERBOSE_OUT(flag);
    return context;
}
SYMB(clCreateContext);

CL_API_ENTRY cl_context CL_API_CALL
icd_clCreateContextFromType(const cl_context_properties * properties,
                            cl_device_type                device_type,
                            void (CL_CALLBACK * pfn_notify)(const char *, const void *, size_t, void *),
                            void *                        user_data,
                            cl_int *                      errcode_ret) CL_API_SUFFIX__VERSION_1_0
{
    VERBOSE_IN();
    if(!pfn_notify && user_data){
        if(errcode_ret) *errcode_ret = CL_INVALID_VALUE;
        VERBOSE_OUT(CL_INVALID_VALUE);
        return NULL;
    }
    cl_int flag;
    cl_context context = oclandCreateContextFromType(properties,
                                                     device_type,
                                                     pfn_notify,
                                                     user_data,
                                                     &flag);
    if(errcode_ret) *errcode_ret = flag;
    VERBOSE_OUT(flag);
    return context;
}
SYMB(clCreateContextFromType);

CL_API_ENTRY cl_int CL_API_CALL
icd_clRetainContext(cl_context context) CL_API_SUFFIX__VERSION_1_0
{
    VERBOSE_IN();
    cl_int flag = oclandRetainContext(context);
    VERBOSE_OUT(flag);
    return flag;
}
SYMB(clRetainContext);

CL_API_ENTRY cl_int CL_API_CALL
icd_clReleaseContext(cl_context context) CL_API_SUFFIX__VERSION_1_0
{
    VERBOSE_IN();
    cl_int flag = oclandReleaseContext(context);
    VERBOSE_OUT(flag);
    return flag;
}
SYMB(clReleaseContext);

CL_API_ENTRY cl_int CL_API_CALL
icd_clGetContextInfo(cl_context         context,
                     cl_context_info    param_name,
                     size_t             param_value_size,
                     void *             param_value,
                     size_t *           param_value_size_ret) CL_API_SUFFIX__VERSION_1_0
{
    VERBOSE_IN();
    cl_int flag = oclandGetContextInfo(context,
                                       param_name,
                                       param_value_size,
                                       param_value,
                                       param_value_size_ret);
    VERBOSE_OUT(flag);
    return flag;
}
SYMB(clGetContextInfo);

// --------------------------------------------------------------
// Command Queue
// --------------------------------------------------------------

CL_API_ENTRY cl_command_queue CL_API_CALL
icd_clCreateCommandQueue(cl_context                     context,
                         cl_device_id                   device,
                         cl_command_queue_properties    properties,
                         cl_int *                       errcode_ret) CL_API_SUFFIX__VERSION_1_0
{
    VERBOSE_IN();
    cl_int flag;
    cl_command_queue queue = oclandCreateCommandQueue(context,
                                                      device,
                                                      properties,
                                                      &flag);
    if(errcode_ret) *errcode_ret = flag;
    VERBOSE_OUT(flag);
    return queue;
}
SYMB(clCreateCommandQueue);

CL_API_ENTRY cl_int CL_API_CALL
icd_clRetainCommandQueue(cl_command_queue command_queue) CL_API_SUFFIX__VERSION_1_0
{
    VERBOSE_IN();
    cl_int flag = oclandRetainCommandQueue(command_queue);
    VERBOSE_OUT(flag);
    return flag;
}
SYMB(clRetainCommandQueue);

CL_API_ENTRY cl_int CL_API_CALL
icd_clReleaseCommandQueue(cl_command_queue command_queue) CL_API_SUFFIX__VERSION_1_0
{
    VERBOSE_IN();
    // Any call to this method implies a clFlush. See
    // https://www.khronos.org/registry/OpenCL/sdk/2.1/docs/man/xhtml/clFlush.html
    cl_int flag = oclandFlush(command_queue);
    if(flag != CL_SUCCESS) {
        // The same errors are still valid
        VERBOSE_OUT(flag);
        return flag;
    }

    flag = oclandReleaseCommandQueue(command_queue);
    VERBOSE_OUT(flag);
    return flag;
}
SYMB(clReleaseCommandQueue);

CL_API_ENTRY cl_int CL_API_CALL
icd_clGetCommandQueueInfo(cl_command_queue      command_queue,
                          cl_command_queue_info param_name,
                          size_t                param_value_size,
                          void *                param_value,
                          size_t *              param_value_size_ret) CL_API_SUFFIX__VERSION_1_0
{
    VERBOSE_IN();
    cl_int flag = oclandGetCommandQueueInfo(command_queue,
                                            param_name,
                                            param_value_size,
                                            param_value,
                                            param_value_size_ret);
    VERBOSE_OUT(flag);
    return flag;
}
SYMB(clGetCommandQueueInfo);

CL_API_ENTRY cl_command_queue CL_API_CALL
icd_clCreateCommandQueueWithProperties(cl_context                  context,
                                       cl_device_id                device,
                                       const cl_queue_properties * properties,
                                       cl_int *                    errcode_ret) CL_API_SUFFIX__VERSION_2_0
{
    VERBOSE_IN();
    cl_int flag;
    cl_command_queue queue = oclandCreateCommandQueueWithProperties(context,
                                                                    device,
                                                                    properties,
                                                                    &flag);
    if(errcode_ret) *errcode_ret = flag;
    VERBOSE_OUT(flag);
    return queue;
}
SYMB(clCreateCommandQueueWithProperties);

CL_API_ENTRY CL_EXT_PREFIX__VERSION_1_0_DEPRECATED cl_int CL_API_CALL
icd_clSetCommandQueueProperty(cl_command_queue command_queue,
                              cl_command_queue_properties properties,
                              cl_bool enable,
                              cl_command_queue_properties *old_properties)  CL_API_SUFFIX__VERSION_1_0
{
    VERBOSE_IN();
    // This method has been even removed in recent OpenCL versions, so if
    // someone is still calling it, we are just simply giving back an error
    VERBOSE_OUT(CL_INVALID_COMMAND_QUEUE);
    return CL_INVALID_COMMAND_QUEUE;
}
SYMB(clSetCommandQueueProperty);



// --------------------------------------------------------------
// Memory objects
// --------------------------------------------------------------

/**
 * Check whether a host pointer is required or not while building a memory
 * object
 * @param  flags Memory object creation flags
 * @return CL_TRUE if host_ptr is required, CL_FALSE otherwise
 */
cl_bool need_host_ptr(cl_mem_flags flags)
{
    return (flags & CL_MEM_USE_HOST_PTR) || (flags & CL_MEM_COPY_HOST_PTR);
}

CL_API_ENTRY cl_mem CL_API_CALL
icd_clCreateBuffer(cl_context    context ,
                   cl_mem_flags  flags ,
                   size_t        size ,
                   void *        host_ptr ,
                   cl_int *      errcode_ret) CL_API_SUFFIX__VERSION_1_0
{
    VERBOSE_IN();

    if(!size) {
        if(errcode_ret) *errcode_ret=CL_INVALID_BUFFER_SIZE;
        VERBOSE_OUT(CL_INVALID_BUFFER_SIZE);
        return NULL;
    }

    if((flags & CL_MEM_USE_HOST_PTR) && (flags & CL_MEM_ALLOC_HOST_PTR)) {
        if(errcode_ret) *errcode_ret = CL_INVALID_VALUE;
        VERBOSE_OUT(CL_INVALID_VALUE);
        return NULL;
    }

    if(    (host_ptr && !need_host_ptr(flags))
        || (!host_ptr && need_host_ptr(flags)) )
    {
        if(errcode_ret) *errcode_ret = CL_INVALID_HOST_PTR;
        VERBOSE_OUT(CL_INVALID_HOST_PTR);
        return NULL;
    }

    cl_int flag;
    cl_mem mem = oclandCreateBuffer(context, flags, size, host_ptr, &flag);
    if(errcode_ret) *errcode_ret = flag;
    VERBOSE_OUT(flag);
    return mem;
}
SYMB(clCreateBuffer);

// clCreateImage moved here to can use that with clCreateImage2D and
// clCreateImage3D (without implicit neither explicit declarations)
CL_API_ENTRY cl_mem CL_API_CALL
icd_clCreateImage(cl_context              context,
                  cl_mem_flags            flags,
                  const cl_image_format * image_format,
                  const cl_image_desc *   image_desc,
                  void *                  host_ptr,
                  cl_int *                errcode_ret) CL_API_SUFFIX__VERSION_1_2
{
    VERBOSE_IN();

    if((flags & CL_MEM_USE_HOST_PTR) && (flags & CL_MEM_ALLOC_HOST_PTR)) {
        if(errcode_ret) *errcode_ret = CL_INVALID_VALUE;
        VERBOSE_OUT(CL_INVALID_VALUE);
        return NULL;
    }

    if(!image_format) {
        if(errcode_ret) *errcode_ret = CL_INVALID_IMAGE_FORMAT_DESCRIPTOR;
        VERBOSE_OUT(CL_INVALID_IMAGE_FORMAT_DESCRIPTOR);
        return NULL;
    }

    if( !image_desc
        || (!host_ptr && image_desc->image_row_pitch)
        || (!host_ptr && image_desc->image_row_pitch) )
    {
        if(errcode_ret) *errcode_ret = CL_INVALID_IMAGE_DESCRIPTOR;
        VERBOSE_OUT(CL_INVALID_IMAGE_DESCRIPTOR);
        return NULL;
    }

    if(    (host_ptr && !need_host_ptr(flags))
        || (!host_ptr && need_host_ptr(flags)) )
    {
        if(errcode_ret) *errcode_ret = CL_INVALID_HOST_PTR;
        VERBOSE_OUT(CL_INVALID_HOST_PTR);
        return NULL;
    }

    cl_int flag;
    cl_mem mem = oclandCreateImage(context,
                                   flags,
                                   image_format,
                                   image_desc,
                                   host_ptr,
                                   &flag);
    if(errcode_ret) *errcode_ret = flag;
    VERBOSE_OUT(flag);
    return mem;
}
SYMB(clCreateImage);

CL_API_ENTRY CL_EXT_PREFIX__VERSION_1_1_DEPRECATED cl_mem CL_API_CALL
icd_clCreateImage2D(cl_context              context ,
                    cl_mem_flags            flags ,
                    const cl_image_format * image_format ,
                    size_t                  image_width ,
                    size_t                  image_height ,
                    size_t                  image_row_pitch ,
                    void *                  host_ptr ,
                    cl_int *                errcode_ret) CL_EXT_SUFFIX__VERSION_1_1_DEPRECATED
{
    VERBOSE_IN();
    cl_image_desc desc = {CL_MEM_OBJECT_IMAGE2D,
                          image_width,
                          image_height,
                          0,
                          0,
                          0,
                          0,
                          0,
                          0,
                          {NULL}};
    return icd_clCreateImage(context,
                             flags,
                             image_format,
                             &desc,
                             host_ptr,
                             errcode_ret);
}
SYMB(clCreateImage2D);

CL_API_ENTRY CL_EXT_PREFIX__VERSION_1_1_DEPRECATED cl_mem CL_API_CALL
icd_clCreateImage3D(cl_context              context,
                    cl_mem_flags            flags,
                    const cl_image_format * image_format,
                    size_t                  image_width,
                    size_t                  image_height ,
                    size_t                  image_depth ,
                    size_t                  image_row_pitch ,
                    size_t                  image_slice_pitch ,
                    void *                  host_ptr ,
                    cl_int *                errcode_ret) CL_EXT_SUFFIX__VERSION_1_1_DEPRECATED
{
    VERBOSE_IN();
    cl_image_desc desc = {CL_MEM_OBJECT_IMAGE2D,
                          image_width,
                          image_height,
                          image_depth,
                          0,
                          0,
                          0,
                          0,
                          0,
                          {NULL}};
    return icd_clCreateImage(context,
                             flags,
                             image_format,
                             &desc,
                             host_ptr,
                             errcode_ret);
}
SYMB(clCreateImage3D);

CL_API_ENTRY cl_int CL_API_CALL
icd_clRetainMemObject(cl_mem memobj) CL_API_SUFFIX__VERSION_1_0
{
    VERBOSE_IN();
    cl_int flag = oclandRetainMemObject(memobj);
    VERBOSE_OUT(flag);
    return flag;
}
SYMB(clRetainMemObject);

CL_API_ENTRY cl_int CL_API_CALL
icd_clReleaseMemObject(cl_mem memobj) CL_API_SUFFIX__VERSION_1_0
{
    VERBOSE_IN();
    cl_int flag = oclandReleaseMemObject(memobj);
    VERBOSE_OUT(flag);
    return flag;
}
SYMB(clReleaseMemObject);

CL_API_ENTRY cl_int CL_API_CALL
icd_clGetSupportedImageFormats(cl_context           context,
                               cl_mem_flags         flags,
                               cl_mem_object_type   image_type ,
                               cl_uint              num_entries ,
                               cl_image_format *    image_formats ,
                               cl_uint *            num_image_formats) CL_API_SUFFIX__VERSION_1_0
{
    VERBOSE_IN();
    if(!num_entries && image_formats){
        VERBOSE_OUT(CL_INVALID_VALUE);
        return CL_INVALID_VALUE;
    }
    return oclandGetSupportedImageFormats(context,
                                          flags,
                                          image_type,
                                          num_entries,
                                          image_formats,
                                          num_image_formats);
}
SYMB(clGetSupportedImageFormats);

CL_API_ENTRY cl_int CL_API_CALL
icd_clGetMemObjectInfo(cl_mem            memobj ,
                       cl_mem_info       param_name ,
                       size_t            param_value_size ,
                       void *            param_value ,
                       size_t *          param_value_size_ret) CL_API_SUFFIX__VERSION_1_0
{
    VERBOSE_IN();
    cl_int flag = oclandGetMemObjectInfo(memobj,
                                         param_name,
                                         param_value_size,
                                         param_value,
                                         param_value_size_ret);
    VERBOSE_OUT(flag);
    return flag;
}
SYMB(clGetMemObjectInfo);

CL_API_ENTRY cl_int CL_API_CALL
icd_clGetImageInfo(cl_mem            image ,
                   cl_image_info     param_name ,
                   size_t            param_value_size ,
                   void *            param_value ,
                   size_t *          param_value_size_ret) CL_API_SUFFIX__VERSION_1_0
{
    VERBOSE_IN();
    cl_int flag = oclandGetImageInfo(image,
                                     param_name,
                                     param_value_size,
                                     param_value,
                                     param_value_size_ret);
    VERBOSE_OUT(flag);
    return flag;
}
SYMB(clGetImageInfo);

CL_API_ENTRY cl_mem CL_API_CALL
icd_clCreateSubBuffer(cl_mem                    buffer ,
                      cl_mem_flags              flags ,
                      cl_buffer_create_type     buffer_create_type ,
                      const void *              buffer_create_info ,
                      cl_int *                  errcode_ret) CL_API_SUFFIX__VERSION_1_1
{
    VERBOSE_IN();

    if(    (!buffer_create_info)
        || (flags & CL_MEM_USE_HOST_PTR)
        || (flags & CL_MEM_ALLOC_HOST_PTR)
        || (flags & CL_MEM_COPY_HOST_PTR) )
    {
        if(errcode_ret) *errcode_ret = CL_INVALID_VALUE;
        VERBOSE_OUT(CL_INVALID_VALUE);
        return NULL;
    }

    cl_int flag;
    cl_mem mem = oclandCreateSubBuffer(buffer,
                                       flags,
                                       buffer_create_type,
                                       buffer_create_info,
                                       &flag);
    if(errcode_ret) *errcode_ret = flag;
    VERBOSE_OUT(flag);
    return mem;
}
SYMB(clCreateSubBuffer);

CL_API_ENTRY cl_int CL_API_CALL
icd_clSetMemObjectDestructorCallback(cl_mem  memobj ,
                                     void (CL_CALLBACK * pfn_notify)(cl_mem  memobj , void* user_data),
                                     void * user_data)  CL_API_SUFFIX__VERSION_1_1
{
    VERBOSE_IN();
    if (!pfn_notify) {
        VERBOSE_OUT(CL_INVALID_VALUE);
        return CL_INVALID_VALUE;
    }

    cl_int flag = oclandSetMemObjectDestructorCallback(memobj,
                                                       pfn_notify,
                                                       user_data);
    VERBOSE_OUT(flag);
    return flag;
}
SYMB(clSetMemObjectDestructorCallback);

// --------------------------------------------------------------
// Samplers
// --------------------------------------------------------------

CL_API_ENTRY cl_sampler CL_API_CALL
icd_clCreateSampler(cl_context           context ,
                    cl_bool              normalized_coords ,
                    cl_addressing_mode   addressing_mode ,
                    cl_filter_mode       filter_mode ,
                    cl_int *             errcode_ret) CL_API_SUFFIX__VERSION_1_0
{
    VERBOSE_IN();
    cl_int flag;
    cl_sampler sampler = oclandCreateSampler(context,
                                             normalized_coords,
                                             addressing_mode,
                                             filter_mode,
                                             &flag);
    if(errcode_ret) *errcode_ret = flag;
    VERBOSE_OUT(flag);
    return sampler;
}
SYMB(clCreateSampler);

CL_API_ENTRY cl_int CL_API_CALL
icd_clRetainSampler(cl_sampler  sampler) CL_API_SUFFIX__VERSION_1_0
{
    VERBOSE_IN();
    cl_int flag = oclandRetainSampler(sampler);
    VERBOSE_OUT(flag);
    return flag;
}
SYMB(clRetainSampler);

CL_API_ENTRY cl_int CL_API_CALL
icd_clReleaseSampler(cl_sampler  sampler) CL_API_SUFFIX__VERSION_1_0
{
    VERBOSE_IN();
    cl_int flag = oclandReleaseSampler(sampler);
    VERBOSE_OUT(flag);
    return flag;
}
SYMB(clReleaseSampler);

CL_API_ENTRY cl_int CL_API_CALL
icd_clGetSamplerInfo(cl_sampler          sampler ,
                     cl_sampler_info     param_name ,
                     size_t              param_value_size ,
                     void *              param_value ,
                     size_t *            param_value_size_ret) CL_API_SUFFIX__VERSION_1_0
{
    VERBOSE_IN();
    cl_int flag = oclandGetSamplerInfo(sampler,
                                       param_name,
                                       param_value_size,
                                       param_value,
                                       param_value_size_ret);
    VERBOSE_OUT(flag);
    return flag;
}
SYMB(clGetSamplerInfo);

// --------------------------------------------------------------
// Programs
// --------------------------------------------------------------

CL_API_ENTRY cl_program CL_API_CALL
icd_clCreateProgramWithSource(cl_context         context ,
                              cl_uint            count ,
                              const char **      strings ,
                              const size_t *     lengths ,
                              cl_int *           errcode_ret) CL_API_SUFFIX__VERSION_1_0
{
    VERBOSE_IN();
    if((!count) || (!strings)){
        if(errcode_ret) *errcode_ret=CL_INVALID_VALUE;
        VERBOSE_OUT(CL_INVALID_VALUE);
        return NULL;
    }
    for(cl_uint i = 0; i < count; i++){
        if(!strings[i]){
            if(errcode_ret) *errcode_ret=CL_INVALID_VALUE;
            VERBOSE_OUT(CL_INVALID_VALUE);
            return NULL;
        }
    }

    cl_int flag;
    cl_program program = oclandCreateProgramWithSource(context,
                                                       count,
                                                       strings,
                                                       lengths,
                                                       &flag);
    if(errcode_ret) *errcode_ret = flag;
    VERBOSE_OUT(flag);
    return program;
}
SYMB(clCreateProgramWithSource);

CL_API_ENTRY cl_program CL_API_CALL
icd_clCreateProgramWithBinary(cl_context                      context ,
                              cl_uint                         num_devices ,
                              const cl_device_id *            device_list ,
                              const size_t *                  lengths ,
                              const unsigned char **          binaries ,
                              cl_int *                        binary_status ,
                              cl_int *                        errcode_ret) CL_API_SUFFIX__VERSION_1_0
{
    VERBOSE_IN();
    if(!num_devices || !device_list || !lengths || !binaries){
        if(errcode_ret) *errcode_ret=CL_INVALID_VALUE;
        VERBOSE_OUT(CL_INVALID_VALUE);
        return NULL;
    }
    for(cl_uint i = 0; i < num_devices; i++){
        if(!lengths[i] || !binaries[i]){
            if(errcode_ret) *errcode_ret=CL_INVALID_VALUE;
            VERBOSE_OUT(CL_INVALID_VALUE);
            return NULL;
        }
    }

    cl_int flag;
    cl_program program = oclandCreateProgramWithBinary(context,
                                                       num_devices,
                                                       device_list,
                                                       lengths,
                                                       binaries,
                                                       binary_status,
                                                       &flag);
    if(errcode_ret) *errcode_ret = flag;
    VERBOSE_OUT(flag);
    return program;
}
SYMB(clCreateProgramWithBinary);

CL_API_ENTRY cl_int CL_API_CALL
icd_clRetainProgram(cl_program  program) CL_API_SUFFIX__VERSION_1_0
{
    VERBOSE_IN();
    cl_int flag = oclandRetainProgram(program);
    VERBOSE_OUT(flag);
    return flag;
}
SYMB(clRetainProgram);

CL_API_ENTRY cl_int CL_API_CALL
icd_clReleaseProgram(cl_program  program) CL_API_SUFFIX__VERSION_1_0
{
    VERBOSE_IN();
    cl_int flag = oclandReleaseProgram(program);
    VERBOSE_OUT(flag);
    return flag;
}
SYMB(clReleaseProgram);

CL_API_ENTRY cl_int CL_API_CALL
icd_clBuildProgram(cl_program            program ,
                   cl_uint               num_devices ,
                   const cl_device_id *  device_list ,
                   const char *          options ,
                   void (CL_CALLBACK *   pfn_notify)(cl_program  program , void *  user_data),
                   void *                user_data) CL_API_SUFFIX__VERSION_1_0
{
    VERBOSE_IN();
    if((!pfn_notify  &&  user_data  ) ||
       ( num_devices && !device_list) ||
       (!num_devices &&  device_list) ){
        VERBOSE_OUT(CL_INVALID_VALUE);
        return CL_INVALID_VALUE;
    }
    cl_int flag = oclandBuildProgram(program,
                                     num_devices,
                                     device_list,
                                     options,
                                     pfn_notify,
                                     user_data);
    VERBOSE_OUT(flag);
    return flag;
}
SYMB(clBuildProgram);

CL_API_ENTRY cl_int CL_API_CALL
icd_clUnloadCompiler() CL_API_SUFFIX__VERSION_1_0
{
    VERBOSE_IN();
    // This method was useless even in OpenCL 1.0, and deprecated in OpenCL 1.1
    // Actually, is quite strange they still keeping it. We are just simply
    // ignoring it
    VERBOSE_OUT(CL_SUCCESS);
    return CL_SUCCESS;
}
SYMB(clUnloadCompiler);

CL_API_ENTRY cl_int CL_API_CALL
icd_clGetProgramInfo(cl_program          program ,
                     cl_program_info     param_name ,
                     size_t              param_value_size ,
                     void *              param_value ,
                     size_t *            param_value_size_ret) CL_API_SUFFIX__VERSION_1_0
{
    VERBOSE_IN();
    cl_int flag = oclandGetProgramInfo(program,
                                       param_name,
                                       param_value_size,
                                       param_value,
                                       param_value_size_ret);
    VERBOSE_OUT(flag);
    return flag;
}
SYMB(clGetProgramInfo);

CL_API_ENTRY cl_int CL_API_CALL
icd_clGetProgramBuildInfo(cl_program             program ,
                          cl_device_id           device ,
                          cl_program_build_info  param_name ,
                          size_t                 param_value_size ,
                          void *                 param_value ,
                          size_t *               param_value_size_ret) CL_API_SUFFIX__VERSION_1_0
{
    VERBOSE_IN();
    cl_int flag = oclandGetProgramBuildInfo(program,
                                            device,
                                            param_name,
                                            param_value_size,
                                            param_value,
                                            param_value_size_ret);
    VERBOSE_OUT(flag);
    return flag;
}
SYMB(clGetProgramBuildInfo);

CL_API_ENTRY cl_program CL_API_CALL
icd_clCreateProgramWithBuiltInKernels(cl_context             context ,
                                      cl_uint                num_devices ,
                                      const cl_device_id *   device_list ,
                                      const char *           kernel_names ,
                                      cl_int *               errcode_ret) CL_API_SUFFIX__VERSION_1_2
{
    VERBOSE_IN();
    if(!num_devices || !device_list || !kernel_names){
        if(errcode_ret) *errcode_ret=CL_INVALID_VALUE;
        VERBOSE_OUT(CL_INVALID_VALUE);
        return NULL;
    }
    cl_int flag;
    cl_program program = oclandCreateProgramWithBuiltInKernels(context,
                                                               num_devices,
                                                               device_list,
                                                               kernel_names,
                                                               &flag);

    if(errcode_ret) *errcode_ret = flag;
    VERBOSE_OUT(flag);
    return program;
}
SYMB(clCreateProgramWithBuiltInKernels);

CL_API_ENTRY cl_int CL_API_CALL
icd_clCompileProgram(cl_program            program ,
                     cl_uint               num_devices ,
                     const cl_device_id *  device_list ,
                     const char *          options ,
                     cl_uint               num_input_headers ,
                     const cl_program *    input_headers,
                     const char **         header_include_names ,
                     void (CL_CALLBACK *   pfn_notify)(cl_program  program , void *  user_data),
                     void *                user_data) CL_API_SUFFIX__VERSION_1_2
{
    VERBOSE_IN();

    if((!pfn_notify  &&  user_data  ) ||
       ( num_devices && !device_list) ||
       (!num_devices &&  device_list) ||
       (!num_input_headers && ( input_headers ||  header_include_names) ) ||
       ( num_input_headers && (!input_headers || !header_include_names) ) ){
        VERBOSE_OUT(CL_INVALID_VALUE);
        return CL_INVALID_VALUE;
    }
    cl_int flag = oclandCompileProgram(program,
                                       num_devices,
                                       device_list,
                                       options,
                                       num_input_headers,
                                       input_headers,
                                       header_include_names,
                                       pfn_notify,
                                       user_data);
    VERBOSE_OUT(flag);
    return flag;
}
SYMB(clCompileProgram);

CL_API_ENTRY cl_program CL_API_CALL
icd_clLinkProgram(cl_context        context ,
              cl_uint               num_devices ,
              const cl_device_id *  device_list ,
              const char *          options ,
              cl_uint               num_input_programs ,
              const cl_program *    input_programs ,
              void (CL_CALLBACK *   pfn_notify)(cl_program  program , void *  user_data),
              void *                user_data ,
              cl_int *              errcode_ret) CL_API_SUFFIX__VERSION_1_2
{
    VERBOSE_IN();
    if((!pfn_notify  &&  user_data  ) ||
       ( num_devices && !device_list) ||
       (!num_devices &&  device_list) ||
       (!num_input_programs || !input_programs) ){
        if(errcode_ret) *errcode_ret=CL_INVALID_VALUE;
        VERBOSE_OUT(CL_INVALID_VALUE);
        return NULL;
    }
    cl_int flag;
    cl_program program = oclandLinkProgram(context,
                                           num_devices,
                                           device_list,
                                           options,
                                           num_input_programs,
                                           input_programs,
                                           pfn_notify,
                                           user_data,
                                           &flag);
    if(errcode_ret) *errcode_ret = flag;
    VERBOSE_OUT(flag);
    return program;
}
SYMB(clLinkProgram);

CL_API_ENTRY cl_int CL_API_CALL
icd_clUnloadPlatformCompiler(cl_platform_id  platform) CL_API_SUFFIX__VERSION_1_2
{
    VERBOSE_IN();
    cl_int flag = oclandUnloadPlatformCompiler(platform);
    VERBOSE_OUT(flag);
    return flag;
}
SYMB(clUnloadPlatformCompiler);

// --------------------------------------------------------------
// Kernels
// --------------------------------------------------------------

CL_API_ENTRY cl_kernel CL_API_CALL
icd_clCreateKernel(cl_program       program ,
                   const char *     kernel_name ,
                   cl_int *         errcode_ret) CL_API_SUFFIX__VERSION_1_0
{
    VERBOSE_IN();
    if(!kernel_name) {
        if(errcode_ret) *errcode_ret=CL_INVALID_VALUE;
        VERBOSE_OUT(CL_INVALID_VALUE);
        return NULL;
    }
    cl_int flag;
    cl_kernel kernel = oclandCreateKernel(program,
                                          kernel_name,
                                          &flag);
    if(errcode_ret) *errcode_ret = flag;
    VERBOSE_OUT(flag);
    return kernel;
}
SYMB(clCreateKernel);

CL_API_ENTRY cl_int CL_API_CALL
icd_clCreateKernelsInProgram(cl_program      program ,
                             cl_uint         num_kernels ,
                             cl_kernel *     kernels ,
                             cl_uint *       num_kernels_ret) CL_API_SUFFIX__VERSION_1_0
{
    VERBOSE_IN();
    // The very CL_INVALID_VALUE return would be caused by a non-null kernels
    // with a num_kernels lower than the number of available kernels. Since
    // the program may eventually have 0 kernels, I cannot check that yet
    cl_int flag = oclandCreateKernelsInProgram(program,
                                               num_kernels,
                                               kernels,
                                               num_kernels_ret);
    VERBOSE_OUT(flag);
    return flag;
}
SYMB(clCreateKernelsInProgram);

CL_API_ENTRY cl_int CL_API_CALL
icd_clRetainKernel(cl_kernel     kernel) CL_API_SUFFIX__VERSION_1_0
{
    VERBOSE_IN();
    cl_int flag = oclandRetainKernel(kernel);
    VERBOSE_OUT(flag);
    return flag;
}
SYMB(clRetainKernel);

CL_API_ENTRY cl_int CL_API_CALL
icd_clReleaseKernel(cl_kernel    kernel) CL_API_SUFFIX__VERSION_1_0
{
    VERBOSE_IN();
    cl_int flag = oclandReleaseKernel(kernel);
    VERBOSE_OUT(flag);
    return flag;
}
SYMB(clReleaseKernel);

CL_API_ENTRY cl_int CL_API_CALL
icd_clSetKernelArg(cl_kernel     kernel ,
                   cl_uint       arg_index ,
                   size_t        arg_size ,
                   const void *  arg_value) CL_API_SUFFIX__VERSION_1_0
{
    VERBOSE_IN();
    cl_int flag = oclandSetKernelArg(kernel,
                                     arg_index,
                                     arg_size,
                                     arg_value);
    VERBOSE_OUT(flag);
    return flag;
}
SYMB(clSetKernelArg);

CL_API_ENTRY cl_int CL_API_CALL
icd_clGetKernelInfo(cl_kernel        kernel ,
                    cl_kernel_info   param_name ,
                    size_t           param_value_size ,
                    void *           param_value ,
                    size_t *         param_value_size_ret) CL_API_SUFFIX__VERSION_1_0
{
    VERBOSE_IN();
    cl_int flag = oclandGetKernelInfo(kernel,
                                      param_name,
                                      param_value_size,
                                      param_value,
                                      param_value_size_ret);
    VERBOSE_OUT(flag);
    return flag;
}
SYMB(clGetKernelInfo);

CL_API_ENTRY cl_int CL_API_CALL
icd_clGetKernelWorkGroupInfo(cl_kernel                   kernel ,
                             cl_device_id                device ,
                             cl_kernel_work_group_info   param_name ,
                             size_t                      param_value_size ,
                             void *                      param_value ,
                             size_t *                    param_value_size_ret) CL_API_SUFFIX__VERSION_1_0
{
    VERBOSE_IN();
    cl_int flag = oclandGetKernelWorkGroupInfo(kernel,
                                               device,
                                               param_name,
                                               param_value_size,
                                               param_value,
                                               param_value_size_ret);
    VERBOSE_OUT(flag);
    return flag;
}
SYMB(clGetKernelWorkGroupInfo);

CL_API_ENTRY cl_int CL_API_CALL
icd_clGetKernelArgInfo(cl_kernel          kernel ,
                       cl_uint            arg_indx ,
                       cl_kernel_arg_info param_name ,
                       size_t             param_value_size ,
                       void *             param_value ,
                       size_t *           param_value_size_ret) CL_API_SUFFIX__VERSION_1_2
{
    VERBOSE_IN();
    cl_int flag = oclandGetKernelArgInfo(kernel,
                                         arg_indx,
                                         param_name,
                                         param_value_size,
                                         param_value,
                                         param_value_size_ret);
    VERBOSE_OUT(flag);
    return flag;
}
SYMB(clGetKernelArgInfo);

// --------------------------------------------------------------
// Events
// --------------------------------------------------------------

CL_API_ENTRY cl_int CL_API_CALL
icd_clWaitForEvents(cl_uint              num_events ,
                    const cl_event *     event_list) CL_API_SUFFIX__VERSION_1_0
{
    VERBOSE_IN();
    if(!num_events || !event_list) {
        VERBOSE_OUT(CL_INVALID_VALUE);
        return CL_INVALID_VALUE;
    }

    cl_int flag = oclandWaitForEvents(num_events, event_list);
    VERBOSE_OUT(flag);
    return flag;
}
SYMB(clWaitForEvents);

CL_API_ENTRY cl_int CL_API_CALL
icd_clGetEventInfo(cl_event          event ,
                   cl_event_info     param_name ,
                   size_t            param_value_size ,
                   void *            param_value ,
                   size_t *          param_value_size_ret) CL_API_SUFFIX__VERSION_1_0
{
    VERBOSE_IN();
    cl_int flag = oclandGetEventInfo(event,
                                     param_name,
                                     param_value_size,
                                     param_value,
                                     param_value_size_ret);
    VERBOSE_OUT(flag);
    return flag;
}
SYMB(clGetEventInfo);

CL_API_ENTRY cl_int CL_API_CALL
icd_clRetainEvent(cl_event  event) CL_API_SUFFIX__VERSION_1_0
{
    VERBOSE_IN();
    cl_int flag = oclandRetainEvent(event);
    VERBOSE_OUT(flag);
    return flag;
    VERBOSE_IN();
}
SYMB(clRetainEvent);

CL_API_ENTRY cl_int CL_API_CALL
icd_clReleaseEvent(cl_event  event) CL_API_SUFFIX__VERSION_1_0
{
    VERBOSE_IN();
    cl_int flag = oclandReleaseEvent(event);
    VERBOSE_OUT(flag);
    return flag;
}
SYMB(clReleaseEvent);

CL_API_ENTRY cl_int CL_API_CALL
icd_clGetEventProfilingInfo(cl_event             event ,
                            cl_profiling_info    param_name ,
                            size_t               param_value_size ,
                            void *               param_value ,
                            size_t *             param_value_size_ret) CL_API_SUFFIX__VERSION_1_0
{
    VERBOSE_IN();
    cl_int flag = oclandGetEventProfilingInfo(event,
                                              param_name,
                                              param_value_size,
                                              param_value,
                                              param_value_size_ret);
    VERBOSE_OUT(flag);
    return flag;
}
SYMB(clGetEventProfilingInfo);

CL_API_ENTRY cl_int CL_API_CALL
icd_clFlush(cl_command_queue  command_queue) CL_API_SUFFIX__VERSION_1_0
{
    VERBOSE_IN();
    cl_int flag = oclandFlush(command_queue);
    VERBOSE_OUT(flag);
    return flag;
}
SYMB(clFlush);

CL_API_ENTRY cl_int CL_API_CALL
icd_clFinish(cl_command_queue  command_queue) CL_API_SUFFIX__VERSION_1_0
{
    VERBOSE_IN();
    cl_int flag = oclandFinish(command_queue);
    VERBOSE_OUT(flag);
    return flag;
}
SYMB(clFinish);

CL_API_ENTRY cl_event CL_API_CALL
icd_clCreateUserEvent(cl_context     context ,
                      cl_int *       errcode_ret) CL_API_SUFFIX__VERSION_1_1
{
    VERBOSE_IN();
    cl_int flag;
    cl_event event = oclandCreateUserEvent(context, &flag);
    if(errcode_ret) *errcode_ret = flag;
    VERBOSE_OUT(flag);
    return event;
}
SYMB(clCreateUserEvent);

CL_API_ENTRY cl_int CL_API_CALL
icd_clSetUserEventStatus(cl_event    event ,
                         cl_int      execution_status) CL_API_SUFFIX__VERSION_1_1
{
    VERBOSE_IN();
    if(execution_status > CL_COMPLETE)
        return CL_INVALID_VALUE;
    cl_int flag = oclandSetUserEventStatus(event, execution_status);
    VERBOSE_OUT(flag);
    return flag;
}
SYMB(clSetUserEventStatus);

CL_API_ENTRY cl_int CL_API_CALL
icd_clSetEventCallback(cl_event     event ,
                       cl_int       command_exec_callback_type ,
                       void (CL_CALLBACK *  pfn_notify)(cl_event, cl_int, void *),
                       void *       user_data) CL_API_SUFFIX__VERSION_1_1
{
    VERBOSE_IN();
    if(!pfn_notify) {
        VERBOSE_OUT(CL_INVALID_VALUE);
        return CL_INVALID_VALUE;
    }
    if(    !pfn_notify
        || (command_exec_callback_type > CL_SUBMITTED)
        || (command_exec_callback_type < CL_COMPLETE) )
    {
        VERBOSE_OUT(CL_INVALID_VALUE);
        return CL_INVALID_VALUE;
    }

    cl_int flag = oclandSetEventCallback(event,
                                         command_exec_callback_type,
                                         pfn_notify,
                                         user_data);
    VERBOSE_OUT(flag);
    return flag;
}
SYMB(clSetEventCallback);

// --------------------------------------------------------------
// Enqueues
// --------------------------------------------------------------

CL_API_ENTRY cl_int CL_API_CALL
icd_clEnqueueReadBuffer(cl_command_queue     command_queue ,
                        cl_mem               buffer ,
                        cl_bool              blocking_read ,
                        size_t               offset ,
                        size_t               size ,
                        void *               ptr ,
                        cl_uint              num_events_in_wait_list ,
                        const cl_event *     event_wait_list ,
                        cl_event *           event) CL_API_SUFFIX__VERSION_1_0
{
    VERBOSE_IN();

    if(!size || !ptr){
        VERBOSE_OUT(CL_INVALID_VALUE);
        return CL_INVALID_VALUE;
    }
    if(    ( num_events_in_wait_list && !event_wait_list)
        || (!num_events_in_wait_list &&  event_wait_list))
    {
        VERBOSE_OUT(CL_INVALID_EVENT_WAIT_LIST);
        return CL_INVALID_EVENT_WAIT_LIST;
    }

    if(event) {
        for (cl_uint i = 0; i < num_events_in_wait_list; i++) {
            if(*event == event_wait_list[i]) {
                VERBOSE_OUT(CL_INVALID_EVENT_WAIT_LIST);
                return CL_INVALID_EVENT_WAIT_LIST;
            }
        }
    }

    cl_int flag;
    // if blocking_read is CL_TRUE, this method implies a clFlush. See
    // https://www.khronos.org/registry/OpenCL/sdk/2.1/docs/man/xhtml/clFlush.html
    if(blocking_read) {
        flag = oclandFlush(command_queue);
        if(flag != CL_SUCCESS) {
            // The same errors are still valid
            VERBOSE_OUT(flag);
            return flag;
        }
    }

    flag = oclandEnqueueReadBuffer(command_queue,
                                   buffer,
                                   blocking_read,
                                   offset,
                                   size,
                                   ptr,
                                   num_events_in_wait_list,
                                   event_wait_list,
                                   event);
    VERBOSE_OUT(flag);
    return flag;
}
SYMB(clEnqueueReadBuffer);

CL_API_ENTRY cl_int CL_API_CALL
icd_clEnqueueWriteBuffer(cl_command_queue    command_queue ,
                         cl_mem              buffer ,
                         cl_bool             blocking_write ,
                         size_t              offset ,
                         size_t              size ,
                         const void *        ptr ,
                         cl_uint             num_events_in_wait_list ,
                         const cl_event *    event_wait_list ,
                         cl_event *          event) CL_API_SUFFIX__VERSION_1_0
{
    VERBOSE_IN();
    if(!size || !ptr){
        VERBOSE_OUT(CL_INVALID_VALUE);
        return CL_INVALID_VALUE;
    }
    if(    ( num_events_in_wait_list && !event_wait_list)
        || (!num_events_in_wait_list &&  event_wait_list)){
        VERBOSE_OUT(CL_INVALID_EVENT_WAIT_LIST);
        return CL_INVALID_EVENT_WAIT_LIST;
    }

    if(event) {
        for (cl_uint i = 0; i < num_events_in_wait_list; i++) {
            if(*event == event_wait_list[i]) {
                VERBOSE_OUT(CL_INVALID_EVENT_WAIT_LIST);
                return CL_INVALID_EVENT_WAIT_LIST;
            }
        }
    }

    cl_int flag;
    // if blocking_write is CL_TRUE, this method implies a clFlush. See
    // https://www.khronos.org/registry/OpenCL/sdk/2.1/docs/man/xhtml/clFlush.html
    if(blocking_write) {
        flag = oclandFlush(command_queue);
        if(flag != CL_SUCCESS) {
            // The same errors are still valid
            VERBOSE_OUT(flag);
            return flag;
        }
    }

    flag = oclandEnqueueWriteBuffer(command_queue,
                                    buffer,
                                    blocking_write,
                                    offset,
                                    size,
                                    ptr,
                                    num_events_in_wait_list,
                                    event_wait_list,
                                    event);
    VERBOSE_OUT(flag);
    return flag;
}
SYMB(clEnqueueWriteBuffer);

CL_API_ENTRY cl_int CL_API_CALL
icd_clEnqueueCopyBuffer(cl_command_queue     command_queue ,
                        cl_mem               src_buffer ,
                        cl_mem               dst_buffer ,
                        size_t               src_offset ,
                        size_t               dst_offset ,
                        size_t               cb ,
                        cl_uint              num_events_in_wait_list ,
                        const cl_event *     event_wait_list ,
                        cl_event *           event) CL_API_SUFFIX__VERSION_1_0
{
    VERBOSE_IN();
    cl_uint i;
    if(    ( num_events_in_wait_list && !event_wait_list)
        || (!num_events_in_wait_list &&  event_wait_list)){
        VERBOSE_OUT(CL_INVALID_EVENT_WAIT_LIST);
        return CL_INVALID_EVENT_WAIT_LIST;
    }
    // Correct input events
    cl_event *events_wait = NULL;
    if(num_events_in_wait_list){
        events_wait = (cl_event*)malloc(num_events_in_wait_list*sizeof(cl_event));
        if(!events_wait){
            VERBOSE_OUT(CL_OUT_OF_HOST_MEMORY);
            return CL_OUT_OF_HOST_MEMORY;
        }
        for(i=0;i<num_events_in_wait_list;i++)
            events_wait[i] = event_wait_list[i]->ptr;
    }
    cl_int flag = oclandEnqueueCopyBuffer(command_queue->ptr,
                                          src_buffer->ptr,dst_buffer->ptr,
                                          src_offset,dst_offset,cb,
                                          num_events_in_wait_list,events_wait,event);
    if(flag != CL_SUCCESS){
        VERBOSE_OUT(flag);
        return flag;
    }
    free(events_wait); events_wait=NULL;
    // Correct output event
    if(event){
        cl_event e = (cl_event)malloc(sizeof(struct _cl_event));
        if(!e){
            VERBOSE_OUT(CL_OUT_OF_HOST_MEMORY);
            return CL_OUT_OF_HOST_MEMORY;
        }
        e->dispatch = &master_dispatch;
        e->ptr = *event;
        e->rcount = 1;
        *event = e;
        num_master_events++;
        master_events[num_master_events-1] = e;
    }
    return CL_SUCCESS;
}
SYMB(clEnqueueCopyBuffer);

CL_API_ENTRY cl_int CL_API_CALL
icd_clEnqueueReadImage(cl_command_queue      command_queue ,
                       cl_mem                image ,
                       cl_bool               blocking_read ,
                       const size_t *        origin ,
                       const size_t *        region ,
                       size_t                row_pitch ,
                       size_t                slice_pitch ,
                       void *                ptr ,
                       cl_uint               num_events_in_wait_list ,
                       const cl_event *      event_wait_list ,
                       cl_event *            event) CL_API_SUFFIX__VERSION_1_0
{
    VERBOSE_IN();
    if(   (!ptr)
       || (!origin)
       || (!region)){
        VERBOSE_OUT(CL_INVALID_VALUE);
        return CL_INVALID_VALUE;
    }
    // Correct some values if not provided
    if(!row_pitch)
        row_pitch   = region[0];
    if(!slice_pitch)
        slice_pitch = region[1]*row_pitch;
    if(   (!region[0]) || (!region[1]) || (!region[2])
       || (row_pitch   < region[0])
       || (slice_pitch < region[1]*row_pitch)
       || (slice_pitch % row_pitch)){
        VERBOSE_OUT(CL_INVALID_VALUE);
        return CL_INVALID_VALUE;
    }
    if(    ( num_events_in_wait_list && !event_wait_list)
        || (!num_events_in_wait_list &&  event_wait_list)){
        VERBOSE_OUT(CL_INVALID_EVENT_WAIT_LIST);
        return CL_INVALID_EVENT_WAIT_LIST;
    }
    // Correct input events
    cl_uint i;
    cl_event *events_wait = NULL;
    if(num_events_in_wait_list){
        events_wait = (cl_event*)malloc(num_events_in_wait_list*sizeof(cl_event));
        if(!events_wait){
            VERBOSE_OUT(CL_OUT_OF_HOST_MEMORY);
            return CL_OUT_OF_HOST_MEMORY;
        }
        for(i=0;i<num_events_in_wait_list;i++)
            events_wait[i] = event_wait_list[i]->ptr;
    }
    cl_int flag = oclandEnqueueReadImage(command_queue->ptr,image->ptr,
                                         blocking_read,origin,region,
                                         row_pitch,slice_pitch,image->image.element_size,ptr,
                                         num_events_in_wait_list,events_wait,
                                         event);
    if(flag != CL_SUCCESS){
        VERBOSE_OUT(flag);
        return flag;
    }
    free(events_wait); events_wait=NULL;
    // Correct output event
    if(event){
        cl_event e = (cl_event)malloc(sizeof(struct _cl_event));
        if(!e){
            VERBOSE_OUT(CL_OUT_OF_HOST_MEMORY);
            return CL_OUT_OF_HOST_MEMORY;
        }
        e->dispatch = &master_dispatch;
        e->ptr = *event;
        e->rcount = 1;
        *event = e;
        num_master_events++;
        master_events[num_master_events-1] = e;
    }
    VERBOSE_OUT(CL_SUCCESS);
    return CL_SUCCESS;
}
SYMB(clEnqueueReadImage);

CL_API_ENTRY cl_int CL_API_CALL
icd_clEnqueueWriteImage(cl_command_queue     command_queue ,
                        cl_mem               image ,
                        cl_bool              blocking_write ,
                        const size_t *       origin ,
                        const size_t *       region ,
                        size_t               row_pitch ,
                        size_t               slice_pitch ,
                        const void *         ptr ,
                        cl_uint              num_events_in_wait_list ,
                        const cl_event *     event_wait_list ,
                        cl_event *           event) CL_API_SUFFIX__VERSION_1_0
{
    VERBOSE_IN();
    // Test minimum data properties
    if(   (!ptr)
       || (!origin)
       || (!region)){
        VERBOSE_OUT(CL_INVALID_VALUE);
        return CL_INVALID_VALUE;
    }
    // Correct some values if not provided
    if(!row_pitch)
        row_pitch   = region[0];
    if(!slice_pitch)
        slice_pitch = region[1]*row_pitch;
    if(   (!region[0]) || (!region[1]) || (!region[2])
       || (row_pitch   < region[0])
       || (slice_pitch < region[1]*row_pitch)
       || (slice_pitch % row_pitch)){
        VERBOSE_OUT(CL_INVALID_VALUE);
        return CL_INVALID_VALUE;
    }
    if(    ( num_events_in_wait_list && !event_wait_list)
        || (!num_events_in_wait_list &&  event_wait_list)){
        VERBOSE_OUT(CL_INVALID_EVENT_WAIT_LIST);
        return CL_INVALID_EVENT_WAIT_LIST;
    }
    // Correct input events
    cl_uint i;
    cl_event *events_wait = NULL;
    if(num_events_in_wait_list){
        events_wait = (cl_event*)malloc(num_events_in_wait_list*sizeof(cl_event));
        if(!events_wait){
            VERBOSE_OUT(CL_OUT_OF_HOST_MEMORY);
            return CL_OUT_OF_HOST_MEMORY;
        }
        for(i=0;i<num_events_in_wait_list;i++)
            events_wait[i] = event_wait_list[i]->ptr;
    }
    cl_int flag = oclandEnqueueWriteImage(command_queue->ptr,image->ptr,
                                          blocking_write,origin,region,
                                          row_pitch,slice_pitch,image->image.element_size,ptr,
                                          num_events_in_wait_list,events_wait,
                                          event);
    if(flag != CL_SUCCESS){
        VERBOSE_OUT(flag);
        return flag;
    }
    free(events_wait); events_wait=NULL;
    // Correct output event
    if(event){
        cl_event e = (cl_event)malloc(sizeof(struct _cl_event));
        if(!e){
            VERBOSE_OUT(CL_OUT_OF_HOST_MEMORY);
            return CL_OUT_OF_HOST_MEMORY;
        }
        e->dispatch = &master_dispatch;
        e->ptr = *event;
        e->rcount = 1;
        *event = e;
        num_master_events++;
        master_events[num_master_events-1] = e;
    }
    VERBOSE_OUT(CL_SUCCESS);
    return CL_SUCCESS;
}
SYMB(clEnqueueWriteImage);

CL_API_ENTRY cl_int CL_API_CALL
icd_clEnqueueCopyImage(cl_command_queue      command_queue ,
                       cl_mem                src_image ,
                       cl_mem                dst_image ,
                       const size_t *        src_origin ,
                       const size_t *        dst_origin ,
                       const size_t *        region ,
                       cl_uint               num_events_in_wait_list ,
                       const cl_event *      event_wait_list ,
                       cl_event *            event) CL_API_SUFFIX__VERSION_1_0
{
    VERBOSE_IN();
    // Test minimum data properties
    if(   (!src_origin)
       || (!dst_origin)
       || (!region)){
        VERBOSE_OUT(CL_INVALID_VALUE);
        return CL_INVALID_VALUE;
    }
    if(    ( num_events_in_wait_list && !event_wait_list)
        || (!num_events_in_wait_list &&  event_wait_list)){
        VERBOSE_OUT(CL_INVALID_EVENT_WAIT_LIST);
        return CL_INVALID_EVENT_WAIT_LIST;
    }
    // Correct input events
    cl_uint i;
    cl_event *events_wait = NULL;
    if(num_events_in_wait_list){
        events_wait = (cl_event*)malloc(num_events_in_wait_list*sizeof(cl_event));
        if(!events_wait){
            VERBOSE_OUT(CL_OUT_OF_HOST_MEMORY);
            return CL_OUT_OF_HOST_MEMORY;
        }
        for(i=0;i<num_events_in_wait_list;i++)
            events_wait[i] = event_wait_list[i]->ptr;
    }
    cl_int flag = oclandEnqueueCopyImage(command_queue->ptr,
                                         src_image->ptr,dst_image->ptr,
                                         src_origin,dst_origin,region,
                                         num_events_in_wait_list,events_wait,
                                         event);
    if(flag != CL_SUCCESS){
        VERBOSE_OUT(flag);
        return flag;
    }
    free(events_wait); events_wait=NULL;
    // Correct output event
    if(event){
        cl_event e = (cl_event)malloc(sizeof(struct _cl_event));
        if(!e){
            VERBOSE_OUT(CL_OUT_OF_HOST_MEMORY);
            return CL_OUT_OF_HOST_MEMORY;
        }
        e->dispatch = &master_dispatch;
        e->ptr = *event;
        e->rcount = 1;
        *event = e;
        num_master_events++;
        master_events[num_master_events-1] = e;
    }
    VERBOSE_OUT(CL_SUCCESS);
    return CL_SUCCESS;
}
SYMB(clEnqueueCopyImage);

CL_API_ENTRY cl_int CL_API_CALL
icd_clEnqueueCopyImageToBuffer(cl_command_queue  command_queue ,
                               cl_mem            src_image ,
                               cl_mem            dst_buffer ,
                               const size_t *    src_origin ,
                               const size_t *    region ,
                               size_t            dst_offset ,
                               cl_uint           num_events_in_wait_list ,
                               const cl_event *  event_wait_list ,
                               cl_event *        event) CL_API_SUFFIX__VERSION_1_0
{
    VERBOSE_IN();
    if(   (!src_origin)
       || (!region)){
        VERBOSE_OUT(CL_INVALID_VALUE);
        return CL_INVALID_VALUE;
    }
    if(    ( num_events_in_wait_list && !event_wait_list)
        || (!num_events_in_wait_list &&  event_wait_list)){
        VERBOSE_OUT(CL_INVALID_EVENT_WAIT_LIST);
        return CL_INVALID_EVENT_WAIT_LIST;
    }
    // Correct input events
    cl_uint i;
    cl_event *events_wait = NULL;
    if(num_events_in_wait_list){
        events_wait = (cl_event*)malloc(num_events_in_wait_list*sizeof(cl_event));
        if(!events_wait){
            VERBOSE_OUT(CL_OUT_OF_HOST_MEMORY);
            return CL_OUT_OF_HOST_MEMORY;
        }
        for(i=0;i<num_events_in_wait_list;i++)
            events_wait[i] = event_wait_list[i]->ptr;
    }
    cl_int flag = oclandEnqueueCopyImageToBuffer(command_queue->ptr,
                                                 src_image->ptr,dst_buffer->ptr,
                                                 src_origin,region,dst_offset,
                                                 num_events_in_wait_list,events_wait,
                                                 event);
    if(flag != CL_SUCCESS){
        VERBOSE_OUT(flag);
        return flag;
    }
    free(events_wait); events_wait=NULL;
    // Correct output event
    if(event){
        cl_event e = (cl_event)malloc(sizeof(struct _cl_event));
        if(!e){
            VERBOSE_OUT(CL_OUT_OF_HOST_MEMORY);
            return CL_OUT_OF_HOST_MEMORY;
        }
        e->dispatch = &master_dispatch;
        e->ptr = *event;
        e->rcount = 1;
        *event = e;
        num_master_events++;
        master_events[num_master_events-1] = e;
    }
    VERBOSE_OUT(CL_SUCCESS);
    return CL_SUCCESS;
}
SYMB(clEnqueueCopyImageToBuffer);

CL_API_ENTRY cl_int CL_API_CALL
icd_clEnqueueCopyBufferToImage(cl_command_queue  command_queue ,
                               cl_mem            src_buffer ,
                               cl_mem            dst_image ,
                               size_t            src_offset ,
                               const size_t *    dst_origin ,
                               const size_t *    region ,
                               cl_uint           num_events_in_wait_list ,
                               const cl_event *  event_wait_list ,
                               cl_event *        event) CL_API_SUFFIX__VERSION_1_0
{
    VERBOSE_IN();
    if(   (!dst_origin)
       || (!region)){
        VERBOSE_OUT(CL_INVALID_VALUE);
        return CL_INVALID_VALUE;
    }
    if(    ( num_events_in_wait_list && !event_wait_list)
        || (!num_events_in_wait_list &&  event_wait_list)){
        VERBOSE_OUT(CL_INVALID_EVENT_WAIT_LIST);
        return CL_INVALID_EVENT_WAIT_LIST;
    }
    // Correct input events
    cl_uint i;
    cl_event *events_wait = NULL;
    if(num_events_in_wait_list){
        events_wait = (cl_event*)malloc(num_events_in_wait_list*sizeof(cl_event));
        if(!events_wait){
            VERBOSE_OUT(CL_OUT_OF_HOST_MEMORY);
            return CL_OUT_OF_HOST_MEMORY;
        }
        for(i=0;i<num_events_in_wait_list;i++)
            events_wait[i] = event_wait_list[i]->ptr;
    }
    cl_int flag = oclandEnqueueCopyBufferToImage(command_queue->ptr,
                                                 src_buffer->ptr,dst_image->ptr,
                                                 src_offset,dst_origin,region,
                                                 num_events_in_wait_list,events_wait,
                                                 event);
    if(flag != CL_SUCCESS){
        VERBOSE_OUT(flag);
        return flag;
    }
    free(events_wait); events_wait=NULL;
    // Correct output event
    if(event){
        cl_event e = (cl_event)malloc(sizeof(struct _cl_event));
        if(!e){
            VERBOSE_OUT(CL_OUT_OF_HOST_MEMORY);
            return CL_OUT_OF_HOST_MEMORY;
        }
        e->dispatch = &master_dispatch;
        e->ptr = *event;
        e->rcount = 1;
        *event = e;
        num_master_events++;
        master_events[num_master_events-1] = e;
    }
    VERBOSE_OUT(CL_SUCCESS);
    return CL_SUCCESS;
}
SYMB(clEnqueueCopyBufferToImage);

CL_API_ENTRY void * CL_API_CALL
icd_clEnqueueMapBuffer(cl_command_queue  command_queue ,
                       cl_mem            buffer ,
                       cl_bool           blocking_map ,
                       cl_map_flags      map_flags ,
                       size_t            offset ,
                       size_t            cb ,
                       cl_uint           num_events_in_wait_list ,
                       const cl_event *  event_wait_list ,
                       cl_event *        event ,
                       cl_int *          errcode_ret) CL_API_SUFFIX__VERSION_1_0
{
    VERBOSE_IN();
    /** ocland doesn't allow mapping memory objects due to the imposibility
     * to have the host pointer and the memory object in the same space.
     */
    if(errcode_ret) *errcode_ret = CL_MAP_FAILURE;
    VERBOSE_OUT(CL_MAP_FAILURE);
    return NULL;
}
SYMB(clEnqueueMapBuffer);

CL_API_ENTRY void * CL_API_CALL
icd_clEnqueueMapImage(cl_command_queue   command_queue ,
                      cl_mem             image ,
                      cl_bool            blocking_map ,
                      cl_map_flags       map_flags ,
                      const size_t *     origin ,
                      const size_t *     region ,
                      size_t *           image_row_pitch ,
                      size_t *           image_slice_pitch ,
                      cl_uint            num_events_in_wait_list ,
                      const cl_event *   event_wait_list ,
                      cl_event *         event ,
                      cl_int *           errcode_ret) CL_API_SUFFIX__VERSION_1_0
{
    VERBOSE_IN();
    /** ocland doesn't allow mapping memory objects due to the imposibility
     * to have the host pointer and the memory object in the same space.
     */
    if(errcode_ret) *errcode_ret = CL_MAP_FAILURE;
    VERBOSE_OUT(CL_MAP_FAILURE);
    return NULL;
}
SYMB(clEnqueueMapImage);

CL_API_ENTRY cl_int CL_API_CALL
icd_clEnqueueUnmapMemObject(cl_command_queue  command_queue ,
                            cl_mem            memobj ,
                            void *            mapped_ptr ,
                            cl_uint           num_events_in_wait_list ,
                            const cl_event *   event_wait_list ,
                            cl_event *         event) CL_API_SUFFIX__VERSION_1_0
{
    VERBOSE_IN();
    /// In ocland memopry cannot be mapped, so never can be unmapped
    VERBOSE_OUT(CL_INVALID_VALUE);
    return CL_INVALID_VALUE;
}
SYMB(clEnqueueUnmapMemObject);

CL_API_ENTRY cl_int CL_API_CALL
icd_clEnqueueNDRangeKernel(cl_command_queue  command_queue ,
                           cl_kernel         kernel ,
                           cl_uint           work_dim ,
                           const size_t *    global_work_offset ,
                           const size_t *    global_work_size ,
                           const size_t *    local_work_size ,
                           cl_uint           num_events_in_wait_list ,
                           const cl_event *  event_wait_list ,
                           cl_event *        event) CL_API_SUFFIX__VERSION_1_0
{
    VERBOSE_IN();
    if((work_dim < 1) || (work_dim > 3)){
        VERBOSE_OUT(CL_INVALID_WORK_DIMENSION);
        return CL_INVALID_WORK_DIMENSION;
    }
    if(!global_work_size)
        return CL_INVALID_WORK_GROUP_SIZE;
    if(    ( num_events_in_wait_list && !event_wait_list)
        || (!num_events_in_wait_list &&  event_wait_list)){
        VERBOSE_OUT(CL_INVALID_EVENT_WAIT_LIST);
        return CL_INVALID_EVENT_WAIT_LIST;
    }
    // Correct input events
    cl_uint i;
    cl_event *events_wait = NULL;
    if(num_events_in_wait_list){
        events_wait = (cl_event*)malloc(num_events_in_wait_list*sizeof(cl_event));
        if(!events_wait){
            VERBOSE_OUT(CL_OUT_OF_HOST_MEMORY);
            return CL_OUT_OF_HOST_MEMORY;
        }
        for(i=0;i<num_events_in_wait_list;i++)
            events_wait[i] = event_wait_list[i]->ptr;
    }
    cl_int flag = oclandEnqueueNDRangeKernel(command_queue->ptr,kernel->ptr,
                                             work_dim,global_work_offset,
                                             global_work_size,local_work_size,
                                             num_events_in_wait_list,events_wait,
                                             event);
    if(flag != CL_SUCCESS){
        VERBOSE_OUT(flag);
        return flag;
    }
    free(events_wait); events_wait=NULL;
    // Correct output event
    if(event){
        cl_event e = (cl_event)malloc(sizeof(struct _cl_event));
        if(!e){
            VERBOSE_OUT(CL_OUT_OF_HOST_MEMORY);
            return CL_OUT_OF_HOST_MEMORY;
        }
        e->dispatch = &master_dispatch;
        e->ptr = *event;
        e->rcount = 1;
        *event = e;
        num_master_events++;
        master_events[num_master_events-1] = e;
    }
    VERBOSE_OUT(CL_SUCCESS);
    return CL_SUCCESS;
}
SYMB(clEnqueueNDRangeKernel);

CL_API_ENTRY cl_int CL_API_CALL
icd_clEnqueueTask(cl_command_queue   command_queue ,
                  cl_kernel          kernel ,
                  cl_uint            num_events_in_wait_list ,
                  const cl_event *   event_wait_list ,
                  cl_event *         event) CL_API_SUFFIX__VERSION_1_0
{
    VERBOSE_IN();
    /** Following OpenCL specification, this method is equivalent
     * to call clEnqueueNDRangeKernel with: \n
     * work_dim = 1
     * global_work_offset = NULL
     * global_work_size[0] = 1
     * local_work_size[0] = 1
     */
    cl_uint work_dim = 1;
    size_t *global_work_offset=NULL;
    size_t global_work_size=1;
    size_t local_work_size=1;
    cl_int flag = icd_clEnqueueNDRangeKernel(command_queue, kernel,work_dim,
                                             global_work_offset,&global_work_size,
                                             &local_work_size,
                                             num_events_in_wait_list,event_wait_list,
                                             event);
    VERBOSE_OUT(flag);
    return flag;
}
SYMB(clEnqueueTask);

CL_API_ENTRY cl_int CL_API_CALL
icd_clEnqueueNativeKernel(cl_command_queue   command_queue ,
                          void (CL_CALLBACK *user_func)(void *),
                          void *             args ,
                          size_t             cb_args ,
                          cl_uint            num_mem_objects ,
                          const cl_mem *     mem_list ,
                          const void **      args_mem_loc ,
                          cl_uint            num_events_in_wait_list ,
                          const cl_event *   event_wait_list ,
                          cl_event *         event) CL_API_SUFFIX__VERSION_1_0
{
    VERBOSE_IN();
    /// Native kernels cannot be supported by remote applications
    VERBOSE_OUT(CL_INVALID_OPERATION);
    return CL_INVALID_OPERATION;
}
SYMB(clEnqueueNativeKernel);

CL_API_ENTRY cl_int CL_API_CALL
icd_clEnqueueReadBufferRect(cl_command_queue     command_queue ,
                            cl_mem               buffer ,
                            cl_bool              blocking_read ,
                            const size_t *       buffer_origin ,
                            const size_t *       host_origin ,
                            const size_t *       region ,
                            size_t               buffer_row_pitch ,
                            size_t               buffer_slice_pitch ,
                            size_t               host_row_pitch ,
                            size_t               host_slice_pitch ,
                            void *               ptr ,
                            cl_uint              num_events_in_wait_list ,
                            const cl_event *     event_wait_list ,
                            cl_event *           event) CL_API_SUFFIX__VERSION_1_1
{
    VERBOSE_IN();
    // Test minimum data properties
    if(   (!ptr)
       || (!buffer_origin)
       || (!host_origin)
       || (!region)){
        VERBOSE_OUT(CL_INVALID_VALUE);
        return CL_INVALID_VALUE;
    }
    // Correct some values if not provided
    if(!buffer_row_pitch)
        buffer_row_pitch   = region[0];
    if(!host_row_pitch)
        host_row_pitch     = region[0];
    if(!buffer_slice_pitch)
        buffer_slice_pitch = region[1]*buffer_row_pitch;
    if(!host_slice_pitch)
        host_slice_pitch   = region[1]*host_row_pitch;
    if(   (!region[0]) || (!region[1]) || (!region[2])
       || (buffer_row_pitch   < region[0])
       || (host_row_pitch     < region[0])
       || (buffer_slice_pitch < region[1]*buffer_row_pitch)
       || (host_slice_pitch   < region[1]*host_row_pitch)
       || (buffer_slice_pitch % buffer_row_pitch)
       || (host_slice_pitch   % host_row_pitch)){
        VERBOSE_OUT(CL_INVALID_VALUE);
        return CL_INVALID_VALUE;
    }
    if(    ( num_events_in_wait_list && !event_wait_list)
        || (!num_events_in_wait_list &&  event_wait_list)){
        VERBOSE_OUT(CL_INVALID_EVENT_WAIT_LIST);
        return CL_INVALID_EVENT_WAIT_LIST;
    }
    // Correct input events
    cl_uint i;
    cl_event *events_wait = NULL;
    if(num_events_in_wait_list){
        events_wait = (cl_event*)malloc(num_events_in_wait_list*sizeof(cl_event));
        if(!events_wait){
            VERBOSE_OUT(CL_OUT_OF_HOST_MEMORY);
            return CL_OUT_OF_HOST_MEMORY;
        }
        for(i=0;i<num_events_in_wait_list;i++)
            events_wait[i] = event_wait_list[i]->ptr;
    }
    cl_int flag = oclandEnqueueReadBufferRect(command_queue->ptr,buffer->ptr,blocking_read,
                                              buffer_origin,host_origin,region,
                                              buffer_row_pitch,buffer_slice_pitch,
                                              host_row_pitch,host_slice_pitch,ptr,
                                              num_events_in_wait_list,events_wait,
                                              event);
    if(flag != CL_SUCCESS){
        VERBOSE_OUT(flag);
        return flag;
    }
    free(events_wait); events_wait=NULL;
    // Correct output event
    if(event){
        cl_event e = (cl_event)malloc(sizeof(struct _cl_event));
        if(!e){
            VERBOSE_OUT(CL_OUT_OF_HOST_MEMORY);
            return CL_OUT_OF_HOST_MEMORY;
        }
        e->dispatch = &master_dispatch;
        e->ptr = *event;
        e->rcount = 1;
        *event = e;
        num_master_events++;
        master_events[num_master_events-1] = e;
    }
    VERBOSE_OUT(CL_SUCCESS);
    return CL_SUCCESS;
}
SYMB(clEnqueueReadBufferRect);

CL_API_ENTRY cl_int CL_API_CALL
icd_clEnqueueWriteBufferRect(cl_command_queue     command_queue ,
                             cl_mem               buffer ,
                             cl_bool              blocking_write ,
                             const size_t *       buffer_origin ,
                             const size_t *       host_origin ,
                             const size_t *       region ,
                             size_t               buffer_row_pitch ,
                             size_t               buffer_slice_pitch ,
                             size_t               host_row_pitch ,
                             size_t               host_slice_pitch ,
                             const void *         ptr ,
                             cl_uint              num_events_in_wait_list ,
                             const cl_event *     event_wait_list ,
                             cl_event *           event) CL_API_SUFFIX__VERSION_1_1
{
    VERBOSE_IN();
    if(   (!ptr)
       || (!buffer_origin)
       || (!host_origin)
       || (!region)){
        VERBOSE_OUT(CL_INVALID_VALUE);
        return CL_INVALID_VALUE;
    }
    // Correct some values if not provided
    if(!buffer_row_pitch)
        buffer_row_pitch   = region[0];
    if(!host_row_pitch)
        host_row_pitch     = region[0];
    if(!buffer_slice_pitch)
        buffer_slice_pitch = region[1]*buffer_row_pitch;
    if(!host_slice_pitch)
        host_slice_pitch   = region[1]*host_row_pitch;
    if(   (!region[0]) || (!region[1]) || (!region[2])
       || (buffer_row_pitch   < region[0])
       || (host_row_pitch     < region[0])
       || (buffer_slice_pitch < region[1]*buffer_row_pitch)
       || (host_slice_pitch   < region[1]*host_row_pitch)
       || (buffer_slice_pitch % buffer_row_pitch)
       || (host_slice_pitch   % host_row_pitch)){
        VERBOSE_OUT(CL_INVALID_VALUE);
        return CL_INVALID_VALUE;
    }
    if(    ( num_events_in_wait_list && !event_wait_list)
        || (!num_events_in_wait_list &&  event_wait_list)){
        VERBOSE_OUT(CL_INVALID_EVENT_WAIT_LIST);
        return CL_INVALID_EVENT_WAIT_LIST;
    }
    // Correct input events
    cl_uint i;
    cl_event *events_wait = NULL;
    if(num_events_in_wait_list){
        events_wait = (cl_event*)malloc(num_events_in_wait_list*sizeof(cl_event));
        if(!events_wait){
            VERBOSE_OUT(CL_OUT_OF_HOST_MEMORY);
            return CL_OUT_OF_HOST_MEMORY;
        }
        for(i=0;i<num_events_in_wait_list;i++)
            events_wait[i] = event_wait_list[i]->ptr;
    }
    cl_int flag = oclandEnqueueWriteBufferRect(command_queue->ptr,buffer->ptr,blocking_write,
                                               buffer_origin,host_origin,region,
                                               buffer_row_pitch,buffer_slice_pitch,
                                               host_row_pitch,host_slice_pitch,ptr,
                                               num_events_in_wait_list,events_wait,
                                               event);
    if(flag != CL_SUCCESS){
        VERBOSE_OUT(flag);
        return flag;
    }
    free(events_wait); events_wait=NULL;
    // Correct output event
    if(event){
        cl_event e = (cl_event)malloc(sizeof(struct _cl_event));
        if(!e){
            VERBOSE_OUT(CL_OUT_OF_HOST_MEMORY);
            return CL_OUT_OF_HOST_MEMORY;
        }
        e->dispatch = &master_dispatch;
        e->ptr = *event;
        e->rcount = 1;
        *event = e;
        num_master_events++;
        master_events[num_master_events-1] = e;
    }
    VERBOSE_OUT(CL_SUCCESS);
    return CL_SUCCESS;
}
SYMB(clEnqueueWriteBufferRect);

CL_API_ENTRY cl_int CL_API_CALL
icd_clEnqueueCopyBufferRect(cl_command_queue     command_queue ,
                            cl_mem               src_buffer ,
                            cl_mem               dst_buffer ,
                            const size_t *       src_origin ,
                            const size_t *       dst_origin ,
                            const size_t *       region ,
                            size_t               src_row_pitch ,
                            size_t               src_slice_pitch ,
                            size_t               dst_row_pitch ,
                            size_t               dst_slice_pitch ,
                            cl_uint              num_events_in_wait_list ,
                            const cl_event *     event_wait_list ,
                            cl_event *           event) CL_API_SUFFIX__VERSION_1_1
{
    VERBOSE_IN();
    if(   (!src_origin)
       || (!dst_origin)
       || (!region)){
        VERBOSE_OUT(CL_INVALID_VALUE);
        return CL_INVALID_VALUE;
    }
    // Correct some values if not provided
    if(!src_row_pitch)
        src_row_pitch   = region[0];
    if(!dst_row_pitch)
        dst_row_pitch   = region[0];
    if(!src_slice_pitch)
        src_slice_pitch = region[1]*src_row_pitch;
    if(!dst_slice_pitch)
        dst_slice_pitch = region[1]*dst_row_pitch;
    if(   (!region[0]) || (!region[1]) || (!region[2])
       || (src_row_pitch   < region[0])
       || (dst_row_pitch   < region[0])
       || (src_slice_pitch < region[1]*src_row_pitch)
       || (dst_slice_pitch < region[1]*dst_row_pitch)
       || (src_slice_pitch % src_row_pitch)
       || (dst_slice_pitch % dst_row_pitch)){
        VERBOSE_OUT(CL_INVALID_VALUE);
        return CL_INVALID_VALUE;
    }
    if(    ( num_events_in_wait_list && !event_wait_list)
        || (!num_events_in_wait_list &&  event_wait_list)){
        VERBOSE_OUT(CL_INVALID_EVENT_WAIT_LIST);
        return CL_INVALID_EVENT_WAIT_LIST;
    }
    // Correct input events
    cl_uint i;
    cl_event *events_wait = NULL;
    if(num_events_in_wait_list){
        events_wait = (cl_event*)malloc(num_events_in_wait_list*sizeof(cl_event));
        if(!events_wait)
            return CL_OUT_OF_HOST_MEMORY;
        for(i=0;i<num_events_in_wait_list;i++)
            events_wait[i] = event_wait_list[i]->ptr;
    }
    cl_int flag = oclandEnqueueCopyBufferRect(command_queue->ptr,src_buffer->ptr,dst_buffer->ptr,
                                              src_origin,dst_origin,region,
                                              src_row_pitch,src_slice_pitch,
                                              dst_row_pitch,dst_slice_pitch,
                                              num_events_in_wait_list,events_wait,
                                              event);
    if(flag != CL_SUCCESS){
        VERBOSE_OUT(flag);
        return flag;
    }
    free(events_wait); events_wait=NULL;
    // Correct output event
    if(event){
        cl_event e = (cl_event)malloc(sizeof(struct _cl_event));
        if(!e){
            VERBOSE_OUT(CL_OUT_OF_HOST_MEMORY);
            return CL_OUT_OF_HOST_MEMORY;
        }
        e->dispatch = &master_dispatch;
        e->ptr = *event;
        e->rcount = 1;
        *event = e;
        num_master_events++;
        master_events[num_master_events-1] = e;
    }
    VERBOSE_OUT(CL_SUCCESS);
    return CL_SUCCESS;
}
SYMB(clEnqueueCopyBufferRect);

CL_API_ENTRY cl_int CL_API_CALL
icd_clEnqueueFillBuffer(cl_command_queue    command_queue ,
                        cl_mem              buffer ,
                        const void *        pattern ,
                        size_t              pattern_size ,
                        size_t              offset ,
                        size_t              cb ,
                        cl_uint             num_events_in_wait_list ,
                        const cl_event *    event_wait_list ,
                        cl_event *          event) CL_API_SUFFIX__VERSION_1_2
{
    VERBOSE_IN();
    if((!pattern) || (!pattern_size)){
        VERBOSE_OUT(CL_INVALID_VALUE);
        return CL_INVALID_VALUE;
    }
    if((offset % pattern_size) || (cb % pattern_size))
        return CL_INVALID_VALUE;
    if(    ( num_events_in_wait_list && !event_wait_list)
        || (!num_events_in_wait_list &&  event_wait_list)){
        VERBOSE_OUT(CL_INVALID_EVENT_WAIT_LIST);
        return CL_INVALID_EVENT_WAIT_LIST;
    }
    // Correct input events
    cl_uint i;
    cl_event *events_wait = NULL;
    if(num_events_in_wait_list){
        events_wait = (cl_event*)malloc(num_events_in_wait_list*sizeof(cl_event));
        if(!events_wait){
            VERBOSE_OUT(CL_OUT_OF_HOST_MEMORY);
            return CL_OUT_OF_HOST_MEMORY;
        }
        for(i=0;i<num_events_in_wait_list;i++)
            events_wait[i] = event_wait_list[i]->ptr;
    }
    cl_int flag = oclandEnqueueFillBuffer(command_queue->ptr,buffer->ptr,
                                          pattern,pattern_size,offset,cb,
                                          num_events_in_wait_list,events_wait,
                                          event);
    if(flag != CL_SUCCESS){
        VERBOSE_OUT(flag);
        return flag;
    }
    free(events_wait); events_wait=NULL;
    // Correct output event
    if(event){
        cl_event e = (cl_event)malloc(sizeof(struct _cl_event));
        if(!e){
            VERBOSE_OUT(CL_OUT_OF_HOST_MEMORY);
            return CL_OUT_OF_HOST_MEMORY;
        }
        e->dispatch = &master_dispatch;
        e->ptr = *event;
        e->rcount = 1;
        *event = e;
        num_master_events++;
        master_events[num_master_events-1] = e;
    }
    VERBOSE_OUT(CL_SUCCESS);
    return CL_SUCCESS;
}
SYMB(clEnqueueFillBuffer);

CL_API_ENTRY cl_int CL_API_CALL
icd_clEnqueueFillImage(cl_command_queue    command_queue ,
                       cl_mem              image ,
                       const void *        fill_color ,
                       const size_t *      origin ,
                       const size_t *      region ,
                       cl_uint             num_events_in_wait_list ,
                       const cl_event *    event_wait_list ,
                       cl_event *          event) CL_API_SUFFIX__VERSION_1_2
{
    VERBOSE_IN();
    // To use this method before we may get the size of fill_color
    size_t fill_color_size = 4*sizeof(float);
    cl_image_format image_format;
    cl_int flag = clGetImageInfo(image, CL_IMAGE_FORMAT, sizeof(cl_image_format), &image_format, NULL);
    if(flag != CL_SUCCESS){
        VERBOSE_OUT(CL_INVALID_MEM_OBJECT);
        return CL_INVALID_MEM_OBJECT;
    }
    if(    image_format.image_channel_data_type == CL_SIGNED_INT8
        || image_format.image_channel_data_type == CL_SIGNED_INT16
        || image_format.image_channel_data_type == CL_SIGNED_INT32 ){
            fill_color_size = 4*sizeof(int);
    }
    if(    image_format.image_channel_data_type == CL_UNSIGNED_INT8
        || image_format.image_channel_data_type == CL_UNSIGNED_INT16
        || image_format.image_channel_data_type == CL_UNSIGNED_INT32 ){
            fill_color_size = 4*sizeof(unsigned int);
    }
    // Test minimum data properties
    if(   (!fill_color)
       || (!origin)
       || (!region)){
        VERBOSE_OUT(CL_INVALID_VALUE);
        return CL_INVALID_VALUE;
    }
    if(   (!region[0]) || (!region[1]) || (!region[2]) ){
        VERBOSE_OUT(CL_INVALID_VALUE);
        return CL_INVALID_VALUE;
    }
    if(    ( num_events_in_wait_list && !event_wait_list)
        || (!num_events_in_wait_list &&  event_wait_list)){
        VERBOSE_OUT(CL_INVALID_EVENT_WAIT_LIST);
        return CL_INVALID_EVENT_WAIT_LIST;
    }
    // Correct input events
    cl_uint i;
    cl_event *events_wait = NULL;
    if(num_events_in_wait_list){
        events_wait = (cl_event*)malloc(num_events_in_wait_list*sizeof(cl_event));
        if(!events_wait){
            VERBOSE_OUT(CL_OUT_OF_HOST_MEMORY);
            return CL_OUT_OF_HOST_MEMORY;
        }
        for(i=0;i<num_events_in_wait_list;i++)
            events_wait[i] = event_wait_list[i]->ptr;
    }
    flag = oclandEnqueueFillImage(command_queue->ptr,image->ptr,
                                  fill_color_size,fill_color,origin,region,
                                  num_events_in_wait_list,events_wait,
                                  event);
    if(flag != CL_SUCCESS){
        VERBOSE_OUT(flag);
        return flag;
    }
    free(events_wait); events_wait=NULL;
    // Correct output event
    if(event){
        cl_event e = (cl_event)malloc(sizeof(struct _cl_event));
        if(!e){
            VERBOSE_OUT(CL_OUT_OF_HOST_MEMORY);
            return CL_OUT_OF_HOST_MEMORY;
        }
        e->dispatch = &master_dispatch;
        e->ptr = *event;
        e->rcount = 1;
        *event = e;
        num_master_events++;
        master_events[num_master_events-1] = e;
    }
    VERBOSE_OUT(CL_SUCCESS);
    return CL_SUCCESS;
}
SYMB(clEnqueueFillImage);

CL_API_ENTRY cl_int CL_API_CALL
icd_clEnqueueMigrateMemObjects(cl_command_queue        command_queue ,
                               cl_uint                 num_mem_objects ,
                               const cl_mem *          mem_objects ,
                               cl_mem_migration_flags  flags ,
                               cl_uint                 num_events_in_wait_list ,
                               const cl_event *        event_wait_list ,
                               cl_event *              event) CL_API_SUFFIX__VERSION_1_2
{
    VERBOSE_IN();
    // Test for valid flags
    if(    (!flags)
        || (    (flags != CL_MIGRATE_MEM_OBJECT_HOST)
             && (flags != CL_MIGRATE_MEM_OBJECT_CONTENT_UNDEFINED))){
        VERBOSE_OUT(CL_INVALID_VALUE);
        return CL_INVALID_VALUE;
    }
    // Test for other invalid values
    if(    (!num_mem_objects)
        || (!mem_objects)){
        VERBOSE_OUT(CL_INVALID_VALUE);
        return CL_INVALID_VALUE;
    }
    if(    ( num_events_in_wait_list && !event_wait_list)
        || (!num_events_in_wait_list &&  event_wait_list)){
        VERBOSE_OUT(CL_INVALID_EVENT_WAIT_LIST);
        return CL_INVALID_EVENT_WAIT_LIST;
    }
    // Correct memory objects
    cl_uint i;
    cl_mem *mems = NULL;
    if(num_mem_objects){
        mems = (cl_mem*)malloc(num_mem_objects*sizeof(cl_mem));
        if(!mems){
            VERBOSE_OUT(CL_OUT_OF_HOST_MEMORY);
            return CL_OUT_OF_HOST_MEMORY;
        }
        for(i=0;i<num_mem_objects;i++)
            mems[i] = mem_objects[i]->ptr;
    }
    // Correct input events
    cl_event *events_wait = NULL;
    if(num_events_in_wait_list){
        events_wait = (cl_event*)malloc(num_events_in_wait_list*sizeof(cl_event));
        if(!events_wait){
            VERBOSE_OUT(CL_OUT_OF_HOST_MEMORY);
            return CL_OUT_OF_HOST_MEMORY;
        }
        for(i=0;i<num_events_in_wait_list;i++)
            events_wait[i] = event_wait_list[i]->ptr;
    }
    cl_int flag = oclandEnqueueMigrateMemObjects(command_queue->ptr,
                                                 num_mem_objects,mems,flags,
                                                 num_events_in_wait_list,events_wait,
                                                 event);
    if(flag != CL_SUCCESS){
        VERBOSE_OUT(flag);
        return flag;
    }
    free(events_wait); events_wait=NULL;
    // Correct output event
    if(event){
        cl_event e = (cl_event)malloc(sizeof(struct _cl_event));
        if(!e){
            VERBOSE_OUT(CL_OUT_OF_HOST_MEMORY);
            return CL_OUT_OF_HOST_MEMORY;
        }
        e->dispatch = &master_dispatch;
        e->ptr = *event;
        e->rcount = 1;
        *event = e;
        num_master_events++;
        master_events[num_master_events-1] = e;
    }
    VERBOSE_OUT(CL_SUCCESS);
    return CL_SUCCESS;
}
SYMB(clEnqueueMigrateMemObjects);

CL_API_ENTRY cl_int CL_API_CALL
icd_clEnqueueMarkerWithWaitList(cl_command_queue  command_queue ,
                                cl_uint            num_events_in_wait_list ,
                                const cl_event *   event_wait_list ,
                                cl_event *         event) CL_API_SUFFIX__VERSION_1_2
{
    VERBOSE_IN();
    if(    ( num_events_in_wait_list && !event_wait_list)
        || (!num_events_in_wait_list &&  event_wait_list)){
        VERBOSE_OUT(CL_INVALID_EVENT_WAIT_LIST);
        return CL_INVALID_EVENT_WAIT_LIST;
    }
    // Correct input events
    cl_uint i;
    cl_event *events_wait = NULL;
    if(num_events_in_wait_list){
        events_wait = (cl_event*)malloc(num_events_in_wait_list*sizeof(cl_event));
        if(!events_wait){
            VERBOSE_OUT(CL_OUT_OF_HOST_MEMORY);
            return CL_OUT_OF_HOST_MEMORY;
        }
        for(i=0;i<num_events_in_wait_list;i++)
            events_wait[i] = event_wait_list[i]->ptr;
    }
    cl_int flag = oclandEnqueueMarkerWithWaitList(command_queue->ptr,
                                                  num_events_in_wait_list,events_wait,
                                                  event);
    if(flag != CL_SUCCESS){
        VERBOSE_OUT(flag);
        return flag;
    }
    free(events_wait); events_wait=NULL;
    // Correct output event
    if(event){
        cl_event e = (cl_event)malloc(sizeof(struct _cl_event));
        if(!e){
            VERBOSE_OUT(CL_OUT_OF_HOST_MEMORY);
            return CL_OUT_OF_HOST_MEMORY;
        }
        e->dispatch = &master_dispatch;
        e->ptr = *event;
        e->rcount = 1;
        *event = e;
        num_master_events++;
        master_events[num_master_events-1] = e;
    }
    VERBOSE_OUT(CL_SUCCESS);
    return CL_SUCCESS;
}
SYMB(clEnqueueMarkerWithWaitList);

CL_API_ENTRY cl_int CL_API_CALL
icd_clEnqueueBarrierWithWaitList(cl_command_queue  command_queue ,
                                 cl_uint            num_events_in_wait_list ,
                                 const cl_event *   event_wait_list ,
                                 cl_event *         event) CL_API_SUFFIX__VERSION_1_2
{
    VERBOSE_IN();
    if(    ( num_events_in_wait_list && !event_wait_list)
        || (!num_events_in_wait_list &&  event_wait_list)){
        VERBOSE_OUT(CL_INVALID_EVENT_WAIT_LIST);
        return CL_INVALID_EVENT_WAIT_LIST;
    }
    // Correct input events
    cl_uint i;
    cl_event *events_wait = NULL;
    if(num_events_in_wait_list){
        events_wait = (cl_event*)malloc(num_events_in_wait_list*sizeof(cl_event));
        if(!events_wait){
            VERBOSE_OUT(CL_OUT_OF_HOST_MEMORY);
            return CL_OUT_OF_HOST_MEMORY;
        }
        for(i=0;i<num_events_in_wait_list;i++)
            events_wait[i] = event_wait_list[i]->ptr;
    }
    cl_int flag = oclandEnqueueBarrierWithWaitList(command_queue->ptr,
                                                   num_events_in_wait_list,events_wait,
                                                   event);
    if(flag != CL_SUCCESS){
        VERBOSE_OUT(flag);
        return flag;
    }
    free(events_wait); events_wait=NULL;
    // Correct output event
    if(event){
        cl_event e = (cl_event)malloc(sizeof(struct _cl_event));
        if(!e){
            VERBOSE_OUT(CL_OUT_OF_HOST_MEMORY);
            return CL_OUT_OF_HOST_MEMORY;
        }
        e->dispatch = &master_dispatch;
        e->ptr = *event;
        e->rcount = 1;
        *event = e;
        num_master_events++;
        master_events[num_master_events-1] = e;
    }
    VERBOSE_OUT(CL_SUCCESS);
    return CL_SUCCESS;
}
SYMB(clEnqueueBarrierWithWaitList);

CL_API_ENTRY CL_EXT_PREFIX__VERSION_1_1_DEPRECATED cl_int CL_API_CALL
icd_clEnqueueMarker(cl_command_queue    command_queue ,
                    cl_event *          event) CL_EXT_SUFFIX__VERSION_1_1_DEPRECATED
{
    VERBOSE_IN();
    cl_int flag = icd_clEnqueueMarkerWithWaitList(command_queue, 0, NULL, event);
    VERBOSE_OUT(flag);
    return flag;
}
SYMB(clEnqueueMarker);

CL_API_ENTRY CL_EXT_PREFIX__VERSION_1_1_DEPRECATED cl_int CL_API_CALL
icd_clEnqueueWaitForEvents(cl_command_queue command_queue ,
                           cl_uint          num_events ,
                           const cl_event * event_list ) CL_EXT_SUFFIX__VERSION_1_1_DEPRECATED
{
    VERBOSE_IN();
    cl_int flag = icd_clWaitForEvents(num_events, event_list);
    VERBOSE_OUT(flag);
    return flag;
}
SYMB(clEnqueueWaitForEvents);

CL_API_ENTRY CL_EXT_PREFIX__VERSION_1_1_DEPRECATED cl_int CL_API_CALL
icd_clEnqueueBarrier(cl_command_queue command_queue ) CL_EXT_SUFFIX__VERSION_1_1_DEPRECATED
{
    VERBOSE_IN();
    cl_int flag = icd_clEnqueueBarrierWithWaitList(command_queue, 0, NULL, NULL);
    VERBOSE_OUT(flag);
    return flag;
}
SYMB(clEnqueueBarrier);

// --------------------------------------------------------------
// OpenGL
// --------------------------------------------------------------

CL_API_ENTRY cl_mem CL_API_CALL
icd_clCreateFromGLBuffer(cl_context     context ,
                         cl_mem_flags   flags ,
                         cl_GLuint      bufobj ,
                         int *          errcode_ret ) CL_API_SUFFIX__VERSION_1_0
{
    VERBOSE_IN();
    *errcode_ret = CL_INVALID_CONTEXT;
    VERBOSE_OUT(CL_INVALID_CONTEXT);
    return NULL;
}
SYMB(clCreateFromGLBuffer);

CL_API_ENTRY CL_EXT_PREFIX__VERSION_1_1_DEPRECATED cl_mem CL_API_CALL
icd_clCreateFromGLTexture2D(cl_context      context ,
                            cl_mem_flags    flags ,
                            cl_GLenum       target ,
                            cl_GLint        miplevel ,
                            cl_GLuint       texture ,
                            cl_int *        errcode_ret ) CL_EXT_SUFFIX__VERSION_1_1_DEPRECATED
{
    VERBOSE_IN();
    *errcode_ret = CL_INVALID_CONTEXT;
    VERBOSE_OUT(CL_INVALID_CONTEXT);
    return NULL;
}
SYMB(clCreateFromGLTexture2D);

CL_API_ENTRY CL_EXT_PREFIX__VERSION_1_1_DEPRECATED cl_mem CL_API_CALL
icd_clCreateFromGLTexture3D(cl_context      context ,
                            cl_mem_flags    flags ,
                            cl_GLenum       target ,
                            cl_GLint        miplevel ,
                            cl_GLuint       texture ,
                            cl_int *        errcode_ret ) CL_EXT_SUFFIX__VERSION_1_1_DEPRECATED
{
    VERBOSE_IN();
    *errcode_ret = CL_INVALID_CONTEXT;
    VERBOSE_OUT(CL_INVALID_CONTEXT);
    return NULL;
}
SYMB(clCreateFromGLTexture3D);

CL_API_ENTRY cl_mem CL_API_CALL
icd_clCreateFromGLRenderbuffer(cl_context   context ,
                               cl_mem_flags flags ,
                               cl_GLuint    renderbuffer ,
                               cl_int *     errcode_ret ) CL_API_SUFFIX__VERSION_1_0
{
    VERBOSE_IN();
    *errcode_ret = CL_INVALID_CONTEXT;
    VERBOSE_OUT(CL_INVALID_CONTEXT);
    return NULL;
}
SYMB(clCreateFromGLRenderbuffer);

CL_API_ENTRY cl_int CL_API_CALL
icd_clGetGLObjectInfo(cl_mem                memobj ,
                      cl_gl_object_type *   gl_object_type ,
                      cl_GLuint *           gl_object_name ) CL_API_SUFFIX__VERSION_1_0
{
    VERBOSE_IN();
    // TODO: Check before if there is such a memory object, to eventually return
    // CL_INVALID_MEM_OBJECT
    VERBOSE_OUT(CL_INVALID_GL_OBJECT);
    return CL_INVALID_GL_OBJECT;
}
SYMB(clGetGLObjectInfo);

CL_API_ENTRY cl_int CL_API_CALL
icd_clGetGLTextureInfo(cl_mem               memobj ,
                       cl_gl_texture_info   param_name ,
                       size_t               param_value_size ,
                       void *               param_value ,
                       size_t *             param_value_size_ret ) CL_API_SUFFIX__VERSION_1_0
{
    VERBOSE_IN();
    // TODO: Check before if there is such a memory object, to eventually return
    // CL_INVALID_MEM_OBJECT
    VERBOSE_OUT(CL_INVALID_GL_OBJECT);
    return CL_INVALID_GL_OBJECT;
}
SYMB(clGetGLTextureInfo);

CL_API_ENTRY cl_int CL_API_CALL
icd_clEnqueueAcquireGLObjects(cl_command_queue      command_queue ,
                              cl_uint               num_objects ,
                              const cl_mem *        mem_objects ,
                              cl_uint               num_events_in_wait_list ,
                              const cl_event *      event_wait_list ,
                              cl_event *            event ) CL_API_SUFFIX__VERSION_1_0
{
    VERBOSE_IN();
    VERBOSE_OUT(CL_INVALID_CONTEXT);
    return CL_INVALID_CONTEXT;
}
SYMB(clEnqueueAcquireGLObjects);

CL_API_ENTRY cl_int CL_API_CALL
icd_clEnqueueReleaseGLObjects(cl_command_queue      command_queue ,
                              cl_uint               num_objects ,
                              const cl_mem *        mem_objects ,
                              cl_uint               num_events_in_wait_list ,
                              const cl_event *      event_wait_list ,
                              cl_event *            event ) CL_API_SUFFIX__VERSION_1_0
{
    VERBOSE_IN();
    VERBOSE_OUT(CL_INVALID_CONTEXT);
    return CL_INVALID_CONTEXT;
}
SYMB(clEnqueueReleaseGLObjects);

CL_API_ENTRY cl_int CL_API_CALL
icd_clGetGLContextInfoKHR(const cl_context_properties * properties ,
                          cl_gl_context_info            param_name ,
                          size_t                        param_value_size ,
                          void *                        param_value ,
                          size_t *                      param_value_size_ret ) CL_API_SUFFIX__VERSION_1_0
{
    VERBOSE_IN();
    VERBOSE_OUT(CL_INVALID_GL_SHAREGROUP_REFERENCE_KHR);
    return CL_INVALID_GL_SHAREGROUP_REFERENCE_KHR;
}
SYMB(clGetGLContextInfoKHR);

CL_API_ENTRY cl_mem CL_API_CALL
icd_clCreateFromGLTexture(cl_context      context ,
                          cl_mem_flags    flags ,
                          cl_GLenum       target ,
                          cl_GLint        miplevel ,
                          cl_GLuint       texture ,
                          cl_int *        errcode_ret ) CL_API_SUFFIX__VERSION_1_2
{
    VERBOSE_IN();
    *errcode_ret = CL_INVALID_CONTEXT;
    VERBOSE_OUT(CL_INVALID_CONTEXT);
    return NULL;
}
SYMB(clCreateFromGLTexture);

CL_API_ENTRY cl_event CL_API_CALL
icd_clCreateEventFromGLsyncKHR(cl_context  context,
                               cl_GLsync   cl_GLsync,
                               cl_int *    errcode_ret) CL_EXT_SUFFIX__VERSION_1_1
{
    VERBOSE_IN();
    *errcode_ret = CL_INVALID_CONTEXT;
    VERBOSE_OUT(CL_INVALID_CONTEXT);
    return NULL;
}
SYMB(clCreateEventFromGLsyncKHR);

CL_API_ENTRY cl_mem CL_API_CALL
icd_clCreateFromEGLImageKHR(cl_context                  context,
                            CLeglDisplayKHR             egldisplay,
                            CLeglImageKHR               eglimage,
                            cl_mem_flags                flags,
                            const cl_egl_image_properties_khr * properties,
                            cl_int *                    errcode_ret) CL_API_SUFFIX__VERSION_1_0
{
    VERBOSE_IN();
    *errcode_ret = CL_INVALID_CONTEXT;
    VERBOSE_OUT(CL_INVALID_CONTEXT);
    return NULL;
}
SYMB(clCreateFromEGLImageKHR);

CL_API_ENTRY cl_int CL_API_CALL
icd_clEnqueueAcquireEGLObjectsKHR(cl_command_queue command_queue,
                                  cl_uint          num_objects,
                                  const cl_mem *   mem_objects,
                                  cl_uint          num_events_in_wait_list,
                                  const cl_event * event_wait_list,
                                  cl_event *       event) CL_API_SUFFIX__VERSION_1_0
{
    VERBOSE_IN();
    VERBOSE_OUT(CL_INVALID_CONTEXT);
    return CL_INVALID_CONTEXT;
}
SYMB(clEnqueueAcquireEGLObjectsKHR);

CL_API_ENTRY cl_int CL_API_CALL
icd_clEnqueueReleaseEGLObjectsKHR(cl_command_queue command_queue,
                              cl_uint          num_objects,
                              const cl_mem *   mem_objects,
                              cl_uint          num_events_in_wait_list,
                              const cl_event * event_wait_list,
                              cl_event *       event) CL_API_SUFFIX__VERSION_1_0
{
    VERBOSE_IN();
    VERBOSE_OUT(CL_INVALID_CONTEXT);
    return CL_INVALID_CONTEXT;
}
SYMB(clEnqueueReleaseEGLObjectsKHR);

CL_API_ENTRY cl_event CL_API_CALL
icd_clCreateEventFromEGLSyncKHR(cl_context      context,
                                CLeglSyncKHR    sync,
                                CLeglDisplayKHR display,
                                cl_int *        errcode_ret) CL_API_SUFFIX__VERSION_1_0
{
    VERBOSE_IN();
    *errcode_ret = CL_INVALID_CONTEXT;
    VERBOSE_OUT(CL_INVALID_CONTEXT);
    return NULL;
}
SYMB(clCreateEventFromEGLSyncKHR);

// --------------------------------------------------------------
// Extensions, only used at the start of icd_loader
// --------------------------------------------------------------

CL_API_ENTRY void * CL_API_CALL
icd_clGetExtensionFunctionAddress(const char * func_name) CL_API_SUFFIX__VERSION_1_0
{
    VERBOSE_IN();
    VERBOSE_OUT(CL_SUCCESS);
    if( func_name != NULL && strcmp("clIcdGetPlatformIDsKHR", func_name) == 0 )
        return (void *)__GetPlatformIDs;
    return NULL;
}
SYMB(clGetExtensionFunctionAddress);

CL_API_ENTRY void * CL_API_CALL
icd_clGetExtensionFunctionAddressForPlatform(cl_platform_id platform,
                                             const char *   func_name) CL_API_SUFFIX__VERSION_1_2
{
    VERBOSE_IN();
    VERBOSE_OUT(CL_SUCCESS);
    if(!is_platform(platform))
        return NULL;
    return icd_clGetExtensionFunctionAddress(func_name);
}
SYMB(clGetExtensionFunctionAddressForPlatform);

#pragma GCC visibility pop

void dummyFunc(void){}

struct _cl_icd_dispatch master_dispatch = {
  (void(*)(void))& icd_clGetPlatformIDs,
  (void(*)(void))& icd_clGetPlatformInfo,
  (void(*)(void))& icd_clGetDeviceIDs,
  (void(*)(void))& icd_clGetDeviceInfo,
  (void(*)(void))& icd_clCreateContext,
  (void(*)(void))& icd_clCreateContextFromType,
  (void(*)(void))& icd_clRetainContext,
  (void(*)(void))& icd_clReleaseContext,
  (void(*)(void))& icd_clGetContextInfo,
  (void(*)(void))& icd_clCreateCommandQueue,
  (void(*)(void))& icd_clRetainCommandQueue,
  (void(*)(void))& icd_clReleaseCommandQueue,
  (void(*)(void))& icd_clGetCommandQueueInfo,
  (void(*)(void))& icd_clSetCommandQueueProperty,
  (void(*)(void))& icd_clCreateBuffer,
  (void(*)(void))& icd_clCreateImage2D,
  (void(*)(void))& icd_clCreateImage3D,
  (void(*)(void))& icd_clRetainMemObject,
  (void(*)(void))& icd_clReleaseMemObject,
  (void(*)(void))& icd_clGetSupportedImageFormats,
  (void(*)(void))& icd_clGetMemObjectInfo,
  (void(*)(void))& icd_clGetImageInfo,
  (void(*)(void))& icd_clCreateSampler,
  (void(*)(void))& icd_clRetainSampler,
  (void(*)(void))& icd_clReleaseSampler,
  (void(*)(void))& icd_clGetSamplerInfo,
  (void(*)(void))& icd_clCreateProgramWithSource,
  (void(*)(void))& icd_clCreateProgramWithBinary,
  (void(*)(void))& icd_clRetainProgram,
  (void(*)(void))& icd_clReleaseProgram,
  (void(*)(void))& icd_clBuildProgram,
  (void(*)(void))& icd_clUnloadCompiler,
  (void(*)(void))& icd_clGetProgramInfo,
  (void(*)(void))& icd_clGetProgramBuildInfo,
  (void(*)(void))& icd_clCreateKernel,
  (void(*)(void))& icd_clCreateKernelsInProgram,
  (void(*)(void))& icd_clRetainKernel,
  (void(*)(void))& icd_clReleaseKernel,
  (void(*)(void))& icd_clSetKernelArg,
  (void(*)(void))& icd_clGetKernelInfo,
  (void(*)(void))& icd_clGetKernelWorkGroupInfo,
  (void(*)(void))& icd_clWaitForEvents,
  (void(*)(void))& icd_clGetEventInfo,
  (void(*)(void))& icd_clRetainEvent,
  (void(*)(void))& icd_clReleaseEvent,
  (void(*)(void))& icd_clGetEventProfilingInfo,
  (void(*)(void))& icd_clFlush,
  (void(*)(void))& icd_clFinish,
  (void(*)(void))& icd_clEnqueueReadBuffer,
  (void(*)(void))& icd_clEnqueueWriteBuffer,
  (void(*)(void))& icd_clEnqueueCopyBuffer,
  (void(*)(void))& icd_clEnqueueReadImage,
  (void(*)(void))& icd_clEnqueueWriteImage,
  (void(*)(void))& icd_clEnqueueCopyImage,
  (void(*)(void))& icd_clEnqueueCopyImageToBuffer,
  (void(*)(void))& icd_clEnqueueCopyBufferToImage,
  (void(*)(void))& icd_clEnqueueMapBuffer,
  (void(*)(void))& icd_clEnqueueMapImage,
  (void(*)(void))& icd_clEnqueueUnmapMemObject,
  (void(*)(void))& icd_clEnqueueNDRangeKernel,
  (void(*)(void))& icd_clEnqueueTask,
  (void(*)(void))& icd_clEnqueueNativeKernel,
  (void(*)(void))& icd_clEnqueueMarker,
  (void(*)(void))& icd_clEnqueueWaitForEvents,
  (void(*)(void))& icd_clEnqueueBarrier,
  (void(*)(void))& dummyFunc,    // 65 : clGetExtensionFunctionAddress (don't set it)
  (void(*)(void))& icd_clCreateFromGLBuffer,
  (void(*)(void))& icd_clCreateFromGLTexture2D,
  (void(*)(void))& icd_clCreateFromGLTexture3D,
  (void(*)(void))& icd_clCreateFromGLRenderbuffer,
  (void(*)(void))& icd_clGetGLObjectInfo,
  (void(*)(void))& icd_clGetGLTextureInfo,
  (void(*)(void))& icd_clEnqueueAcquireGLObjects,
  (void(*)(void))& icd_clEnqueueReleaseGLObjects,
  (void(*)(void))& icd_clGetGLContextInfoKHR,
  (void(*)(void))& dummyFunc,    // 75: clGetDeviceIDsFromD3D10KHR,
  (void(*)(void))& dummyFunc,    // 76: clCreateFromD3D10BufferKHR,
  (void(*)(void))& dummyFunc,    // 77: clCreateFromD3D10Texture2DKHR,
  (void(*)(void))& dummyFunc,    // 78: clCreateFromD3D10Texture3DKHR,
  (void(*)(void))& dummyFunc,    // 79: clEnqueueAcquireD3D10ObjectsKHR,
  (void(*)(void))& dummyFunc,    // 80: clEnqueueReleaseD3D10ObjectsKHR,
  (void(*)(void))& icd_clSetEventCallback,
  (void(*)(void))& icd_clCreateSubBuffer,
  (void(*)(void))& icd_clSetMemObjectDestructorCallback,
  (void(*)(void))& icd_clCreateUserEvent,
  (void(*)(void))& icd_clSetUserEventStatus,
  (void(*)(void))& icd_clEnqueueReadBufferRect,
  (void(*)(void))& icd_clEnqueueWriteBufferRect,
  (void(*)(void))& icd_clEnqueueCopyBufferRect,
  (void(*)(void))& dummyFunc,    // 89: clCreateSubDevicesEXT
  (void(*)(void))& dummyFunc,    // 90: clRetainDeviceEXT
  (void(*)(void))& dummyFunc,    // 91: clReleaseDeviceEXT
  (void(*)(void))& icd_clCreateEventFromGLsyncKHR,
  (void(*)(void))& icd_clCreateSubDevices,
  (void(*)(void))& icd_clRetainDevice,
  (void(*)(void))& icd_clReleaseDevice,
  (void(*)(void))& icd_clCreateImage,
  (void(*)(void))& icd_clCreateProgramWithBuiltInKernels,
  (void(*)(void))& icd_clCompileProgram,
  (void(*)(void))& icd_clLinkProgram,
  (void(*)(void))& icd_clUnloadPlatformCompiler,
  (void(*)(void))& icd_clGetKernelArgInfo,
  (void(*)(void))& icd_clEnqueueFillBuffer,
  (void(*)(void))& icd_clEnqueueFillImage,
  (void(*)(void))& icd_clEnqueueMigrateMemObjects,
  (void(*)(void))& icd_clEnqueueMarkerWithWaitList,
  (void(*)(void))& icd_clEnqueueBarrierWithWaitList,
  (void(*)(void))& icd_clGetExtensionFunctionAddressForPlatform,
  (void(*)(void))& icd_clCreateFromGLTexture,
  (void(*)(void))& dummyFunc,    // 109: clGetDeviceIDsFromD3D11KHR,
  (void(*)(void))& dummyFunc,    // 110: clCreateFromD3D11BufferKHR,
  (void(*)(void))& dummyFunc,    // 111: clCreateFromD3D11Texture2DKHR,
  (void(*)(void))& dummyFunc,    // 112: clCreateFromD3D11Texture3DKHR,
  (void(*)(void))& dummyFunc,    // 113: clCreateFromDX9MediaSurfaceKHR,
  (void(*)(void))& dummyFunc,    // 114: clEnqueueAcquireD3D11ObjectsKHR,
  (void(*)(void))& dummyFunc,    // 115: clEnqueueReleaseD3D11ObjectsKHR,
  (void(*)(void))& dummyFunc,    // 116: clGetDeviceIDsFromDX9MediaAdapterKHR,
  (void(*)(void))& dummyFunc,    // 117: clEnqueueAcquireDX9MediaSurfacesKHR,
  (void(*)(void))& dummyFunc,    // 118: clEnqueueReleaseDX9MediaSurfacesKHR,
  (void(*)(void))& icd_clCreateFromEGLImageKHR,
  (void(*)(void))& icd_clEnqueueAcquireEGLObjectsKHR,
  (void(*)(void))& icd_clEnqueueReleaseEGLObjectsKHR,
  (void(*)(void))& icd_clCreateEventFromEGLSyncKHR,
  (void(*)(void))& icd_clCreateCommandQueueWithProperties,
  (void(*)(void))& dummyFunc,    // 124 : clCreatePipe
  (void(*)(void))& dummyFunc,    // 125 : clGetPipeInfo
  (void(*)(void))& dummyFunc,    // 126 : clSVMAlloc
  (void(*)(void))& dummyFunc,    // 127 : clSVMFree
  (void(*)(void))& dummyFunc,    // 128 : clEnqueueSVMFree
  (void(*)(void))& dummyFunc,    // 129 : clEnqueueSVMMemcpy
  (void(*)(void))& dummyFunc,    // 130 : clEnqueueSVMMemFill
  (void(*)(void))& dummyFunc,    // 131 : clEnqueueSVMMap
  (void(*)(void))& dummyFunc,    // 132 : clEnqueueSVMUnmap
  (void(*)(void))& dummyFunc,    // 133 : clCreateSamplerWithProperties
  (void(*)(void))& dummyFunc,    // 134 : clSetKernelArgSVMPointer
  (void(*)(void))& dummyFunc,    // 135 : clSetKernelExecInfo
  (void(*)(void))& dummyFunc,    // 136 : clGetKernelSubGroupInfoKHR
  (void(*)(void))& dummyFunc,    // 137 : clCloneKernel
  (void(*)(void))& dummyFunc,    // 138 : clCreateProgramWithIL
  (void(*)(void))& dummyFunc,    // 139 : clEnqueueSVMMigrateMem
  (void(*)(void))& dummyFunc,    // 140 : clGetDeviceAndHostTimer
  (void(*)(void))& dummyFunc,    // 141 : clGetHostTimer
  (void(*)(void))& dummyFunc,    // 142 : clGetKernelSubGroupInfo
  (void(*)(void))& dummyFunc,    // 143 : clSetDefaultDeviceCommandQueue
  (void(*)(void))& dummyFunc,    // 144 : clSetProgramReleaseCallback
  (void(*)(void))& dummyFunc,    // 145 : clSetProgramSpecializationConstant
};
