/*
 *  This file is part of ocland, a free cloud OpenCL interface.
 *  Copyright (C) 2012-2019  Jose Luis Cercos Pita <jl.cercos@upm.es>
 *
 *  ocland is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  ocland is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with ocland.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <string.h>
#include <unistd.h>
#include <pthread.h>

#include <ocland/common/dataExchange.h>
#include <ocland/client/command_queue.h>
#include <ocland/client/context.h>
#include <ocland/client/event.h>
#include <ocland/client/log.h>

DEFINE_LIST(cl_event, events)

/// List of events
static List* events = NULL;

bool is_event(cl_event event)
{
    if(!events)
        events = events_new();
    return events_find(events, event) < events_len(events);
}

/// Event status change callback data structure
struct _event_status_cb {
    /// Calling event
    cl_event event;
    /// Status change notification callback
    void (CL_CALLBACK *pfn_notify)(cl_event, cl_int, void *);
    /// Compiling notification user data
    void* user_data;
    /// Intended execution status (may missmatch for CL_COMPLETE if the
    /// command failed)
    cl_int execution_status;
};

typedef struct _event_status_cb* event_status_cb;

DEFINE_LIST(event_status_cb, event_cbs)

/**
 * Asynchronously callable thread to dispatch event callbacks execution
 * @param  user_data Casted pointer to event_status_cb structure
 * @return           NULL
 */
void *event_cb_thread(void *user_data)
{
    event_status_cb data = (event_status_cb)user_data;
    data->pfn_notify(data->event,
                     get_event_status(data->event),
                     data->user_data);
    free(data);
    return NULL;
}

/**
 * Fire up a detached parallel thread to execute a callback
 * @param cb Callback to execute
 * @return   0 on success, error code otherwise
 */
int event_cb_execute(event_status_cb cb)
{
    int err_code;
    pthread_t thread;
    err_code = pthread_create(&thread, NULL, event_cb_thread, (void*)cb);
    if(err_code)
        return err_code;
    err_code = pthread_detach(thread);
    return err_code;
}

void set_event_status(cl_event event, cl_int status)
{
    pthread_mutex_lock(&(event->lock));
    if(status >= event->execution_status) {
        pthread_mutex_unlock(&(event->lock));
        return;
    }
    event->execution_status = status;
    event_cbs_node cb_node = event_cbs_front(event->status_callbacks);
    unsigned int i = 0;
    while(!event_cbs_is_null(cb_node))
    {
        event_status_cb cb = event_cbs_data(cb_node);
        cb_node = event_cbs_next(cb_node);
        if(cb->execution_status > status) {
            i++;
            continue;
        }
        const int err_code = event_cb_execute(cb);
        if(err_code)
            VERBOSE_MSG("Threaded callback execution failed (%d)", err_code);
        event_cbs_remove_at(event->status_callbacks, i);
    }
    pthread_mutex_unlock(&(event->lock));
    if(status <= CL_COMPLETE && event->queue) {
        oclandReleaseEvent(event);
    }
}

cl_int get_event_status(cl_event event)
{
    cl_int status;
    pthread_mutex_lock(&(event->lock));
    status = event->execution_status;
    pthread_mutex_unlock(&(event->lock));

    return status;
}

/**
 * Function to be called when a remote peer would notify a context error
 * @param compile_ref  Server context instance
 * @param msg_size     Size of the message received by the server
 * @param msg          Message received by the server
 * @param context      Local context instance
 */
void event_notifier(void* event_peer,
                    size_t msg_size,
                    void* msg,
                    void* event_local)
{
    cl_event event = (cl_event)event_local;
    if(!is_event(event)) {
        VERBOSE_MSG(
            "Event status notification for undefined event %p\n", (void*)event)
        return;
    }

    if(msg_size != sizeof(cl_int)) {
        VERBOSE_MSG("Event status notification expected %lu bytes, not %lu\n",
                    sizeof(cl_int), msg_size)
        return;
    }

    set_event_status(event, *((cl_int*)msg));
}

cl_event new_event(cl_context context, cl_command_queue queue)
{
    if(!events)
        events = events_new();

    cl_event event = (cl_event)malloc(sizeof(struct _cl_event));
    if(!event)
       return NULL;

    event->dispatch = context->dispatch;
    event->ptr = event;  // Special case, see struct _cl_event
    event->rcount = 1;
    event->server = context->server;
    event->context = context;
    event->queue = queue;
    event->execution_status = CL_QUEUED;
    // Just in case...
    event->status_callbacks = NULL;

    oclandRetainContext(context);
    if(queue) {
        oclandRetainCommandQueue(queue);
        // Retain the event itself until it is completed
        event->rcount++;
    }

    if (pthread_mutex_init(&(event->lock), NULL) != 0) {
        del_event(event);
        return NULL;
    }

    event->status_callbacks = event_cbs_new();
    if(!event->status_callbacks) {
        del_event(event);
        return NULL;
    }

    notifier_append(event->server->cb_notifier,
                    (void*)event,
                    &event_notifier,
                    (void*)event,
                    false);

    events_push_back(events, event);

    return event;
}

void del_event(cl_event event)
{
    if(is_event(event))
        events_remove_at(events, events_find(events, event));

    notifier_remove(event->server->cb_notifier, event);

    if(event->status_callbacks) {
        // Then the mutex was already created for good, and we can destroy it
        pthread_mutex_destroy(&(event->lock));
        event_cbs_delete(event->status_callbacks);
    }

    oclandReleaseContext(event->context);
    if(event->queue)
        oclandReleaseCommandQueue(event->queue);

    free(event);
}

cl_int wait_for_events(cl_uint num_events,
                       const cl_event *event_list,
                       cl_int execution_status)
{
    cl_int flag = execution_status;
    for(cl_uint i = 0; i < num_events; i++) {
        cl_int status;
        while((status = get_event_status(event_list[i])) > execution_status)
            usleep(10);

        if(status < 0)
            flag = status;
    }
    return flag;
}

cl_event* get_command_queue_events(cl_command_queue command_queue,
                                   cl_uint *num_events)
{
    if(!events)
        events = events_new();

    cl_event* event_list = NULL;
    *num_events = 0;
    for(events_node node = events_front(events);
        !events_is_null(node);
        node = events_next(node))
    {
        cl_event event = events_data(node);
        if(event->queue == command_queue)
            (*num_events)++;
    }

    if(!*num_events)
        return NULL;

    event_list = (cl_event*)malloc(*num_events * sizeof(cl_event));
    if(!event_list)
        return NULL;
    cl_uint i = 0;
    for(events_node node = events_front(events);
        !events_is_null(node);
        node = events_next(node))
    {
        cl_event event = events_data(node);
        if(event->queue == command_queue) {
            oclandRetainEvent(event);
            event_list[i] = event;
            i++;
        }
    }

    return event_list;
}

cl_int oclandWaitForEvents(cl_uint          num_events,
                           const cl_event * event_list)
{
    cl_context context = NULL;
    for(cl_uint i = 0; i < num_events; i++) {
        if(!is_event(event_list[i]))
            return CL_INVALID_EVENT;
        if(!context) {
            context = event_list[i]->context;
        } else if(context != event_list[i]->context) {
            return CL_INVALID_CONTEXT;
        }
    }

    cl_int flag = wait_for_events(num_events,
                                  event_list,
                                  CL_COMPLETE);
    if(flag < CL_COMPLETE)
        return CL_EXEC_STATUS_ERROR_FOR_EVENTS_IN_WAIT_LIST;
    return CL_SUCCESS;
}

cl_int oclandGetEventInfo(cl_event          event,
                          cl_event_info     param_name,
                          size_t            param_value_size,
                          void *            param_value,
                          size_t *          param_value_size_ret)
{
    if(!is_event(event))
        return CL_INVALID_EVENT;

    // There is a special error when asking for CL_EVENT_COMMAND_TYPE, but the
    // event was not generated by a command (e.g. clCreateUserEvent)
    if((param_name == CL_EVENT_COMMAND_TYPE) && !event->queue)
        return CL_INVALID_VALUE;

    size_t size_ret = 0;
    void* data_ret = NULL;
    switch(param_name) {
        case CL_EVENT_REFERENCE_COUNT:
            size_ret = sizeof(cl_uint);
            data_ret = &(event->rcount);
            break;
        case CL_EVENT_COMMAND_QUEUE:
            size_ret = sizeof(cl_command_queue);
            data_ret = &(event->queue);
            break;
        case CL_EVENT_CONTEXT:
            size_ret = sizeof(cl_context);
            data_ret = &(event->context);
            break;
        case CL_EVENT_COMMAND_TYPE:
            size_ret = sizeof(cl_command_type);
            data_ret = &(event->command_type);
            break;
        case CL_EVENT_COMMAND_EXECUTION_STATUS:
            size_ret = sizeof(cl_command_type);
            data_ret = &(event->command_type);
            break;
        default:
            return CL_INVALID_VALUE;
    }

    if(param_value_size_ret)
        *param_value_size_ret = size_ret;
    if(param_value) {
        if(param_value_size < size_ret)
            return CL_INVALID_VALUE;
        memcpy(param_value, data_ret, size_ret);
    }

    return CL_SUCCESS;
}

cl_int oclandRetainEvent(cl_event event)
{
    if(!is_event(event))
        return CL_INVALID_EVENT;

    pthread_mutex_lock(&(event->lock));
    event->rcount++;
    pthread_mutex_unlock(&(event->lock));
    return CL_SUCCESS;
}

cl_int oclandReleaseEvent(cl_event event)
{
    if(!is_event(event))
        return CL_INVALID_EVENT;

    pthread_mutex_lock(&(event->lock));
    event->rcount--;
    pthread_mutex_unlock(&(event->lock));
    if(event->rcount > 0) {
        return CL_SUCCESS;
    }

    // The event shall be destroyed, including from server
    int err;
    const unsigned int method = ocland_clReleaseEvent;
    ocland_server server = event->server;
    lock(server);

    err = send_comm(server->socket, sizeof(unsigned int),
                                    sizeof(cl_event),
                                    0,
                                    &method,
                                    &(event->ptr),
                                    NULL);
    unlock(server);
    if(err)
        return CL_OUT_OF_HOST_MEMORY;

    // We don't event wait for a server response, we assume everything is OK
    // In case of errors, the server may send a report to the context callback
    del_event(event);
    return CL_SUCCESS;
}

cl_int oclandGetEventProfilingInfo(cl_event             event ,
                                   cl_profiling_info    param_name ,
                                   size_t               param_value_size ,
                                   void *               param_value ,
                                   size_t *             param_value_size_ret)
{
    if(!is_event(event))
        return CL_INVALID_EVENT;

    const unsigned int method = ocland_clGetEventProfilingInfo;
    return oclandGetGenericInfo(event->server,
                                param_value_size,
                                param_value,
                                param_value_size_ret,
                                // Variable arguments
                                sizeof(unsigned int),
                                sizeof(cl_event),
                                sizeof(cl_profiling_info),
                                sizeof(size_t),
                                0,
                                &method,
                                &(event->ptr),
                                &param_name,
                                &param_value_size,
                                NULL);
}

cl_int oclandFlush(cl_command_queue  command_queue)
{
    if(!is_queue(command_queue))
        return CL_INVALID_COMMAND_QUEUE;

    cl_uint num_events;
    cl_event *event_list = get_command_queue_events(command_queue, &num_events);
    if(!num_events) {
        free(event_list);
        return CL_SUCCESS;
    }

    if(!event_list)
        return CL_OUT_OF_HOST_MEMORY;

    wait_for_events(num_events, (const cl_event*)event_list, CL_SUBMITTED);
    return CL_SUCCESS;
}

cl_int oclandFinish(cl_command_queue  command_queue)
{
    if(!is_queue(command_queue))
        return CL_INVALID_COMMAND_QUEUE;

    cl_uint num_events;
    cl_event *event_list = get_command_queue_events(command_queue, &num_events);
    if(!num_events) {
        free(event_list);
        return CL_SUCCESS;
    }

    if(!event_list)
        return CL_OUT_OF_HOST_MEMORY;

    wait_for_events(num_events, (const cl_event*)event_list, CL_COMPLETE);
    return CL_SUCCESS;
}

// -------------------------------------------- //
//                                              //
// OpenCL 1.1 methods                           //
//                                              //
// -------------------------------------------- //

cl_event oclandCreateUserEvent(cl_context     context ,
                               cl_int *       errcode_ret)
{
    if(!events)
        events = events_new();

    if(!is_context(context)) {
        if(errcode_ret) *errcode_ret = CL_INVALID_CONTEXT;
        return NULL;
    }

    // Create the event
    cl_event event = new_event(context, NULL);
    if(!event) {
        if(errcode_ret) *errcode_ret = CL_OUT_OF_HOST_MEMORY;
        return NULL;
    }

    // Call the remote peer
    int err;
    const unsigned int method = ocland_clCreateUserEvent;
    ocland_server server = context->server;
    lock(server);

    err = send_comm(server->socket, sizeof(unsigned int),
                                    sizeof(cl_context),
                                    sizeof(cl_event),
                                    0,
                                    &method,
                                    &(context->ptr),
                                    &(event->ptr),
                                    NULL);
    unlock(server);

    if(err) {
        del_event(event);
        if(errcode_ret) *errcode_ret = CL_OUT_OF_HOST_MEMORY;
        return NULL;
    }

    // We don't event wait for a server response, we assume everything is OK
    // In case of errors, the server may send a report to the context callback
    if(errcode_ret) *errcode_ret = CL_SUCCESS;
    return event;
}



cl_int oclandSetUserEventStatus(cl_event    event ,
                                cl_int      execution_status)
{
    if(!is_event(event) || event->queue)
        return CL_INVALID_EVENT;

    if(get_event_status(event) != CL_QUEUED)
        return CL_INVALID_OPERATION;

    // Call the remote peer
    int err;
    const unsigned int method = ocland_clSetUserEventStatus;
    ocland_server server = event->server;
    lock(server);

    err = send_comm(server->socket, sizeof(unsigned int),
                                    sizeof(cl_event),
                                    sizeof(cl_int),
                                    0,
                                    &method,
                                    &(event->ptr),
                                    &execution_status,
                                    NULL);
    unlock(server);
    if(err)
        return CL_OUT_OF_HOST_MEMORY;

    set_event_status(event, execution_status);

    return CL_SUCCESS;
}

cl_int oclandSetEventCallback(cl_event event,
                              cl_int  command_exec_callback_type ,
                              void (CL_CALLBACK *pfn_notify) (cl_event event, cl_int event_command_exec_status, void *user_data),
                              void *user_data)
{
    if(!is_event(event))
        return CL_INVALID_EVENT;

    event_status_cb cb = (event_status_cb)malloc(
        sizeof(struct _event_status_cb));
    if(!cb)
        return CL_OUT_OF_HOST_MEMORY;
    cb->event = event;
    cb->pfn_notify = pfn_notify;
    cb->user_data = user_data;
    cb->execution_status = command_exec_callback_type;

    if(get_event_status(event) <= command_exec_callback_type) {
        // The event has already rised to the queried execution status, so we
        // just launch the callback execution and forget
        if(event_cb_execute(cb))
            return CL_OUT_OF_HOST_MEMORY;
        return CL_SUCCESS;
    }

    pthread_mutex_lock(&(event->lock));
    event_cbs_push_back(event->status_callbacks, cb);
    pthread_mutex_unlock(&(event->lock));

    return CL_SUCCESS;
}
