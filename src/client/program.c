/*
 *  This file is part of ocland, a free cloud OpenCL interface.
 *  Copyright (C) 2012-2019  Jose Luis Cercos Pita <jl.cercos@upm.es>
 *
 *  ocland is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  ocland is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with ocland.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <pthread.h>
#include <string.h>

#include <ocland/common/dataExchange.h>
#include <ocland/client/platform.h>
#include <ocland/client/device.h>
#include <ocland/client/context.h>
#include <ocland/client/program.h>
#include <ocland/client/log.h>

#include <ocland/common/typed_list.h>
DEFINE_LIST(cl_program, programs)

/// List of programs
static List* programs = NULL;

cl_program get_program(cl_program remote_program)
{
    if(!programs_len(programs))
        return NULL;
    for(programs_node program_node = programs_front(programs);
        !programs_is_null(program_node);
        program_node = programs_next(program_node))
    {
        cl_program local_program = programs_data(program_node);
        if(local_program->ptr == remote_program)
            return local_program;
    }

    return NULL;
}

bool is_program(cl_program program)
{
    if(!programs)
        programs = programs_new();
    return programs_find(programs, program) < programs_len(programs);
}

struct _program_compile {
    /// Compiling notification
    void (CL_CALLBACK *pfn_notify)(cl_program, void *);
    /// Compiling notification user data
    void* user_data;
    /// Compilation state
    cl_build_status build_status;
    /// Compilation options
    char *options;
    /// Compilation resulting binary size
    size_t binary_size;
    /// Compilation resulting binary
    unsigned char *binary;
};

/**
 * Create the structures to stores information about compilation
 * @param  program Program considered. The new compilation arrays is already
 *                 set in the program
 * @return         New array of compilation structures
 */
program_compile* new_program_compiles(cl_program program)
{
    const cl_uint num_devices = program->num_devices;
    program_compile* compiles = (program_compile*)malloc(
        num_devices * sizeof(program_compile));
    if(!compiles)
        return NULL;
    for(cl_uint i = 0; i < num_devices; i++) {
        compiles[i] = (program_compile)malloc(sizeof(struct _program_compile));
        if(!compiles[i]) {
            for(cl_uint j = 0; j < i; j++) {
                free(compiles[j]);
            }
            free(compiles);
            return NULL;
        }
        compiles[i]->pfn_notify = NULL;
        compiles[i]->user_data = NULL;
        compiles[i]->build_status = CL_BUILD_NONE;
        compiles[i]->options = NULL;
        compiles[i]->binary_size = 0;
        compiles[i]->binary = NULL;
    }
    program->compilations = compiles;
    return compiles;
}

/**
 * Delete an array of compilation structures
 *
 * This method can be safely called even if there were errors during the
 * program generation
 * @param  program Program considered. The new compilation arrays is already
 *                 set in the program
 */
void del_program_compiles(cl_program program)
{
    const cl_uint num_devices = program->num_devices;
    program_compile* compiles = program->compilations;
    program->compilations = NULL;
    if(!compiles)
        return;

    for(cl_uint i = 0; i < num_devices; i++) {
        // It is actually safe to ask for the notifier endpoint removal, even
        // if it does not exists. That might be the case when clBuildProgram
        // has been built on top of several devices
        notifier_remove(program->server->cb_notifier, (void*)(compiles[i]));
        free(compiles[i]->options);
        free(compiles[i]->binary);
        free(compiles[i]);
    }
    free(compiles);
}

/**
 * Safely remove a program, even if it was not correctly generated
 * @param program Program to remove
 */
void del_program(cl_program program)
{
    if(is_program(program))
        programs_remove_at(programs, programs_find(programs, program));

    free(program->devices);
    free(program->src);
    del_program_compiles(program);

    oclandReleaseContext(program->context);

    free(program);
}

/**
 * Create a new program, and append it to the programs list
 * @param  context      Context where the program shall be created in
 * @param  program_peer Program server reference
 * @param  num_devices  Number of devices considered in the program
 * @param  devices      Devices considered in the program
 * @return              The new generated program
 */
cl_program new_program(cl_context context,
                       cl_program program_peer,
                       cl_uint num_devices,
                       const cl_device_id *devices)
{
    cl_program program = (cl_program)malloc(sizeof(struct _cl_program));
    if(!program)
       return NULL;
    program->dispatch = context->dispatch;
    program->ptr = program_peer;
    program->rcount = 1;
    program->server = context->server;
    program->context = context;
    program->num_devices = context->num_devices;
    program->src = NULL;
    program->num_kernels_attached = 0;

    program->devices = (cl_device_id*)malloc(
        program->num_devices * sizeof(cl_device_id));
    program_compile *compiles = new_program_compiles(program);
    if(!program->devices || !compiles) {
        del_program(program);
        return NULL;
    }
    memcpy(program->devices, devices, num_devices * sizeof(cl_device_id));

    programs_push_back(programs, program);

    // It is not required to retain each device, since the context we are
    // retaining is already retaining them
    oclandRetainContext(program->context);

    return program;
}

/**
 * Concatenate all the source code strings
 * @param  count   Number of strings
 * @param  strings Strings to concatenate
 * @param  lengths Length of each string
 * @return         Concatenated strings, NULL terminated. NULL pointer if the
 *                 system failed to allocate memory
 */
char* concatenate_sources(cl_uint count,
                          const char **strings,
                          const size_t *lengths)
{
    size_t len = 0;
    for(cl_uint i = 0; i < count; i++) {
        if( !lengths || !lengths[i] ) {
            // Null terrminated string
            len += strlen(strings[i]) * sizeof(char);
        }
        else {
            len += lengths[i];
        }
    }

    // We append one single character to grant the output is NULL terminated
    len += sizeof(char);
    char* src = (char*)malloc(len);
    if(!src)
        return NULL;
    memset(src, '\0', len);

    for(cl_uint i = 0; i < count; i++) {
        if( !lengths || !lengths[i] ) {
            // Null terrminated string
            strcat(src, strings[i]);
        }
        else {
            memcpy(src, strings[i], lengths[i]);
        }
    }

    return src;
}

cl_program oclandCreateProgramWithSource(cl_context         context ,
                                         cl_uint            count ,
                                         const char **      strings ,
                                         const size_t *     lengths ,
                                         cl_int *           errcode_ret)
{
    if(!programs)
        programs = programs_new();

    if(!is_context(context)) {
        if(errcode_ret) *errcode_ret = CL_INVALID_CONTEXT;
        return NULL;
    }

    // Create the source code from the provided data
    char* src = concatenate_sources(count, strings, lengths);
    if(!src) {
        if(errcode_ret) *errcode_ret = CL_OUT_OF_HOST_MEMORY;
        return NULL;
    }
    size_t src_size = (strlen(src) + 1) * sizeof(char);

    // Call the remote peer
    int err;
    const unsigned int method = ocland_clCreateProgramWithSource;
    ocland_server server = context->server;
    lock(server);

    err = send_comm(server->socket, sizeof(unsigned int),
                                    sizeof(cl_context),
                                    sizeof(size_t),
                                    src_size,
                                    0,
                                    &method,
                                    &(context->ptr),
                                    &src_size,
                                    src,
                                    NULL);
    if(err) {
        free(src);
        unlock(server);
        if(errcode_ret) *errcode_ret = CL_OUT_OF_HOST_MEMORY;
        return NULL;
    }

    cl_int flag;
    cl_program program;
    err = recv_comm(server->socket, NULL, sizeof(cl_int),
                                          sizeof(cl_program),
                                          0,
                                          &flag,
                                          &program,
                                          NULL);

    unlock(server);

    if(err) {
        free(src);
        if(errcode_ret) *errcode_ret = CL_OUT_OF_HOST_MEMORY;
        return NULL;
    }

    if(errcode_ret) *errcode_ret = flag;

    if(flag != CL_SUCCESS) {
        free(src);
        return NULL;
    }

    cl_program data = new_program(context,
                                  program,
                                  context->num_devices,
                                  (const cl_device_id*)(context->devices));
    if(!data) {
        if(errcode_ret) *errcode_ret = CL_OUT_OF_HOST_MEMORY;
        return NULL;
    }
    data->src = src;

    return data;
}

/**
 * Ask the server for the associated source code.
 *
 * Depending on the way the program was built can be the actual source code, or
 * a "\0" null string
 * @param  program Program to query
 * @return         CL_SUCCESS if the info is successfully obtained, or an error
 *                 code otherwise
 */
cl_int get_program_src(cl_program program)
{
    cl_int flag;
    size_t param_value_size = 0;
    const cl_program_info param_name = CL_PROGRAM_SOURCE;
    const unsigned int method = ocland_clGetProgramInfo;
    flag = oclandGetGenericInfo(program->server,
                                0,
                                NULL,
                                &param_value_size,
                                // Variable arguments
                                sizeof(unsigned int),
                                sizeof(cl_program),
                                sizeof(cl_program_info),
                                sizeof(size_t),
                                0,
                                &method,
                                &(program->ptr),
                                &param_name,
                                &param_value_size,
                                NULL);
    if(flag != CL_SUCCESS)
        return CL_OUT_OF_HOST_MEMORY;

    program->src = (char*)malloc(param_value_size);
    if(!program->src)
        return CL_OUT_OF_HOST_MEMORY;

    flag = oclandGetGenericInfo(program->server,
                                param_value_size,
                                program->src,
                                NULL,
                                // Variable arguments
                                sizeof(unsigned int),
                                sizeof(cl_program),
                                sizeof(cl_program_info),
                                sizeof(size_t),
                                0,
                                &method,
                                &(program->ptr),
                                &param_name,
                                &param_value_size,
                                NULL);
    return flag;
}

cl_program oclandCreateProgramWithBinary(cl_context              context ,
                                         cl_uint                 num_devices ,
                                         const cl_device_id *    device_list ,
                                         const size_t *          lengths ,
                                         const unsigned char **  binaries ,
                                         cl_int *                binary_status ,
                                         cl_int *                errcode_ret)
{
    if(!programs)
        programs = programs_new();

    if(!is_context(context)) {
        if(errcode_ret) *errcode_ret = CL_INVALID_CONTEXT;
        return NULL;
    }

    // Set the remote devices array
    cl_device_id* devices = (cl_device_id*)malloc(
        num_devices * sizeof(cl_device_id));
    if(!devices) {
        if(errcode_ret) *errcode_ret = CL_OUT_OF_HOST_MEMORY;
        return NULL;
    }
    for(cl_uint i = 0; i < num_devices; i++) {
        if(!is_device(device_list[i])) {
            free(devices);
            if(errcode_ret) *errcode_ret = CL_INVALID_DEVICE;
            return NULL;
        }
        devices[i] = device_list[i]->ptr;
    }

    // Create the monolithic contiguous binary from the provided ones
    size_t binary_size = 0;
    for(cl_uint i = 0; i < num_devices; i++)
        binary_size += lengths[i];
    unsigned char* binary = (unsigned char*)malloc(binary_size);
    if(!binary) {
        free(devices);
        if(errcode_ret) *errcode_ret = CL_OUT_OF_HOST_MEMORY;
        return NULL;
    }
    unsigned char* binary_ptr = binary;
    for(cl_uint i = 0; i < num_devices; i++) {
        memcpy(binary_ptr, binaries[i], lengths[i]);
        binary_ptr += lengths[i];
    }

    // Call the remote peer
    int err;
    const unsigned int method = ocland_clCreateProgramWithBinary;
    ocland_server server = context->server;
    lock(server);

    err = send_comm(server->socket, sizeof(unsigned int),
                                    sizeof(cl_context),
                                    sizeof(cl_uint),
                                    num_devices * sizeof(cl_device_id),
                                    num_devices * sizeof(size_t),
                                    binary_size,
                                    0,
                                    &method,
                                    &(context->ptr),
                                    &num_devices,
                                    devices,
                                    lengths,
                                    binary,
                                    NULL);
    free(devices);
    free(binary);

    if(err) {
        unlock(server);
        if(errcode_ret) *errcode_ret = CL_OUT_OF_HOST_MEMORY;
        return NULL;
    }

    cl_int flag;
    cl_program program;
    recv_pending rest;
    err = recv_comm(server->socket, &rest, sizeof(cl_int),
                                           sizeof(cl_program),
                                           0,
                                           &flag,
                                           &program,
                                           NULL);

    unlock(server);

    if(err || rest.size != num_devices * sizeof(cl_int)) {
        free(rest.data);
        if(errcode_ret) *errcode_ret = CL_OUT_OF_HOST_MEMORY;
        return NULL;
    }
    if(binary_status)
        memcpy(binary_status, rest.data, rest.size);
    free(rest.data);

    if(errcode_ret)
        *errcode_ret = flag;

    if(flag != CL_SUCCESS)
        return NULL;

    cl_program data = new_program(context,
                                  program,
                                  num_devices,
                                  device_list);
    if(!data) {
        if(errcode_ret) *errcode_ret = CL_OUT_OF_HOST_MEMORY;
        return NULL;
    }

    // Set the appropriate compilation status
    for(cl_uint i = 0; i < num_devices; i++) {
        data->compilations[i]->binary_size = lengths[i];
        data->compilations[i]->binary = (unsigned char*)malloc(lengths[i]);
        if(!data->compilations[i]->binary) {
            del_program(program);
            if(errcode_ret) *errcode_ret = CL_OUT_OF_HOST_MEMORY;
            return NULL;
        }
        memcpy(data->compilations[i]->binary, binaries[i], lengths[i]);
    }

    // Collect data from server
    flag = get_program_src(data);
    if(flag != CL_SUCCESS) {
        del_program(data);
        if(errcode_ret) *errcode_ret = CL_OUT_OF_HOST_MEMORY;
        return NULL;
    }

    return data;
}

cl_int oclandRetainProgram(cl_program program)
{
    if(!is_program(program))
        return CL_INVALID_PROGRAM;

    program->rcount++;
    return CL_SUCCESS;
}

cl_int oclandReleaseProgram(cl_program program)
{
    if(!is_program(program))
        return CL_INVALID_PROGRAM;

    if(program->rcount > 1) {
        program->rcount--;
        return CL_SUCCESS;
    }

    // The program shall be destroyed, including from server
    int err;
    cl_int flag;
    const unsigned int method = ocland_clReleaseProgram;
    ocland_server server = program->server;
    lock(server);

    err = send_comm(server->socket, sizeof(unsigned int),
                                    sizeof(cl_program),
                                    0,
                                    &method,
                                    &(program->ptr),
                                    NULL);
    if(err) {
        unlock(server);
        return CL_OUT_OF_HOST_MEMORY;
    }

    err = recv_comm(server->socket, NULL, sizeof(cl_int),
                                          0,
                                          &flag,
                                          NULL);

    unlock(server);

    if(err)
        return CL_OUT_OF_HOST_MEMORY;

    if(flag != CL_SUCCESS)
        return flag;

    del_program(program);
    return CL_SUCCESS;
}

/**
 * Check if a device is associatred with a program
 * @param  program Program to be checked
 * @param  device  Device to be checked
 * @return         index of the device inside the program, program->num_devices
 *                 if there is not such device
 */
cl_uint program_device_id(cl_program program, cl_device_id device)
{
    if(!is_device(device))
        return program->num_devices;
    for(cl_uint i = 0; i < program->num_devices; i++) {
        if(program->devices[i] == device)
            return i;
    }
    return program->num_devices;
}

/**
 * Extract a compilation from the received message
 *
 * This method is also moving the message pointer, and the remaining message
 * size
 * @param  program     Program where the data shall be stored
 * @param  device_peer Remote reference to the device
 * @param  msg         Received message (pointer)
 * @param  msg_size    Received message size (pointer)
 * @return             The program compilation affected
 */
program_compile set_compilation(cl_program program,
                                cl_device_id device_peer,
                                char **msg,
                                size_t *msg_size)
{
    cl_uint i;
    for(i = 0; i < program->num_devices; i++) {
        if(!is_device(program->devices[i]))
            continue;
        if(program->devices[i]->ptr == device_peer)
            break;
    }
    if(i >= program->num_devices) {
        VERBOSE_MSG(
            "Cannot find device with remote ref %p\n", (void*)device_peer)
        return NULL;
    }

    program_compile c = program->compilations[i];
    if(*msg_size < sizeof(cl_build_status) + sizeof(size_t)) {
        VERBOSE_MSG("Invalid message size, %lu\n",
                    sizeof(cl_build_status) + sizeof(size_t))
        return c;
    }

    *msg = disassemble_msg(*msg,
        sizeof(cl_build_status), &(c->build_status),
        sizeof(size_t), &(c->binary_size),
        0, NULL);
    *msg_size -= sizeof(cl_build_status) + sizeof(size_t);
    if(!c->binary_size)
        return c;

    c->binary = (unsigned char*)malloc(c->binary_size);
    if(!c->binary) {
        VERBOSE_MSG("Failure allocating %lu bytes\n",
                    c->binary_size)
        c->binary_size = 0;
        return c;
    }
    *msg = disassemble_msg(*msg, c->binary_size, c->binary, 0, NULL);
    *msg_size -= c->binary_size;

    return c;
}

/**
 * Function to be called when a remote peer would notify a context error
 * @param compile_ref  Server context instance
 * @param msg_size     Size of the message received by the server
 * @param msg          Message received by the server
 * @param context      Local context instance
 */
void program_notifier(void *program_peer,
                      size_t msg_size,
                      void* msg,
                      void* program)
{
    cl_program p = (cl_program)program;
    if(!is_program(p)) {
        VERBOSE_MSG(
            "Program compilation notification for undefined program %p\n",
            program)
        return;
    }

    char *ptr = (char*)msg;
    cl_uint num_devices = 0;
    if(msg_size < sizeof(cl_uint)) {
        VERBOSE_MSG(
            "Program compilation notification with msg_size = %lu\n", msg_size)
        return;
    }
    ptr = disassemble_msg(ptr, sizeof(cl_uint), &num_devices, 0, NULL);
    msg_size -= sizeof(cl_uint);
    cl_device_id *devices = (cl_device_id*)malloc(
        num_devices * sizeof(cl_device_id));
    if(!devices) {
        VERBOSE_MSG("Failure allocating %lu bytes\n",
                    num_devices * sizeof(cl_device_id))
        return;
    }
    ptr = disassemble_msg(ptr, num_devices * sizeof(cl_device_id), devices,
                               0, NULL);

    program_compile compilation = NULL;
    for(cl_uint i = 0; i < num_devices; i++) {
        compilation = set_compilation(
            p, devices[i], &ptr, &msg_size);
        if(!compilation) {
            free(devices);
            return;
        }
    }

    free(devices);

    // We can finally call to the user provided method
    if(compilation && compilation->pfn_notify)
        compilation->pfn_notify(p, compilation->user_data);
}

cl_int oclandBuildProgram(cl_program            program ,
                          cl_uint               num_devices ,
                          const cl_device_id *  device_list ,
                          const char *          options ,
                          void (CL_CALLBACK *   pfn_notify)(cl_program  program , void *  user_data),
                          void *                user_data)
{
    if(!is_program(program))
        return CL_INVALID_PROGRAM;

    if(program->num_kernels_attached)
        return CL_INVALID_OPERATION;

    if(!num_devices) {
        num_devices = program->num_devices;
        device_list = program->devices;
    }

    // There is an inconsistency in the OpenCL documentation regarding options:
    // https://github.com/KhronosGroup/OpenCL-Docs/issues/158
    // while it is fixed, we are accepting NULL options by a wierd string
    char *options_ptr = OCLAND_NULL_PTR_STR;
    if(options)
        options_ptr = (char *)options;

    // Set the remote devices array, while checking the devices are valid
    const size_t options_size = (strlen(options_ptr) + 1) * sizeof(char);
    void* notifier_ref = NULL;
    cl_device_id* devices = (cl_device_id*)malloc(
        num_devices * sizeof(cl_device_id));
    if(!devices) {
        free(devices);
        return CL_OUT_OF_HOST_MEMORY;
    }

    // Check if there is any invalid device or program
    for(cl_uint i = 0; i < num_devices; i++) {
        cl_uint j = program_device_id(program, device_list[i]);
        if(j >= program->num_devices) {
            free(devices);
            return CL_INVALID_DEVICE;
        }
        devices[i] = device_list[i]->ptr;
        program_compile compilation = program->compilations[j];
        if(compilation->build_status == CL_BUILD_IN_PROGRESS) {
            free(devices);
            return CL_INVALID_OPERATION;
        }
    }

    // Set the program in building state in the appropriate devices
    for(cl_uint i = 0; i < num_devices; i++) {
        cl_uint j = program_device_id(program, device_list[i]);
        program_compile compilation = program->compilations[j];
        compilation->pfn_notify = pfn_notify;
        compilation->user_data = user_data;
        // Set the new program options
        free(compilation->options);
        compilation->options = NULL;
        if(options) {
            compilation->options = (char*)malloc(options_size);
            if(!compilation->options) {
                free(devices);
                return CL_OUT_OF_HOST_MEMORY;
            }
            strncpy(compilation->options, options, options_size);
        }
        compilation->build_status = CL_BUILD_IN_PROGRESS;
        if(pfn_notify && !notifier_ref)
            notifier_ref = (void*)compilation;
    }

    int err;
    const unsigned int method = ocland_clBuildProgram;
    ocland_server server = program->server;

    if(pfn_notify) {
        // Register the notifier
        notifier_append(server->cb_notifier,
                        notifier_ref,
                        &program_notifier,
                        (void*)program,
                        true);
    }

    // Call the remote peer
    lock(server);

    err = send_comm(server->socket, sizeof(unsigned int),
                                    sizeof(cl_program),
                                    sizeof(cl_uint),
                                    num_devices * sizeof(cl_device_id),
                                    sizeof(size_t),
                                    options_size,
                                    sizeof(void*),
                                    0,
                                    &method,
                                    &(program->ptr),
                                    &num_devices,
                                    devices,
                                    &options_size,
                                    options_ptr,
                                    &notifier_ref,
                                    NULL);
    free(devices);

    if(err) {
        for(cl_uint i = 0; i < num_devices; i++) {
            cl_uint j = program_device_id(program, device_list[i]);
            program_compile compilation = program->compilations[j];
            compilation->build_status = CL_BUILD_ERROR;
        }
        if(pfn_notify)
            notifier_remove(server->cb_notifier, notifier_ref);
        unlock(server);
        return CL_OUT_OF_HOST_MEMORY;
    }

    // Even if we asynchronously called to compile, we would get an answer from
    // server, to check that compiling options were right
    cl_int flag;
    recv_pending rest;
    err = recv_comm(server->socket, &rest, sizeof(cl_int), 0,
                                           &flag, NULL);
    unlock(server);

    if(err)
        flag = CL_OUT_OF_HOST_MEMORY;
    if(flag != CL_SUCCESS) {
        for(cl_uint i = 0; i < num_devices; i++) {
            cl_uint j = program_device_id(program, device_list[i]);
            program_compile compilation = program->compilations[j];
            compilation->build_status = CL_BUILD_ERROR;
        }
        free(rest.data);
        if(pfn_notify)
            notifier_remove(server->cb_notifier, notifier_ref);
        return flag;
    }

    if(!pfn_notify) {
        // Synced build process, the server shall notify the building result
        // by this channel, so we are just simply calling the compilation
        // notifier endpoint synchronously. Thus, the server shall reply with
        // the same information as it would be sent in case of async compilation
        program_notifier(NULL, rest.size, rest.data, program);
    }
    free(rest.data);

    return flag;
}

/**
 * Get info from the program
 *
 * This method only returns data that shall not be queried to the server
 * @param  program Memory object
 * @param  param_name Parameter
 * @param  param_value_size Allocated programory inside #param_value
 * @param  param_value Allocated programory where the queried data shall be copy
 * @param  param_value_size_ret Returned size of the queried data
 * @return 1 if the queried data cannot be extracted from the
 * local instance, and shall be therefore queried to the server. CL_SUCCESS
 * if the query can be processed. CL_INVALID_VALUE if the query can be processed
 * but param_value_size is smaller than the size of the returned value.
 */
cl_int get_program_info(cl_program       program,
                        cl_program_info  param_name,
                        size_t           param_value_size,
                        void *           param_value,
                        size_t *         param_value_size_ret)
{
    size_t size_ret = 0;
    // Special cases where we have not the memory stored in contiguous memory
    if (param_name == CL_PROGRAM_BINARY_SIZES) {
        size_ret = program->num_devices * sizeof(size_t);
        if(param_value_size_ret)
            *param_value_size_ret = size_ret;
        if(param_value) {
            if(param_value_size < size_ret)
                return CL_INVALID_VALUE;
            size_t* binary_sizes = (size_t*)param_value;
            for(cl_uint i = 0; i < program->num_devices; i++)
                binary_sizes[i] = program->compilations[i]->binary_size;
        }
        return CL_SUCCESS;
    } else if (param_name ==  CL_PROGRAM_BINARIES) {
        size_ret = program->num_devices * sizeof(unsigned char *);
        if(param_value_size_ret)
            *param_value_size_ret = size_ret;
        if(param_value) {
            if(param_value_size < size_ret)
                return CL_INVALID_VALUE;
            unsigned char **binaries = (unsigned char**)param_value;
            for(cl_uint i = 0; i < program->num_devices; i++) {
                if(!program->compilations[i]->binary) {
                    binaries[i] = NULL;
                    continue;
                }
                memcpy(binaries[i],
                       program->compilations[i]->binary,
                       program->compilations[i]->binary_size);
            }
        }
        return CL_SUCCESS;
    }

    void* data_ret = NULL;
    char* empty_str = "";
    switch(param_name) {
        case CL_PROGRAM_REFERENCE_COUNT:
            size_ret = sizeof(cl_uint);
            data_ret = &(program->rcount);
            break;
        case CL_PROGRAM_CONTEXT:
            size_ret = sizeof(cl_context);
            data_ret = &(program->context);
            break;
        case CL_PROGRAM_NUM_DEVICES:
            size_ret = sizeof(cl_uint);
            data_ret = &(program->num_devices);
            break;
        case CL_PROGRAM_DEVICES:
            size_ret = program->num_devices * sizeof(cl_device_id);
            data_ret = program->devices;
            break;
        case CL_PROGRAM_SOURCE:
            if(!program->src) {
                size_ret = sizeof(char);
                data_ret = empty_str;
            } else {
                size_ret = (strlen(program->src) + 1) * sizeof(char);
                data_ret = program->src;
            }
            break;
        default:
            return 1;
    }

    if(param_value_size_ret)
        *param_value_size_ret = size_ret;
    if(param_value) {
        if(param_value_size < size_ret)
            return CL_INVALID_VALUE;
        memcpy(param_value, data_ret, size_ret);
    }

    return CL_SUCCESS;
}

cl_int oclandGetProgramInfo(cl_program          program ,
                            cl_program_info     param_name ,
                            size_t              param_value_size ,
                            void *              param_value ,
                            size_t *            param_value_size_ret)
{
    if(!is_program(program))
        return CL_INVALID_PROGRAM;

    cl_int flag;
    flag = get_program_info(program,
                            param_name,
                            param_value_size,
                            param_value,
                            param_value_size_ret);
    if(flag != 1)  // flag = 1 means we should call the server
        return flag;

    // Call the remote peer to try to get the missing info
    const unsigned int method = ocland_clGetProgramInfo;
    return oclandGetGenericInfo(program->server,
                                param_value_size,
                                param_value,
                                param_value_size_ret,
                                // Variable arguments
                                sizeof(unsigned int),
                                sizeof(cl_program),
                                sizeof(cl_program_info),
                                sizeof(size_t),
                                0,
                                &method,
                                &(program->ptr),
                                &param_name,
                                &param_value_size,
                                NULL);
}

/**
 * Get info from a program compilation
 *
 * This method only returns data that shall not be queried to the server
 * @param  compilation Compilation structure
 * @param  param_name Parameter
 * @param  param_value_size Allocated programory inside #param_value
 * @param  param_value Allocated programory where the queried data shall be copy
 * @param  param_value_size_ret Returned size of the queried data
 * @return 1 if the queried data cannot be extracted from the
 * local instance, and shall be therefore queried to the server. CL_SUCCESS
 * if the query can be processed. CL_INVALID_VALUE if the query can be processed
 * but param_value_size is smaller than the size of the returned value.
 */
cl_int get_compilation_info(program_compile        compilation,
                            cl_program_build_info  param_name,
                            size_t                 param_value_size,
                            void *                 param_value,
                            size_t *               param_value_size_ret)
{
    size_t size_ret = 0;
    void* data_ret = NULL;
    char* empty_str = "";
    switch(param_name) {
        case CL_PROGRAM_BUILD_STATUS:
            size_ret = sizeof(cl_build_status);
            data_ret = &(compilation->build_status);
            break;
        case CL_PROGRAM_BUILD_OPTIONS:
            if(!compilation->options) {
                size_ret = sizeof(char);
                data_ret = empty_str;
            } else {
                size_ret = (strlen(compilation->options) + 1) * sizeof(char);
                data_ret = compilation->options;
            }
            break;
        default:
            return 1;
    }

    if(param_value_size_ret)
        *param_value_size_ret = size_ret;
    if(param_value) {
        if(param_value_size < size_ret)
            return CL_INVALID_VALUE;
        memcpy(param_value, data_ret, size_ret);
    }

    return CL_SUCCESS;
}

cl_int oclandGetProgramBuildInfo(cl_program             program ,
                                 cl_device_id           device ,
                                 cl_program_build_info  param_name ,
                                 size_t                 param_value_size ,
                                 void *                 param_value ,
                                 size_t *               param_value_size_ret)
{
    if(!is_program(program))
        return CL_INVALID_PROGRAM;

    cl_uint i = program_device_id(program, device);
    if(i >= program->num_devices)
        return CL_INVALID_DEVICE;
    program_compile compilation = program->compilations[i];

    cl_int flag;
    flag = get_compilation_info(compilation,
                                param_name,
                                param_value_size,
                                param_value,
                                param_value_size_ret);
    if(flag != 1)  // flag = 1 means we should call the server
        return flag;

    // Call the remote peer to try to get the missing info
    const unsigned int method = ocland_clGetProgramBuildInfo;
    return oclandGetGenericInfo(program->server,
                                param_value_size,
                                param_value,
                                param_value_size_ret,
                                // Variable arguments
                                sizeof(unsigned int),
                                sizeof(cl_program),
                                sizeof(cl_device_id),
                                sizeof(cl_program_build_info),
                                sizeof(size_t),
                                0,
                                &method,
                                &(program->ptr),
                                &(device->ptr),
                                &param_name,
                                &param_value_size,
                                NULL);
}

// -------------------------------------------- //
//                                              //
// OpenCL 1.2 methods                           //
//                                              //
// -------------------------------------------- //

cl_program oclandCreateProgramWithBuiltInKernels(cl_context            context ,
                                                 cl_uint               num_devices ,
                                                 const cl_device_id *  device_list ,
                                                 const char *          kernel_names ,
                                                 cl_int *              errcode_ret)
{
    if(!programs)
        programs = programs_new();

    if(!is_context(context)) {
        if(errcode_ret) *errcode_ret = CL_INVALID_CONTEXT;
        return NULL;
    }

    // Set the remote devices array
    cl_device_id* devices = (cl_device_id*)malloc(
        num_devices * sizeof(cl_device_id));
    if(!devices) {
        if(errcode_ret) *errcode_ret = CL_OUT_OF_HOST_MEMORY;
        return NULL;
    }
    for(cl_uint i = 0; i < num_devices; i++) {
        if(!is_device(device_list[i])) {
            free(devices);
            if(errcode_ret) *errcode_ret = CL_INVALID_DEVICE;
            return NULL;
        }
        devices[i] = device_list[i]->ptr;
    }

    // Call the remote peer
    int err;
    const unsigned int method = ocland_clCreateProgramWithBuiltInKernels;
    const size_t kernel_names_size = (strlen(kernel_names) + 1) * sizeof(char);
    ocland_server server = context->server;
    lock(server);

    err = send_comm(server->socket, sizeof(unsigned int),
                                    sizeof(cl_context),
                                    sizeof(cl_uint),
                                    num_devices * sizeof(cl_device_id),
                                    kernel_names_size,
                                    0,
                                    &method,
                                    &(context->ptr),
                                    &num_devices,
                                    devices,
                                    kernel_names,
                                    NULL);
    free(devices);

    if(err) {
        unlock(server);
        if(errcode_ret) *errcode_ret = CL_OUT_OF_HOST_MEMORY;
        return NULL;
    }

    cl_int flag;
    cl_program program;
    err = recv_comm(server->socket, NULL, sizeof(cl_int),
                                          sizeof(cl_program),
                                          0,
                                          &flag,
                                          &program,
                                          NULL);

    unlock(server);

    if(err) {
        if(errcode_ret) *errcode_ret = CL_OUT_OF_HOST_MEMORY;
        return NULL;
    }

    if(errcode_ret)
        *errcode_ret = flag;

    if(flag != CL_SUCCESS)
        return NULL;

    cl_program data = new_program(context,
                                  program,
                                  num_devices,
                                  device_list);
    if(!data) {
        if(errcode_ret) *errcode_ret = CL_OUT_OF_HOST_MEMORY;
        return NULL;
    }

    // Collect data from server
    flag = get_program_src(data);
    if(flag != CL_SUCCESS) {
        del_program(data);
        if(errcode_ret) *errcode_ret = CL_OUT_OF_HOST_MEMORY;
        return NULL;
    }

    return data;
}

cl_int oclandCompileProgram(cl_program            program ,
                            cl_uint               num_devices ,
                            const cl_device_id *  device_list ,
                            const char *          options ,
                            cl_uint               num_input_headers ,
                            const cl_program *    input_headers,
                            const char **         header_include_names ,
                            void (CL_CALLBACK *   pfn_notify)(cl_program  program , void *  user_data),
                            void *                user_data)
{
    if(!is_program(program))
        return CL_INVALID_PROGRAM;

    if(program->num_kernels_attached)
        return CL_INVALID_OPERATION;

    if(!num_devices) {
        num_devices = program->num_devices;
        device_list = program->devices;
    }

    // There is an inconsistency in the OpenCL documentation regarding options:
    // https://github.com/KhronosGroup/OpenCL-Docs/issues/158
    // while it is fixed, we are accepting NULL options by a wierd string
    char *options_ptr = OCLAND_NULL_PTR_STR;
    if(options)
        options_ptr = (char *)options;


    // Set the remote devices array
    const size_t options_size = (strlen(options_ptr) + 1) * sizeof(char);
    void* notifier_ref = NULL;
    cl_device_id* devices = (cl_device_id*)malloc(
        num_devices * sizeof(cl_device_id));
    if(!devices) {
        free(devices);
        return CL_OUT_OF_HOST_MEMORY;
    }

    // Check if there is any invalid device or program
    for(cl_uint i = 0; i < num_devices; i++) {
        cl_uint j = program_device_id(program, device_list[i]);
        if(j >= program->num_devices) {
            free(devices);
            return CL_INVALID_DEVICE;
        }
        devices[i] = device_list[i]->ptr;
        program_compile compilation = program->compilations[j];
        if(compilation->build_status == CL_BUILD_IN_PROGRESS) {
            free(devices);
            return CL_INVALID_OPERATION;
        }
    }

    // Set the program in building state in the appropriate devices
    for(cl_uint i = 0; i < num_devices; i++) {
        cl_uint j = program_device_id(program, device_list[i]);
        program_compile compilation = program->compilations[j];
        compilation->pfn_notify = pfn_notify;
        compilation->user_data = user_data;
        // Set the new program options
        free(compilation->options);
        compilation->options = NULL;
        if(options) {
            compilation->options = (char*)malloc(options_size);
            if(!compilation->options) {
                free(devices);
                return CL_OUT_OF_HOST_MEMORY;
            }
            strncpy(compilation->options, options, options_size);
        }
        compilation->build_status = CL_BUILD_IN_PROGRESS;
        if(pfn_notify && !notifier_ref)
            notifier_ref = (void*)compilation;
    }

    // Set the remote programs array, while checking the programs are valid
    // Concatenate the header names at the same time
    cl_program* programs = NULL;
    size_t *header_names_sizes = NULL;
    size_t header_names_size = 0;
    char* header_names = NULL;

    if(num_input_headers) {
        programs = (cl_program*)malloc(
            num_input_headers * sizeof(cl_program));
        header_names_sizes = (size_t*)malloc(
            num_input_headers * sizeof(size_t));
        if(!programs || !header_names_sizes) {
            free(devices);
            free(programs);
            free(header_names_sizes);
            for(cl_uint i = 0; i < num_devices; i++) {
                cl_uint j = program_device_id(program, device_list[i]);
                program_compile compilation = program->compilations[j];
                compilation->build_status = CL_BUILD_ERROR;
            }
            return CL_OUT_OF_HOST_MEMORY;
        }
        for(cl_uint i = 0; i < num_input_headers; i++) {
            if(!is_program(input_headers[i])) {
                free(devices);
                free(programs);
                free(header_names_sizes);
                for(cl_uint i = 0; i < num_devices; i++) {
                    cl_uint j = program_device_id(program, device_list[i]);
                    program_compile compilation = program->compilations[j];
                    compilation->build_status = CL_BUILD_ERROR;
                }
                return CL_INVALID_PROGRAM;
            }
            programs[i] = input_headers[i]->ptr;

            size_t str_size = (strlen(header_include_names[i]) + 1) * sizeof(char);
            if(!str_size) {
                free(devices);
                free(programs);
                free(header_names_sizes);
                for(cl_uint i = 0; i < num_devices; i++) {
                    cl_uint j = program_device_id(program, device_list[i]);
                    program_compile compilation = program->compilations[j];
                    compilation->build_status = CL_BUILD_ERROR;
                }
                return CL_OUT_OF_HOST_MEMORY;
            }
            header_names_sizes[i] = str_size;
            header_names_size += str_size;
        }
        header_names = concatenate_sources(num_input_headers,
                                           header_include_names,
                                           header_names_sizes);
        if(!header_names) {
            free(devices);
            free(programs);
            free(header_names_sizes);
            return CL_OUT_OF_HOST_MEMORY;
            for(cl_uint i = 0; i < num_devices; i++) {
                cl_uint j = program_device_id(program, device_list[i]);
                program_compile compilation = program->compilations[j];
                compilation->build_status = CL_BUILD_ERROR;
            }
        }
    }

    int err;
    const unsigned int method = ocland_clCompileProgram;
    ocland_server server = program->server;

    if(pfn_notify) {
        // Register the notifier
        notifier_append(server->cb_notifier,
                        notifier_ref,
                        &program_notifier,
                        (void*)program,
                        true);
    }

    // Call the remote peer
    lock(server);

    err = send_comm(server->socket, sizeof(unsigned int),
                                    sizeof(cl_program),
                                    sizeof(cl_uint),
                                    num_devices * sizeof(cl_device_id),
                                    sizeof(size_t),
                                    options_size,
                                    sizeof(void*),
                                    sizeof(cl_uint),
                                    // We can start getting zeroes here
                                    num_input_headers * sizeof(cl_program),
                                    num_input_headers * sizeof(size_t),
                                    header_names_size,
                                    0,
                                    &method,
                                    &(program->ptr),
                                    &num_devices,
                                    devices,
                                    &options_size,
                                    options_ptr,
                                    &notifier_ref,
                                    &num_input_headers,
                                    programs,
                                    header_names_sizes,
                                    header_names,
                                    NULL);
    free(devices);
    free(programs);
    free(header_names_sizes);
    free(header_names);

    if(err) {
        for(cl_uint i = 0; i < num_devices; i++) {
            cl_uint j = program_device_id(program, device_list[i]);
            program_compile compilation = program->compilations[j];
            compilation->build_status = CL_BUILD_ERROR;
        }
        if(notifier_ref)
            notifier_remove(server->cb_notifier, notifier_ref);
        unlock(server);
        return CL_OUT_OF_HOST_MEMORY;
    }

    // Even if we asynchronously called to compile, we would get an answer from
    // server, to check that compiling options were right, the programs all
    // belonged to the same context, etc...
    cl_int flag;
    recv_pending rest;
    err = recv_comm(server->socket, &rest, sizeof(cl_int), 0,
                                           &flag, NULL);
    unlock(server);

    if(err)
        flag = CL_OUT_OF_HOST_MEMORY;
    if(flag != CL_SUCCESS) {
        for(cl_uint i = 0; i < num_devices; i++) {
            cl_uint j = program_device_id(program, device_list[i]);
            program_compile compilation = program->compilations[j];
            compilation->build_status = CL_BUILD_ERROR;
        }
        free(rest.data);
        if(notifier_ref)
            notifier_remove(server->cb_notifier, notifier_ref);
        return flag;
    }

    if(!pfn_notify) {
        // Synced build process, the server shall notify the building result
        // by this channel, so we are just simply calling the compilation
        // notifier endpoint synchronously. Thus, the server shall reply with
        // the same information as it would be sent in case of async compilation
        program_notifier(NULL, rest.size, rest.data, program);
    }
    free(rest.data);

    return CL_SUCCESS;
}

cl_program oclandLinkProgram(cl_context            context ,
                             cl_uint               num_devices ,
                             const cl_device_id *  device_list ,
                             const char *          options ,
                             cl_uint               num_input_programs ,
                             const cl_program *    input_programs ,
                             void (CL_CALLBACK *   pfn_notify)(cl_program  program , void *  user_data),
                             void *                user_data ,
                             cl_int *              errcode_ret)
{
    if(!programs)
        programs = programs_new();

    if(!is_context(context)) {
        if(errcode_ret) *errcode_ret = CL_INVALID_CONTEXT;
        return NULL;
    }

    if(!num_devices) {
        num_devices = context->num_devices;
        device_list = context->devices;
    }

    // We need to get ready the program to can eventually setup the notifier
    ocland_server server = context->server;
    cl_program data = new_program(context,
                                  NULL,
                                  num_devices,
                                  device_list);
    if(!data) {
        if(errcode_ret) *errcode_ret = CL_OUT_OF_HOST_MEMORY;
        return NULL;
    }

    void* notifier_ref = NULL;
    if(pfn_notify) {
        notifier_ref = (void*)(data->compilations[0]);
        notifier_append(server->cb_notifier,
                        notifier_ref,
                        &program_notifier,
                        (void*)data,
                        true);
    }

    // Set the remote devices array
    const size_t options_size = (strlen(options) + 1) * sizeof(char);
    cl_device_id* devices = (cl_device_id*)malloc(
        num_devices * sizeof(cl_device_id));
    if(!devices) {
        del_program(data);
        if(errcode_ret) *errcode_ret = CL_OUT_OF_HOST_MEMORY;
        return NULL;
    }
    for(cl_uint i = 0; i < num_devices; i++) {
        if(!is_device(device_list[i])) {
            del_program(data);
            free(devices);
            if(errcode_ret) *errcode_ret = CL_INVALID_DEVICE;
            return NULL;
        }
        devices[i] = device_list[i]->ptr;
        data->compilations[i]->build_status = CL_BUILD_IN_PROGRESS;
        data->compilations[i]->pfn_notify = pfn_notify;
        data->compilations[i]->user_data = user_data;
        data->compilations[i]->options = (char*)malloc(options_size);
        if(!data->compilations[i]->options) {
            del_program(data);
            free(devices);
            if(errcode_ret) *errcode_ret = CL_OUT_OF_HOST_MEMORY;
            return NULL;
        }
        strncpy(data->compilations[i]->options, options, options_size);
    }

    // Set the remote programs array, while checking the programs are valid
    cl_program* programs = (cl_program*)malloc(
        num_input_programs * sizeof(cl_program));
    if(!programs) {
        del_program(data);
        free(devices);
        free(programs);
        if(errcode_ret) *errcode_ret = CL_OUT_OF_HOST_MEMORY;
        return NULL;
    }
    for(cl_uint i = 0; i < num_input_programs; i++) {
        if(!is_program(input_programs[i])) {
            del_program(data);
            free(devices);
            free(programs);
            if(errcode_ret) *errcode_ret = CL_INVALID_PROGRAM;
            return NULL;
        }
        programs[i] = input_programs[i]->ptr;
    }

    // Call the remote peer
    int err;
    const unsigned int method = ocland_clLinkProgram;
    lock(server);

    err = send_comm(server->socket, sizeof(unsigned int),
                                    sizeof(cl_context),
                                    sizeof(cl_uint),
                                    num_devices * sizeof(cl_device_id),
                                    sizeof(size_t),
                                    options_size,
                                    sizeof(cl_uint),
                                    num_input_programs * sizeof(cl_program),
                                    sizeof(void*),
                                    0,
                                    &method,
                                    &(context->ptr),
                                    &num_devices,
                                    devices,
                                    &options_size,
                                    options,
                                    &num_input_programs,
                                    programs,
                                    &notifier_ref,
                                    NULL);
    free(devices);
    free(programs);

    if(err) {
        unlock(server);
        del_program(data);
        if(errcode_ret) *errcode_ret = CL_OUT_OF_HOST_MEMORY;
        return NULL;
    }

    cl_int flag;
    cl_program program;
    recv_pending rest;
    err = recv_comm(server->socket, &rest, sizeof(cl_int),
                                           sizeof(cl_program),
                                           0,
                                           &flag,
                                           &program,
                                           NULL);

    unlock(server);

    if(err)
        flag = CL_OUT_OF_HOST_MEMORY;

    if(errcode_ret)
        *errcode_ret = flag;

    if(flag != CL_SUCCESS) {
        del_program(data);
        free(rest.data);
        return NULL;
    }

    // Since we have built the program before we have the remote reference, we
    // must set it now
    data->ptr = program;

    if(!pfn_notify) {
        // Synced build process, the server shall notify the building result
        // by this channel, so we are just simply calling the compilation
        // notifier endpoint synchronously. Thus, the server shall reply with
        // the same information as it would be sent in case of async compilation
        program_notifier(NULL, rest.size, rest.data, data);
    }
    free(rest.data);

    return data;
}

cl_int oclandUnloadPlatformCompiler(cl_platform_id platform)
{
    if(!is_platform(platform))
        return CL_INVALID_PROGRAM;

    int err;
    const unsigned int method = ocland_clUnloadPlatformCompiler;
    ocland_server server = platform->server;

    // Call the remote peer
    lock(server);

    err = send_comm(server->socket, sizeof(unsigned int),
                                    sizeof(cl_platform_id),
                                    0,
                                    &method,
                                    &(platform->ptr),
                                    NULL);
    cl_int flag;
    err = recv_comm(server->socket, NULL, sizeof(cl_int), 0,
                                          &flag, NULL);
    unlock(server);

    if(err) {
        return CL_OUT_OF_HOST_MEMORY;
    }

    return flag;
}
