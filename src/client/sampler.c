/*
 *  This file is part of ocland, a free cloud OpenCL interface.
 *  Copyright (C) 2012-2019  Jose Luis Cercos Pita <jl.cercos@upm.es>
 *
 *  ocland is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  ocland is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with ocland.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <ocland/common/dataExchange.h>
#include <ocland/client/context.h>
#include <ocland/client/sampler.h>

#include <ocland/common/typed_list.h>
DEFINE_LIST(cl_sampler, samplers)

/// List of samplers
static List* samplers = NULL;

cl_sampler get_sampler(cl_sampler remote_sampler)
{
    if(!samplers_len(samplers))
        return NULL;
    for(samplers_node sampler_node = samplers_front(samplers);
        !samplers_is_null(sampler_node);
        sampler_node = samplers_next(sampler_node))
    {
        cl_sampler local_sampler = samplers_data(sampler_node);
        if(local_sampler->ptr == remote_sampler)
            return local_sampler;
    }

    return NULL;
}

bool is_sampler(cl_sampler sampler)
{
    if(!samplers)
        samplers = samplers_new();
    return samplers_find(samplers, sampler) < samplers_len(samplers);
}

cl_sampler oclandCreateSampler(cl_context           context ,
                               cl_bool              normalized_coords ,
                               cl_addressing_mode   addressing_mode ,
                               cl_filter_mode       filter_mode ,
                               cl_int *             errcode_ret)
{
    if(!samplers)
        samplers = samplers_new();

    if(!is_context(context)) {
        if(errcode_ret) *errcode_ret = CL_INVALID_CONTEXT;
        return NULL;
    }

    // Call the remote peer
    int err;
    const unsigned int method = ocland_clCreateSampler;
    ocland_server server = context->server;
    lock(server);

    err = send_comm(server->socket, sizeof(unsigned int),
                                    sizeof(cl_context),
                                    sizeof(cl_bool),
                                    sizeof(cl_addressing_mode),
                                    sizeof(cl_filter_mode),
                                    0,
                                    &method,
                                    &(context->ptr),
                                    &normalized_coords,
                                    &addressing_mode,
                                    &filter_mode,
                                    NULL);
    if(err) {
        unlock(server);
        if(errcode_ret) *errcode_ret = CL_OUT_OF_HOST_MEMORY;
        return NULL;
    }

    cl_int flag;
    cl_sampler sampler;
    err = recv_comm(server->socket, NULL, sizeof(cl_int),
                                          sizeof(cl_sampler),
                                          0,
                                          &flag,
                                          &sampler,
                                          NULL);

    unlock(server);

    if(err) {
        if(errcode_ret) *errcode_ret = CL_OUT_OF_HOST_MEMORY;
        return NULL;
    }

    if(errcode_ret) *errcode_ret = flag;

    if(flag != CL_SUCCESS) {
        return NULL;
    }

    // Register the new samplerory object (we are just simply not setting image
    // data)
    cl_sampler data = (cl_sampler)malloc(
       sizeof(struct _cl_sampler));
    data->dispatch = context->dispatch;
    data->ptr = sampler;
    data->rcount = 1;
    data->server = context->server;
    data->context = context;
    data->normalized_coords = normalized_coords;
    data->addressing_mode = addressing_mode;
    data->filter_mode = filter_mode;
    samplers_push_back(samplers, data);
    oclandRetainContext(data->context);

    return data;
}

cl_int oclandRetainSampler(cl_sampler sampler)
{
    if(!is_sampler(sampler))
        return CL_INVALID_MEM_OBJECT;

    sampler->rcount++;
    return CL_SUCCESS;
}

cl_int oclandReleaseSampler(cl_sampler sampler)
{
    if(!is_sampler(sampler))
        return CL_INVALID_MEM_OBJECT;

    if(sampler->rcount > 1) {
        sampler->rcount--;
        return CL_SUCCESS;
    }

    // The sampler shall be destroyed, including from server
    int err;
    cl_int flag;
    const unsigned int method = ocland_clReleaseSampler;
    ocland_server server = sampler->server;
    lock(server);

    err = send_comm(server->socket, sizeof(unsigned int),
                                    sizeof(cl_sampler),
                                    0,
                                    &method,
                                    &(sampler->ptr),
                                    NULL);
    if(err) {
        unlock(server);
        return CL_OUT_OF_HOST_MEMORY;
    }

    err = recv_comm(server->socket, NULL, sizeof(cl_int),
                                          0,
                                          &flag,
                                          NULL);

    unlock(server);

    if(err) {
        return CL_OUT_OF_HOST_MEMORY;
    }

    if(flag != CL_SUCCESS){
        return flag;
    }

    // So we can remove the Memory object
    oclandReleaseContext(sampler->context);
    samplers_remove_at(samplers, samplers_find(samplers, sampler));
    free(sampler);

    return CL_SUCCESS;
}

/**
 * Get info from the sampler
 *
 * This method only returns data that shall not be queried to the server
 * @param  sampler Memory object
 * @param  param_name Parameter
 * @param  param_value_size Allocated samplerory inside #param_value
 * @param  param_value Allocated samplerory where the queried data shall be copy
 * @param  param_value_size_ret Returned size of the queried data
 * @return 1 if the queried data cannot be extracted from the
 * local instance, and shall be therefore queried to the server. CL_SUCCESS
 * if the query can be processed. CL_INVALID_VALUE if the query can be processed
 * but param_value_size is smaller than the size of the returned value.
 */
cl_int get_sampler_info(cl_sampler       sampler,
                        cl_sampler_info  param_name,
                        size_t           param_value_size,
                        void *           param_value,
                        size_t *         param_value_size_ret)
{
    size_t size_ret = 0;
    void* data_ret = NULL;
    switch(param_name) {
        case CL_SAMPLER_REFERENCE_COUNT:
            size_ret = sizeof(cl_uint);
            data_ret = &(sampler->rcount);
            break;
        case CL_SAMPLER_CONTEXT:
            size_ret = sizeof(cl_context);
            data_ret = &(sampler->context);
            break;
        case CL_SAMPLER_NORMALIZED_COORDS:
            size_ret = sizeof(cl_bool);
            data_ret = &(sampler->normalized_coords);
            break;
        case CL_SAMPLER_ADDRESSING_MODE:
            size_ret = sizeof(cl_addressing_mode);
            data_ret = &(sampler->addressing_mode);
            break;
        case CL_SAMPLER_FILTER_MODE:
            size_ret = sizeof(cl_filter_mode);
            data_ret = &(sampler->filter_mode);
            break;
        default:
            return 1;
    }

    if(param_value_size_ret)
        *param_value_size_ret = size_ret;
    if(param_value) {
        if(param_value_size < size_ret)
            return CL_INVALID_VALUE;
        memcpy(param_value, data_ret, size_ret);
    }

    return CL_SUCCESS;
}

cl_int oclandGetSamplerInfo(cl_sampler          sampler ,
                            cl_sampler_info     param_name ,
                            size_t              param_value_size ,
                            void *              param_value ,
                            size_t *            param_value_size_ret)
{
    if(!is_sampler(sampler))
        return CL_INVALID_SAMPLER;

    cl_int flag;
    flag = get_sampler_info(sampler,
                            param_name,
                            param_value_size,
                            param_value,
                            param_value_size_ret);
    if(flag != 1)  // flag = 1 means we should call the server
        return flag;

    // Call the remote peer to try to get the missing info
    const unsigned int method = ocland_clGetMemObjectInfo;
    return oclandGetGenericInfo(sampler->server,
                                param_value_size,
                                param_value,
                                param_value_size_ret,
                                // Variable arguments
                                sizeof(unsigned int),
                                sizeof(cl_sampler),
                                sizeof(cl_sampler_info),
                                sizeof(size_t),
                                0,
                                &method,
                                &(sampler->ptr),
                                &param_name,
                                &param_value_size,
                                NULL);
}

// -------------------------------------------- //
//                                              //
// OpenCL 2.0 methods                           //
//                                              //
// -------------------------------------------- //
