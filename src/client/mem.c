/*
 *  This file is part of ocland, a free cloud OpenCL interface.
 *  Copyright (C) 2012-2019  Jose Luis Cercos Pita <jl.cercos@upm.es>
 *
 *  ocland is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  ocland is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with ocland.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <pthread.h>

#include <ocland/common/dataExchange.h>
#include <ocland/client/context.h>
#include <ocland/client/mem.h>

#include <ocland/common/typed_list.h>
DEFINE_LIST(cl_mem, mems)

/// List of mems
static List* mems = NULL;

cl_mem get_mem(cl_mem remote_mem)
{
    if(!mems_len(mems))
        return NULL;
    for(mems_node mem_node = mems_front(mems);
        !mems_is_null(mem_node);
        mem_node = mems_next(mem_node))
    {
        cl_mem local_mem = mems_data(mem_node);
        if(local_mem->ptr == remote_mem)
            return local_mem;
    }

    return NULL;
}

bool is_mem(cl_mem mem)
{
    if(!mems)
        mems = mems_new();
    return mems_find(mems, mem) < mems_len(mems);
}

cl_mem oclandCreateBuffer(cl_context    context ,
                          cl_mem_flags  flags ,
                          size_t        size ,
                          void *        host_ptr ,
                          cl_int *      errcode_ret)
{
    if(!mems)
        mems = mems_new();

    if(!is_context(context)) {
        if(errcode_ret) *errcode_ret = CL_INVALID_CONTEXT;
        return NULL;
    }

    // Call the remote peer
    int err;
    const unsigned int method = ocland_clCreateBuffer;
    ocland_server server = context->server;
    lock(server);

    if(flags & CL_MEM_COPY_HOST_PTR) {
        err = send_comm(server->socket, sizeof(unsigned int),
                                        sizeof(cl_context),
                                        sizeof(cl_mem_flags),
                                        sizeof(size_t),
                                        size,
                                        0,
                                        &method,
                                        &(context->ptr),
                                        &flags,
                                        &size,
                                        host_ptr,
                                        NULL);
    } else {
        err = send_comm(server->socket, sizeof(unsigned int),
                                        sizeof(cl_context),
                                        sizeof(cl_mem_flags),
                                        sizeof(size_t),
                                        0,
                                        &method,
                                        &(context->ptr),
                                        &flags,
                                        &size,
                                        NULL);
    }
    if(err) {
        unlock(server);
        if(errcode_ret) *errcode_ret = CL_OUT_OF_HOST_MEMORY;
        return NULL;
    }

    cl_int flag;
    cl_mem mem;
    err = recv_comm(server->socket, NULL, sizeof(cl_int),
                                          sizeof(cl_mem),
                                          0,
                                          &flag,
                                          &mem,
                                          NULL);

    unlock(server);

    if(err) {
        if(errcode_ret) *errcode_ret = CL_OUT_OF_HOST_MEMORY;
        return NULL;
    }

    if(errcode_ret) *errcode_ret = flag;

    if(flag != CL_SUCCESS) {
        return NULL;
    }

    // Register the new memory object (we are just simply not setting image
    // data)
    cl_mem data = (cl_mem)malloc(
       sizeof(struct _cl_mem));
    data->dispatch = context->dispatch;
    data->ptr = mem;
    data->rcount = 1;
    data->server = context->server;
    data->type = CL_MEM_OBJECT_BUFFER;
    data->flags = flags;
    data->size = size;
    if(flags & CL_MEM_USE_HOST_PTR)
        data->host_mem = host_ptr;
    else
        data->host_mem = NULL;
    data->context = context;
    data->sub_mem.parent = NULL;
    data->sub_mem.offset = 0;
    data->pfn_notify = NULL;
    data->user_data = NULL;
    mems_push_back(mems, data);
    oclandRetainContext(data->context);

    return data;
}

cl_int oclandRetainMemObject(cl_mem mem)
{
    if(!is_mem(mem))
        return CL_INVALID_MEM_OBJECT;

    mem->rcount++;
    return CL_SUCCESS;
}

void mem_pfn_notify(cl_mem mem);

cl_int oclandReleaseMemObject(cl_mem mem)
{
    if(!is_mem(mem))
        return CL_INVALID_MEM_OBJECT;

    if(mem->rcount > 1) {
        mem->rcount--;
        return CL_SUCCESS;
    }

    // The mem shall be destroyed, including from server
    int err;
    cl_int flag;
    const unsigned int method = ocland_clReleaseMemObject;
    ocland_server server = mem->server;
    lock(server);

    err = send_comm(server->socket, sizeof(unsigned int),
                                    sizeof(cl_mem),
                                    0,
                                    &method,
                                    &(mem->ptr),
                                    NULL);
    if(err) {
        unlock(server);
        return CL_OUT_OF_HOST_MEMORY;
    }

    err = recv_comm(server->socket, NULL, sizeof(cl_int),
                                          0,
                                          &flag,
                                          NULL);

    unlock(server);

    if(err) {
        return CL_OUT_OF_HOST_MEMORY;
    }

    if(flag != CL_SUCCESS){
        return flag;
    }

    // So we can remove the Memory object
    oclandReleaseContext(mem->context);
    if(mem->sub_mem.parent)
        oclandReleaseMemObject(mem->sub_mem.parent);
    mem_pfn_notify(mem);
    mems_remove_at(mems, mems_find(mems, mem));
    free(mem);

    return CL_SUCCESS;
}

cl_int oclandGetSupportedImageFormats(cl_context           context,
                                      cl_mem_flags         flags,
                                      cl_mem_object_type   image_type ,
                                      cl_uint              num_entries ,
                                      cl_image_format *    image_formats ,
                                      cl_uint *            num_image_formats)
{
    if(!is_context(context))
        return CL_INVALID_CONTEXT;

    // We can hack oclandGetGenericInfo(), however, it will make it less
    // readable, and anyway we shall later make all the checks regarding
    // returned sizes and so on...
    cl_int flag;
    int err;

    ocland_server server = context->server;
    lock(server);

    const unsigned int method = ocland_clGetSupportedImageFormats;
    err = send_comm(server->socket, sizeof(unsigned int),
                                    sizeof(cl_context),
                                    sizeof(cl_mem_flags),
                                    sizeof(cl_mem_object_type),
                                    sizeof(cl_uint),
                                    0,
                                    &method,
                                    &(context->ptr),
                                    &flags,
                                    &image_type,
                                    &num_entries,
                                    NULL);
    if(err) {
        unlock(server);
        return CL_OUT_OF_HOST_MEMORY;
    }

    recv_pending rest;
    cl_uint num_ret;
    err = recv_comm(server->socket, &rest, sizeof(cl_int),
                                           sizeof(cl_uint),
                                           0,
                                           &flag,
                                           &num_ret,
                                           NULL);

    unlock(server);

    if(err) {
        free(rest.data);
        return CL_OUT_OF_HOST_MEMORY;
    }
    if(flag != CL_SUCCESS){
        free(rest.data);
        return flag;
    }

    if(num_image_formats) *num_image_formats = num_ret;
    // The remaining message is already the param_value
    if(image_formats) {
        if(num_entries < num_ret) {
            free(rest.data);
            return CL_INVALID_VALUE;
        }
        if(rest.size != num_ret * sizeof(cl_image_format)) {
            free(rest.data);
            return CL_OUT_OF_HOST_MEMORY;
        }
        memcpy(image_formats, rest.data, num_ret * sizeof(cl_image_format));
    }
    free(rest.data);

    return CL_SUCCESS;
}

/**
 * Get info from the mem
 *
 * This method only returns data that shall not be queried to the server
 * @param  mem Memory object
 * @param  param_name Parameter
 * @param  param_value_size Allocated memory inside #param_value
 * @param  param_value Allocated memory where the queried data shall be copy
 * @param  param_value_size_ret Returned size of the queried data
 * @return 1 if the queried data cannot be extracted from the
 * local instance, and shall be therefore queried to the server. CL_SUCCESS
 * if the query can be processed. CL_INVALID_VALUE if the query can be processed
 * but param_value_size is smaller than the size of the returned value.
 */
cl_int get_mem_info(cl_mem       mem,
                    cl_mem_info  param_name,
                    size_t       param_value_size,
                    void *       param_value,
                    size_t *     param_value_size_ret)
{
    size_t size_ret = 0;
    void* data_ret = NULL;
    cl_bool false_var = CL_FALSE;
    switch(param_name) {
        case CL_MEM_TYPE:
            size_ret = sizeof(cl_mem_object_type);
            data_ret = &(mem->type);
            break;
        case CL_MEM_FLAGS:
            size_ret = sizeof(cl_mem_flags);
            data_ret = &(mem->flags);
            break;
        case CL_MEM_SIZE:
            size_ret = sizeof(size_t);
            data_ret = &(mem->size);
            break;
        case CL_MEM_HOST_PTR:
            size_ret = sizeof(void *);
            data_ret = &(mem->host_mem);
            break;
        case CL_MEM_REFERENCE_COUNT:
            size_ret = sizeof(cl_uint);
            data_ret = &(mem->rcount);
            break;
        case CL_MEM_CONTEXT:
            size_ret = sizeof(cl_context);
            data_ret = &(mem->context);
            break;
        case CL_MEM_ASSOCIATED_MEMOBJECT:
            size_ret = sizeof(cl_mem);
            data_ret = &(mem->sub_mem.parent);
            break;
        case CL_MEM_OFFSET:
            size_ret = sizeof(size_t);
            data_ret = &(mem->sub_mem.offset);
            break;
        case CL_MEM_USES_SVM_POINTER:
            size_ret = sizeof(cl_bool);
            data_ret = &false_var;
            break;
        default:
            return 1;
    }

    if(param_value_size_ret)
        *param_value_size_ret = size_ret;
    if(param_value) {
        if(param_value_size < size_ret)
            return CL_INVALID_VALUE;
        memcpy(param_value, data_ret, size_ret);
    }

    return CL_SUCCESS;
}

cl_int oclandGetMemObjectInfo(cl_mem            mem ,
                              cl_mem_info       param_name ,
                              size_t            param_value_size ,
                              void *            param_value ,
                              size_t *          param_value_size_ret)
{
    if(!is_mem(mem))
        return CL_INVALID_MEM_OBJECT;

    cl_int flag;
    flag = get_mem_info(mem,
                        param_name,
                        param_value_size,
                        param_value,
                        param_value_size_ret);
    if(flag != 1)  // flag = 1 means we should call the server
        return flag;

    // Call the remote peer to try to get the missing info
    const unsigned int method = ocland_clGetMemObjectInfo;
    return oclandGetGenericInfo(mem->server,
                                param_value_size,
                                param_value,
                                param_value_size_ret,
                                // Variable arguments
                                sizeof(unsigned int),
                                sizeof(cl_mem),
                                sizeof(cl_mem_info),
                                sizeof(size_t),
                                0,
                                &method,
                                &(mem->ptr),
                                &param_name,
                                &param_value_size,
                                NULL);
}

/**
 * Check whether the memory object is an image or not
 * @param  mem Memory object
 * @return CL_TRUE if it is and image, CL_FALSE otherwise
 */
cl_bool is_mem_an_image(cl_mem mem)
{
    return (    (mem->type == CL_MEM_OBJECT_IMAGE1D)
             || (mem->type == CL_MEM_OBJECT_IMAGE1D_BUFFER)
             || (mem->type == CL_MEM_OBJECT_IMAGE1D_ARRAY)
             || (mem->type == CL_MEM_OBJECT_IMAGE2D)
             || (mem->type == CL_MEM_OBJECT_IMAGE2D_ARRAY)
             || (mem->type == CL_MEM_OBJECT_IMAGE3D));
}

/**
 * Get info from the image
 *
 * This method only returns data that shall not be queried to the server.
 * @param  mem Memory object
 * @param  param_name Parameter
 * @param  param_value_size Allocated memory inside #param_value
 * @param  param_value Allocated memory where the queried data shall be copy
 * @param  param_value_size_ret Returned size of the queried data
 * @return 1 if the queried data cannot be extracted from the
 * local instance, and shall be therefore queried to the server. CL_SUCCESS
 * if the query can be processed. CL_INVALID_VALUE if the query can be processed
 * but param_value_size is smaller than the size of the returned value.
 * CL_INVALID_MEM_OBJECT if the memory object is not an image.
 */
cl_int get_image_info(cl_mem         mem,
                      cl_image_info  param_name,
                      size_t         param_value_size,
                      void *         param_value,
                      size_t *       param_value_size_ret)
{

    size_t size_ret = 0;
    void* data_ret = NULL;
    switch(param_name) {
        case CL_IMAGE_FORMAT:
            size_ret = sizeof(cl_image_format);
            data_ret = &(mem->image.format);
            break;
        case CL_IMAGE_ELEMENT_SIZE:
            size_ret = sizeof(size_t);
            data_ret = &(mem->image.element_size);
            break;
        case CL_IMAGE_ROW_PITCH:
            size_ret = sizeof(size_t);
            data_ret = &(mem->image.desc.image_row_pitch);
            break;
        case CL_IMAGE_SLICE_PITCH:
            size_ret = sizeof(size_t);
            data_ret = &(mem->image.desc.image_slice_pitch);
            break;
        case CL_IMAGE_WIDTH:
            size_ret = sizeof(size_t);
            data_ret = &(mem->image.desc.image_width);
            break;
        case CL_IMAGE_HEIGHT:
            size_ret = sizeof(size_t);
            data_ret = &(mem->image.desc.image_height);
            break;
        case CL_IMAGE_DEPTH:
            size_ret = sizeof(size_t);
            data_ret = &(mem->image.desc.image_depth);
            break;
        case CL_IMAGE_ARRAY_SIZE:
            size_ret = sizeof(size_t);
            data_ret = &(mem->image.desc.image_array_size);
            break;
        case CL_IMAGE_NUM_MIP_LEVELS:
            size_ret = sizeof(cl_uint);
            data_ret = &(mem->image.desc.num_mip_levels);
            break;
        case CL_IMAGE_NUM_SAMPLES:
            size_ret = sizeof(cl_uint);
            data_ret = &(mem->image.desc.num_samples);
            break;
        default:
            return 1;
    }

    if(param_value_size_ret)
        *param_value_size_ret = size_ret;
    if(param_value) {
        if(param_value_size < size_ret)
            return CL_INVALID_VALUE;
        memcpy(param_value, data_ret, size_ret);
    }

    return CL_SUCCESS;
}

cl_int oclandGetImageInfo(cl_mem            image ,
                          cl_image_info     param_name ,
                          size_t            param_value_size ,
                          void *            param_value ,
                          size_t *          param_value_size_ret)
{
    if(!is_mem(image) || !is_mem_an_image(image))
        return CL_INVALID_MEM_OBJECT;

    cl_int flag;
    flag = get_image_info(image,
                          param_name,
                          param_value_size,
                          param_value,
                          param_value_size_ret);
    if(flag != 1)  // flag = 1 means we should call the server
        return flag;

    // Call the remote peer to try to get the missing info
    const unsigned int method = ocland_clGetImageInfo;
    return oclandGetGenericInfo(image->server,
                                param_value_size,
                                param_value,
                                param_value_size_ret,
                                // Variable arguments
                                sizeof(unsigned int),
                                sizeof(cl_mem),
                                sizeof(cl_image_info),
                                sizeof(size_t),
                                0,
                                &method,
                                &(image->ptr),
                                &param_name,
                                &param_value_size,
                                NULL);
}

// -------------------------------------------- //
//                                              //
// OpenCL 1.1 methods                           //
//                                              //
// -------------------------------------------- //

cl_mem oclandCreateSubBuffer(cl_mem                    buffer ,
                             cl_mem_flags              flags ,
                             cl_buffer_create_type     buffer_create_type ,
                             const void *              buffer_create_info ,
                             cl_int *                  errcode_ret)
{
    if(    !is_mem(buffer)
        || (buffer->type != CL_MEM_OBJECT_BUFFER)
        || buffer->sub_mem.parent)
    {
        if(errcode_ret) *errcode_ret = CL_INVALID_MEM_OBJECT;
        return NULL;
    }

    // Check the subbuffer info. At the write of these lines (OpenCL 2.1), just
    // the generation by means of CL_BUFFER_CREATE_TYPE_REGION is accepted
    if(buffer_create_type != CL_BUFFER_CREATE_TYPE_REGION) {
        if(errcode_ret) *errcode_ret = CL_INVALID_VALUE;
        return NULL;
    }
    size_t param_size = sizeof(cl_buffer_region);
    const cl_buffer_region region = *(cl_buffer_region *)buffer_create_info;

    // Call the remote peer
    int err;
    const unsigned int method = ocland_clCreateSubBuffer;
    ocland_server server = buffer->server;
    lock(server);

    err = send_comm(server->socket, sizeof(unsigned int),
                                    sizeof(cl_mem),
                                    sizeof(cl_mem_flags),
                                    sizeof(cl_buffer_create_type),
                                    sizeof(size_t),
                                    param_size,
                                    0,
                                    &method,
                                    &(buffer->ptr),
                                    &flags,
                                    &buffer_create_type,
                                    &param_size,
                                    buffer_create_info,
                                    NULL);
    if(err) {
        unlock(server);
        if(errcode_ret) *errcode_ret = CL_OUT_OF_HOST_MEMORY;
        return NULL;
    }

    cl_int flag;
    cl_mem mem;
    err = recv_comm(server->socket, NULL, sizeof(cl_int),
                                          sizeof(cl_mem),
                                          0,
                                          &flag,
                                          &mem,
                                          NULL);

    unlock(server);

    if(err) {
        if(errcode_ret) *errcode_ret = CL_OUT_OF_HOST_MEMORY;
        return NULL;
    }

    if(errcode_ret) *errcode_ret = flag;

    if(flag != CL_SUCCESS) {
        return NULL;
    }

    // Register the new memory object (we are just simply not setting image
    // data)
    cl_mem data = (cl_mem)malloc(
       sizeof(struct _cl_mem));
    data->dispatch = buffer->dispatch;
    data->ptr = mem;
    data->rcount = 1;
    data->server = buffer->server;
    data->type = CL_MEM_OBJECT_BUFFER;
    data->flags = flags;
    data->size = region.size;
    if(buffer->host_mem)
        data->host_mem = (char*)buffer->host_mem + region.origin;
    else
        data->host_mem = NULL;
    data->context = buffer->context;
    data->sub_mem.parent = buffer;
    data->sub_mem.offset = region.origin;
    data->pfn_notify = NULL;
    data->user_data = NULL;
    mems_push_back(mems, data);
    oclandRetainContext(data->context);
    oclandRetainMemObject(data->sub_mem.parent);

    return data;
}

/**
 * Data required to asynchronously launch a memory object destruction event
 */
typedef struct _pfn_notify_thread_data {
    /// Reference to the object, which will be probably destroyed
    cl_mem mem;
    /// Function to call
    void (CL_CALLBACK * pfn_notify)(cl_mem  memobj , void* user_data);
    /// User data to be passed as argument to pfn_notify
    void * user_data;
} pfn_notify_thread_data;

/**
 * Parallel thread to asynchronously call the memory object destruction callback
 * @param  casted_data Pointer to a pfn_notify_thread_data structure
 * @return             NULL
 */
void *mem_pfn_notify_thread(void* casted_data)
{
    pfn_notify_thread_data *data = (pfn_notify_thread_data*)casted_data;
    data->pfn_notify(data->mem, data->user_data);
    return NULL;
}

/**
 * Asynchronously call a memory object destruction callback, in case it was set
 * with clSetMemObjectDestructorCallback()
 * @param mem Memory object with an eventual callback registered
 */
void mem_pfn_notify(cl_mem mem)
{
    if(!mem->pfn_notify)
        return;

    pfn_notify_thread_data data = {mem, mem->pfn_notify, mem->user_data};

    pthread_t t;
    if(pthread_create(&t, NULL, mem_pfn_notify_thread, (void*) &data))
        return;
    pthread_detach(t);
}

cl_int oclandSetMemObjectDestructorCallback(cl_mem  mem ,
                                            void (CL_CALLBACK * pfn_notify)(cl_mem  memobj , void* user_data),
                                            void * user_data)
{
    if(!is_mem(mem))
        return CL_INVALID_MEM_OBJECT;

    mem->pfn_notify = pfn_notify;
    mem->user_data = user_data;

    return CL_SUCCESS;
}

// -------------------------------------------- //
//                                              //
// OpenCL 1.2 methods                           //
//                                              //
// -------------------------------------------- //

/**
 * Give the number of image channels
 * @param  format Image format
 * @return Number of channels
 */
cl_uint image_num_channels(cl_image_format format)
{
    switch(format.image_channel_order) {
        case CL_R:
        case CL_A:
        case CL_INTENSITY:
        case CL_LUMINANCE:
        case CL_DEPTH:
        case CL_DEPTH_STENCIL:
        case CL_RGB:
        case CL_RGBx:
            return 1;
        case CL_Rx:
        case CL_RG:
        case CL_RA:
            return 2;
        case CL_RGx:
        case CL_sRGB:
            return 3;
        case CL_RGBA:
        case CL_sRGBx:
        case CL_sRGBA:
        case CL_sBGRA:
        case CL_ARGB:
        case CL_BGRA:
        case CL_ABGR:
            return 4;
    }
    return 0;
}

/**
 * Give the image data type size
 * @param  format Image format
 * @return Data type size
 */
size_t image_type_size(cl_image_format format)
{
    switch(format.image_channel_data_type) {
        case CL_SNORM_INT8:
        case CL_UNORM_INT8:
        case CL_SIGNED_INT8:
        case CL_UNSIGNED_INT8:
            return 1;
        case CL_SNORM_INT16:
        case CL_UNORM_INT16:
        case CL_SIGNED_INT16:
        case CL_UNSIGNED_INT16:
        case CL_HALF_FLOAT:
        case CL_UNORM_SHORT_565:
        case CL_UNORM_SHORT_555:
            return 3;
        case CL_SIGNED_INT32:
        case CL_UNSIGNED_INT32:
        case CL_FLOAT:
        case CL_UNORM_INT_101010:
        case CL_UNORM_INT24:
            return 4;
    }
    return 0;
}

/**
 * Get the image element size
 * @param  format Image format
 * @return Size of the image
 */
size_t image_element_size(cl_image_format format)
{
    return image_num_channels(format) * image_type_size(format);
}

cl_mem oclandCreateImage(cl_context              context,
                         cl_mem_flags            flags,
                         const cl_image_format * image_format,
                         const cl_image_desc *   image_desc,
                         void *                  host_ptr,
                         cl_int *                errcode_ret)
{
    if(!mems)
        mems = mems_new();

    if(!is_context(context)) {
        if(errcode_ret) *errcode_ret = CL_INVALID_CONTEXT;
        return NULL;
    }

    // Create a copy of the image descriptor, recomputing the required data
    size_t element_size = image_type_size(*image_format);
    cl_image_desc desc = *image_desc;
    if(host_ptr) {
        if(!desc.image_row_pitch)
            desc.image_row_pitch = element_size * desc.image_width;
        if(!desc.image_slice_pitch)
            desc.image_slice_pitch = desc.image_row_pitch * desc.image_height;
    }

    size_t size = element_size;
    switch(desc.image_type) {
        case CL_MEM_OBJECT_IMAGE1D:
        case CL_MEM_OBJECT_IMAGE1D_BUFFER:
            size *= desc.image_width;
            break;
        case CL_MEM_OBJECT_IMAGE2D:
            size *= desc.image_width * desc.image_height;
            break;
        case CL_MEM_OBJECT_IMAGE3D:
            size *= desc.image_width * desc.image_height * desc.image_depth;
            break;
        case CL_MEM_OBJECT_IMAGE1D_ARRAY:
            size *= desc.image_width * desc.image_array_size;
            break;
        case CL_MEM_OBJECT_IMAGE2D_ARRAY:
            size *= desc.image_width * desc.image_height * desc.image_array_size;
            break;
        default:
            size = 0;
    }

    // Call the remote peer
    int err;
    const unsigned int method = ocland_clCreateImage;
    ocland_server server = context->server;
    lock(server);

    if(flags & CL_MEM_COPY_HOST_PTR) {
        err = send_comm(server->socket, sizeof(unsigned int),
                                        sizeof(cl_context),
                                        sizeof(cl_mem_flags),
                                        sizeof(cl_image_format),
                                        sizeof(cl_image_desc),
                                        sizeof(size_t),
                                        size,
                                        0,
                                        &method,
                                        &(context->ptr),
                                        &flags,
                                        image_format,
                                        &desc,
                                        &size,
                                        host_ptr,
                                        NULL);
    } else {
        // We are sending the size of the object, which is useful for debugging,
        // but it is not considered at all for the server object generation
        err = send_comm(server->socket, sizeof(unsigned int),
                                        sizeof(cl_context),
                                        sizeof(cl_mem_flags),
                                        sizeof(cl_image_format),
                                        sizeof(cl_image_desc),
                                        sizeof(size_t),
                                        0,
                                        &method,
                                        &(context->ptr),
                                        &flags,
                                        image_format,
                                        &desc,
                                        &size,
                                        NULL);
    }
    if(err) {
        unlock(server);
        if(errcode_ret) *errcode_ret = CL_OUT_OF_HOST_MEMORY;
        return NULL;
    }

    cl_int flag;
    cl_mem mem;
    err = recv_comm(server->socket, NULL, sizeof(cl_int),
                                          sizeof(cl_mem),
                                          0,
                                          &flag,
                                          &mem,
                                          NULL);

    unlock(server);

    if(err) {
        if(errcode_ret) *errcode_ret = CL_OUT_OF_HOST_MEMORY;
        return NULL;
    }

    if(errcode_ret) *errcode_ret = flag;

    if(flag != CL_SUCCESS) {
        return NULL;
    }

    // Register the new memory object
    cl_mem data = (cl_mem)malloc(
       sizeof(struct _cl_mem));
    data->dispatch = context->dispatch;
    data->ptr = mem;
    data->rcount = 1;
    data->server = context->server;
    data->type = desc.image_type;
    data->flags = flags;
    data->size = size;
    if(flags & CL_MEM_USE_HOST_PTR)
        data->host_mem = host_ptr;
    else
        data->host_mem = NULL;
    data->context = context;
    data->sub_mem.parent = NULL;
    data->sub_mem.offset = 0;
    data->pfn_notify = NULL;
    data->user_data = NULL;
    data->image.format = *image_format;
    data->image.element_size = element_size;
    data->image.desc = desc;
    mems_push_back(mems, data);
    oclandRetainContext(data->context);

    return data;
}
