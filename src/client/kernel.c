/*
 *  This file is part of ocland, a free cloud OpenCL interface.
 *  Copyright (C) 2012-2019  Jose Luis Cercos Pita <jl.cercos@upm.es>
 *
 *  ocland is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  ocland is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with ocland.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <ocland/common/dataExchange.h>
#include <ocland/client/context.h>
#include <ocland/client/mem.h>
#include <ocland/client/command_queue.h>
#include <ocland/client/program.h>
#include <ocland/client/kernel.h>

DEFINE_LIST(cl_kernel, kernels)

/**
 * Kernel argument
 */
struct _kernel_arg {
    /// Argument size
    size_t size;
    /// Argument value
    void* value;
    /// Has been the argument set?
    cl_bool set;
};

typedef struct _kernel_arg* kernel_arg;

DEFINE_LIST(kernel_arg, args)

/// List of kernels
static List* kernels = NULL;

cl_kernel get_kernel(cl_kernel remote_kernel)
{
    if(!kernels_len(kernels))
        return NULL;
    for(kernels_node kernel_node = kernels_front(kernels);
        !kernels_is_null(kernel_node);
        kernel_node = kernels_next(kernel_node))
    {
        cl_kernel local_kernel = kernels_data(kernel_node);
        if(local_kernel->ptr == remote_kernel)
            return local_kernel;
    }

    return NULL;
}

bool is_kernel(cl_kernel kernel)
{
    if(!kernels)
        kernels = kernels_new();
    return kernels_find(kernels, kernel) < kernels_len(kernels);
}

cl_kernel oclandCreateKernel(cl_program       program ,
                             const char *     kernel_name ,
                             cl_int *         errcode_ret)
{
    if(!kernels)
        kernels = kernels_new();

    if(!is_program(program)) {
        if(errcode_ret) *errcode_ret = CL_INVALID_PROGRAM;
        return NULL;
    }

    // Call the remote peer
    int err;
    const unsigned int method = ocland_clCreateKernel;
    const size_t kernel_name_size = (strlen(kernel_name) + 1) * sizeof(char);
    ocland_server server = program->server;
    lock(server);

    err = send_comm(server->socket, sizeof(unsigned int),
                                    sizeof(cl_program),
                                    sizeof(size_t),
                                    kernel_name_size,
                                    0,
                                    &method,
                                    &(program->ptr),
                                    &kernel_name_size,
                                    kernel_name,
                                    NULL);
    if(err) {
        unlock(server);
        if(errcode_ret) *errcode_ret = CL_OUT_OF_HOST_MEMORY;
        return NULL;
    }

    cl_int flag;
    cl_kernel kernel;
    err = recv_comm(server->socket, NULL, sizeof(cl_int),
                                          sizeof(cl_kernel),
                                          0,
                                          &flag,
                                          &kernel,
                                          NULL);
    unlock(server);

    if(err) {
        if(errcode_ret) *errcode_ret = CL_OUT_OF_HOST_MEMORY;
        return NULL;
    }

    if(errcode_ret) *errcode_ret = flag;

    if(flag != CL_SUCCESS)
        return NULL;

    // Register the new kernel
    cl_kernel data = (cl_kernel)malloc(
       sizeof(struct _cl_kernel));
    if(!data) {
        if(errcode_ret) *errcode_ret = CL_OUT_OF_HOST_MEMORY;
        return NULL;
    }
    data->function_name = (char*)malloc(kernel_name_size);
    if(!data->function_name) {
        free(data);
        if(errcode_ret) *errcode_ret = CL_OUT_OF_HOST_MEMORY;
        return NULL;
    }
    data->args = args_new();
    if(!data->args) {
        free(data);
        free(data->function_name);
        if(errcode_ret) *errcode_ret = CL_OUT_OF_HOST_MEMORY;
        return NULL;
    }

    data->dispatch = program->dispatch;
    data->ptr = kernel;
    data->rcount = 1;
    data->server = program->server;
    data->program = program;
    memcpy(data->function_name, kernel_name, kernel_name_size);
    kernels_push_back(kernels, data);
    oclandRetainProgram(data->program);
    data->program->num_kernels_attached++;

    return data;
}

cl_int oclandCreateKernelsInProgram(cl_program      program ,
                                    cl_uint         num_kernels ,
                                    cl_kernel *     out_kernels ,
                                    cl_uint *       num_kernels_ret)
{
    if(!kernels)
        kernels = kernels_new();

    if(!is_program(program))
        return CL_INVALID_PROGRAM;

    // For this methiod, the specification tolerates out_kernels = NULL, but
    // num_kernels != 0. To avoid problems, we are just correcting out_kernels
    if(!out_kernels)
        num_kernels = 0;

    // Call the remote peer
    int err;
    const unsigned int method = ocland_clCreateKernelsInProgram;
    ocland_server server = program->server;
    lock(server);

    err = send_comm(server->socket, sizeof(unsigned int),
                                    sizeof(cl_program),
                                    sizeof(cl_uint),
                                    0,
                                    &method,
                                    &(program->ptr),
                                    &num_kernels,
                                    NULL);
    if(err) {
        unlock(server);
        return CL_OUT_OF_HOST_MEMORY;
    }

    cl_int flag;
    cl_uint num_ret;
    recv_pending rest;
    err = recv_comm(server->socket, &rest, sizeof(cl_int),
                                           sizeof(cl_uint),
                                           0,
                                           &flag,
                                           &num_ret,
                                           NULL);
    unlock(server);

    if(err) {
        free(rest.data);
        return CL_OUT_OF_HOST_MEMORY;
    }

    if(flag != CL_SUCCESS) {
        free(rest.data);
        return flag;
    }

    if(num_kernels_ret)
        *num_kernels_ret = num_ret;

    if(!out_kernels) {
        free(rest.data);
        return flag;
    }

    if((num_kernels < num_ret)) {
        // No matters what server says, this is an error
        free(rest.data);
        return CL_INVALID_VALUE;
    }

    // The server have sent the new kernels references, as well as their
    // names
    if(rest.size < num_ret * sizeof(cl_kernel)) {
        free(rest.data);
        return CL_OUT_OF_HOST_MEMORY;
    }

    cl_kernel *refs = (cl_kernel*)malloc(num_ret * sizeof(cl_kernel));
    memcpy(refs, rest.data, num_ret * sizeof(cl_kernel));

    char *ptr = rest.data + num_ret * sizeof(cl_kernel);
    size_t remaining = rest.size - num_ret * sizeof(cl_kernel);

    for(cl_uint i = 0; i < num_ret; i++) {
        size_t function_name_size = strnlen(ptr, remaining);
        if(!function_name_size || (function_name_size >= remaining)) {
            free(rest.data);
            free(refs);
            return CL_OUT_OF_HOST_MEMORY;
        }
        function_name_size++;

        // Register the new kernel
        cl_kernel data = (cl_kernel)malloc(
           sizeof(struct _cl_kernel));
        if(!data) {
            free(rest.data);
            free(refs);
            return CL_OUT_OF_HOST_MEMORY;
        }
        data->function_name = (char*)malloc(function_name_size);
        if(!data->function_name) {
            free(rest.data);
            free(refs);
            free(data);
            return CL_OUT_OF_HOST_MEMORY;
        }
        data->args = args_new();
        if(!data->args) {
            free(rest.data);
            free(refs);
            free(data);
            free(data->function_name);
            return CL_OUT_OF_HOST_MEMORY;
        }

        data->dispatch = program->dispatch;
        data->ptr = refs[i];
        data->rcount = 1;
        data->server = program->server;
        data->program = program;
        memcpy(data->function_name, ptr, function_name_size);
        kernels_push_back(kernels, data);
        oclandRetainProgram(data->program);
        data->program->num_kernels_attached++;

        out_kernels[i] = data;

        // Move to next name
        ptr += function_name_size;
        remaining -= function_name_size;
    }

    free(rest.data);
    free(refs);

    if(remaining) {
        // Very strange... The server sent more data???
        return CL_OUT_OF_HOST_MEMORY;
    }

    return CL_SUCCESS;
}

cl_int oclandRetainKernel(cl_kernel kernel)
{
    if(!is_kernel(kernel))
        return CL_INVALID_KERNEL;

    kernel->rcount++;
    return CL_SUCCESS;
}

void del_kernel_arg(kernel_arg arg)
{
    if(arg->value)
        free(arg->value);
    free(arg);
}

cl_int oclandReleaseKernel(cl_kernel kernel)
{
    if(!is_kernel(kernel))
        return CL_INVALID_KERNEL;

    if(kernel->rcount > 1) {
        kernel->rcount--;
        return CL_SUCCESS;
    }

    // The kernel shall be destroyed, including from server
    int err;
    cl_int flag;
    const unsigned int method = ocland_clReleaseKernel;
    ocland_server server = kernel->server;
    lock(server);

    err = send_comm(server->socket, sizeof(unsigned int),
                                    sizeof(cl_kernel),
                                    0,
                                    &method,
                                    &(kernel->ptr),
                                    NULL);
    if(err) {
        unlock(server);
        return CL_OUT_OF_HOST_MEMORY;
    }

    err = recv_comm(server->socket, NULL, sizeof(cl_int),
                                          0,
                                          &flag,
                                          NULL);
    unlock(server);

    if(err)
        return CL_OUT_OF_HOST_MEMORY;

    if(flag != CL_SUCCESS)
        return flag;

    // So we can remove the kernel
    kernel->program->num_kernels_attached--;
    oclandReleaseProgram(kernel->program);
    kernels_remove_at(kernels, kernels_find(kernels, kernel));
    for(args_node arg_node = args_front(kernel->args);
        !args_is_null(arg_node);
        arg_node = args_next(arg_node))
    {
        del_kernel_arg(args_data(arg_node));
    }

    args_delete(kernel->args);
    free(kernel->function_name);
    free(kernel);

    return CL_SUCCESS;
}

/**
 * Check wheter an argument was already set, and don't need to be updated
 * @param  kernel    Considered kernel
 * @param  arg_index Argument index
 * @param  arg_size  Argument size
 * @param  arg_value Argument value
 * @return           true if the argument was already set, with exact same data,
 *                   false otherwise
 */
cl_bool is_same_kernel_arg(cl_kernel kernel,
                           cl_uint arg_index,
                           size_t arg_size,
                           const void *arg_value)
{
    if(arg_index >= args_len(kernel->args))
        return false;
    kernel_arg arg = args_data(args_at(kernel->args, arg_index));
    if((!arg->set) || (arg->size != arg_size))
        return false;
    if(!arg->value && !arg_value) {
        // Local memory case
        return true;
    }
    if(!memcmp(arg->value, arg_value, arg_size))
        return true;

    return false;
}

/**
 * Create a new empty kernel argument structure
 *
 * For safety, a set = false attribute is set, so it would be checked that the
 * argument was never set, and is not a 0 local memory argument
 * @return The new argument structure
 */
kernel_arg new_kernel_arg()
{
    kernel_arg arg = (kernel_arg)malloc(sizeof(struct _kernel_arg));
    arg->size = 0;
    arg->value = NULL;
    arg->set = false;
    return arg;
}

/**
 * Set a kernel arg for good
 * @param  kernel    Considered kernel
 * @param  arg_index Argument index
 * @param  arg_size  Argument size
 * @param  arg_value Argument value
 */
void set_kernel_arg(cl_kernel kernel,
                    cl_uint arg_index,
                    size_t arg_size,
                    const void *arg_value)
{
    while(arg_index >= args_len(kernel->args)) {
        // Create new empty (unset) arguments
        args_push_back(kernel->args, new_kernel_arg());
    }

    kernel_arg arg = args_data(args_at(kernel->args, arg_index));
    arg->size = arg_size;
    if(arg_value) {
        arg->value = malloc(arg_size);
        if(!arg->value)
            return;
        memcpy(arg->value, arg_value, arg_size);
    } else {
        arg->value = NULL;
    }
    arg->set = true;
}

cl_int oclandSetKernelArg(cl_kernel     kernel ,
                          cl_uint       arg_index ,
                          size_t        arg_size ,
                          const void *  arg_value)
{
    if(!is_kernel(kernel))
        return CL_INVALID_KERNEL;

    if(is_same_kernel_arg(kernel, arg_index, arg_size, arg_value))
        return CL_SUCCESS;

    size_t size = arg_size;
    void* val = (void*)arg_value;
    const char *empty_string = "";
    // There are a number of special cases here which requires editing both the
    // pointer and the size
    if(val == NULL) {
        // 1st.- arg_value is NULL, so arg_size is actually signaling the memory
        // size. Then we are setting the actual value size as 0, while we point
        // the value to an empty string, just in case (since we are not sending
        // nothing, it shall not matters)
        size = 0;
        val = (void*)empty_string;
    }
    else if(    (arg_size == sizeof(cl_mem))
             && is_mem(*(cl_mem*)arg_value))
    {
        // 2nd.- arg_value aims to a memory object, so we can keep the size but
        // we need to correct the pointer
        val = (void*) &( (*(cl_mem*)arg_value)->ptr );
    }
    else if(    (arg_size == sizeof(cl_sampler))
             && is_sampler(*(cl_sampler*)arg_value))
    {
        // 2nd.- arg_value aims to a sampler, so we can keep the size but
        // we need to correct the pointer
        val = (void*) &( (*(cl_sampler*)arg_value)->ptr );
    }
    else if(    (arg_size == sizeof(cl_command_queue))
             && is_queue(*(cl_command_queue*)arg_value))
    {
        // 3rd.- arg_value aims to a command queue, so we can keep the size but
        // we need to correct the pointer
        val = (void*) &( (*(cl_command_queue*)arg_value)->ptr );
    }

    // Call the remote peer
    int err;
    cl_int flag;
    const unsigned int method = ocland_clSetKernelArg;
    ocland_server server = kernel->server;
    lock(server);

    err = send_comm(server->socket, sizeof(unsigned int),
                                    sizeof(cl_kernel),
                                    sizeof(cl_uint),
                                    sizeof(size_t),
                                    sizeof(size_t),
                                    size,
                                    0,
                                    &method,
                                    &(kernel->ptr),
                                    &arg_index,
                                    &arg_size,
                                    &size,
                                    val,
                                    NULL);
    if(err) {
        unlock(server);
        return CL_OUT_OF_HOST_MEMORY;
    }

    err = recv_comm(server->socket, NULL, sizeof(cl_int),
                                          0,
                                          &flag,
                                          NULL);
    unlock(server);

    if(err)
        return CL_OUT_OF_HOST_MEMORY;

    if(flag == CL_SUCCESS)
        set_kernel_arg(kernel, arg_index, arg_size, arg_value);

    return flag;
}

/**
 * Get info from the kernel
 *
 * This method only returns data that shall not be queried to the server
 * @param  kernel Kernel
 * @param  param_name Parameter
 * @param  param_value_size Allocated memory inside #param_value
 * @param  param_value Allocated memory where the queried data shall be copy
 * @param  param_value_size_ret Returned size of the queried data
 * @return 1 if the queried data cannot be extracted from the
 * local instance, and shall be therefore queried to the server. CL_SUCCESS
 * if the query can be processed. CL_INVALID_VALUE if the query can be processed
 * but param_value_size is smaller than the size of the returned value.
 */
cl_int get_kernel_info(cl_kernel       kernel,
                       cl_kernel_info  param_name,
                       size_t          param_value_size,
                       void *          param_value,
                       size_t *        param_value_size_ret)
{
    size_t size_ret = 0;
    void* data_ret = NULL;
    switch(param_name) {
        case CL_KERNEL_FUNCTION_NAME:
            size_ret = (strlen(kernel->function_name) + 1) * sizeof(char);
            data_ret = kernel->function_name;
            break;
        case CL_KERNEL_REFERENCE_COUNT:
            size_ret = sizeof(cl_uint);
            data_ret = &(kernel->rcount);
            break;
        case CL_KERNEL_PROGRAM:
            size_ret = sizeof(cl_program);
            data_ret = &(kernel->program);
            break;
        case CL_KERNEL_CONTEXT:
            size_ret = sizeof(cl_context);
            data_ret = &(kernel->program->context);
            break;
        default:
            return 1;
    }

    if(param_value_size_ret)
        *param_value_size_ret = size_ret;
    if(param_value) {
        if(param_value_size < size_ret)
            return CL_INVALID_VALUE;
        memcpy(param_value, data_ret, size_ret);
    }

    return CL_SUCCESS;
}

cl_int oclandGetKernelInfo(cl_kernel       kernel,
                           cl_kernel_info  param_name,
                           size_t          param_value_size,
                           void *          param_value,
                           size_t *        param_value_size_ret)
{
    if(!is_kernel(kernel))
        return CL_INVALID_KERNEL;

    cl_int flag;
    flag = get_kernel_info(kernel,
                            param_name,
                            param_value_size,
                            param_value,
                            param_value_size_ret);
    if(flag != 1)  // flag = 1 means we should call the server
        return flag;

    // Call the remote peer to try to get the missing info
    const unsigned int method = ocland_clGetKernelInfo;
    return oclandGetGenericInfo(kernel->server,
                                param_value_size,
                                param_value,
                                param_value_size_ret,
                                // Variable arguments
                                sizeof(unsigned int),
                                sizeof(cl_kernel),
                                sizeof(cl_kernel_info),
                                sizeof(size_t),
                                0,
                                &method,
                                &(kernel->ptr),
                                &param_name,
                                &param_value_size,
                                NULL);
}

cl_int oclandGetKernelWorkGroupInfo(cl_kernel                   kernel ,
                                    cl_device_id                device ,
                                    cl_kernel_work_group_info   param_name ,
                                    size_t                      param_value_size ,
                                    void *                      param_value ,
                                    size_t *                    param_value_size_ret)
{
    if(!is_kernel(kernel))
        return CL_INVALID_KERNEL;

    // Apparently, NULL device is acceptable if the kernel is associated to a
    // single device
    if(!device && (kernel->program->num_devices == 1))
        device = kernel->program->devices[0];

    if(!is_device(device))
        return CL_INVALID_DEVICE;

    // Call the remote peer to try to get the missing info
    const unsigned int method = ocland_clGetKernelWorkGroupInfo;
    return oclandGetGenericInfo(kernel->server,
                                param_value_size,
                                param_value,
                                param_value_size_ret,
                                // Variable arguments
                                sizeof(unsigned int),
                                sizeof(cl_kernel),
                                sizeof(cl_device_id),
                                sizeof(cl_kernel_work_group_info),
                                sizeof(size_t),
                                0,
                                &method,
                                &(kernel->ptr),
                                &(device->ptr),
                                &param_name,
                                &param_value_size,
                                NULL);
}

cl_int oclandGetKernelArgInfo(cl_kernel           kernel ,
                              cl_uint             arg_index ,
                              cl_kernel_arg_info  param_name ,
                              size_t              param_value_size ,
                              void *              param_value ,
                              size_t *            param_value_size_ret)
{
    if(!is_kernel(kernel))
        return CL_INVALID_KERNEL;

    // Call the remote peer to try to get the missing info
    const unsigned int method = ocland_clGetKernelArgInfo;
    return oclandGetGenericInfo(kernel->server,
                                param_value_size,
                                param_value,
                                param_value_size_ret,
                                // Variable arguments
                                sizeof(unsigned int),
                                sizeof(cl_kernel),
                                sizeof(cl_uint),
                                sizeof(cl_kernel_arg_info),
                                sizeof(size_t),
                                0,
                                &method,
                                &(kernel->ptr),
                                &arg_index,
                                &param_name,
                                &param_value_size,
                                NULL);
}
