/*
 *  This file is part of ocland, a free cloud OpenCL interface.
 *  Copyright (C) 2012-2019  Jose Luis Cercos Pita <jl.cercos@upm.es>
 *
 *  ocland is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  ocland is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with ocland.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <ocland/common/dataExchange.h>
#include <ocland/client/platform.h>
#include <ocland/client/device.h>
#include <ocland/client/context.h>

#include <ocland/common/typed_list.h>
DEFINE_LIST(cl_context, contexts)
DEFINE_LIST(cl_context_properties, context_props)


/// List of contexts
static List* contexts = NULL;

cl_context get_context(cl_context remote_context)
{
    if(!contexts_len(contexts))
        return NULL;
    for(contexts_node context_node = contexts_front(contexts);
        !contexts_is_null(context_node);
        context_node = contexts_next(context_node))
    {
        cl_context local_context = contexts_data(context_node);
        if(local_context->ptr == remote_context)
            return local_context;
    }

    return NULL;
}

bool is_context(cl_context context)
{
    if(!contexts)
        contexts = contexts_new();
    return contexts_find(contexts, context) < contexts_len(contexts);
}

bool has_context_device(cl_context context, cl_device_id device)
{
    if(!is_context(context) || !is_device(device))
        return false;
    for(unsigned int i = 0; i < context->num_devices; i++) {
        if(context->devices[i] == device)
            return true;
    }
    return false;
}

/**
 * Check for errors in the cl_context_properties properties array
 * @param  properties List of properties to create a context
 * @return            CL_SUCCESS if the properties are right, or an OpenCL error
 *                    otherwise
 */
cl_int check_context_properties(const cl_context_properties *properties)
{
    if(!properties)
        return CL_SUCCESS;

    List *props = context_props_new();

    unsigned int i = 0;
    while(properties[i] != 0) {
        if(context_props_find(props, properties[i]) < context_props_len(props))
        {
            context_props_delete(props);
            return CL_INVALID_PROPERTY;
        }
        context_props_push_back(props, properties[i]);

        if(properties[i] == CL_CONTEXT_PLATFORM) {
            if(!is_platform((cl_platform_id)properties[i + 1])) {
                context_props_delete(props);
                return CL_INVALID_PLATFORM;
            }
        }
        i += 2;
    }

    context_props_delete(props);
    return CL_SUCCESS;
}

/**
 * Function to be called when a remote peer would notify a context error
 * @param context_peer Server context instance
 * @param msg_size     Size of the message received by the server
 * @param msg          Message received by the server
 * @param context      Local context instance
 */
void context_notifier (void *context_peer,
                       size_t msg_size,
                       void* msg,
                       void* context)
{
    cl_context c = (cl_context)context;
    c->pfn_notify((char*)msg, context, sizeof(cl_context), c->user_data);
}

cl_context oclandCreateContext(const cl_context_properties * properties,
                               cl_uint                       num_devices,
                               const cl_device_id *          devices,
                               void (CL_CALLBACK * pfn_notify)(const char *, const void *, size_t, void *),
                               void *                        user_data,
                               cl_int *                      errcode_ret)
{
    if(!contexts)
        contexts = contexts_new();

    cl_int flag = check_context_properties(properties);
    if(flag != CL_SUCCESS) {
        if(errcode_ret) *errcode_ret = flag;
        return NULL;
    }

    // Try to get the platform from the properties
    cl_platform_id platform = NULL;
    cl_uint num_properties = 0;
    if(properties != NULL) {
        while(properties[num_properties] != 0) {
            if(properties[num_properties] == CL_CONTEXT_PLATFORM) {
                platform = (cl_platform_id)properties[num_properties + 1];
            }
            num_properties += 2;
        }
        num_properties++;  // Tailing 0
    }
    size_t props_size = num_properties * sizeof(cl_context_properties);

    // Let's check the passed devices
    for(cl_uint i = 0; i < num_devices; i++) {
        if(!is_device(devices[i])) {
            if(errcode_ret) *errcode_ret = CL_INVALID_DEVICE;
            return NULL;
        }
        if(!platform) {
            platform = devices[i]->platform;
        }
        else if(platform != devices[i]->platform) {
            if(errcode_ret) *errcode_ret = CL_INVALID_PLATFORM;
            return NULL;
        }
    }

    // Let's create a copy of the properties, with the platform changed by the
    // server instance
    if(!properties) {
        num_properties = 3;
        props_size = num_properties * sizeof(cl_context_properties);
    }
    cl_context_properties *server_properties = (cl_context_properties *)malloc(
        props_size);
    cl_context_properties *local_properties = (cl_context_properties *)malloc(
        props_size);
    if(!server_properties || !local_properties) {
        if(errcode_ret) *errcode_ret = CL_OUT_OF_HOST_MEMORY;
        return NULL;
    }

    if(properties) {
        memcpy(server_properties, properties, props_size);
        memcpy(local_properties, properties, props_size);
        for(cl_uint i = 0; i < num_properties; i += 2) {
            if(server_properties[i] == CL_CONTEXT_PLATFORM) {
                server_properties[i + 1] = (cl_context_properties)platform->ptr;
                break;
            }
        }
    }
    else {
        server_properties[0] = CL_CONTEXT_PLATFORM;
        server_properties[1] = (cl_context_properties)platform->ptr;
        server_properties[2] = 0;
        local_properties[0] = CL_CONTEXT_PLATFORM;
        local_properties[1] = (cl_context_properties)platform;
        local_properties[2] = 0;
    }

    // Create an array of server device instances
    cl_device_id *server_devices = (cl_device_id *)malloc(
        num_devices * sizeof(cl_device_id));
    cl_device_id *local_devices = (cl_device_id *)malloc(
        num_devices * sizeof(cl_device_id));
    if(!server_devices || !local_devices) {
        free(server_properties);
        free(local_properties);
        if(errcode_ret) *errcode_ret = CL_OUT_OF_HOST_MEMORY;
        return NULL;
    }
    for(cl_uint i = 0; i < num_devices; i++) {
        server_devices[i] = devices[i]->ptr;
    }
    memcpy(local_devices, devices, num_devices * sizeof(cl_device_id));

    // Call the remote peer
    int err;
    const unsigned int method = ocland_clCreateContext;
    ocland_server server = platform->server;
    lock(server);

    err = send_comm(server->socket, sizeof(unsigned int),
                                    sizeof(cl_uint),
                                    props_size,
                                    sizeof(cl_uint),
                                    num_devices * sizeof(cl_device_id),
                                    0,
                                    &method,
                                    &num_properties,
                                    server_properties,
                                    &num_devices,
                                    server_devices,
                                    NULL);
    // No matters the result, we don't want the server info anymore
    free(server_properties);
    free(server_devices);
    if(err) {
        unlock(server);
        free(local_properties);
        free(local_devices);
        if(errcode_ret) *errcode_ret = CL_OUT_OF_HOST_MEMORY;
        return NULL;
    }

    cl_context context;
    err = recv_comm(server->socket, NULL, sizeof(cl_int),
                                          sizeof(cl_context),
                                          0,
                                          &flag,
                                          &context,
                                          NULL);
    unlock(server);

    if(err) {
        free(local_properties);
        free(local_devices);
        if(errcode_ret) *errcode_ret = CL_OUT_OF_HOST_MEMORY;
        return NULL;
    }

    if(errcode_ret) *errcode_ret = flag;

    if(flag != CL_SUCCESS) {
        free(local_properties);
        free(local_devices);
        return NULL;
    }

    // Register the new context
    cl_context data = (cl_context)malloc(
       sizeof(struct _cl_context));
    if(!data) {
        free(local_properties);
        free(local_devices);
        if(errcode_ret) *errcode_ret = CL_OUT_OF_HOST_MEMORY;
        return NULL;
    }
    data->dispatch = platform->dispatch;
    data->ptr = context;
    data->rcount = 1;
    data->server = platform->server;
    data->num_props = num_properties;
    data->props = local_properties;
    data->num_devices = num_devices;
    data->devices = local_devices;
    data->pfn_notify = pfn_notify;
    data->user_data = user_data;
    contexts_push_back(contexts, data);

    if(pfn_notify)
        notifier_append(server->cb_notifier,
                        (void*)context,
                        &context_notifier,
                        (void*)data,
                        false);

    // We want to keep the involved devices alive. Since the devices could be
    // eventually generated with clCreateSubDevices, they can be eventually
    // released. Therefore, we are just simply asking to retain all the involved
    // devices, without checking the errors
    for(cl_uint i = 0; i < data->num_devices; i++) {
        oclandRetainDevice(data->devices[i]);
    }

    return data;
}

cl_context oclandCreateContextFromType(const cl_context_properties * properties,
                                       cl_device_type                device_type,
                                       void (CL_CALLBACK * pfn_notify)(const char *, const void *, size_t, void *),
                                       void *                        user_data,
                                       cl_int *                      errcode_ret)
{
    // Get the platform from the properties
    cl_platform_id platform = NULL;
    cl_uint i = 0;
    while(properties[i] != 0) {
        if(properties[i] == CL_CONTEXT_PLATFORM) {
            platform = (cl_platform_id)properties[i + 1];
            break;
        }
        i += 2;
    }
    if(!platform || !is_platform(platform)) {
        if(errcode_ret) *errcode_ret = CL_INVALID_PLATFORM;
        return NULL;
    }

    // Get the devices array from the platform
    cl_int flag;
    cl_uint num_devices = 0;
    flag = oclandGetDeviceIDs(platform, device_type, 0, NULL, &num_devices);
    if((flag != CL_SUCCESS) || !num_devices) {
        if(errcode_ret) *errcode_ret = CL_DEVICE_NOT_FOUND;
        return NULL;
    }
    cl_device_id *devices = (cl_device_id*)malloc(
        num_devices * sizeof(cl_device_id));
    if(!devices) {
        if(errcode_ret) *errcode_ret = CL_OUT_OF_HOST_MEMORY;
        return NULL;
    }
    flag = oclandGetDeviceIDs(platform, device_type, num_devices, devices, NULL);
    if(flag != CL_SUCCESS) {
        free(devices);
        if(errcode_ret) *errcode_ret = CL_DEVICE_NOT_FOUND;
        return NULL;
    }

    // Redirect the work to the more specific context generator
    cl_context context = oclandCreateContext(properties,
                                             num_devices,
                                             devices,
                                             pfn_notify,
                                             user_data,
                                             errcode_ret);
    free(devices);
    return context;
}

cl_int oclandRetainContext(cl_context context)
{
    if(!is_context(context))
        return CL_INVALID_CONTEXT;

    context->rcount++;
    return CL_SUCCESS;
}

cl_int oclandReleaseContext(cl_context context)
{
    if(!is_context(context))
        return CL_INVALID_CONTEXT;

    if(context->rcount > 1) {
        context->rcount--;
        return CL_SUCCESS;
    }

    // The context shall be destroyed, including from server
    int err;
    cl_int flag;
    const unsigned int method = ocland_clReleaseContext;
    ocland_server server = context->server;
    lock(server);

    err = send_comm(server->socket, sizeof(unsigned int),
                                    sizeof(cl_context),
                                    0,
                                    &method,
                                    &(context->ptr),
                                    NULL);
    if(err) {
        unlock(server);
        return CL_OUT_OF_HOST_MEMORY;
    }

    err = recv_comm(server->socket, NULL, sizeof(cl_int),
                                          0,
                                          &flag,
                                          NULL);
    if(err) {
        unlock(server);
        return CL_OUT_OF_HOST_MEMORY;
    }

    unlock(server);

    if(flag != CL_SUCCESS){
        return flag;
    }

    // In the same way we tried to retain the devices, we want to release them
    // without checking for errors, in such a way that devices created with
    // clCreateSubDevices() are just simply ignored
    for(cl_uint i = 0; i < context->num_devices; i++) {
        oclandReleaseDevice(context->devices[i]);
    }

    // So we can remove the context
    contexts_remove_at(contexts, contexts_find(contexts, context));
    free(context->props);
    free(context->devices);
    if(context->pfn_notify)
        notifier_remove(context->server->cb_notifier, (void*)(context->ptr));
    free(context);

    return CL_SUCCESS;
}

/**
 * Get info from the context
 *
 * This method only returns data that shall not be queried to the server
 * @param  context Context
 * @param  param_name Parameter
 * @param  param_value_size Allocated memory inside #param_value
 * @param  param_value Allocated memory where the queried data shall be copy
 * @param  param_value_size_ret Returned size of the queried data
 * @return 1 if the queried data cannot be extracted from the
 * local instance, and shall be therefore queried to the server. CL_SUCCESS
 * if the query can be processed. CL_INVALID_VALUE if the query can be processed
 * but param_value_size is smaller than the size of the returned value.
 */
cl_int get_context_info(cl_context       context,
                        cl_context_info  param_name,
                        size_t           param_value_size,
                        void *           param_value,
                        size_t *         param_value_size_ret)
{
    size_t size_ret = 0;
    void* data_ret = NULL;
    switch(param_name) {
        case CL_CONTEXT_REFERENCE_COUNT:
            size_ret = sizeof(cl_uint);
            data_ret = &(context->rcount);
            break;
        case CL_CONTEXT_NUM_DEVICES:
            size_ret = sizeof(cl_uint);
            data_ret = &(context->num_devices);
            break;
        case CL_CONTEXT_DEVICES:
            size_ret = context->num_devices * sizeof(cl_device_id);
            data_ret = context->devices;
            break;
        case CL_CONTEXT_PROPERTIES:
            size_ret = context->num_props * sizeof(cl_context_properties);
            data_ret = context->props;
            break;
        default:
            return 1;
    }

    if(param_value_size_ret)
        *param_value_size_ret = size_ret;
    if(param_value) {
        if(param_value_size < size_ret)
            return CL_INVALID_VALUE;
        memcpy(param_value, data_ret, size_ret);
    }

    return CL_SUCCESS;
}

cl_int oclandGetContextInfo(cl_context         context,
                            cl_context_info    param_name,
                            size_t             param_value_size,
                            void *             param_value,
                            size_t *           param_value_size_ret)
{
    if(!is_context(context))
        return CL_INVALID_CONTEXT;

    cl_int flag;
    flag = get_context_info(context,
                            param_name,
                            param_value_size,
                            param_value,
                            param_value_size_ret);
    if(flag != 1)  // flag = 1 means we should call the server
        return flag;

    // Call the remote peer to try to get the missing info
    const unsigned int method = ocland_clGetContextInfo;
    return oclandGetGenericInfo(context->server,
                                param_value_size,
                                param_value,
                                param_value_size_ret,
                                // Variable arguments
                                sizeof(unsigned int),
                                sizeof(cl_context),
                                sizeof(cl_context_info),
                                sizeof(size_t),
                                0,
                                &method,
                                &(context->ptr),
                                &param_name,
                                &param_value_size,
                                NULL);
}
