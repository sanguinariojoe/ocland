/*
 *  This file is part of ocland, a free cloud OpenCL interface.
 *  Copyright (C) 2012-2019  Jose Luis Cercos Pita <jl.cercos@upm.es>
 *
 *  ocland is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  ocland is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with ocland.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <ocland/common/dataExchange.h>
#include <ocland/client/platform.h>
#include <ocland/client/device.h>
#include <ocland/client/log.h>

#include <ocland/common/typed_list.h>
DEFINE_LIST(cl_device_id, devices)

/// List of platforms
static List* devices = NULL;

/**
 * Query the remote peer for the type of device
 * @param  device Remote peer device instance
 * @param  server Remote peer
 * @return The type of device, CL_DEVICE_TYPE_DEFAULT if errors happened
 */
cl_device_type query_device_type(cl_device_id device, ocland_server server)
{
    int err;
    cl_int flag;
    const unsigned int method = ocland_clGetDeviceInfo;
    const cl_device_info param_name = CL_DEVICE_TYPE;
    size_t param_value_size = sizeof(cl_device_type);
    cl_device_type device_type;
    lock(server);

    // Ask the remote peer for the number of available platforms
    err = send_comm(server->socket, sizeof(unsigned int),
                                    sizeof(cl_device_id),
                                    sizeof(cl_device_info),
                                    sizeof(size_t),
                                    0,
                                    &method,
                                    &device,
                                    &param_name,
                                    &param_value_size,
                                    NULL);
    if(err) {
        return CL_DEVICE_TYPE_DEFAULT;
    }

    err = recv_comm(server->socket, NULL, sizeof(cl_int),
                                          sizeof(size_t),
                                          sizeof(cl_device_type),
                                          0,
                                          &flag,
                                          &param_value_size,
                                          &device_type,
                                          NULL);
    if(err) {
        return CL_DEVICE_TYPE_DEFAULT;
    }

    unlock(server);  // We don't need to monopolize the conn anymore

    if((flag != CL_SUCCESS) || (param_value_size != sizeof(cl_device_type))) {
        return CL_DEVICE_TYPE_DEFAULT;
    }

    return device_type;
}

void add_devices(cl_platform_id platform)
{
    if(!devices)
        devices = devices_new();
    // We can assume the platform is valid, since this method is called by
    // add_platform()
    int err;
    cl_int flag;
    const unsigned int method = ocland_clGetDeviceIDs;
    const cl_platform_id peer_platform = platform->ptr;
    cl_device_type device_type = CL_DEVICE_TYPE_ALL;
    cl_uint num_entries = 0;
    ocland_server server = platform->server;
    lock(server);

    // Ask the remote peer for the number of available platforms
    err = send_comm(server->socket, sizeof(unsigned int),
                                    sizeof(cl_platform_id),
                                    sizeof(cl_device_type),
                                    sizeof(cl_uint),
                                    0,
                                    &method,
                                    &peer_platform,
                                    &device_type,
                                    &num_entries,
                                    NULL);
    if(err) {
        unlock(server);
        VERBOSE_MSG("Failure sending clGetDeviceIDs to %s\n",
                    server->address);
        return;
    }

    err = recv_comm(server->socket, NULL, sizeof(cl_int),
                                          sizeof(cl_uint),
                                          0,
                                          &flag,
                                          &num_entries,
                                          NULL);
    if(err) {
        unlock(server);
        VERBOSE_MSG("Failure receiving clGetDeviceIDs from %s\n",
                    server->address);
        return;
    }

    if((flag != CL_SUCCESS) || (!num_entries)) {
        // Just simply a server without OpenCL platforms
        unlock(server);
        return;
    }

    // Ask the remote peer for the platforms
    cl_device_id *peer_devices = (cl_device_id*)malloc(
        num_entries * sizeof(cl_device_id));
    if(!peer_devices) {
        unlock(server);
        VERBOSE_MSG("Failure allocating %lu bytes for %s devices\n",
                    num_entries * sizeof(cl_device_id), server->address);
        return;
    }

    err = send_comm(server->socket, sizeof(unsigned int),
                                    sizeof(cl_platform_id),
                                    sizeof(cl_device_type),
                                    sizeof(cl_uint),
                                    0,
                                    &method,
                                    &peer_platform,
                                    &device_type,
                                    &num_entries,
                                    NULL);
    if(err) {
        unlock(server);
        free(peer_devices);
        VERBOSE_MSG("Failure sending clGetDeviceIDs (2) to %s\n",
                    server->address);
        return;
    }

    recv_pending rest;
    err = recv_comm(server->socket, &rest, sizeof(cl_int),
                                           sizeof(cl_uint),
                                           0,
                                           &flag,
                                           &num_entries,
                                           NULL);
    if(err) {
        unlock(server);
        free(peer_devices);
        free(rest.data);
        VERBOSE_MSG("Failure receiving clGetDeviceIDs (2) from %s\n",
                    server->address);
        return;
    }

    unlock(server);  // We don't need to monopolize the conn anymore

    // The remaining message is already the platforms array
    if(rest.size != num_entries * sizeof(cl_device_id)) {
        free(peer_devices);
        free(rest.data);
        VERBOSE_MSG("Malformed devices from %s\n", server->address);
        return;
    }
    memcpy(peer_devices, rest.data, rest.size);
    free(rest.data);

    // Setup the devices
    for(cl_uint i = 0; i < num_entries; i++) {
        cl_device_type device_type = query_device_type(peer_devices[i],
                                                       server);
       cl_device_id data = (cl_device_id)malloc(
           sizeof(struct _cl_device_id));
       data->dispatch = platform->dispatch;
       data->ptr = peer_devices[i];
       data->platform = platform;
       data->server = server;
       data->parent = NULL;
       data->type = device_type;
       data->rcount = 1;
       devices_push_back(devices, data);
    }

    free(peer_devices);
}

cl_device_id get_device(cl_device_id remote_device)
{
    if(!devices_len(devices))
        return NULL;
    for(devices_node device_node = devices_front(devices);
        !devices_is_null(device_node);
        device_node = devices_next(device_node))
    {
        cl_device_id local_device = devices_data(device_node);
        if(local_device->ptr == remote_device)
            return local_device;
    }

    return NULL;
}

bool is_device(cl_device_id device)
{
    if(!devices)
        devices = devices_new();
    return devices_find(devices, device) < devices_len(devices);
}

/**
 * Check whether a device type is included in the queried type
 *
 * At the time of writing, OpenCL give the following matching types:
 *   - #device_type is equal to #query_type
 *   - #query_type = CL_DEVICE_TYPE_ALL and
 *     #device_type != CL_DEVICE_TYPE_CUSTOM
 * There is also a special type called CL_DEVICE_TYPE_DEFAULT, which in
 * principle depends on the specific platform. We workaround this problem
 * considering that CL_DEVICE_TYPE_DEFAULT is the same than CL_DEVICE_TYPE_ALL
 *
 * @param  device_type Type of device
 * @param  query_type Queried type
 * @return CL_TRUE if the device type is a subtype of the queried one, CL_FALSE
 * otherwise
 */
cl_bool is_device_of_type(cl_device_type device_type, cl_device_type query_type)
{
    if(query_type == CL_DEVICE_TYPE_DEFAULT)
        query_type = CL_DEVICE_TYPE_ALL;

    if(device_type == query_type)
        return CL_TRUE;
    else if ( (device_type != CL_DEVICE_TYPE_CUSTOM) &&
              (query_type == CL_DEVICE_TYPE_ALL) )
        return CL_TRUE;
    return CL_FALSE;
}

cl_int oclandGetDeviceIDs(cl_platform_id   platform,
                          cl_device_type   device_type,
                          cl_uint          num_entries,
                          cl_device_id *   local_devices,
                          cl_uint *        num_devices)
{
    if(!is_platform(platform))
        return CL_INVALID_PLATFORM;

    unsigned int n = 0;
    for(devices_node device_node = devices_front(devices);
        !devices_is_null(device_node);
        device_node = devices_next(device_node))
    {
        cl_device_id device = devices_data(device_node);
        if ( (device->platform != platform) ||
             !is_device_of_type(device->type, device_type) )
            continue;

        if(n < num_entries)
            local_devices[n] = device;
        n++;
    }

    if(num_devices) *num_devices = n;
    // The platform has not been found in any server
    return CL_SUCCESS;
}

/**
 * Get info from the device
 *
 * This method only returns data that shall not be queried to the server
 * @param  device Device
 * @param  param_name Parameter
 * @param  param_value_size Allocated memory inside #param_value
 * @param  param_value Allocated memory where the queried data shall be copy
 * @param  param_value_size_ret Returned size of the queried data
 * @return CL_INVALID_VALUE if the queried data cannot be extracted from the
 * local instance, and shall be therefore queried to the server. CL_SUCCESS
 * otherwise.
 */
cl_int get_device_info(cl_device_id    device,
                       cl_device_info  param_name,
                       size_t          param_value_size,
                       void *          param_value,
                       size_t *        param_value_size_ret)
{
    size_t size_ret = 0;
    void* data_ret = NULL;
    switch(param_name) {
        case CL_DEVICE_PLATFORM:
            size_ret = sizeof(cl_platform_id);
            data_ret = &(device->platform);
            break;
        case CL_DEVICE_TYPE:
            size_ret = sizeof(cl_device_type);
            data_ret = &(device->type);
            break;
        case CL_DEVICE_PARENT_DEVICE:
            size_ret = sizeof(cl_device_id);
            data_ret = &(device->parent);
            break;
        case CL_DEVICE_REFERENCE_COUNT:
            size_ret = sizeof(cl_uint);
            data_ret = &(device->rcount);
            break;
        default:
            return 1;
    }

    if(param_value_size_ret)
        *param_value_size_ret = size_ret;
    if(param_value) {
        if(param_value_size < size_ret)
            return CL_INVALID_VALUE;
        memcpy(param_value, data_ret, size_ret);
    }

    return CL_SUCCESS;
}

cl_int oclandGetDeviceInfo(cl_device_id    device,
                           cl_device_info  param_name,
                           size_t          param_value_size,
                           void *          param_value,
                           size_t *        param_value_size_ret)
{
    if(!is_device(device))
        return CL_INVALID_DEVICE;

    cl_int flag;
    flag = get_device_info(device,
                           param_name,
                           param_value_size,
                           param_value,
                           param_value_size_ret);
    if(flag != 1)  // flag = 1 means we should call the server
        return CL_SUCCESS;

    // Call the remote peer to try to get the missing info
    const unsigned int method = ocland_clGetDeviceInfo;
    return oclandGetGenericInfo(device->server,
                                param_value_size,
                                param_value,
                                param_value_size_ret,
                                // Variable arguments
                                sizeof(unsigned int),
                                sizeof(cl_device_id),
                                sizeof(cl_device_info),
                                sizeof(size_t),
                                0,
                                &method,
                                &(device->ptr),
                                &param_name,
                                &param_value_size,
                                NULL);
}

cl_int oclandCreateSubDevices(cl_device_id                         in_device,
                              const cl_device_partition_property * properties,
                              cl_uint                              num_entries,
                              cl_device_id                       * out_devices,
                              cl_uint                            * num_devices)
{
    if(!is_device(in_device))
        return CL_INVALID_DEVICE;

    // Count the number of properties
    cl_uint num_properties = 0;
    while(properties[num_properties] != 0)
        num_properties++;
    num_properties++;
    size_t props_size = num_properties * sizeof(cl_device_partition_property);

    // Call the remote peer
    int err;
    cl_int flag;
    const unsigned int method = ocland_clCreateSubDevices;
    ocland_server server = in_device->server;
    lock(server);

    err = send_comm(server->socket, sizeof(unsigned int),
                                    sizeof(cl_device_id),
                                    props_size,
                                    sizeof(cl_uint),
                                    0,
                                    &method,
                                    &(in_device->ptr),
                                    properties,
                                    &num_entries,
                                    NULL);
    if(err) {
        unlock(server);
        return CL_OUT_OF_HOST_MEMORY;
    }

    recv_pending rest;
    cl_uint n_ret;
    err = recv_comm(server->socket, &rest, sizeof(cl_int),
                                           sizeof(cl_uint),
                                           0,
                                           &flag,
                                           &n_ret,
                                           NULL);
    if(err) {
        unlock(server);
        free(rest.data);
        return CL_OUT_OF_HOST_MEMORY;
    }

    unlock(server);

    if(flag != CL_SUCCESS){
        free(rest.data);
        return flag;
    }
    if(num_devices) *num_devices = n_ret;

    // The remaining message is already the devices array
    if(num_entries) {
        if(num_entries < n_ret) {
            free(rest.data);
            return CL_INVALID_VALUE;
        }
        if(rest.size != num_entries * sizeof(cl_device_id)) {
            free(rest.data);
            return CL_OUT_OF_HOST_MEMORY;
        }
        memcpy(out_devices, rest.data, num_entries * sizeof(cl_device_id));
    }
    free(rest.data);

    // Register the new devices
    for(cl_uint i = 0; i < n_ret; i++) {
        cl_device_id data = (cl_device_id)malloc(
           sizeof(struct _cl_device_id));
        data->dispatch = in_device->dispatch;
        data->ptr = out_devices[i];
        data->platform = in_device->platform;
        data->server = in_device->server;
        data->parent = in_device;
        data->type = in_device->type;
        data->rcount = 1;
        devices_push_back(devices, data);
        // Retain the parent device until all children are removed. If the
        // parent cannot be retained, we are just ignoring the returned error
        oclandRetainDevice(in_device);
    }

    return CL_SUCCESS;
}

cl_int oclandRetainDevice(cl_device_id device)
{
    if( !is_device(device) || !device->parent )
        return CL_INVALID_DEVICE;

    device->rcount++;
    return CL_SUCCESS;
}

cl_int oclandReleaseDevice(cl_device_id device)
{
    if( !is_device(device) || !device->parent )
        return CL_INVALID_DEVICE;

    if(device->rcount > 1) {
        device->rcount--;
        return CL_SUCCESS;
    }

    // The device shall be destroyed, including from server
    int err;
    cl_int flag;
    const unsigned int method = ocland_clReleaseDevice;
    ocland_server server = device->server;
    lock(server);

    err = send_comm(server->socket, sizeof(unsigned int),
                                    sizeof(cl_device_id),
                                    0,
                                    &method,
                                    &(device->ptr),
                                    NULL);
    if(err) {
        unlock(server);
        return CL_OUT_OF_HOST_MEMORY;
    }

    err = recv_comm(server->socket, NULL, sizeof(cl_int),
                                           0,
                                           &flag,
                                           NULL);
    if(err) {
        unlock(server);
        return CL_OUT_OF_HOST_MEMORY;
    }

    unlock(server);

    if(flag != CL_SUCCESS){
        return flag;
    }

    // The parent has therefore one less subdevice referencing to him. If the
    // parent could not became retained, we are just ignoring the error
    oclandReleaseDevice(device->parent);

    // So we can remove the device
    devices_remove_at(devices, devices_find(devices, device));
    free(device);

    return CL_SUCCESS;
}
