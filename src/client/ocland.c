/*
 *  This file is part of ocland, a free cloud OpenCL interface.
 *  Copyright (C) 2012  Jose Luis Cercos Pita <jl.cercos@upm.es>
 *
 *  ocland is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  ocland is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with ocland.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <fcntl.h>
#include <stdlib.h>
#include <sys/select.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netdb.h>
#include <errno.h>
#include <netinet/in.h>
#include <netinet/tcp.h>
#include <arpa/inet.h>
#include <pthread.h>
#include <signal.h>
#include <unistd.h>

#include <ocland/common/arch.h>
#include <ocland/common/dataExchange.h>
#include <ocland/client/ocland.h>
#include <ocland/client/ocland_icd.h>
#include <ocland/client/log.h>

#include <ocland/common/typed_list.h>
DEFINE_LIST(ocland_server, servers)

#ifndef OCLAND_PORT
    #define OCLAND_PORT 51000u
#endif

#ifndef CONN_TIMEOUT_SEC
    #define CONN_TIMEOUT_SEC 5
#endif
#ifndef CONN_TIMEOUT_USEC
    #define CONN_TIMEOUT_USEC 0
#endif

/// List of servers
static List* servers = NULL;
/// Servers initialization flag
static cl_bool initialized = CL_FALSE;

void lock(ocland_server server)
{
    pthread_mutex_lock(&(server->lock));
}

void unlock(ocland_server server)
{
    pthread_mutex_unlock(&(server->lock));
}

/** @brief Load servers file "ocland"
 *
 * File must contain IP address of each server, one per line.
 * @return Number of servers.
 */
size_t loadServers()
{
    // Create the servers list
    if(servers)
        servers_delete(servers);
    servers = servers_new();

    // Load servers definition files
    FILE *fin = NULL;
    fin = fopen("ocland", "r");
    if(!fin){
        // File don't exist, ocland must be ignored
        return 0;
    }
    // Set servers
    char *line = NULL;
    size_t linelen = 0;
    while((getline(&line, &linelen, fin)) != -1) {
        if(!strcmp(line, "\n")){
            continue;
        }
        // Create the server instance
        ocland_server server = (ocland_server)malloc(
            sizeof(struct _ocland_server));
        if(!server) {
            VERBOSE_MSG("Failure allocating %lu bytes for server %s\n",
                        sizeof(struct _ocland_server), line);
            continue;
        }
        server->address = (char*)malloc(strlen(line));
        strncpy(server->address, line, strlen(line) - 1);
        server->address[strlen(line) - 1] = '\0';
        if (pthread_mutex_init(&(server->lock), NULL) != 0) {
            VERBOSE_MSG("Failure initializating server %s mutex\n",
                        line);
            free(server);
            continue;
        }
        server->socket = -1;
        // Append it to the list
        servers_push_back(servers, server);
    }
    // Calling free once is OK (https://linux.die.net/man/3/getline)
    free(line);

    return servers_len(servers);
}

/** @brief Helper to store host address and port
 */
typedef struct _ip
{
    /// Address
    char* address;
    /// Port
    char* port;
} ip;

/** @brief Split an address string into the host address and its port
 *
 * If the address has not a port, the default one, OCLAND_PORT, will be used
 * @param address Address to split
 * @return Datas structure with the address string and the port number
 * @note No checks for malformed IP addresses is carried out
 */
ip splitAddress(char* address)
{
    ip result;
    result.address = (char*)malloc(strlen(address) + 1);
    strncpy(result.address, address, strlen(address));
    result.address[strlen(address)] = '\0';
    result.port = (char*)malloc(9);
    memset(result.port, '\0', 9);
    snprintf(result.port, 8, "%u", OCLAND_PORT);

    if(strchr(address, '.')) {
        // IPv4
        char *port = strchr(result.address, ':');
        if(port) {
            strncpy(result.port, port + 1, 8);
            strcpy(port, "");
        }
    }
    else{
        // IPv6
        char *bracket = strchr(result.address, '[');
        if(bracket) {
            // Has port
            // e.g. [::1]:80
            // So we first discard the initial bracket
            memmove(result.address, bracket, strlen(bracket) + 1);
            // And then locate the port data
            char *port = strchr(result.address, ']');
            strncpy(result.port, port + 2, 8);
            strcpy(port, "");
        }
    }

    return result;
}

/** @brief Reassemble an ip structure as an address string
 *
 * @param address Split address
 * @return address string
 * @note No checks for malformed IP addresses is carried out
 */
char* assembleAddress(ip address)
{
    size_t addr_size = strlen(address.address);
    size_t port_size = strlen(address.port);

    size_t extra_size;
    char prefix[2], suffix[3];

    if(strchr(address.address, '.')) {
        // IPv4
        extra_size = 2;
        strcpy(prefix, "");
        strcpy(suffix, ":");
    } else {
        // IPv6
        extra_size = 4;
        strcpy(prefix, "[");
        strcpy(suffix, "]:");
    }

    char *addr = (char*)malloc(addr_size + port_size + extra_size);
    if(!addr)
        return NULL;
    memset(addr, '\0', addr_size + port_size + extra_size);
    strcpy(addr, prefix);
    strncat(addr, address.address, addr_size);
    strcat(addr, suffix);
    strncat(addr, address.port, port_size);

    return addr;
}

/** @brief Stablish a connection with a remote peer, with a timeout
 * @param sfd Already created socket
 * @param addr Server address
 * @param timeout Timeout
 * @return 0 if the connection has been successfully stablished, or -1 otherwise
 */
int connect_timeout(int sfd, struct addrinfo *addr, struct timeval *timeout)
{
	int res, opt;

    // set socket in non-blocking mode, storing previous options
	if ((opt = fcntl(sfd, F_GETFL, NULL)) < 0) {
		return -1;
	}
	if (fcntl(sfd, F_SETFL, opt | O_NONBLOCK) < 0) {
		return -1;
	}

	// Connect
    res = connect(sfd, addr->ai_addr, addr->ai_addrlen);
    if (res == 0) {
        // Nice, the connection worked like a charm.
        // In case the connection needs more time to finish, select will return
        // 0 in case of timeout, or 1 in case of successful connection. -1 if
        // errors. So we emulate that setting res as 1
        res = 1;
    }
	else {
		if (errno == EINPROGRESS) {
            // Wait until socket is writable, or either timeout passed
			fd_set wait_set;
			FD_ZERO(&wait_set);
			FD_SET(sfd, &wait_set);
			res = select(sfd + 1, NULL, &wait_set, NULL, timeout);
		}
	}

	// reset socket flags, no matter the connection status
	if (fcntl(sfd, F_SETFL, opt) < 0) {
		return -1;
	}

	if (res < 0) {
		return -1;
	}
	else if (res == 0) {
		errno = ETIMEDOUT;
		return -1;
	}
	else {
        // check for errors in socket layer
		socklen_t len = sizeof (opt);

		if (getsockopt(sfd, SOL_SOCKET, SO_ERROR, &opt, &len) < 0) {
			return -1;
		}

		if (opt) {
			errno = opt;
			return -1;
		}
	}

	return 0;
}

/** @brief Connect to a server
 * @param address Server address, optionally with port
 * @return Connection socket, lower than 0 if connection cannot be stablished
 */
int connectToServer(char* address)
{
    struct addrinfo hints;
    struct addrinfo *result, *rp;
    int sfd = -1, s;

    struct timeval timeout;
    timeout.tv_sec = CONN_TIMEOUT_SEC;
    timeout.tv_usec = CONN_TIMEOUT_USEC;

    ip host = splitAddress(address);

    // Extract the socket required data from the address
    memset(&hints, 0, sizeof(struct addrinfo));
    hints.ai_family = AF_UNSPEC;
    hints.ai_socktype = SOCK_STREAM;
    hints.ai_flags = AI_NUMERICSERV;
    hints.ai_protocol = 0;
    s = getaddrinfo(host.address, host.port, &hints, &result);
    if(s != 0) {
        VERBOSE_MSG("Invalid address: %s (%s): %s\n",
                    host.address, host.port, gai_strerror(s));
        free(host.address);
        free(host.port);
        return -1;
    }

    // Traverse all the alternatives looking for a good connected socket
    for (rp = result; rp != NULL; rp = rp->ai_next) {
        sfd = socket(rp->ai_family, rp->ai_socktype,
                     rp->ai_protocol);
        if (sfd == -1)
            continue;

        s = connect_timeout(sfd, rp, &timeout);
        if (s == 0)
            break;

        // Invalid socket, close it and set the value as invalid
        close(sfd);
        sfd = -1;
    }
    freeaddrinfo(result);

    if(rp == NULL) {
        VERBOSE_MSG("Can't connect to: %s (%s)\n",
                    host.address, host.port);
    }
    free(host.address);
    free(host.port);

    return sfd;
}

/** @brief Connect to servers found on "ocland" file.
 * @return Number of active servers.
 */
size_t connectServers()
{
    const int switch_on = 1;

    // Traverse the servers
    if(!servers_len(servers))
        return 0;
    size_t i = 0;
    for(servers_node server_node = servers_front(servers);
        !servers_is_null(server_node);
        server_node = servers_next(server_node))
    {
        ocland_server server = servers_data(server_node);
        // Try to connect to server
#ifdef OCLAND_CLIENT_VERBOSE
        VERBOSE_MSG("Connecting with %s...\n", server->address)
#endif
        int sockfd = connectToServer(server->address);
        if(sockfd < 0) {
            servers_remove_at(servers, i);
            continue;
        }
        setsockopt(sockfd,
                   IPPROTO_TCP,
                   TCP_NODELAY,
                   (char *) &switch_on,
                   sizeof(int));
        setsockopt(sockfd,
                   IPPROTO_TCP,
                   TCP_QUICKACK,
                   (char *) &switch_on,
                   sizeof(int));
        // Check that the same architecture is applied
        char* arch = architecture_string();
        size_t msg_size = strlen(arch) * sizeof(char);
        Send(&sockfd, &msg_size, sizeof(size_t), 0);
        Send(&sockfd, arch, msg_size, 0);
        Recv(&sockfd, &msg_size, sizeof(size_t), MSG_WAITALL);
        char* msg = (void*)malloc(msg_size);
        Recv(&sockfd, msg, msg_size, MSG_WAITALL);
        if(strncmp(arch, msg, msg_size)) {
            free(msg);
            VERBOSE_MSG("%s architecture differs: %s vs %s\n",
                        server->address, arch, msg);
            close(sockfd);
            servers_remove_at(servers, i);
            continue;
        }
        free(msg);

        // Connect to the callbacks service
        unsigned int port;
        Recv(&sockfd, &port, sizeof(unsigned int), MSG_WAITALL);
        ip host = splitAddress(server->address);
        snprintf(host.port, 8, "%u", port);
        char *address = assembleAddress(host);
        free(host.address);
        free(host.port);
        int cbfd = connectToServer(address);
        if(cbfd < 0) {
            VERBOSE_MSG("Failed to connect to callbacks service %s\n",
                        address);
            free(address);
            close(sockfd);
            servers_remove_at(servers, i);
            continue;
        }

        // Setup a notifier to allow the server to call the callbacks
        notifier cb_notifier = notifier_new(cbfd);
        if(!cb_notifier) {
            VERBOSE_MSG("Failed to setup a callbacks notifier %s\n",
                        address);
            free(address);
            close(sockfd);
            close(cbfd);
            servers_remove_at(servers, i);
            continue;
        }

        free(address);

        // Connect to the data exchange service
        Recv(&sockfd, &port, sizeof(unsigned int), MSG_WAITALL);
        host = splitAddress(server->address);
        snprintf(host.port, 8, "%u", port);
        address = assembleAddress(host);
        free(host.address);
        free(host.port);
        int datafd = connectToServer(address);
        if(datafd < 0) {
            VERBOSE_MSG("Failed to connect to data exchange service %s\n",
                        address);
            free(address);
            notifier_delete(cb_notifier);
            close(sockfd);
            close(cbfd);
            servers_remove_at(servers, i);
            continue;
        }

        // Setup the sender and receiver
        async_sender data_sender = async_sender_new(datafd);
        if(!data_sender) {
            VERBOSE_MSG("Failed to setup asynchronous sender for %s\n",
                        address);
            free(address);
            notifier_delete(cb_notifier);
            close(sockfd);
            close(cbfd);
            close(datafd);
            servers_remove_at(servers, i);
            continue;
        }
        async_receiver data_receiver = async_receiver_new(datafd);
        if(!data_receiver) {
            VERBOSE_MSG("Failed to setup asynchronous receiver for %s\n",
                        address);
            free(address);
            notifier_delete(cb_notifier);
            async_sender_delete(data_sender);
            close(sockfd);
            close(cbfd);
            close(datafd);
            servers_remove_at(servers, i);
            continue;
        }

        free(address);

        // Store socket
        server->socket = sockfd;
        server->cb_socket = cbfd;
        server->data_socket = datafd;
        server->cb_notifier = cb_notifier;
        server->data_sender = data_sender;
        server->data_receiver = data_receiver;
        i++;
    }
    return servers_len(servers);
}

/** @brief Return the server address for an specific socket
 @param sockfd Server socket.
 @return Server addresses. NULL if the server does not exist.
 */
char* serverAddress(int socket)
{
    if(!servers_len(servers))
        return NULL;
    for(servers_node server_node = servers_front(servers);
        !servers_is_null(server_node);
        server_node = servers_next(server_node))
    {
        ocland_server server = servers_data(server_node);
        if(server->socket == socket)
            return server->address;
    }

    return NULL;
}

/** @brief Setup all the available platforms from the connected servers
 */
void setupPlatforms()
{
    // Traverse the servers
    if(!servers_len(servers))
        return;
    for(servers_node server_node = servers_front(servers);
        !servers_is_null(server_node);
        server_node = servers_next(server_node))
    {
        int err;
        cl_int flag;
        cl_uint num_entries = 0;
        const unsigned int method = ocland_clGetPlatformIDs;
        ocland_server server = servers_data(server_node);
        lock(server);

        // Ask the remote peer for the number of available platforms
        err = send_comm(server->socket, sizeof(unsigned int),
                                        sizeof(cl_uint),
                                        0,
                                        &method,
                                        &num_entries,
                                        NULL);
        if(err) {
            unlock(server);
            char * err_str = SocketsError();
            VERBOSE_MSG("Failure sending clGetPlatformIDs to %s: '%s'\n",
                        server->address, err_str);
            free(err_str);
            continue;
        }

        err = recv_comm(server->socket, NULL, sizeof(cl_int),
                                              sizeof(cl_uint),
                                              0,
                                              &flag,
                                              &num_entries,
                                              NULL);
        if(err) {
            unlock(server);
            char * err_str = SocketsError();
            VERBOSE_MSG("Failure receiving clGetPlatformIDs from %s: '%s'\n",
                        server->address, err_str);
            free(err_str);
            continue;
        }

        if((flag != CL_SUCCESS) || (!num_entries)) {
            // Just simply a server without OpenCL platforms
            continue;
        }

        // Ask the remote peer for the platforms
        cl_platform_id *platforms = (cl_platform_id*)malloc(
            num_entries * sizeof(cl_platform_id));
        if(!platforms) {
            unlock(server);
            VERBOSE_MSG("Failure allocating %lu bytes for %s platforms\n",
                        num_entries * sizeof(cl_platform_id), server->address);
            continue;
        }

        err = send_comm(server->socket, sizeof(unsigned int),
                                        sizeof(cl_uint),
                                        0,
                                        &method,
                                        &num_entries,
                                        NULL);
        if(err) {
            free(platforms);
            unlock(server);
            char * err_str = SocketsError();
            VERBOSE_MSG("Failure sending clGetPlatformIDs (2) to %s: '%s'\n",
                        server->address, err_str);
            free(err_str);
            continue;
        }

        recv_pending rest;
        err = recv_comm(server->socket, &rest, sizeof(cl_int),
                                               sizeof(cl_uint),
                                               0,
                                               &flag,
                                               &num_entries,
                                               NULL);

        unlock(server);  // We don't need to monopolize the conn anymore

        if(err) {
            free(platforms);
            char * err_str = SocketsError();
            VERBOSE_MSG("Failure receiving clGetPlatformIDs (2) from %s: '%s'\n",
                        server->address, err_str);
            free(err_str);
            continue;
        }

        // The remaining message is already the platforms array
        if(rest.size != num_entries * sizeof(cl_platform_id)) {
            free(platforms);
            VERBOSE_MSG(
                "Malformed platforms from %s: got %lu bytes instead of %lu\n",
                server->address,
                rest.size,
                num_entries * sizeof(cl_platform_id));
            continue;
        }
        memcpy(platforms, rest.data, rest.size);
        free(rest.data);

        // Setup the platforms
        for(cl_uint i = 0; i < num_entries; i++) {
            add_platform(platforms[i], server);
        }
        free(platforms);
    }
}

size_t oclandInit()
{
    if(!initialized){
        loadServers();
        if(!connectServers()) {
            // Very bizarre things may happened, so we report 0 servers and let
            // the stuff uninitialized, so if the user further try to init
            // ocland things will not blow up
            return 0;
        }
        setupPlatforms();
        initialized = CL_TRUE;
    }
    return servers_len(servers);
}

cl_int oclandGetGenericInfo(ocland_server server,
                            size_t param_value_size,
                            void *param_value,
                            size_t *param_value_size_ret,
                            ...)
{
    cl_int flag;
    int err;
    va_list args;

    lock(server);

    va_start(args, param_value_size_ret);
    err = vsend_comm(server->socket, args);
    va_end(args);

    if(err) {
        unlock(server);
        return CL_OUT_OF_HOST_MEMORY;
    }

    recv_pending rest;
    size_t size_ret;
    err = recv_comm(server->socket, &rest, sizeof(cl_int),
                                           sizeof(size_t),
                                           0,
                                           &flag,
                                           &size_ret,
                                           NULL);

    unlock(server);

    if(err) {
        free(rest.data);
        return CL_OUT_OF_HOST_MEMORY;
    }
    if(flag != CL_SUCCESS){
        free(rest.data);
        return flag;
    }

    if(param_value_size_ret) *param_value_size_ret = size_ret;
    // The remaining message is already the param_value
    if(param_value) {
        if(param_value_size < size_ret) {
            free(rest.data);
            return CL_INVALID_VALUE;
        }
        if(rest.size != size_ret) {
            free(rest.data);
            return CL_OUT_OF_HOST_MEMORY;
        }
        memcpy(param_value, rest.data, size_ret);
    }
    free(rest.data);

    return CL_SUCCESS;
}

/**
 * Check the validity of the memory object for the reading, writing and copy
 * operations.
 * @param  command_queue Command queue where the operation is carried out
 * @param  buffer        Memory object
 * @param  offset        The offset in bytes in the buffer
 * @param  size          The size in bytes of data
 * @return CL_SUCCESS if the passed data is allright, or an error code upon a
 * failure
 */
cl_int check_mem(cl_command_queue command_queue,
                 cl_mem           buffer,
                 size_t           offset,
                 size_t           size)
{
    if(!is_queue(command_queue))
        return CL_INVALID_COMMAND_QUEUE;
    if(!is_mem(buffer))
        return CL_INVALID_MEM_OBJECT;
    if(command_queue->context != buffer->context)
        return CL_INVALID_CONTEXT;
    if(!size || (offset + size > buffer->size))
        return CL_INVALID_VALUE;

    return CL_SUCCESS;
}

/**
 * Check the validity of the events waiting list
 * @param  command_queue Command queue where the operation is carried out
 * @param  blocking Whether it is a blocking operation or not
 * @param  num_events_in_wait_list Number of events to be waited for
 * @param  event_wait_list         List of events to be waited for
 * @return CL_SUCCESS if the passed data is allright, or an error code upon a
 * failure
 */
cl_int check_events(cl_command_queue command_queue,
                    cl_bool          blocking,
                    cl_uint          num_events_in_wait_list ,
                    const cl_event * event_wait_list)
{
    if(!is_queue(command_queue))
        return CL_INVALID_COMMAND_QUEUE;
    for(cl_uint i = 0; i < num_events_in_wait_list; i++) {
        if(!is_event(event_wait_list[i]))
            return CL_INVALID_EVENT_WAIT_LIST;
        if(event_wait_list[i]->context != command_queue->context)
            return CL_INVALID_CONTEXT;
        if(blocking && (get_event_status(event_wait_list[i]) < 0))
            return CL_EXEC_STATUS_ERROR_FOR_EVENTS_IN_WAIT_LIST;
    }
    return CL_SUCCESS;
}

/**
 * Check the Parameters for writing and reading buffer operations
 * @param  command_queue Command queue where the operation is carried out
 * @param  buffer        Memory object
 * @param  offset        The offset in bytes in the buffer
 * @param  size          The size in bytes of data
 * @param  blocking      Whether it is a blocking operation or not
 * @param  num_events_in_wait_list Number of events to be waited for
 * @param  event_wait_list         List of events to be waited for
 * @return CL_SUCCESS if the passed data is allright, or an error code upon a
 * failure
 */
cl_int check_read_or_write(cl_command_queue command_queue,
                           cl_mem           buffer,
                           size_t           offset,
                           size_t           size,
                           cl_bool          blocking,
                           cl_uint          num_events_in_wait_list ,
                           const cl_event * event_wait_list)
{
    cl_int err = check_mem(command_queue, buffer, offset, size);
    if(err != CL_SUCCESS)
        return err;
    return check_events(command_queue,
                        blocking,
                        num_events_in_wait_list,
                        event_wait_list);
}

/** Variables needed for an asynchronously data read.
 * @see thread_read_buffer()
 * @see thread_write_buffer()
 */
struct _transfer_buffer_data {
    /// Server sending the data
    ocland_server server;
    /// Identifier agreed with the server
    void *id;
    /// Size of the allocated memory
    size_t size;
    /// Allocated memory
    void *ptr;
    /// Event to mark as completed
    cl_event event;
    /// Event to mark as completed
    cl_uint num_events_in_wait_list;
    /// Event to mark as completed
    cl_event* event_wait_list;
};

typedef struct _transfer_buffer_data* transfer_buffer_data;

/** Parallel thread to receive the data using oclandEnqueueReadBuffer()
 *
 * This process is pretty obvious, we are just simply waiting for the data
 * arrives, or either an error is reported... It is actually responsibility of
 * the server to take care on the queue execution
 * @param d Casted transfer_buffer_data
 * @return NULL
 */
void *thread_read_buffer(void *d)
{
    transfer_buffer_data data = (transfer_buffer_data)d;
    while(get_event_status(data->event) > CL_COMPLETE) {
        if(!async_has_msg(data->server->data_receiver, data->id)) {
            usleep(10);
            continue;
        }
        recv_pending msg = async_recv(data->server->data_receiver, data->id);
        if(!msg.data) {
            VERBOSE_MSG("Asynchronous data reception from %s failed\n",
                        data->server->address);
            set_event_status(data->event, CL_OUT_OF_HOST_MEMORY);
            free(data);
            pthread_exit(NULL);
            return NULL;
        }
        if(msg.size != data->size) {
            VERBOSE_MSG("Wrong size of received data from %s. %lu vs. %lu\n",
                        data->server->address, msg.size, data->size);
            set_event_status(data->event, CL_OUT_OF_HOST_MEMORY);
            free(msg.data);
            free(data);
            pthread_exit(NULL);
            return NULL;
        }
        memcpy(data->ptr, msg.data, msg.size);
        free(msg.data);
        set_event_status(data->event, CL_COMPLETE);
    }

    oclandReleaseEvent(data->event);
    free(data);

    pthread_exit(NULL);
    return NULL;
}

cl_int oclandEnqueueReadBuffer(cl_command_queue     command_queue ,
                               cl_mem               buffer ,
                               cl_bool              blocking_read ,
                               size_t               offset ,
                               size_t               size ,
                               void *               ptr ,
                               cl_uint              num_events_in_wait_list ,
                               const cl_event *     event_wait_list ,
                               cl_event *           event)
{
    cl_int err = check_read_or_write(command_queue,
                                     buffer,
                                     offset,
                                     size,
                                     blocking_read,
                                     num_events_in_wait_list ,
                                     event_wait_list);
    if(err != CL_SUCCESS)
        return err;

    // Create a new event to coordinate with the server
    cl_event ret_event = new_event(command_queue->context, command_queue);
    if(!ret_event) {
        return CL_OUT_OF_HOST_MEMORY;
    }

    // Data structure to launch the parallel thread
    ocland_server server = command_queue->server;
    transfer_buffer_data thread_data = (transfer_buffer_data)malloc(
        sizeof(struct _transfer_buffer_data));
    if(!thread_data) {
        oclandReleaseEvent(ret_event);
        return CL_OUT_OF_HOST_MEMORY;
    }
    thread_data->server = server;
    thread_data->id = ret_event;
    thread_data->size = size;
    thread_data->ptr = ptr;
    thread_data->event = ret_event;
    // We just simply don't need to wait for events, since the server will not
    // start sending data until the clEnqueueReadBuffer finished
    thread_data->num_events_in_wait_list = 0;
    thread_data->event_wait_list = NULL;

    // Call the remote peer
    const unsigned int method = ocland_clEnqueueReadBuffer;
    lock(server);

    err = send_comm(server->socket, sizeof(unsigned int),
                                    sizeof(cl_command_queue),
                                    sizeof(cl_mem),
                                    sizeof(size_t),
                                    sizeof(size_t),
                                    sizeof(cl_event),
                                    sizeof(cl_uint),
                                    num_events_in_wait_list * sizeof(cl_event),
                                    0,
                                    &method,
                                    &(command_queue->ptr),
                                    &(buffer->ptr),
                                    &offset,
                                    &size,
                                    &ret_event,
                                    &num_events_in_wait_list,
                                    event_wait_list,
                                    NULL);
    unlock(server);

    if(err) {
        free(thread_data);
        oclandReleaseEvent(ret_event);
        return CL_OUT_OF_HOST_MEMORY;
    }

    // We can launch the parallel thread to download the data. We must retain
    // the event twice, one for the parallel transmission, and another one for
    // the afterwards processing (eventually including returning the event to
    // user, so clReleaseEvent() should be called later)
    pthread_t thread;
    oclandRetainEvent(ret_event);
    oclandRetainEvent(ret_event);
    err = pthread_create(&thread, NULL, thread_read_buffer, (void*)thread_data);
    if(err) {
        free(thread_data);
        ret_event->rcount = 1;
        oclandReleaseEvent(ret_event);
        return CL_OUT_OF_HOST_MEMORY;
    }

    if(blocking_read) {
        // Joining the thread would be faster and safer than waiting for the
        // event
        pthread_join(thread, NULL);
        cl_int flag = get_event_status(ret_event);
        if(flag < 0) {
            oclandReleaseEvent(ret_event);
            return flag;
        }
    }

    if(event) {
        *event = ret_event;
    } else {
        // The user has not asked for the event, and we don't need it anymore,
        // so let the parallel thread to destroy it when transmission finish
        oclandReleaseEvent(ret_event);
    }

    return CL_SUCCESS;
}

/**
 * Get the total list of events to be waited before an uploading process
 * would start
 *
 * The returned events are retained, so they shall be released at some point
 * calling oclandReleaseEvent()
 * @param  command_queue           Command queue
 * @param  num_events_in_wait_list Number of events provided by the user
 * @param  event_wait_list         List of events provided by the user
 * @param  ret_num_events          Total number of events in the returned list,
 *                                 it cannot be NULL
 * @return                         List of events to be waited. If NULL is
 *                                 returned, but ret_num_events is greater than
 *                                 0, then errors happened
 */
cl_event* get_writing_events(cl_command_queue command_queue,
                             cl_uint num_events_in_wait_list,
                             const cl_event* event_wait_list,
                             cl_uint *ret_num_events)
{
    cl_event *events = NULL;
    if(!(command_queue->props & CL_QUEUE_OUT_OF_ORDER_EXEC_MODE_ENABLE)) {
        *ret_num_events = num_events_in_wait_list;
        events = (cl_event*)malloc(*ret_num_events * sizeof(cl_event));
        if(!events)
            return NULL;
        memcpy(events, event_wait_list, *ret_num_events * sizeof(cl_event));
        for(cl_uint i = 0; i < *ret_num_events; i++) {
            oclandRetainEvent(events[i]);
        }
    } else {
        // If the command queue has CL_QUEUE_OUT_OF_ORDER_EXEC_MODE_ENABLE
        // enabled, then all previous commands shall be dispatched before we
        // can start uploading data
        cl_uint num_events_queue = 0;
        cl_event *events_queue = get_command_queue_events(command_queue,
                                                          &num_events_queue);
        if(num_events_queue && !events_queue)
            return NULL;
        // Add the number of events in the user list which are not included in
        // the command queue one
        *ret_num_events = num_events_queue;
        for(cl_uint i = 0; i < num_events_in_wait_list; i++) {
            if(event_wait_list[i]->queue != command_queue) {
                (*ret_num_events)++;
            }
        }
        if(*ret_num_events) {
            events = (cl_event*)malloc(*ret_num_events * sizeof(cl_event));
            if(!events)
                return NULL;

            memcpy(events, events_queue, num_events_queue * sizeof(cl_event));
            free(events_queue);

            cl_event *next_event = events + num_events_queue;
            for(cl_uint i = 0; i < num_events_in_wait_list; i++) {
                if(event_wait_list[i]->queue != command_queue) {
                    oclandRetainEvent(event_wait_list[i]);
                    *next_event = event_wait_list[i];
                    next_event++;
                }
            }
        }
    }

    return events;
}

/** Parallel thread to receive the data using oclandEnqueueWriteBuffer()
 * This process is a bit more tedious, since we must wait everything is ready,
 * i.e. all the preceding processes already completed, before start sending the
 * data. Thus we are waiting for all previous events, but we are also checking
 * for errors in the returning event, since we already submitted the command in
 * the peer
 * @param d Casted write_buffer_data
 * @return NULL
 */
void *thread_write_buffer(void *d)
{
    transfer_buffer_data data = (transfer_buffer_data)d;

    // Wait for the preceding events. We actually don't care whether they
    // successfully finish or not
    wait_for_events(data->num_events_in_wait_list,
                    (const cl_event*)data->event_wait_list,
                    CL_COMPLETE);
    for(cl_uint i = 0; i < data->num_events_in_wait_list; i++) {
        oclandReleaseEvent(data->event_wait_list[i]);
    }
    free(data->event_wait_list);

    // Start uploading the data
    ssize_t sent = async_send(data->server->data_sender,
                              (void*)(data->event),
                              data->size,
                              data->ptr);
    if(sent != sizeof(void*) + sizeof(size_t) + data->size) {
        VERBOSE_MSG("Upload error, %lu bytes sent, while %lu were expected\n",
                    sent, sizeof(void*) + sizeof(size_t) + data->size);
        set_event_status(data->event, CL_OUT_OF_HOST_MEMORY);
    }

    oclandReleaseEvent(data->event);
    free(data);

    pthread_exit(NULL);
    return NULL;
}

cl_int oclandEnqueueWriteBuffer(cl_command_queue    command_queue ,
                                cl_mem              buffer ,
                                cl_bool             blocking_write ,
                                size_t              offset ,
                                size_t              size ,
                                const void *        ptr ,
                                cl_uint             num_events_in_wait_list ,
                                const cl_event *    event_wait_list ,
                                cl_event *          event)
{
    cl_int err = check_read_or_write(command_queue,
                                     buffer,
                                     offset,
                                     size,
                                     blocking_write,
                                     num_events_in_wait_list ,
                                     event_wait_list);
    if(err != CL_SUCCESS)
        return err;

    // Create a new event to coordinate with the server
    cl_event ret_event = new_event(command_queue->context, command_queue);
    if(!ret_event) {
        return CL_OUT_OF_HOST_MEMORY;
    }

    // Create the data structure to launch a parallel thread, which will be
    // asynchronouly waiting for the preceding events to be completed before
    // uploading the data. This is needed since the same pointer would be
    // simultaneously used for reading and writing afterwards, so the content
    // itself would change meanwhile we are waiting
    ocland_server server = command_queue->server;
    transfer_buffer_data thread_data = (transfer_buffer_data)malloc(
        sizeof(struct _transfer_buffer_data));
    if(!thread_data) {
        oclandReleaseEvent(ret_event);
        return CL_OUT_OF_HOST_MEMORY;
    }
    thread_data->server = server;
    thread_data->id = ret_event;
    thread_data->size = size;
    thread_data->ptr = (void*)ptr;
    thread_data->event = ret_event;
    thread_data->event_wait_list = get_writing_events(
        command_queue,
        num_events_in_wait_list,
        event_wait_list,
        &(thread_data->num_events_in_wait_list));

    // We can launch the parallel thread to upload the data. We must retain
    // the event twice, one for the parallel transmission, and another one for
    // the afterwards processing (eventually including returning the event to
    // user, so clReleaseEvent() should be called by him later on)
    pthread_t thread;
    oclandRetainEvent(ret_event);
    oclandRetainEvent(ret_event);
    err = pthread_create(&thread, NULL,
                         thread_write_buffer, (void*)thread_data);
    if(err) {
        free(thread_data->event_wait_list);
        free(thread_data);
        ret_event->rcount = 1;
        oclandReleaseEvent(ret_event);
        return CL_OUT_OF_HOST_MEMORY;
    }

    // Call the remote peer
    const unsigned int method = ocland_clEnqueueWriteBuffer;
    lock(server);

    err = send_comm(server->socket, sizeof(unsigned int),
                                    sizeof(cl_command_queue),
                                    sizeof(cl_mem),
                                    sizeof(size_t),
                                    sizeof(size_t),
                                    sizeof(cl_event),
                                    sizeof(cl_uint),
                                    num_events_in_wait_list * sizeof(cl_event),
                                    0,
                                    &method,
                                    &(command_queue->ptr),
                                    &(buffer->ptr),
                                    &offset,
                                    &size,
                                    &ret_event,
                                    &num_events_in_wait_list,
                                    event_wait_list,
                                    NULL);
    unlock(server);

    if(err) {
        ret_event->rcount = 1;
        oclandReleaseEvent(ret_event);
        return CL_OUT_OF_HOST_MEMORY;
    }

    if(blocking_write) {
        // This time we cannot just simply join the thread, but we need to wait
        // until the server provides feedback
        wait_for_events(1, &ret_event, CL_COMPLETE);
        cl_int flag = get_event_status(ret_event);
        if(flag < 0) {
            oclandReleaseEvent(ret_event);
            return flag;
        }
    }

    if(event) {
        *event = ret_event;
    } else {
        // The event can be destroyed after the data has been sent and the
        // server reported an answer
        oclandReleaseEvent(ret_event);
    }

    return CL_SUCCESS;
}

cl_int oclandEnqueueCopyBuffer(cl_command_queue     command_queue ,
                               cl_mem               src_buffer ,
                               cl_mem               dst_buffer ,
                               size_t               src_offset ,
                               size_t               dst_offset ,
                               size_t               cb ,
                               cl_uint              num_events_in_wait_list ,
                               const cl_event *     event_wait_list ,
                               cl_event *           event)
{
    return CL_OUT_OF_RESOURCES;
}

cl_int oclandEnqueueCopyImage(cl_command_queue      command_queue ,
                              cl_mem                src_image ,
                              cl_mem                dst_image ,
                              const size_t *        src_origin ,
                              const size_t *        dst_origin ,
                              const size_t *        region ,
                              cl_uint               num_events_in_wait_list ,
                              const cl_event *      event_wait_list ,
                              cl_event *            event)
{
    return CL_OUT_OF_RESOURCES;
}

cl_int oclandEnqueueCopyImageToBuffer(cl_command_queue  command_queue ,
                                      cl_mem            src_image ,
                                      cl_mem            dst_buffer ,
                                      const size_t *    src_origin ,
                                      const size_t *    region ,
                                      size_t            dst_offset ,
                                      cl_uint           num_events_in_wait_list ,
                                      const cl_event *  event_wait_list ,
                                      cl_event *        event)
{
    return CL_OUT_OF_RESOURCES;
}

cl_int oclandEnqueueCopyBufferToImage(cl_command_queue  command_queue ,
                                      cl_mem            src_buffer ,
                                      cl_mem            dst_image ,
                                      size_t            src_offset ,
                                      const size_t *    dst_origin ,
                                      const size_t *    region ,
                                      cl_uint           num_events_in_wait_list ,
                                      const cl_event *  event_wait_list ,
                                      cl_event *        event)
{
    return CL_OUT_OF_RESOURCES;
}

cl_int oclandEnqueueNDRangeKernel(cl_command_queue  command_queue ,
                                  cl_kernel         kernel ,
                                  cl_uint           work_dim ,
                                  const size_t *    global_work_offset ,
                                  const size_t *    global_work_size ,
                                  const size_t *    local_work_size ,
                                  cl_uint           num_events_in_wait_list ,
                                  const cl_event *  event_wait_list ,
                                  cl_event *        event)
{
    return CL_OUT_OF_RESOURCES;
}

cl_int oclandEnqueueReadImage(cl_command_queue      command_queue ,
                              cl_mem                image ,
                              cl_bool               blocking_read ,
                              const size_t *        origin ,
                              const size_t *        region ,
                              size_t                row_pitch ,
                              size_t                slice_pitch ,
                              size_t                element_size ,
                              void *                ptr ,
                              cl_uint               num_events_in_wait_list ,
                              const cl_event *      event_wait_list ,
                              cl_event *            event)
{
    return CL_OUT_OF_RESOURCES;
}

cl_int oclandEnqueueWriteImage(cl_command_queue     command_queue ,
                               cl_mem               image ,
                               cl_bool              blocking_write ,
                               const size_t *       origin ,
                               const size_t *       region ,
                               size_t               row_pitch ,
                               size_t               slice_pitch ,
                               size_t               element_size ,
                               const void *         ptr ,
                               cl_uint              num_events_in_wait_list ,
                               const cl_event *     event_wait_list ,
                               cl_event *           event)
{
    return CL_OUT_OF_RESOURCES;
}

// -------------------------------------------- //
//                                              //
// OpenCL 1.1 methods                           //
//                                              //
// -------------------------------------------- //

cl_int oclandEnqueueReadBufferRect(cl_command_queue     command_queue ,
                                   cl_mem               mem ,
                                   cl_bool              blocking_read ,
                                   const size_t *       buffer_origin ,
                                   const size_t *       host_origin ,
                                   const size_t *       region ,
                                   size_t               buffer_row_pitch ,
                                   size_t               buffer_slice_pitch ,
                                   size_t               host_row_pitch ,
                                   size_t               host_slice_pitch ,
                                   void *               ptr ,
                                   cl_uint              num_events_in_wait_list ,
                                   const cl_event *     event_wait_list ,
                                   cl_event *           event)
{
    return CL_OUT_OF_RESOURCES;
}

cl_int oclandEnqueueWriteBufferRect(cl_command_queue     command_queue ,
                                    cl_mem               mem ,
                                    cl_bool              blocking_write ,
                                    const size_t *       buffer_origin ,
                                    const size_t *       host_origin ,
                                    const size_t *       region ,
                                    size_t               buffer_row_pitch ,
                                    size_t               buffer_slice_pitch ,
                                    size_t               host_row_pitch ,
                                    size_t               host_slice_pitch ,
                                    const void *         ptr ,
                                    cl_uint              num_events_in_wait_list ,
                                    const cl_event *     event_wait_list ,
                                    cl_event *           event)
{
    return CL_OUT_OF_RESOURCES;
}

cl_int oclandEnqueueCopyBufferRect(cl_command_queue     command_queue ,
                                   cl_mem               src_buffer ,
                                   cl_mem               dst_buffer ,
                                   const size_t *       src_origin ,
                                   const size_t *       dst_origin ,
                                   const size_t *       region ,
                                   size_t               src_row_pitch ,
                                   size_t               src_slice_pitch ,
                                   size_t               dst_row_pitch ,
                                   size_t               dst_slice_pitch ,
                                   cl_uint              num_events_in_wait_list ,
                                   const cl_event *     event_wait_list ,
                                   cl_event *           event)
{
    return CL_OUT_OF_RESOURCES;
}

cl_int oclandEnqueueFillBuffer(cl_command_queue    command_queue ,
                               cl_mem              mem ,
                               const void *        pattern ,
                               size_t              pattern_size ,
                               size_t              offset ,
                               size_t              cb ,
                               cl_uint             num_events_in_wait_list ,
                               const cl_event *    event_wait_list ,
                               cl_event *          event)
{
    return CL_OUT_OF_RESOURCES;
}

cl_int oclandEnqueueFillImage(cl_command_queue    command_queue ,
                              cl_mem              image ,
                              size_t              fill_color_size ,
                              const void *        fill_color ,
                              const size_t *      origin ,
                              const size_t *      region ,
                              cl_uint             num_events_in_wait_list ,
                              const cl_event *    event_wait_list ,
                              cl_event *          event)
{
    return CL_OUT_OF_RESOURCES;
}

cl_int oclandEnqueueMigrateMemObjects(cl_command_queue        command_queue ,
                                      cl_uint                 num_mem_objects ,
                                      const cl_mem *          mem_objects ,
                                      cl_mem_migration_flags  flags ,
                                      cl_uint                 num_events_in_wait_list ,
                                      const cl_event *        event_wait_list ,
                                      cl_event *              event)
{
    return CL_OUT_OF_RESOURCES;
}

cl_int oclandEnqueueMarkerWithWaitList(cl_command_queue  command_queue ,
                                       cl_uint            num_events_in_wait_list ,
                                       const cl_event *   event_wait_list ,
                                       cl_event *         event)
{
    return CL_OUT_OF_RESOURCES;
}

cl_int oclandEnqueueBarrierWithWaitList(cl_command_queue  command_queue ,
                                        cl_uint            num_events_in_wait_list ,
                                        const cl_event *   event_wait_list ,
                                        cl_event *         event)
{
    return CL_OUT_OF_RESOURCES;
}
