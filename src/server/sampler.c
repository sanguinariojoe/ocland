/*
 *  This file is part of ocland, a free cloud OpenCL interface.
 *  Copyright (C) 2012-2019  Jose Luis Cercos Pita <jlcercos@gmail.com>
 *
 *  ocland is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  ocland is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with ocland.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <ocland/server/context.h>
#include <ocland/server/sampler.h>
#include <ocland/server/log.h>
#include <ocland/server/ocland_version.h>

void ocland_clCreateSampler(int* clientfd, validator v, recv_pending msg)
{
    VERBOSE_IN();

    cl_int flag = CL_OUT_OF_HOST_MEMORY;
    cl_context context;
    cl_bool normalized_coords;
    cl_addressing_mode addressing_mode;
    cl_filter_mode filter_mode;
    cl_sampler sampler = NULL;

    size_t msg_size = sizeof(cl_context) +
                      sizeof(cl_bool) +
                      sizeof(cl_addressing_mode) +
                      sizeof(cl_filter_mode);
    if( (msg.size != msg_size) ||
        !disassemble_msg(msg.data, sizeof(cl_context), &context,
                                   sizeof(cl_bool), &normalized_coords,
                                   sizeof(cl_addressing_mode), &addressing_mode,
                                   sizeof(cl_filter_mode), &filter_mode,
                                   0, NULL) )
    {
        send_comm(*clientfd, sizeof(cl_int), sizeof(cl_sampler), 0,
                             &flag, &sampler, NULL);
        VERBOSE_OUT(flag);
        return;
    }

    if(!is_context(v, context)) {
        flag = CL_INVALID_CONTEXT;
        send_comm(*clientfd, sizeof(cl_int), sizeof(cl_sampler), 0,
                             &flag, &sampler, NULL);
        VERBOSE_OUT(flag);
        return;
    }

    sampler = clCreateSampler(context,
                              normalized_coords,
                              addressing_mode,
                              filter_mode,
                              &flag);
    send_comm(*clientfd, sizeof(cl_int), sizeof(cl_sampler), 0,
                         &flag, &sampler, NULL);
    if(flag != CL_SUCCESS){
        VERBOSE_OUT(flag);
        return;
    }
    validator_add_sampler(v, sampler);
    VERBOSE_OUT(flag);
}

void ocland_clRetainSampler(int* clientfd, validator v, recv_pending msg)
{
    VERBOSE_IN();

    cl_int flag = CL_OUT_OF_HOST_MEMORY;

    // The received data shall be directly the context
    if(msg.size != sizeof(cl_sampler)) {
        send_comm(*clientfd, sizeof(cl_int), 0, &flag, NULL);
        VERBOSE_OUT(flag);
        return;
    }
    const cl_sampler sampler = *((cl_sampler*)(msg.data));
    // Even though we can keep reference counting at this side, actually is the
    // client who is taking care on such task, so we actually never call to
    // clRetainMemObject()
    if(!is_sampler(v, sampler))
        flag = CL_INVALID_SAMPLER;
    else
        flag = CL_SUCCESS;
    send_comm(*clientfd, sizeof(cl_int), 0, &flag, NULL);
    VERBOSE_OUT(flag);
}

void ocland_clReleaseSampler(int* clientfd, validator v, recv_pending msg)
{
    VERBOSE_IN();

    cl_int flag = CL_OUT_OF_HOST_MEMORY;

    // The received data shall be directly the sampler
    if(msg.size != sizeof(cl_sampler)) {
        send_comm(*clientfd, sizeof(cl_int), 0, &flag, NULL);
        VERBOSE_OUT(flag);
        return;
    }
    const cl_sampler sampler = *((cl_sampler*)(msg.data));
    // Even though we can keep reference counting at this side, actually is the
    // client who is taking care on such task, so if we reached this point we
    // can assume that the samplerory object shall be completelly detroyed
    if(!is_sampler(v, sampler))
        flag = CL_INVALID_SAMPLER;
    else {
        flag = clReleaseSampler(sampler);
        if(flag == CL_SUCCESS)
            validator_remove_sampler(v, sampler);
    }

    send_comm(*clientfd, sizeof(cl_int), 0, &flag, NULL);
    VERBOSE_OUT(flag);
}

DEFINE_INFO_GETTER(clGetSamplerInfo, cl_sampler, cl_image_info, sampler)

// ----------------------------------
// OpenCL 2.0
// ----------------------------------
