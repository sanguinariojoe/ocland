/*
 *  This file is part of ocland, a free cloud OpenCL interface.
 *  Copyright (C) 2012  Jose Luis Cercos Pita <jl.cercos@upm.es>
 *
 *  ocland is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  ocland is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with ocland.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <pthread.h>
#include <signal.h>

#include <ocland/server/validator.h>
#include <ocland/server/context.h>
#include <ocland/server/ocland_event.h>

#include <ocland/common/async_sender.h>
#include <ocland/common/async_receiver.h>
#include <ocland/common/typed_list.h>
DEFINE_LIST(cl_platform_id, platforms)
DEFINE_LIST(cl_device_id, devices)
DEFINE_LIST(cl_context, contexts)
DEFINE_LIST(cl_command_queue, queues)
DEFINE_LIST(cl_mem, mems)
DEFINE_LIST(cl_sampler, samplers)
DEFINE_LIST(cl_program, programs)
DEFINE_LIST(cl_kernel, kernels)
DEFINE_LIST(cl_event, events)
DEFINE_LIST(pthread_t, threads)

struct _validator{
    /// Client socket
    int client_fd;
    /// Client callbacks socket
    int client_cb_fd;
    /// Callbacks asynchronous sender
    async_sender cb_sender;
    /// Client data exchange socket
    int client_data_fd;
    /// Data asynchronous sender
    async_sender data_sender;
    /// Data asynchronous sender
    async_receiver data_receiver;
    /// Client address
    struct sockaddr_in6 client_address;
    /// Client address string
    char client_addr_str[INET6_ADDRSTRLEN];
    /// List of platforms
    List* platforms;
    /// List of devices
    List* devices;
    /// List of contexts
    List* contexts;
    /// List of command queues
    List* queues;
    /// List of memory objects
    List* mems;
    /// List of samplers
    List* samplers;
    /// List of programs
    List* programs;
    /// List of kernels
    List* kernels;
    /// List of event linkers
    ocland_events_linker events;
    /// List of threads to be stop before destroying the validator
    List* threads;
    /// Mutex to grant threads is not simultanenously changed by several threads
    pthread_mutex_t lock;
};

/**
 * Set the devices associated to a platform inside the validator
 * @param v Validator
 * @param platform Considered platform
 */
void validator_add_devices(validator v,
                           cl_platform_id platform,
                           cl_device_type device_type)
{
    cl_uint i;
    cl_int flag;
    cl_uint num_devices = 0;
    cl_device_id *devices = NULL;

    flag = clGetDeviceIDs(platform, device_type, 0, NULL, &num_devices);
    if( (flag != CL_SUCCESS) || (!num_devices) )
        return;
    devices = (cl_device_id*)malloc(num_devices * sizeof(cl_device_id));
    if(!devices)
        return;
    flag = clGetDeviceIDs(platform, device_type, num_devices, devices, NULL);
    if(flag != CL_SUCCESS)
        return;

    for(i = 0; i < num_devices; i++){
        devices_push_back(v->devices, devices[i]);
    }
    free(devices);
}

/**
 * Set the platforms and devices inside the validator
 * @param v Validator
 */
void validator_add_platforms_and_devices(validator v)
{
    cl_uint i;
    cl_int flag;
    cl_uint num_platforms = 0;
    cl_platform_id *platforms = NULL;

    flag = clGetPlatformIDs(0, NULL, &num_platforms);
    if( (flag != CL_SUCCESS) || (!num_platforms) )
        return;
    platforms = (cl_platform_id*)malloc(num_platforms * sizeof(cl_platform_id));
    if(!platforms)
        return;
    flag = clGetPlatformIDs(num_platforms, platforms, NULL);
    if(flag != CL_SUCCESS)
        return;

    for(i = 0; i < num_platforms; i++){
        platforms_push_back(v->platforms, platforms[i]);
        validator_add_devices(v, platforms[i], CL_DEVICE_TYPE_ALL);
        validator_add_devices(v, platforms[i], CL_DEVICE_TYPE_CUSTOM);
    }
    free(platforms);
}

validator validator_new(int clientfd) {
    struct sockaddr_in6 addr;
    socklen_t len_addr = sizeof(addr);
    if(getpeername(clientfd, (struct sockaddr *)&addr, &len_addr))
        return NULL;

    validator v = (validator)malloc(sizeof(struct _validator));
    v->client_fd = clientfd;
    v->client_cb_fd = clientfd;    // To avoid some race conditions
    v->cb_sender = NULL;
    v->client_data_fd = clientfd;  // To avoid some race conditions
    v->data_sender = NULL;
    v->data_receiver = NULL;
    v->client_address = addr;
    inet_ntop(AF_INET6, &addr.sin6_addr,
              v->client_addr_str, sizeof(v->client_addr_str));
    v->platforms = platforms_new();
    v->devices = devices_new();
    validator_add_platforms_and_devices(v);
    v->contexts = contexts_new();
    v->queues = queues_new();
    v->mems = mems_new();
    v->samplers = samplers_new();
    v->programs = programs_new();
    v->kernels = kernels_new();
    v->events = new_ocland_events_linker();
    v->threads = threads_new();

    pthread_mutex_init(&(v->lock), NULL);

    return v;
}

void validator_delete(validator v)
{
    disconnect(v);

    // Wait until all running commands finish (to avoid callbacks crash the
    // whole server)
    for(queues_node node = queues_front(v->queues);
        !queues_is_null(node);
        node = queues_next(node))
    {
        clFinish(queues_data(node));
    }
    // Wait until the parallel threads are finishing. Actually, at this point
    // the parallel thread might not finished, but at least we can grant it will
    // safely finish afterwards (no more validator data is required)
    while(true) {
        pthread_mutex_lock(&(v->lock));
        const cl_uint n = threads_len(v->threads);
        pthread_mutex_unlock(&(v->lock));
        if(!n)
            break;
    }
    pthread_mutex_destroy(&(v->lock));

    for(contexts_node node = contexts_front(v->contexts);
        !contexts_is_null(node);
        node = contexts_next(node))
    {
        release_context(contexts_data(node));
    }
    for(queues_node node = queues_front(v->queues);
        !queues_is_null(node);
        node = queues_next(node))
    {
        clReleaseCommandQueue(queues_data(node));
    }
    for(mems_node node = mems_front(v->mems);
        !mems_is_null(node);
        node = mems_next(node))
    {
        clReleaseMemObject(mems_data(node));
    }
    for(samplers_node node = samplers_front(v->samplers);
        !samplers_is_null(node);
        node = samplers_next(node))
    {
        clReleaseSampler(samplers_data(node));
    }
    for(programs_node node = programs_front(v->programs);
        !programs_is_null(node);
        node = programs_next(node))
    {
        clReleaseProgram(programs_data(node));
    }
    for(kernels_node node = kernels_front(v->kernels);
        !kernels_is_null(node);
        node = kernels_next(node))
    {
        clReleaseKernel(kernels_data(node));
    }

    platforms_delete(v->platforms);
    devices_delete(v->devices);
    contexts_delete(v->contexts);
    queues_delete(v->queues);
    mems_delete(v->mems);
    samplers_delete(v->samplers);
    programs_delete(v->programs);
    kernels_delete(v->kernels);
    del_ocland_events_linker(v->events);

    free(v);
}

int get_client_socket(validator v)
{
    return v->client_fd;
}

bool is_connected(validator v)
{
    if(v->client_fd < 0)
        return false;

    struct sockaddr_in6 addr;
    socklen_t len_addr = sizeof(addr);
    bool fd_on = !getpeername(v->client_fd,
                              (struct sockaddr *)(&addr),
                              &len_addr);
    bool cb_on = !getpeername(v->client_cb_fd,
                              (struct sockaddr *)(&addr),
                              &len_addr);
    bool data_on = !getpeername(v->client_data_fd,
                                (struct sockaddr *)(&addr),
                                &len_addr);
    if(fd_on && cb_on && data_on)
        return true;

    if(fd_on)
        close(v->client_fd);
    if(cb_on && v->cb_sender) {
        async_sender_delete(v->cb_sender);
        close(v->client_cb_fd);
    }
    if(data_on && v->data_sender && v->data_receiver) {
        async_sender_delete(v->data_sender);
        async_receiver_delete(v->data_receiver);
        close(v->client_data_fd);
    }

    v->client_fd = -1;
    v->client_cb_fd = -1;
    v->cb_sender = NULL;
    v->client_data_fd = -1;
    v->data_sender = NULL;
    v->data_receiver = NULL;
    return false;
}

void disconnect(validator v)
{
    if(!is_connected(v))
        return;
    close(v->client_fd);
    v->client_fd = -1;
    async_sender_delete(v->cb_sender);
    close(v->client_cb_fd);
    v->client_cb_fd = -1;
    v->cb_sender = NULL;
    async_sender_delete(v->data_sender);
    async_receiver_delete(v->data_receiver);
    close(v->client_data_fd);
    v->client_data_fd = -1;
    v->data_sender = NULL;
    v->data_receiver = NULL;
}

struct sockaddr_in6 get_client_addr(validator v)
{
    return v->client_address;
}

char* get_client_addr_str(validator v)
{
    return v->client_addr_str;
}

bool has_same_addr(validator v, struct sockaddr_in6 addr)
{
    if(memcmp(v->client_address.sin6_addr.s6_addr,
              addr.sin6_addr.s6_addr,
              sizeof(struct in6_addr)))
        return false;
    return true;
}

void set_client_cb_socket(validator v, int fd)
{
    async_sender_delete(v->cb_sender);
    v->cb_sender = async_sender_new(fd);
    if(!v->cb_sender)
        return;

    v->client_cb_fd = fd;
}

int get_client_cb_socket(validator v)
{
    return v->client_cb_fd;
}

bool async_send_cb(validator v, void* id, size_t size, void* data)
{
    if(!v->cb_sender)
        return false;
    ssize_t sent = async_send(v->cb_sender, id, size, data);
    if( (sent <= 0) || ((size_t)sent != sizeof(void*) + size) )
        return false;
    return true;
}

void set_client_data_socket(validator v, int fd)
{
    async_sender_delete(v->data_sender);
    v->data_sender = async_sender_new(fd);
    if(!v->data_sender)
        return;
    async_receiver_delete(v->data_receiver);
    v->data_receiver = async_receiver_new(fd);
    if(!v->data_receiver) {
        async_sender_delete(v->data_sender);
        v->data_sender = async_sender_new(fd);
        return;
    }

    v->client_data_fd = fd;
}

int get_client_data_socket(validator v)
{
    return v->client_data_fd;
}

bool async_send_data(validator v, void* id, size_t size, void* data)
{
    if(!v->data_sender)
        return false;
    ssize_t sent = async_send(v->data_sender, id, size, data);
    if( (sent <= 0) || ((size_t)sent != sizeof(void*) + size) )
        return false;
    return true;
}

recv_pending async_recv_data(validator v, void* id)
{
    if(!v->data_receiver) {
        recv_pending bad_data = {0, NULL};
        return bad_data;
    }
    return async_recv(v->data_receiver, id);
}

#undef PASTER
#define PASTER(x, y) x ## y
// #undef CAT
// #define CAT(x,y) PASTER(x,y)
#define FUNCNAME(fname, name) CAT(fname, name)
#define SUFFIXED(name, suffix) CAT(name, suffix)

/**
 * Functions to check the existence of an OpenCL entity
 *
 * For instance, IS(platform, cl_platform_id) will take the following form:
 *
 * \code{.c}
 * cl_bool is_platform(validator v, cl_platform_id platform)
 * {
 *      return platforms_find(v->platforms, platform) <
 *          platforms_len(v->platforms);
 * }
 * \endcode
 */
#define IS(name, tname)                                                        \
cl_bool FUNCNAME(is_, name)(validator v, tname name)                           \
{                                                                              \
    return SUFFIXED(name, s_find)(v->SUFFIXED(name, s), name) <                \
        SUFFIXED(name, s_len)(v->SUFFIXED(name, s));                           \
}

/**
 * Functions to add OpenCL entities
 *
 * For instance, ADD(device, cl_device_id) will take the following form:
 *
 * \code{.c}
 * void validator_add_device(validator v, cl_device_id device)
 * {
 *     devices_push_back(v->devices, device);
 * }
 * \endcode
 */
#define ADD(name, tname)                                                       \
void FUNCNAME(validator_add_, name)(validator v, tname name)                   \
{                                                                              \
    SUFFIXED(name, s_push_back)(v->SUFFIXED(name, s), name);                   \
}

/**
 * Functions to remove OpenCL entities
 *
 * For instance, DEL(device, cl_device_id) will take the following form:
 *
 * \code{.c}
 * cl_bool validator_remove_device(validator v, cl_device_id device)
 * {
 *      size_t index = devices_find(v->devices, device);
 *      if(index >= devices_len(v->devices))
 *          return CL_FALSE;
 *      devices_remove_at(v->devices, index);
 *      return CL_TRUE;
 * }
 * \endcode
 */
#define DEL(name, tname)                                                       \
cl_bool FUNCNAME(validator_remove_, name)(validator v, tname name)             \
{                                                                              \
    size_t index = SUFFIXED(name, s_find)(v->SUFFIXED(name, s), name);         \
    if(index >= SUFFIXED(name, s_len)(v->SUFFIXED(name, s)))                   \
        return CL_FALSE;                                                       \
    SUFFIXED(name, s_remove_at)(v->SUFFIXED(name, s), index);                  \
    return CL_TRUE;                                                            \
}

IS(platform, cl_platform_id)

IS(device, cl_device_id)
ADD(device, cl_device_id)
DEL(device, cl_device_id)

IS(context, cl_context)
ADD(context, cl_context)
DEL(context, cl_context)

IS(queue, cl_command_queue)
ADD(queue, cl_command_queue)
DEL(queue, cl_command_queue)

IS(mem, cl_mem)
ADD(mem, cl_mem)
DEL(mem, cl_mem)

IS(sampler, cl_sampler)
ADD(sampler, cl_sampler)
DEL(sampler, cl_sampler)

IS(program, cl_program)
ADD(program, cl_program)
DEL(program, cl_program)

IS(kernel, cl_kernel)
ADD(kernel, cl_kernel)
DEL(kernel, cl_kernel)

cl_bool is_event(validator v, cl_event event)
{
    return event_local2peer(v->events, event) != NULL;
}

cl_event from_remote_event(validator v, cl_event event)
{
    return event_peer2local(v->events, event);
}

cl_event to_remote_event(validator v, cl_event event)
{
    return event_local2peer(v->events, event);
}

void validator_add_event(validator v, cl_event event, cl_event peer)
{
    add_ocland_event_linker(v->events, event, peer);
}

cl_bool validator_remove_event(validator v, cl_event event)
{
    if(!is_event(v, event))
        return CL_FALSE;
    remove_ocland_event_linker(v->events, event);
    return CL_TRUE;
}

#define ADD(name, tname)                                                       \
void FUNCNAME(validator_add_, name)(validator v, tname name)                   \
{                                                                              \
    SUFFIXED(name, s_push_back)(v->SUFFIXED(name, s), name);                   \
}

#define DEL(name, tname)                                                       \
cl_bool FUNCNAME(validator_remove_, name)(validator v, tname name)             \
{                                                                              \
    size_t index = SUFFIXED(name, s_find)(v->SUFFIXED(name, s), name);         \
    if(index >= SUFFIXED(name, s_len)(v->SUFFIXED(name, s)))                   \
        return CL_FALSE;                                                       \
    SUFFIXED(name, s_remove_at)(v->SUFFIXED(name, s), index);                  \
    return CL_TRUE;                                                            \
}

void validator_add_thread(validator v, pthread_t t)
{
    pthread_mutex_lock(&(v->lock));
    threads_push_back(v->threads, t);
    pthread_mutex_unlock(&(v->lock));
}

cl_bool validator_remove_thread(validator v, pthread_t t)
{
    pthread_mutex_lock(&(v->lock));
    if(!v->threads) {
        pthread_mutex_unlock(&(v->lock));
        return CL_FALSE;
    }
    size_t index = threads_find(v->threads, t);
    if(index >= threads_len(v->threads)) {
        pthread_mutex_unlock(&(v->lock));
        return CL_FALSE;
    }
    threads_remove_at(v->threads, index);
    pthread_mutex_unlock(&(v->lock));
    return CL_TRUE;
}
