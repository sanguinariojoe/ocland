/*
 *  This file is part of ocland, a free cloud OpenCL interface.
 *  Copyright (C) 2012-2019  Jose Luis Cercos Pita <jlcercos@gmail.com>
 *
 *  ocland is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  ocland is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with ocland.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <ocland/server/context.h>
#include <ocland/server/log.h>
#include <ocland/server/ocland_version.h>

/**
 * Data passed as user data to context notify callback, context_notify()
 */
struct _context_notify_data
{
    /// Useful information regarding the affected peer
    validator v;
    /// The instance affected
    cl_context context;
};

typedef struct _context_notify_data* context_notify_data;

#include <ocland/common/typed_list.h>
DEFINE_LIST(context_notify_data, context_user_datas)

/// List of contexts
static List* context_user_datas = NULL;

cl_int release_context(cl_context context)
{
    cl_int flag = clReleaseContext(context);
    if(flag != CL_SUCCESS)
        return flag;

    // We shall destroy as well the associated user_data
    context_user_datas_node node;
    size_t i = 0;
    for(node = context_user_datas_front(context_user_datas);
        !context_user_datas_is_null(node);
        node = context_user_datas_next(node))
    {
        context_notify_data user_data = context_user_datas_data(node);
        if(user_data->context != context) {
            i += 1;
            continue;
        }
        free(user_data);
        context_user_datas_remove_at(context_user_datas, i);
        break;
    }

    return flag;
}

void report_context_error(validator v,
                          cl_context context,
                          const char *errinfo)
{
    size_t msg_size = (strlen(errinfo) + 1) * sizeof(char);
    if(async_send_cb(v, (void*)context, msg_size, (void*)errinfo)) {
        printf("Failure reporting context error to %s: ",
               get_client_addr_str(v));
        fflush(stdout);
    }
}

/**
 * Process the errors happening in the context
 * @param  errinfo      Human readable error description
 * @param  private_info Pointer to binary data that is returned by the OpenCL
 *                      implementation
 * @param  cb           Size of #private_info
 * @param  user_data    User provided data
 */
void CL_CALLBACK context_notify(const char *errinfo,
                                const void *private_info,
                                size_t cb,
                                void *user_data)
{
    context_notify_data data = (context_notify_data)user_data;
    report_context_error(data->v, data->context, errinfo);
}

void ocland_clCreateContext(int* clientfd, validator v, recv_pending msg)
{
    VERBOSE_IN();

    cl_int flag = CL_OUT_OF_HOST_MEMORY;
    cl_uint num_properties = 0;
    cl_context_properties *properties = NULL;
 	cl_uint num_devices = 0;
  	cl_device_id *devices = NULL;
    cl_context context = NULL;

    char *ptr;
    size_t msg_size = sizeof(cl_uint);
    if( (msg.size < msg_size) ||
        !(ptr = disassemble_msg(msg.data, sizeof(cl_uint), &num_properties,
                                          0, NULL)) ||
        !num_properties )
    {
        send_comm(*clientfd, sizeof(cl_int), sizeof(cl_context), 0,
                             &flag, &context, NULL);
        VERBOSE_OUT(flag);
        return;
    }
    size_t props_size = num_properties * sizeof(cl_context_properties);
    msg_size += props_size + sizeof(cl_uint);
    properties = (cl_device_partition_property*)malloc(props_size);
    if( (!properties) ||
        (msg.size < msg_size) ||
        !(ptr = disassemble_msg(ptr, props_size, properties,
                                sizeof(cl_uint), &num_devices,
                                0, NULL)) )
    {
        send_comm(*clientfd, sizeof(cl_int), sizeof(cl_context), 0,
                             &flag, &context, NULL);
        VERBOSE_OUT(flag);
        return;
    }
    if(!num_devices) {
        flag = CL_INVALID_VALUE;
        send_comm(*clientfd, sizeof(cl_int), sizeof(cl_context), 0,
                             &flag, &context, NULL);
        VERBOSE_OUT(flag);
        return;
    }
    msg_size += num_devices * sizeof(cl_device_id);
    devices = (cl_device_id*)malloc(num_devices * sizeof(cl_device_id));
    if( (!properties) ||
        (msg.size != msg_size) ||
        !disassemble_msg(ptr, num_devices * sizeof(cl_device_id), devices,
                         0, NULL) )
    {
        send_comm(*clientfd, sizeof(cl_int), sizeof(cl_context), 0,
                             &flag, &context, NULL);
        VERBOSE_OUT(flag);
        return;
    }

    context_notify_data user_data = (context_notify_data)malloc(
        sizeof(struct _context_notify_data));
    user_data->v = v;
    user_data->context = NULL;
    context = clCreateContext(properties,
                              num_devices,
                              devices,
                              context_notify,
                              user_data,
                              &flag);
    free(properties);
    free(devices);
    send_comm(*clientfd, sizeof(cl_int), sizeof(cl_context), 0,
                         &flag, &context, NULL);
    if(flag != CL_SUCCESS){
        free(user_data);
        VERBOSE_OUT(flag);
        return;
    }
    user_data->context = context;

    if(!context_user_datas)
        context_user_datas = context_user_datas_new();
    context_user_datas_push_back(context_user_datas, user_data);
    validator_add_context(v, context);
    VERBOSE_OUT(flag);
}

void ocland_clCreateContextFromType(int* clientfd, validator v, recv_pending msg)
{
    VERBOSE_IN();

    cl_int flag = CL_OUT_OF_HOST_MEMORY;
    cl_uint num_properties = 0;
    cl_context_properties *properties = NULL;
 	cl_device_type device_type = CL_DEVICE_TYPE_ALL;
    cl_context context = NULL;

    char *ptr;
    size_t msg_size = sizeof(cl_uint);
    if( (msg.size < msg_size) ||
        !(ptr = disassemble_msg(msg.data, sizeof(cl_uint), &num_properties,
                                          0, NULL)) ||
        !num_properties )
    {
        send_comm(*clientfd, sizeof(cl_int), sizeof(cl_context), 0,
                             &flag, &context, NULL);
        VERBOSE_OUT(flag);
        return;
    }
    size_t props_size = num_properties * sizeof(cl_context_properties);
    msg_size += props_size + sizeof(cl_device_type);
    properties = (cl_device_partition_property*)malloc(props_size);
    if( (!properties) ||
        (msg.size != msg_size) ||
        !disassemble_msg(ptr, props_size, properties,
                              sizeof(cl_device_type), &device_type,
                              0, NULL) )
    {
        send_comm(*clientfd, sizeof(cl_int), sizeof(cl_context), 0,
                             &flag, &context, NULL);
        VERBOSE_OUT(flag);
        return;
    }

    context_notify_data user_data = (context_notify_data)malloc(
        sizeof(struct _context_notify_data));
    user_data->v = v;
    user_data->context = NULL;
    context = clCreateContextFromType(properties,
                                      device_type,
                                      context_notify,
                                      user_data,
                                      &flag);
    free(properties);
    send_comm(*clientfd, sizeof(cl_int), sizeof(cl_context), 0,
                         &flag, &context, NULL);
    if(flag != CL_SUCCESS){
        free(user_data);
        VERBOSE_OUT(flag);
        return;
    }
    user_data->context = context;

    if(!context_user_datas)
        context_user_datas = context_user_datas_new();
    context_user_datas_push_back(context_user_datas, user_data);
    validator_add_context(v, context);
    VERBOSE_OUT(flag);
}

void ocland_clRetainContext(int* clientfd, validator v, recv_pending msg)
{
    VERBOSE_IN();

    cl_int flag = CL_OUT_OF_HOST_MEMORY;

    // The received data shall be directly the context
    if(msg.size != sizeof(cl_context)) {
        send_comm(*clientfd, sizeof(cl_int), 0, &flag, NULL);
        VERBOSE_OUT(flag);
        return;
    }
    const cl_context context = *((cl_context*)(msg.data));
    // Even though we can keep reference counting at this side, actually is the
    // client who is taking care on such task, so we actually never call to
    // clRetainContext()
    if(!is_context(v, context))
        flag = CL_INVALID_CONTEXT;
    else
        flag = CL_SUCCESS;
    send_comm(*clientfd, sizeof(cl_int), 0, &flag, NULL);
    VERBOSE_OUT(flag);
}

void ocland_clReleaseContext(int* clientfd, validator v, recv_pending msg)
{
    VERBOSE_IN();

    cl_int flag = CL_OUT_OF_HOST_MEMORY;

    // The received data shall be directly the context
    if(msg.size != sizeof(cl_context)) {
        send_comm(*clientfd, sizeof(cl_int), 0, &flag, NULL);
        VERBOSE_OUT(flag);
        return;
    }
    const cl_context context = *((cl_context*)(msg.data));
    // Even though we can keep reference counting at this side, actually is the
    // client who is taking care on such task, so if we reached this point we
    // can assume that context shall be completelly detroyed
    if(!is_context(v, context))
        flag = CL_INVALID_CONTEXT;
    else {
        flag = release_context(context);
        if(flag == CL_SUCCESS)
            validator_remove_context(v, context);
    }

    send_comm(*clientfd, sizeof(cl_int), 0, &flag, NULL);
    VERBOSE_OUT(flag);
}

DEFINE_INFO_GETTER(clGetContextInfo, cl_context, cl_context_info, context)
