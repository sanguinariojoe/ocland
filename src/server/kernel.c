/*
 *  This file is part of ocland, a free cloud OpenCL interface.
 *  Copyright (C) 2012-2019  Jose Luis Cercos Pita <jlcercos@gmail.com>
 *
 *  ocland is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  ocland is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with ocland.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <ocland/server/kernel.h>
#include <ocland/server/log.h>

void ocland_clCreateKernel(int* clientfd, validator v, recv_pending msg)
{
    VERBOSE_IN();

    cl_int flag = CL_OUT_OF_HOST_MEMORY;
    cl_kernel kernel = NULL;
    cl_program program;
    size_t kernel_name_size;
    char* kernel_name;

    char *ptr;
    size_t msg_size = sizeof(cl_program) + sizeof(size_t);
    if( (msg.size < msg_size) ||
        !(ptr = disassemble_msg(msg.data, sizeof(cl_program), &program,
                                          sizeof(size_t), &kernel_name_size,
                                          0, NULL)))
    {
        send_comm(*clientfd, sizeof(cl_int), sizeof(cl_kernel), 0,
                             &flag, &kernel, NULL);
        VERBOSE_OUT(flag);
        return;
    }

    if(   !kernel_name_size
        || (msg.size != msg_size + kernel_name_size)
        || (strnlen(ptr, kernel_name_size) >= kernel_name_size))
    {
        send_comm(*clientfd, sizeof(cl_int), sizeof(cl_kernel), 0,
                             &flag, &kernel, NULL);
        VERBOSE_OUT(flag);
        return;
    }

    kernel_name = (char*)malloc(kernel_name_size);
    if(!kernel_name) {
        send_comm(*clientfd, sizeof(cl_int), sizeof(cl_kernel), 0,
                             &flag, &kernel, NULL);
        VERBOSE_OUT(flag);
        return;
    }
    memcpy(kernel_name, ptr, kernel_name_size);

    kernel = clCreateKernel(program,
                            kernel_name,
                            &flag);
    free(kernel_name);
    send_comm(*clientfd, sizeof(cl_int), sizeof(cl_kernel), 0,
                         &flag, &kernel, NULL);

    if(flag == CL_SUCCESS)
        validator_add_kernel(v, kernel);

    VERBOSE_OUT(flag);
}

void ocland_clCreateKernelsInProgram(int* clientfd,
                                     validator v,
                                     recv_pending msg)
{
    VERBOSE_IN();

    cl_int flag = CL_OUT_OF_HOST_MEMORY;
    cl_kernel *kernels = NULL;
    size_t *kernel_name_sizes = NULL;
    char *kernel_names = NULL;
    cl_uint num_ret = 0;
    cl_program program;
    cl_uint num_kernels;

    size_t msg_size = sizeof(cl_program) + sizeof(cl_uint);
    if( (msg.size != msg_size) ||
        !disassemble_msg(msg.data, sizeof(cl_program), &program,
                                   sizeof(cl_uint), &num_kernels,
                                   0, NULL) )
    {
        send_comm(*clientfd, sizeof(cl_int), sizeof(cl_uint), 0,
                             &flag, &num_ret, NULL);
        VERBOSE_OUT(flag);
        return;
    }

    if(num_kernels) {
        kernels = (cl_kernel*)malloc(num_kernels * sizeof(cl_kernel));
        kernel_name_sizes = (size_t*)malloc(num_kernels * sizeof(size_t));
        if(!kernels || !kernel_name_sizes) {
            free(kernels);
            free(kernel_name_sizes);
            send_comm(*clientfd, sizeof(cl_int), sizeof(cl_uint), 0,
                                 &flag, &num_ret, NULL);
            VERBOSE_OUT(flag);
            return;
        }
    }

    flag = clCreateKernelsInProgram(program,
                                    num_kernels,
                                    kernels,
                                    &num_ret);
    if((flag != CL_SUCCESS) && (num_kernels < num_ret))
        flag = CL_INVALID_VALUE;

    if(flag != CL_SUCCESS) {
        free(kernels);
        free(kernel_name_sizes);
        send_comm(*clientfd, sizeof(cl_int), sizeof(cl_uint), 0,
                             &flag, &num_ret, NULL);
        VERBOSE_OUT(flag);
        return;
    }

    if(!num_kernels) {
        send_comm(*clientfd, sizeof(cl_int), sizeof(cl_uint), 0,
                             &flag, &num_ret, NULL);
        VERBOSE_OUT(flag);
        return;
    }

    // Now we need to setup an array of names. First we are computing the
    // required memory
    size_t kernel_names_size = 0;
    for(cl_uint i = 0; i < num_ret; i++) {
        clGetKernelInfo(kernels[i],
                        CL_KERNEL_FUNCTION_NAME,
                        0,
                        NULL,
                        &(kernel_name_sizes[i]));
        if(!kernel_name_sizes[i]) {
            free(kernels);
            free(kernel_name_sizes);
            send_comm(*clientfd, sizeof(cl_int), sizeof(cl_uint), 0,
                                 &flag, &num_ret, NULL);
            VERBOSE_OUT(flag);
            return;
        }
        kernel_names_size += kernel_name_sizes[i];
    }
    // We are allocating a bit of extra memory because OpenCL specification is
    // not stating that the returned atring is null terminated
    kernel_names_size += num_ret * sizeof(char);
    kernel_names = (char*)malloc(kernel_names_size);
    if(!kernel_names) {
        for(cl_uint i = 0; i < num_ret; i++) {
            clReleaseKernel(kernels[i]);
            free(kernels);
            free(kernel_name_sizes);
            send_comm(*clientfd, sizeof(cl_int), sizeof(cl_uint), 0,
                                 &flag, &num_ret, NULL);
            VERBOSE_OUT(flag);
            return;
        }
    }

    // Now we can fiil the names
    memset(kernel_names, '\0', kernel_names_size);
    char* ptr = kernel_names;
    kernel_names_size = 0;
    for(cl_uint i = 0; i < num_ret; i++) {
        clGetKernelInfo(kernels[i],
                        CL_KERNEL_FUNCTION_NAME,
                        kernel_name_sizes[i],
                        ptr,
                        NULL);
        if(strnlen(ptr, kernel_name_sizes[i]) == kernel_name_sizes[i]) {
            // The implementation is returning the name without null
            // termination, so we must take the next char (which is already a
            // null terminator)
            ptr++;
            kernel_names_size += sizeof(char);
        }
        ptr += kernel_name_sizes[i];
        kernel_names_size += kernel_name_sizes[i];
    }

    send_comm(*clientfd, sizeof(cl_int),
                         sizeof(cl_uint),
                         num_ret * sizeof(cl_kernel),
                         kernel_names_size,
                         0,
                         &flag,
                         &num_ret,
                         kernels,
                         kernel_names,
                         NULL);
    free(kernels);
    free(kernel_name_sizes);
    free(kernel_names);
    VERBOSE_OUT(flag);
    return;
}

void ocland_clRetainKernel(int* clientfd, validator v, recv_pending msg)
{
    VERBOSE_IN();

    cl_int flag = CL_OUT_OF_HOST_MEMORY;

    // The received data shall be directly the kernel
    if(msg.size != sizeof(cl_kernel)) {
        send_comm(*clientfd, sizeof(cl_int), 0, &flag, NULL);
        VERBOSE_OUT(flag);
        return;
    }
    const cl_kernel kernel = *((cl_kernel*)(msg.data));
    // Even though we can keep reference counting at this side, actually is the
    // client who is taking care on such task, so we actually never call to
    // clRetainKernel()
    if(!is_kernel(v, kernel))
        flag = CL_INVALID_KERNEL;
    else
        flag = CL_SUCCESS;
    send_comm(*clientfd, sizeof(cl_int), 0, &flag, NULL);
    VERBOSE_OUT(flag);
}

void ocland_clReleaseKernel(int* clientfd, validator v, recv_pending msg)
{
    VERBOSE_IN();

    cl_int flag = CL_OUT_OF_HOST_MEMORY;

    // The received data shall be directly the kernel
    if(msg.size != sizeof(cl_kernel)) {
        send_comm(*clientfd, sizeof(cl_int), 0, &flag, NULL);
        VERBOSE_OUT(flag);
        return;
    }
    const cl_kernel kernel = *((cl_kernel*)(msg.data));
    // Even though we can keep reference counting at this side, actually is the
    // client who is taking care on such task, so if we reached this point we
    // can assume that kernel shall be completelly detroyed
    if(!is_kernel(v, kernel))
        flag = CL_INVALID_KERNEL;
    else {
        flag = clReleaseKernel(kernel);
        if(flag == CL_SUCCESS)
            validator_remove_kernel(v, kernel);
    }

    send_comm(*clientfd, sizeof(cl_int), 0, &flag, NULL);
    VERBOSE_OUT(flag);
}

void ocland_clSetKernelArg(int* clientfd, validator v, recv_pending msg)
{
    VERBOSE_IN();

    cl_int flag = CL_OUT_OF_HOST_MEMORY;
    cl_kernel kernel;
    cl_uint arg_index;
    size_t arg_size;
    size_t value_size;
    void* value = NULL;

    char *ptr;
    size_t msg_size = sizeof(cl_kernel) + sizeof(cl_uint) + 2 * sizeof(size_t);
    if( (msg.size < msg_size) ||
        !(ptr = disassemble_msg(msg.data, sizeof(cl_kernel), &kernel,
                                          sizeof(cl_uint), &arg_index,
                                          sizeof(size_t), &arg_size,
                                          sizeof(size_t), &value_size,
                                          0, NULL)))
    {
        send_comm(*clientfd, sizeof(cl_int), 0, &flag, NULL);
        VERBOSE_OUT(flag);
        return;
    }

    if(msg.size != msg_size + value_size) {
        send_comm(*clientfd, sizeof(cl_int), 0, &flag, NULL);
        VERBOSE_OUT(flag);
        return;
    }

    if(value_size)
        value = (void*)(msg.data + msg_size);

    flag = clSetKernelArg(kernel,
                          arg_index,
                          arg_size,
                          value);
    send_comm(*clientfd, sizeof(cl_int), 0, &flag, NULL);
    VERBOSE_OUT(flag);
}

DEFINE_INFO_GETTER(clGetKernelInfo, cl_kernel, cl_kernel_info, kernel)

void ocland_clGetKernelWorkGroupInfo(int* clientfd,
                                     validator v,
                                     recv_pending msg)
{
    VERBOSE_IN();
    cl_int flag = CL_OUT_OF_HOST_MEMORY;
    cl_kernel kernel;
    cl_device_id device;
    cl_kernel_work_group_info param_name;
    size_t param_value_size, param_value_size_ret=0;
    void *param_value = NULL;
    const size_t msg_size = sizeof(cl_kernel) +
                            sizeof(cl_device_id) +
                            sizeof(cl_kernel_work_group_info) +
                            sizeof(size_t);
    if( (msg.size != msg_size) ||
        !disassemble_msg(msg.data, sizeof(cl_kernel), &kernel,
                                   sizeof(cl_device_id), &device,
                                   sizeof(cl_kernel_work_group_info), &param_name,
                                   sizeof(size_t), &param_value_size,
                                   0, NULL) )
    {
        send_comm(*clientfd, sizeof(cl_int), sizeof(size_t), 0,
                             &flag, &param_value_size_ret, NULL);
        VERBOSE_OUT(flag);
        return;
    }
    if(!is_kernel(v, kernel)) {
        flag = CL_INVALID_KERNEL;
        send_comm(*clientfd, sizeof(cl_int), sizeof(size_t), 0,
                             &flag, &param_value_size_ret, NULL);
        VERBOSE_OUT(flag);
        return;
    }
    if(!is_device(v, device)) {
        flag = CL_INVALID_DEVICE;
        send_comm(*clientfd, sizeof(cl_int), sizeof(size_t), 0,
                             &flag, &param_value_size_ret, NULL);
        VERBOSE_OUT(flag);
        return;
    }
    if(param_value_size) {
        param_value = (void*)malloc(param_value_size);
        if(!param_value) {
            send_comm(*clientfd, sizeof(cl_int), sizeof(size_t), 0,
                                 &flag, &param_value_size_ret, NULL);
            VERBOSE_OUT(flag);
            return;
        }
        memset(param_value, '0', param_value_size);
    }
    flag = clGetKernelWorkGroupInfo(kernel,
                                    device,
                                    param_name,
                                    param_value_size,
                                    param_value,
                                    &param_value_size_ret);
    if( (flag != CL_SUCCESS) || !param_value_size ) {
        free(param_value);
        send_comm(*clientfd, sizeof(cl_int), sizeof(size_t), 0,
                             &flag, &param_value_size_ret, NULL);
        VERBOSE_OUT(flag);
        return;
    }
    send_comm(*clientfd, sizeof(cl_int),
                         sizeof(size_t),
                         param_value_size_ret,
                         0,
                         &flag,
                         &param_value_size_ret,
                         param_value,
                         NULL);
    free(param_value);
    VERBOSE_OUT(flag);
}

void ocland_clGetKernelArgInfo(int* clientfd, validator v, recv_pending msg)
{
    VERBOSE_IN();
    cl_int flag = CL_OUT_OF_HOST_MEMORY;
    cl_kernel kernel;
    cl_uint arg_indx;
    cl_kernel_work_group_info param_name;
    size_t param_value_size, param_value_size_ret=0;
    void *param_value = NULL;
    const size_t msg_size = sizeof(cl_kernel) +
                            sizeof(cl_uint) +
                            sizeof(cl_kernel_work_group_info) +
                            sizeof(size_t);
    if( (msg.size != msg_size) ||
        !disassemble_msg(msg.data, sizeof(cl_kernel), &kernel,
                                   sizeof(cl_uint), &arg_indx,
                                   sizeof(cl_kernel_work_group_info), &param_name,
                                   sizeof(size_t), &param_value_size,
                                   0, NULL) )
    {
        send_comm(*clientfd, sizeof(cl_int), sizeof(size_t), 0,
                             &flag, &param_value_size_ret, NULL);
        VERBOSE_OUT(flag);
        return;
    }
    if(!is_kernel(v, kernel)) {
        flag = CL_INVALID_KERNEL;
        send_comm(*clientfd, sizeof(cl_int), sizeof(size_t), 0,
                             &flag, &param_value_size_ret, NULL);
        VERBOSE_OUT(flag);
        return;
    }
    if(param_value_size) {
        param_value = (void*)malloc(param_value_size);
        if(!param_value) {
            send_comm(*clientfd, sizeof(cl_int), sizeof(size_t), 0,
                                 &flag, &param_value_size_ret, NULL);
            VERBOSE_OUT(flag);
            return;
        }
        memset(param_value, '0', param_value_size);
    }
    flag = clGetKernelArgInfo(kernel,
                              arg_indx,
                              param_name,
                              param_value_size,
                              param_value,
                              &param_value_size_ret);
    if( (flag != CL_SUCCESS) || !param_value_size ) {
        free(param_value);
        send_comm(*clientfd, sizeof(cl_int), sizeof(size_t), 0,
                             &flag, &param_value_size_ret, NULL);
        VERBOSE_OUT(flag);
        return;
    }
    send_comm(*clientfd, sizeof(cl_int),
                         sizeof(size_t),
                         param_value_size_ret,
                         0,
                         &flag,
                         &param_value_size_ret,
                         param_value,
                         NULL);
    free(param_value);
    VERBOSE_OUT(flag);
}
