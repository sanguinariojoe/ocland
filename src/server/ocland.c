/*
 *  This file is part of ocland, a free cloud OpenCL interface.
 *  Copyright (C) 2012  Jose Luis Cercos Pita <jl.cercos@upm.es>
 *
 *  ocland is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  ocland is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with ocland.  If not, see <http://www.gnu.org/licenses/>.
 */

/** \mainpage ocland (OpenCL land).
 * In Spanish ocland phonetics is similar to Oakland one,
 * an EEUU western city, Area code 510. \n
 * ocland is a free software to transparent remote devices
 * virtualization tool in order to use it with any OpenCL
 * based program. So ocland is the first OpenCL cloud
 * computing tookit. \n
 * ocland needs 510XX TCP ports open (Area code). ocland
 * server will bind to port 51000 listening for clients,
 * but since the clients can perform asynchronous memory
 * transfers, additional ports will be binded, so please
 * open TCP ports at least 2 times the maximum number of
 * clients allowed (to 32 default clients TCP Ports from
 * 51000 to 51064 must be open). \n
 * Please, read README file and user manual in order to
 * know how to use ocland. \n
 * Ocland have two main components, the server, that is an
 * executable running on the machines that host remote
 * desired devices, and the client, that is a library
 * installed as an OpenCL ICD (Installable Client Driver),
 * providing platforms on selected servers.
 * @remarks Ocland client requires an operative OpenCL-1.2
 * ICD loader.
 */

#include <sys/socket.h>
#include <netinet/in.h>
#include <netinet/tcp.h>
#include <arpa/inet.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <getopt.h>
#include <string.h>
#include <signal.h>
#include <pthread.h>

#include <ocland/common/arch.h>
#include <ocland/common/dataExchange.h>
#include <ocland/server/log.h>
#include <ocland/server/validator.h>
#include <ocland/server/dispatcher.h>
#include <ocland/common/typed_list.h>


/**
 * Joint of client attributes
 */
typedef struct _client_t {
    /// Validator associarted to the client
    validator v;
    /// Thread dispatching client
    pthread_t t;
} client;

DEFINE_LIST(client, clients)

/// Default service port
#ifndef OCLAND_PORT
    #define OCLAND_PORT 51000u
#endif

/** Maximum number of client connections accepted by server
 *
 * Value must be defined by CMake.
 */
#ifndef MAX_CLIENTS
    #define MAX_CLIENTS 32u
#endif

/// ocland name and version
#ifndef PACKAGE_STRING
    #define PACKAGE_STRING "ocland 0.0.00"
#endif

/// Valid command line sort options.
static const char *opts = "lp:vh?";
/// Valid command line long options.
static const struct option longOpts[] = {
    { "log-file", required_argument, NULL, 'l' },
    { "port", required_argument, NULL, 'p' },
    { "version", no_argument, NULL, 'v' },
    { "help", no_argument, NULL, 'h' },
    { NULL, no_argument, NULL, 0 }
};
/// Option argument
extern char *optarg;

/** Show usage/help page and stops ocland server execution.
 */
void displayUsage()
{
    printf("Usage:\tocland [Option]...\n");
    printf("Launch ocland server.\n");
    printf("\n");
    printf("Required arguments for long options are also required for the short ones.\n");
    printf("  -l, --log-file=LOG           Output log file. If unset /var/log/ocland.log\n");
    printf("                               will used\n");
    printf("  -p, --port=PORT              Binding port (51000 by default)\n");
    printf("  -v, --version                Show ocland name and version\n");
    printf("  -h, --help                   Show this help page\n");
}

/// Binding port
static unsigned int port = OCLAND_PORT;

/** Parse command line options. Execute ocland --help to see
 * valid command line options.
 * @param argc Number of command line arguments.
 * @param argv List of command line arguments.
 */
void parseOptions(int argc, char *argv[])
{
    int index;
    int opt = getopt_long( argc, argv, opts, longOpts, &index );
    while( opt != -1 ) {
        switch( opt ) {
            case 'l':
                if(!setLogFile(optarg)){
                    printf("File \"%s\" could not be opened!\n", optarg);
                    exit(EXIT_FAILURE);
                }
                break;

            case 'p':
                if(atoi(optarg) <= 0) {
                    printf("Invalid port number \"%s\"\n", optarg);
                    exit(EXIT_FAILURE);
                }
                port = (unsigned int)atoi(optarg);
                break;

            case 'v':
                printf(PACKAGE_STRING);
                printf("\n");
                exit(EXIT_SUCCESS);

            case ':':
            case '?':
                printf("\n");
                displayUsage();
                exit(EXIT_FAILURE);
            case 'h':
                displayUsage();
                exit(EXIT_SUCCESS);

            default:
                displayUsage();
                exit(EXIT_FAILURE);
        }
        opt = getopt_long( argc, argv, opts, longOpts, &index );
    }
}

/**
 * Create a server listening
 * @return The server instance, 0 if errors happened
 */
int create_server(unsigned int port)
{
    int switch_on = 1;
    int serverfd = 0;
    struct sockaddr_in6 serv_addr;

    memset(&serv_addr, '0', sizeof(serv_addr));

    serverfd = socket(AF_INET6, SOCK_STREAM | SOCK_NONBLOCK, 0);
    if(serverfd < 0){
        printf("Socket can be registered!\n");
        fflush(stdout);
        return 0;
    }

    if (setsockopt(serverfd, SOL_SOCKET, SO_REUSEADDR,
                   (char *) &switch_on, sizeof(int)) < 0)
    {
         perror("SO_REUSEADDR option cannot be set");
         fflush(stdout);
         return 0;
    }
    if (setsockopt(serverfd, IPPROTO_TCP, TCP_NODELAY,
                   (char *) &switch_on, sizeof(int)) < 0)
    {
         perror("TCP_NODELAY option cannot be set");
         fflush(stdout);
         return 0;
    }
    if (setsockopt(serverfd, IPPROTO_TCP, TCP_QUICKACK,
                   (char *) &switch_on, sizeof(int)) < 0)
    {
         perror("TCP_QUICKACK option cannot be set");
         fflush(stdout);
         return 0;
    }

    serv_addr.sin6_family = AF_INET6;
    serv_addr.sin6_addr   = in6addr_any;
    serv_addr.sin6_port   = htons(port);

    if(bind(serverfd, (struct sockaddr*)&serv_addr, sizeof(serv_addr)) < 0){
        printf("Can't bind on port %u!\n", port);
        fflush(stdout);
        return 0;
    }
    if(listen(serverfd, MAX_CLIENTS) < 0){
        printf("Can't listen on port %u!\n", port);
        fflush(stdout);
        return 0;
    }
    printf("Server ready on port %u.\n", port);
    printf("%u connections will be accepted...\n", MAX_CLIENTS);
    fflush(stdout);

    return serverfd;
}

/// Variable to let the server know when a SIGINT has been sent
static volatile int keep_alive = 1;

/**
 * A SIGINT has been received, so we must let know the server that it can
 * stop
 * @param signal_id Received signal
 */
void sigint_handler(int signal_id)
{
    keep_alive = 0;
}

/**
 * Blocking connection accept for a specific client
 *
 * This method is tirelessly accepting connections until the specified client
 * is connected. All other clients will be automatically rejected
 * @param  server Server socket were connections will be accepted
 * @param  v      Validator associated to the client we are looking for
 * @return        The socket to the client, -1 if the task is interrupted
 *                by sigint_handler(), or if any other error was found
 */
int accept_from_client(int server, validator v)
{
    int fd = -1;
    while(keep_alive) {
        fd = accept(server, (struct sockaddr*)NULL, NULL);
        if(fd < 0) {
            usleep(1000);
            continue;
        }
        struct sockaddr_in6 addr;
        socklen_t len_addr = sizeof(addr);
        if(getpeername(fd, (struct sockaddr *)&addr, &len_addr)) {
            printf("Failed getting parallel connection client address for %s\n",
                   get_client_addr_str(v));
            fflush(stdout);
            close(fd);
            disconnect(v);
            return -1;
        }
        if(!has_same_addr(v, addr)) {
            close(fd);
            fd = -1;
            continue;
        }
        break;
    }
    return fd;
}

/**
 * A thread for dispatching each client independently
 * @param  arg Casted validator
 * @return     NULL
 */
void *client_thread(void *arg)
{
    ssize_t exchanged;

    validator v = (validator)arg;
    int fd = get_client_socket(v);

    printf("%s connected, hello!\n", get_client_addr_str(v));
    fflush(stdout);

    // Check the architecture
    char* arch = architecture_string();
    size_t msg_size = strlen(arch) * sizeof(char);
    exchanged = Send(&fd, &msg_size, sizeof(size_t), MSG_MORE);
    exchanged += Send(&fd, arch, msg_size, 0);
    if(exchanged != sizeof(size_t) + msg_size) {
        char * err_str = SocketsError();
        printf("Failed sending architecture to %s: '%s'\n",
               get_client_addr_str(v), err_str);
        free(err_str);
        fflush(stdout);
        disconnect(v);
        return NULL;
    }
    exchanged = Recv(&fd, &msg_size, sizeof(size_t), MSG_WAITALL);
    if(exchanged != sizeof(size_t)) {
        char * err_str = SocketsError();
        printf("Failed receiving architecture size from %s: '%s'\n",
               get_client_addr_str(v), err_str);
        free(err_str);
        fflush(stdout);
        disconnect(v);
        return NULL;
    }
    if(!msg_size) {
        printf("NULL architecture size received from %s\n",
               get_client_addr_str(v));
        fflush(stdout);
        disconnect(v);
        return NULL;
    }
    char* msg = (void*)malloc(msg_size);
    if(!msg) {
        printf("Failure allocating %lu bytes for the %s architecture\n",
               msg_size, get_client_addr_str(v));
        fflush(stdout);
        disconnect(v);
        return NULL;
    }
    exchanged = Recv(&fd, msg, msg_size, MSG_WAITALL);
    if(exchanged != msg_size) {
        char * err_str = SocketsError();
        printf("Failed receiving architecture from %s: '%s'\n",
               get_client_addr_str(v), err_str);
        free(err_str);
        fflush(stdout);
        disconnect(v);
        return NULL;
    }
    if(strncmp(arch, msg, msg_size)) {
        printf("%s architecture differs\n",
               get_client_addr_str(v));
        fflush(stdout);
        disconnect(v);
        return NULL;
    }

    // Setup a server for callbacks, and pass the port to the client
    int cb_server;
    unsigned int p = port + 1;
    while((cb_server = create_server(p)) == 0) {
        p += 1;
    }
    exchanged = Send(&fd, &p, sizeof(unsigned int), 0);
    if(exchanged != sizeof(unsigned int)) {
        char * err_str = SocketsError();
        printf("Failed sending callbacks port to %s: '%s'\n",
               get_client_addr_str(v), err_str);
        free(err_str);
        fflush(stdout);
        disconnect(v);
        return NULL;
    }

    // IDEM for data exchange
    int data_server;
    p += 1;
    while((data_server = create_server(p)) == 0) {
        p += 1;
    }
    exchanged = Send(&fd, &p, sizeof(unsigned int), 0);
    if(exchanged != sizeof(unsigned int)) {
        char * err_str = SocketsError();
        printf("Failed sending data exchange port to %s: '%s'\n",
               get_client_addr_str(v), err_str);
        free(err_str);
        fflush(stdout);
        disconnect(v);
        return NULL;
    }

    const int cb_fd = accept_from_client(cb_server, v);
    if(!cb_fd)
        return NULL;
    set_client_cb_socket(v, cb_fd);
    // We don't need the connections listener anymore
    close(cb_server);

    const int data_fd = accept_from_client(data_server, v);
    if(!data_fd)
        return NULL;
    set_client_data_socket(v, data_fd);
    // We don't need the connections listener anymore
    close(data_server);

    while(keep_alive) {
        dispatch(v);
        if(!is_connected(v)){
            break;
        }
        usleep(1);
    }

    return NULL;
}

/** Server entry point. ocland-server is an executable called ocland
 * that runs on machines that must serve computing devices remotely.
 * @param argc Number of command line arguments.
 * @param argv List of command line arguments.
 * @return EXIT_SUCCESS if server ran right, EXIT_FAILURE otherwise.
 */
int main(int argc, char *argv[])
{
    unsigned int i;
    unsigned int n_clients = 0;

    parseOptions(argc, argv);

    int serverfd = create_server(port);
    if(!serverfd)
        return EXIT_FAILURE;

    List* clients = clients_new();

    signal(SIGINT, sigint_handler);
    while(keep_alive) {
        // Accepts new connection if possible
        int fd = accept(serverfd, (struct sockaddr*)NULL, NULL);
        if(fd >= 0) {
            validator v = validator_new(fd);
            pthread_t t;
            int err = pthread_create(&t, NULL, client_thread, (void*)v);
            if(err) {
                printf("Failed creating a thread for %s\n",
                       get_client_addr_str(v));
                fflush(stdout);
                validator_delete(v);
                continue;
            }
            client c = {v, t};
            clients_push_back(clients, c);
        }
        // Check clients
        i = 0;
        for(clients_node client_node = clients_front(clients);
            !clients_is_null(client_node);
            client_node = clients_next(client_node))
        {
            client c = clients_data(client_node);
            if(!is_connected(c.v)){
                pthread_join(c.t, NULL);
                validator_delete(c.v);
                clients_remove_at(clients, i);
                continue;
            }
            i++;
        }

        if(clients_len(clients) != n_clients){
            n_clients = clients_len(clients);
            printf("%u connection slots free.\n", MAX_CLIENTS - n_clients);
            fflush(stdout);
            if(!(MAX_CLIENTS - n_clients)){
                printf("NO MORE CLIENTS WILL BE ACCEPTED\n"); fflush(stdout);
            }
        }
        usleep(1000);
    }

    printf("Disconnecting %lu clients...\n", clients_len(clients));
    fflush(stdout);
    // pthread_join(thread, NULL); /*wait until the created thread terminates*/


    for(clients_node client_node = clients_front(clients);
        !clients_is_null(client_node);
        client_node = clients_next(client_node))
    {
        client c = clients_data(client_node);
        pthread_join(c.t, NULL);
        validator_delete(c.v);
    }
    clients_delete(clients);

    return EXIT_SUCCESS;
}
