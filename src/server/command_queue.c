/*
 *  This file is part of ocland, a free cloud OpenCL interface.
 *  Copyright (C) 2012-2019  Jose Luis Cercos Pita <jlcercos@gmail.com>
 *
 *  ocland is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  ocland is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with ocland.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <ocland/server/command_queue.h>
#include <ocland/server/log.h>
#include <ocland/server/ocland_version.h>

void ocland_clCreateCommandQueue(int* clientfd, validator v, recv_pending msg)
{
    VERBOSE_IN();

    cl_int flag = CL_OUT_OF_HOST_MEMORY;
    cl_context context = NULL;
    cl_device_id device = NULL;
    cl_command_queue_properties properties = 0;
    cl_command_queue queue = NULL;

    size_t msg_size = sizeof(cl_context) +
                      sizeof(cl_device_id) +
                      sizeof(cl_command_queue_properties);
    if( (msg.size < msg_size) ||
        !disassemble_msg(msg.data, sizeof(cl_context), &context,
                                   sizeof(cl_device_id), &device,
                                   sizeof(cl_command_queue_properties), &properties,
                                   0, NULL) )
    {
        send_comm(*clientfd, sizeof(cl_int), sizeof(cl_command_queue), 0,
                             &flag, &queue, NULL);
        VERBOSE_OUT(flag);
        return;
    }

    if(!is_context(v, context)) {
        flag = CL_INVALID_CONTEXT;
        send_comm(*clientfd, sizeof(cl_int), sizeof(cl_command_queue), 0,
                             &flag, &queue, NULL);
        VERBOSE_OUT(flag);
        return;
    }
    if(!is_device(v, device)) {
        flag = CL_INVALID_DEVICE;
        send_comm(*clientfd, sizeof(cl_int), sizeof(cl_command_queue), 0,
                             &flag, &queue, NULL);
        VERBOSE_OUT(flag);
        return;
    }

    queue = clCreateCommandQueue(context,
                                 device,
                                 properties,
                                 &flag);
    send_comm(*clientfd, sizeof(cl_int), sizeof(cl_command_queue), 0,
                         &flag, &queue, NULL);
    if(flag == CL_SUCCESS)
        validator_add_queue(v, queue);
    VERBOSE_OUT(flag);
}

void ocland_clRetainCommandQueue(int* clientfd, validator v, recv_pending msg)
{
    VERBOSE_IN();

    cl_int flag = CL_OUT_OF_HOST_MEMORY;

    // The received data shall be directly the context
    if(msg.size != sizeof(cl_command_queue)) {
        send_comm(*clientfd, sizeof(cl_int), 0, &flag, NULL);
        VERBOSE_OUT(flag);
        return;
    }
    const cl_command_queue queue = *((cl_command_queue*)(msg.data));
    // Even though we can keep reference counting at this side, actually is the
    // client who is taking care on such task, so we actually never call to
    // clRetainContext()
    if(!is_queue(v, queue))
        flag = CL_INVALID_COMMAND_QUEUE;
    else
        flag = CL_SUCCESS;
    send_comm(*clientfd, sizeof(cl_int), 0, &flag, NULL);
    VERBOSE_OUT(flag);
}

void ocland_clReleaseCommandQueue(int* clientfd, validator v, recv_pending msg)
{
    VERBOSE_IN();

    cl_int flag = CL_OUT_OF_HOST_MEMORY;

    // The received data shall be directly the context
    if(msg.size != sizeof(cl_command_queue)) {
        send_comm(*clientfd, sizeof(cl_int), 0, &flag, NULL);
        VERBOSE_OUT(flag);
        return;
    }
    const cl_command_queue queue = *((cl_command_queue*)(msg.data));
    // Even though we can keep reference counting at this side, actually is the
    // client who is taking care on such task, so if we reached this point we
    // can assume that context shall be completelly detroyed
    if(!is_queue(v, queue))
        flag = CL_INVALID_COMMAND_QUEUE;
    else {
        flag = clReleaseCommandQueue(queue);
        if(flag == CL_SUCCESS)
            validator_remove_queue(v, queue);
    }

    send_comm(*clientfd, sizeof(cl_int), 0, &flag, NULL);
    VERBOSE_OUT(flag);
}

DEFINE_INFO_GETTER(clGetCommandQueueInfo,
                   cl_command_queue,
                   cl_command_queue_info,
                   queue)

void ocland_clCreateCommandQueueWithProperties(int* clientfd,
                                              validator v,
                                              recv_pending msg)
{
    VERBOSE_IN();

    cl_int flag = CL_OUT_OF_HOST_MEMORY;
    cl_context context = NULL;
    cl_device_id device = NULL;
    size_t properties_size = 0;
    cl_queue_properties *properties = NULL;
    cl_command_queue queue = NULL;

    char *ptr;
    size_t msg_size = sizeof(cl_context) +
                      sizeof(cl_device_id) +
                      sizeof(size_t);
    if( (msg.size < msg_size) ||
        !(ptr = disassemble_msg(msg.data, sizeof(cl_context), &context,
                                          sizeof(cl_device_id), &device,
                                          sizeof(size_t), &properties_size,
                                          0, NULL)) )
    {
        send_comm(*clientfd, sizeof(cl_int), sizeof(cl_command_queue), 0,
                             &flag, &queue, NULL);
        VERBOSE_OUT(flag);
        return;
    }

    // The properties shall be directly the pending message
    if(msg.size != msg_size + properties_size) {
        send_comm(*clientfd, sizeof(cl_int), sizeof(cl_command_queue), 0,
                             &flag, &queue, NULL);
        VERBOSE_OUT(flag);
        return;
    }
    if(properties_size) {
        properties = (cl_queue_properties*)ptr;
    }

    if(!is_context(v, context)) {
        flag = CL_INVALID_CONTEXT;
        send_comm(*clientfd, sizeof(cl_int), sizeof(cl_command_queue), 0,
                             &flag, &queue, NULL);
        VERBOSE_OUT(flag);
        return;
    }
    if(!is_device(v, device)) {
        flag = CL_INVALID_DEVICE;
        send_comm(*clientfd, sizeof(cl_int), sizeof(cl_command_queue), 0,
                             &flag, &queue, NULL);
        VERBOSE_OUT(flag);
        return;
    }

    queue = clCreateCommandQueueWithProperties(context,
                                               device,
                                               properties,
                                               &flag);
    send_comm(*clientfd, sizeof(cl_int), sizeof(cl_command_queue), 0,
                         &flag, &queue, NULL);
    if(flag == CL_SUCCESS)
        validator_add_queue(v, queue);
    VERBOSE_OUT(flag);
}
