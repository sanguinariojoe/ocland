/*
 *  This file is part of ocland, a free cloud OpenCL interface.
 *  Copyright (C) 2012  Jose Luis Cercos Pita <jl.cercos@upm.es>
 *
 *  ocland is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  ocland is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with ocland.  If not, see <http://www.gnu.org/licenses/>.
 */


#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>

#include <ocland/server/ocland_version.h>
#include <ocland/server/platform.h>
#include <ocland/server/device.h>
#include <ocland/server/context.h>

cl_bool version_is_at_least(cl_version version, cl_version target)
{
    if(version.major < target.major)
        return CL_FALSE;
    else if ( (version.major == target.major) &&
              (version.minor < target.minor) )
        return CL_FALSE;
    return CL_TRUE;
}

cl_version get_platform_version(validator v, cl_platform_id platform)
{
    cl_version version = {0, 0};

    if(!is_platform(v, platform))
        return version;

    size_t param_value_size = 0;
    cl_int flag = clGetPlatformInfo(platform, CL_PLATFORM_VERSION,
                                    0, NULL, &param_value_size);
    if(flag != CL_SUCCESS)
        return version;
    char* param_value = (char*)malloc(param_value_size);
    if(!param_value)
        return version;
    flag = clGetPlatformInfo(platform, CL_PLATFORM_VERSION,
                             param_value_size, param_value, NULL);
    if(flag != CL_SUCCESS){
        free(param_value); param_value=NULL;
        return version;
    }
    // The platform version may start with "OpenCL " string, so discard it
    char* str = strstr(param_value,"OpenCL ");
    if(!str){
        free(param_value); param_value=NULL;
        return version;
    }
    str += 7;
    // Get the major version
    version.major = atoi(str);
    // Look for the minor version (is exist)
    str = strstr(str,".");
    if(str)
        version.minor = atoi(str + 1);
    free(param_value); param_value=NULL;
    return version;
}

cl_version get_device_version(validator v, cl_device_id device)
{
    cl_version version = {0, 0};

    if(!is_device(v, device))
        return version;

    cl_platform_id platform = NULL;
    cl_int flag = clGetDeviceInfo(device, CL_DEVICE_PLATFORM,
                                  sizeof(cl_platform_id), &platform, NULL);
    if(flag != CL_SUCCESS)
        return version;
    return get_platform_version(v, platform);
}

cl_version get_context_version(validator v, cl_context context)
{
    cl_version version = {0, 0};

    if(!is_context(v, context))
        return version;

    size_t param_value_size = 0;
    cl_int flag = clGetContextInfo(context, CL_CONTEXT_DEVICES,
                                   0, NULL, &param_value_size);
    if( (flag != CL_SUCCESS) || !param_value_size )
        return version;
    cl_device_id *devices = (cl_device_id*)malloc(param_value_size);
    if(!devices)
        return version;
    flag = clGetContextInfo(context, CL_CONTEXT_DEVICES,
                            param_value_size, devices, NULL);
    cl_device_id device = devices[0];
    free(devices); devices=NULL;
    if(flag != CL_SUCCESS)
        return version;

    return get_device_version(v, device);
}

cl_version clGetCommandQueueVersion(validator v, cl_command_queue command_queue)
{
    cl_version version;
    version.major = 0;
    version.minor = 0;
    cl_device_id device = NULL;
    cl_int flag = clGetCommandQueueInfo(command_queue, CL_QUEUE_DEVICE,
                                        sizeof(cl_device_id), &device, NULL);
    if(flag != CL_SUCCESS)
        return version;
    return get_device_version(v, device);
}

cl_version clGetMemObjectVersion(validator v, cl_mem memobj)
{
    cl_version version;
    version.major = 0;
    version.minor = 0;
    cl_context context = NULL;
    cl_int flag = clGetMemObjectInfo(memobj, CL_MEM_CONTEXT,
                                     sizeof(cl_context), &context, NULL);
    if(flag != CL_SUCCESS)
        return version;
    return get_context_version(v, context);
}

cl_version clGetSamplerVersion(validator v, cl_sampler sampler)
{
    cl_version version;
    version.major = 0;
    version.minor = 0;
    cl_context context = NULL;
    cl_int flag = clGetSamplerInfo(sampler, CL_SAMPLER_CONTEXT,
                                   sizeof(cl_context), &context, NULL);
    if(flag != CL_SUCCESS)
        return version;
    return get_context_version(v, context);
}

cl_version clGetProgramVersion(validator v, cl_program program)
{
    cl_version version;
    version.major = 0;
    version.minor = 0;
    cl_context context = NULL;
    cl_int flag =  clGetProgramInfo(program, CL_PROGRAM_CONTEXT,
                                    sizeof(cl_context), &context, NULL);
    if(flag != CL_SUCCESS)
        return version;
    return get_context_version(v, context);
}

cl_version clGetKernelVersion(validator v, cl_kernel kernel)
{
    cl_version version;
    version.major = 0;
    version.minor = 0;
    cl_context context = NULL;
    cl_int flag = clGetKernelInfo(kernel, CL_KERNEL_CONTEXT,
                                  sizeof(cl_context), &context, NULL);
    if(flag != CL_SUCCESS)
        return version;
    return get_context_version(v, context);
}

cl_version clGetEventVersion(validator v, cl_event event)
{
    cl_version version;
    version.major = 0;
    version.minor = 0;
    cl_command_queue command_queue = NULL;
    cl_int flag = clGetEventInfo(event, CL_EVENT_COMMAND_QUEUE,
                                 sizeof(cl_command_queue), &command_queue, NULL);
    if(flag != CL_SUCCESS)
        return version;
    return clGetCommandQueueVersion(v, command_queue);
}
