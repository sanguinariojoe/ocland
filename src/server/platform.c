/*
 *  This file is part of ocland, a free cloud OpenCL interface.
 *  Copyright (C) 2012-2019  Jose Luis Cercos Pita <jlcercos@gmail.com>
 *
 *  ocland is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  ocland is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with ocland.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <pthread.h>
#include <signal.h>

#include <ocland/server/platform.h>
#include <ocland/server/log.h>

void ocland_clGetPlatformIDs(int* clientfd, validator v, recv_pending msg)
{
    VERBOSE_IN();
    cl_int flag = CL_OUT_OF_HOST_MEMORY;
    cl_uint num_platforms = 0;
    cl_platform_id *platforms = NULL;
    // The received data shall be directly num_entries
    if(msg.size != sizeof(cl_uint)) {
        send_comm(*clientfd, sizeof(cl_int), sizeof(cl_uint), 0,
                             &flag, &num_platforms, NULL);
        VERBOSE_OUT(flag);
        return;
    }
    const cl_uint num_entries = *((cl_uint*)(msg.data));
    if(num_entries) {
        platforms = (cl_platform_id*)malloc(
            num_entries * sizeof(cl_platform_id));
        if(!platforms) {
            send_comm(*clientfd, sizeof(cl_int), sizeof(cl_uint), 0,
                                 &flag, &num_platforms, NULL);
            VERBOSE_OUT(flag);
            return;
        }
    }
    flag = clGetPlatformIDs(num_entries, platforms, &num_platforms);
    if(!num_entries) {
        send_comm(*clientfd, sizeof(cl_int), sizeof(cl_uint), 0,
                             &flag, &num_platforms, NULL);
        VERBOSE_OUT(flag);
        return;
    }
    send_comm(*clientfd, sizeof(cl_int),
                         sizeof(cl_uint),
                         num_entries * sizeof(cl_platform_id),
                         0,
                         &flag,
                         &num_platforms,
                         platforms,
                         NULL);
    VERBOSE_OUT(flag);
}

DEFINE_INFO_GETTER(clGetPlatformInfo,
                   cl_platform_id,
                   cl_platform_info,
                   platform)
