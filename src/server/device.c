/*
 *  This file is part of ocland, a free cloud OpenCL interface.
 *  Copyright (C) 2012-2019  Jose Luis Cercos Pita <jlcercos@gmail.com>
 *
 *  ocland is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  ocland is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with ocland.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <ocland/server/device.h>
#include <ocland/server/log.h>
#include <ocland/server/ocland_version.h>

void ocland_clGetDeviceIDs(int* clientfd, validator v, recv_pending msg)
{
    VERBOSE_IN();
    cl_int flag = CL_OUT_OF_HOST_MEMORY;
    cl_platform_id platform;
    cl_device_type device_type;
    cl_uint num_entries, num_devices = 0;
    cl_device_id *devices = NULL;
    // The received data shall be directly num_entries
    const size_t msg_size = sizeof(cl_platform_id) +
                            sizeof(cl_device_type) +
                            sizeof(cl_uint);
    if( (msg.size != msg_size) ||
        !disassemble_msg(msg.data, sizeof(cl_platform_id), &platform,
                                   sizeof(cl_device_type), &device_type,
                                   sizeof(cl_uint), &num_entries,
                                   0, NULL) )
    {
        send_comm(*clientfd, sizeof(cl_int), sizeof(cl_uint), 0,
                             &flag, &num_devices, NULL);
        VERBOSE_OUT(flag);
        return;
    }

    if(!is_platform(v, platform)) {
        flag = CL_INVALID_PLATFORM;
        send_comm(*clientfd, sizeof(cl_int), sizeof(cl_uint), 0,
                             &flag, &num_devices, NULL);
        VERBOSE_OUT(flag);
        return;
    }

    if(num_entries) {
        devices = (cl_device_id*)malloc(
            num_entries * sizeof(cl_device_id));
        if(!devices) {
            send_comm(*clientfd, sizeof(cl_int), sizeof(cl_uint), 0,
                                 &flag, &num_devices, NULL);
            VERBOSE_OUT(flag);
            return;
        }
    }

    flag = clGetDeviceIDs(platform,
                          device_type,
                          num_entries,
                          devices,
                          &num_devices);
    if(!num_entries) {
        send_comm(*clientfd, sizeof(cl_int), sizeof(cl_uint), 0,
                             &flag, &num_devices, NULL);
        VERBOSE_OUT(flag);
        return;
    }
    send_comm(*clientfd, sizeof(cl_int),
                         sizeof(cl_uint),
                         num_entries * sizeof(cl_device_id),
                         0,
                         &flag,
                         &num_devices,
                         devices,
                         NULL);
    VERBOSE_OUT(flag);
}

DEFINE_INFO_GETTER(clGetDeviceInfo, cl_device_id, cl_device_info, device)

// ----------------------------------
// OpenCL 1.2
// ----------------------------------
void ocland_clCreateSubDevices(int* clientfd, validator v, recv_pending msg)
{
    VERBOSE_IN();

    cl_int flag = CL_OUT_OF_HOST_MEMORY;
    cl_device_id device;
    size_t props_size;
    cl_device_partition_property *properties = NULL;
    cl_uint num_entries;
    cl_device_id *devices = NULL;
    cl_uint num_devices = 0;

    const size_t msg_size = sizeof(cl_device_id) +
                            sizeof(size_t);
    char *ptr;
    if( (msg.size < msg_size) ||
        !(ptr = disassemble_msg(msg.data, sizeof(cl_device_id), &device,
                                          sizeof(size_t), &props_size,
                                          0, NULL)) )
    {
        send_comm(*clientfd, sizeof(cl_int), sizeof(cl_uint), 0,
                             &flag, &num_devices, NULL);
        VERBOSE_OUT(flag);
        return;
    }

    if(props_size){
        properties = (cl_device_partition_property*)malloc(props_size);
        if( (!properties) ||
            (msg.size != msg_size + props_size + sizeof(cl_uint)) ||
            !disassemble_msg(ptr, props_size, properties,
                                  sizeof(cl_uint), &num_entries,
                                  0, NULL) )
        {
            send_comm(*clientfd, sizeof(cl_int), sizeof(cl_uint), 0,
                                 &flag, &num_devices, NULL);
            VERBOSE_OUT(flag);
            return;
        }
    }

    cl_version version = get_device_version(v, device);
    cl_version min_version = {1, 2};
    if(!version_is_at_least(version, min_version)) {
        // OpenCL < 1.2, so this function just simply does not exist. This also
        // covers the case of non-existent devices
        flag = CL_INVALID_DEVICE;
    }
    else{
        flag = clCreateSubDevices(device, properties, 0, NULL, &num_devices);
    }
    if(!num_entries || (flag != CL_SUCCESS)){
        send_comm(*clientfd, sizeof(cl_int), sizeof(cl_uint), 0,
                             &flag, &num_devices, NULL);
        VERBOSE_OUT(flag);
        return;
    }
    // Client asked for devices array
    devices = (cl_device_id*)malloc(num_entries * sizeof(cl_device_id));
    if(!devices) {
        flag = CL_OUT_OF_HOST_MEMORY;
        send_comm(*clientfd, sizeof(cl_int), sizeof(cl_uint), 0,
                             &flag, &num_devices, NULL);
        VERBOSE_OUT(flag);
        return;
    }
    flag = clCreateSubDevices(device, properties, num_entries, devices, NULL);
    send_comm(*clientfd, sizeof(cl_int),
                         sizeof(cl_uint),
                         num_devices * sizeof(cl_device_id),
                         0,
                         &flag,
                         &num_devices,
                         devices,
                         NULL);
    // Register the new devices
    for(cl_uint i = 0; i < num_entries; i++)
        validator_add_device(v, devices[i]);
    free(devices);
}

void ocland_clRetainDevice(int* clientfd, validator v, recv_pending msg)
{
    VERBOSE_IN();

    cl_int flag = CL_OUT_OF_HOST_MEMORY;

    // The received data shall be directly the device
    if(msg.size != sizeof(cl_device_id)) {
        send_comm(*clientfd, sizeof(cl_int), 0, &flag, NULL);
        VERBOSE_OUT(flag);
        return;
    }
    const cl_device_id device = *((cl_device_id*)(msg.data));

    // Even though we can keep reference counting at this side, actually is the
    // client who is taking care on such task, so we actually never call to
    // clRetainDevice()
    cl_version version = get_device_version(v, device);
    cl_version min_version = {1, 2};
    if(!version_is_at_least(version, min_version)) {
        // OpenCL < 1.2, so this function just simply does not exist. This also
        // covers the case of non-existent devices
        flag = CL_INVALID_DEVICE;
    }
    else{
        flag = CL_SUCCESS;
    }
    send_comm(*clientfd, sizeof(cl_int), 0, &flag, NULL);
    VERBOSE_OUT(flag);
}

void ocland_clReleaseDevice(int* clientfd, validator v, recv_pending msg)
{
    VERBOSE_IN();

    cl_int flag = CL_OUT_OF_HOST_MEMORY;

    // The received data shall be directly the device
    if(msg.size != sizeof(cl_device_id)) {
        send_comm(*clientfd, sizeof(cl_int), 0, &flag, NULL);
        VERBOSE_OUT(flag);
        return;
    }
    const cl_device_id device = *((cl_device_id*)(msg.data));

    cl_version version = get_device_version(v, device);
    cl_version min_version = {1, 2};
    if(!version_is_at_least(version, min_version)) {
        // OpenCL < 1.2, so this function just simply does not exist. This also
        // covers the case of non-existent devices
        flag = CL_INVALID_DEVICE;
    }
    else{
        flag = clReleaseDevice(device);
        if(flag == CL_SUCCESS)
            validator_remove_device(v, device);
    }

    send_comm(*clientfd, sizeof(cl_int), 0, &flag, NULL);
    VERBOSE_OUT(flag);
}
