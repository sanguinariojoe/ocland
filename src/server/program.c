/*
 *  This file is part of ocland, a free cloud OpenCL interface.
 *  Copyright (C) 2012-2019  Jose Luis Cercos Pita <jlcercos@gmail.com>
 *
 *  ocland is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  ocland is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with ocland.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <ocland/server/program.h>
#include <ocland/server/log.h>
#include <ocland/server/ocland_version.h>

/**
 * Data passed as user data to program notify callback, program_notify()
 */
struct _program_notify_data
{
    /// Useful information regarding the affected peer
    validator v;
    /// The number of devices involved
    cl_uint num_devices;
    /// The involved devices
    cl_device_id *devices;
    /// A reference received from the client
    void* reference;
};

typedef struct _program_notify_data* program_notify_data;

/**
 * Create a new program notifier data structure
 * @param  v           Validator associated with the program
 * @param  num_devices Number of devices
 * @param  devices     Devices list
 * @param  reference   Client provided reference
 * @return             The created notifier structure, NULL in case of errors
 */
program_notify_data new_program_notify_data(validator v,
                                            cl_uint num_devices,
                                            cl_device_id *devices,
                                            void* reference)
{
    program_notify_data data = (program_notify_data)malloc(
        sizeof(struct _program_notify_data));
    if(!data)
        return NULL;
    data->devices = (cl_device_id*)malloc(num_devices * sizeof(cl_device_id));
    if(!devices) {
        free(data);
        return NULL;
    }

    data->v = v;
    data->num_devices = num_devices;
    memcpy(data->devices, devices, num_devices * sizeof(cl_device_id));
    data->reference = reference;
    return data;
}

/**
 * Release memory allocated for a notifier data structure
 * @param data Notifier data structure
 */
void del_program_notify_data(program_notify_data data)
{
    free(data->devices);
    free(data);
}

recv_pending assemble_build_result(cl_program program,
                                   program_notify_data data)
{
    cl_int flag;
    recv_pending result;
    result.size = 0;
    result.data = NULL;

    size_t msg_size = sizeof(cl_uint) +
                      data->num_devices * sizeof(cl_device_id) +
                      data->num_devices * sizeof(cl_build_status) +
                      data->num_devices * sizeof(size_t);

    // Get the resulting binaries
    size_t *binary_sizes = (size_t*)malloc(
        data->num_devices * sizeof(size_t));
    unsigned char **binaries = (unsigned char**)malloc(
        data->num_devices * sizeof(unsigned char*));
    if(!binary_sizes || !binaries) {
        fflush(stdout);
        free(binary_sizes);
        free(binaries);
        del_program_notify_data(data);
    }
    flag = clGetProgramInfo(program,
                            CL_PROGRAM_BINARY_SIZES,
                            data->num_devices * sizeof(size_t),
                            binary_sizes,
                            NULL);
    if(flag != CL_SUCCESS) {
        fflush(stdout);
        free(binary_sizes);
        free(binaries);
        del_program_notify_data(data);
        return result;
    }
    for(cl_uint i = 0; i < data->num_devices; i++) {
        msg_size += binary_sizes[i];
        if(!binary_sizes[i]) {
            binaries[i] = NULL;
            continue;
        }
        binaries[i] = (unsigned char*)malloc(binary_sizes[i]);
        if(!binaries[i]) {
            fflush(stdout);
            free(binary_sizes);
            for(cl_uint j = 0; j < i; j++)
                free(binaries[j]);
            free(binaries);
            del_program_notify_data(data);
        }
    }
    flag = clGetProgramInfo(program,
                            CL_PROGRAM_BINARIES,
                            data->num_devices * sizeof(unsigned char*),
                            binaries,
                            NULL);
    if(flag != CL_SUCCESS) {
        fflush(stdout);
        free(binary_sizes);
        for(cl_uint i = 0; i < data->num_devices; i++)
            free(binaries[i]);
        free(binaries);
        del_program_notify_data(data);
        return result;
    }

    // Now we have the info we can start assembling the meesage
    result.data = (char*)malloc(msg_size);
    if(!result.data) {
        fflush(stdout);
        free(binary_sizes);
        for(cl_uint i = 0; i < data->num_devices; i++)
            free(binaries[i]);
        free(binaries);
        del_program_notify_data(data);
        return result;
    }
    result.size = msg_size;

    char *ptr = result.data;
    // Copy the list of devices
    memcpy(ptr, &(data->num_devices), sizeof(cl_uint));
    ptr += sizeof(cl_uint);
    memcpy(ptr, data->devices, data->num_devices * sizeof(cl_device_id));
    ptr += data->num_devices * sizeof(cl_device_id);
    // Set the building result of each device
    for(cl_uint i = 0; i < data->num_devices; i++) {
        cl_build_status build_status;
        flag = clGetProgramBuildInfo(program,
                                     data->devices[i],
                                     CL_PROGRAM_BUILD_STATUS,
                                     sizeof(cl_build_status),
                                     &build_status,
                                     NULL);
        if(flag != CL_SUCCESS) {
            // WTF???
            build_status = binary_sizes[i] ? CL_BUILD_SUCCESS : CL_BUILD_ERROR;
        }

        memcpy(ptr, &build_status, sizeof(cl_build_status));
        ptr += sizeof(cl_build_status);
        memcpy(ptr, &(binary_sizes[i]), sizeof(size_t));
        ptr += sizeof(size_t);
        if(binary_sizes[i]) {
            memcpy(ptr, binaries[i], binary_sizes[i]);
            ptr += binary_sizes[i];
        }
    }

    free(binary_sizes);
    for(cl_uint i = 0; i < data->num_devices; i++)
        free(binaries[i]);
    free(binaries);
    del_program_notify_data(data);
    return result;
}

/**
 * Program compilation
 * @param  program   Triggering program
 * @param  user_data User provided data
 */
void CL_CALLBACK program_notify(cl_program program, void *user_data)
{
    program_notify_data data = (program_notify_data)user_data;
    int clientfd = get_client_cb_socket(data->v);

    // Assemble the information
    recv_pending build_data = assemble_build_result(program,
                                                    data);
    if(!build_data.data) {
        printf("Failure reporting program build to %s: ",
               get_client_addr_str(data->v));
        printf("No building data available\n");
        fflush(stdout);
        return;
    }

    // Send the reference to the client, so he can know we are reporting a
    // program building result
    ssize_t sent = Send(&clientfd, &(data->reference), sizeof(void*), MSG_MORE);
    if(sent != sizeof(void*)) {
        printf("Failure reporting program build to %s: ",
               get_client_addr_str(data->v));
        printf("Error sending the reference\n");
        fflush(stdout);
        free(build_data.data);
        return;
    }

    // Send the remaining data
    sent = Send(&clientfd, &(build_data.size), sizeof(size_t), 0);
    sent += Send(&clientfd, build_data.data, build_data.size, 0);
    free(build_data.data);
    if(sent != sizeof(size_t) + build_data.size) {
        printf("Failure reporting program build to %s: ",
               get_client_addr_str(data->v));
        printf("Error sending the building results\n");
        fflush(stdout);
        return;
    }
}

void ocland_clCreateProgramWithSource(int* clientfd,
                                      validator v,
                                      recv_pending msg)
{
    VERBOSE_IN();

    cl_int flag = CL_OUT_OF_HOST_MEMORY;
    cl_context context;
    size_t src_size;
 	char* src;
    cl_program program = NULL;

    char *ptr;
    size_t msg_size = sizeof(cl_context) +
                      sizeof(size_t);
    if( (msg.size < msg_size) ||
        !(ptr = disassemble_msg(msg.data, sizeof(cl_context), &context,
                                          sizeof(size_t), &src_size,
                                          0, NULL)) )
    {
        send_comm(*clientfd, sizeof(cl_int), sizeof(cl_program), 0,
                             &flag, &program, NULL);
        VERBOSE_OUT(flag);
        return;
    }

    if(!is_context(v, context)) {
        flag = CL_INVALID_CONTEXT;
        send_comm(*clientfd, sizeof(cl_int), sizeof(cl_program), 0,
                             &flag, &program, NULL);
        VERBOSE_OUT(flag);
        return;
    }

    if(!src_size) {
        flag = CL_INVALID_VALUE;
        send_comm(*clientfd, sizeof(cl_int), sizeof(cl_program), 0,
                             &flag, &program, NULL);
        VERBOSE_OUT(flag);
        return;
    }
    src = (char*)malloc(src_size);
    if(!src) {
        send_comm(*clientfd, sizeof(cl_int), sizeof(cl_program), 0,
                             &flag, &program, NULL);
        VERBOSE_OUT(flag);
        return;
    }

    msg_size += src_size;
    if( (msg.size != msg_size) ||
        !(ptr = disassemble_msg(ptr, src_size, src,
                                     0, NULL)) )
    {
        send_comm(*clientfd, sizeof(cl_int), sizeof(cl_program), 0,
                             &flag, &program, NULL);
        free(src);
        VERBOSE_OUT(flag);
        return;
    }

    program = clCreateProgramWithSource(context,
                                        1,
                                        (const char**)&src,
                                        &src_size,
                                        &flag);
    free(src);
    send_comm(*clientfd, sizeof(cl_int), sizeof(cl_program), 0,
                         &flag, &program, NULL);
    if(flag != CL_SUCCESS){
        VERBOSE_OUT(flag);
        return;
    }
    validator_add_program(v, program);
    VERBOSE_OUT(flag);
}

void ocland_clCreateProgramWithBinary(int* clientfd,
                                      validator v,
                                      recv_pending msg)
{
    VERBOSE_IN();

    cl_int flag = CL_OUT_OF_HOST_MEMORY;
    cl_context context;
    cl_uint num_devices;
 	cl_device_id *devices = NULL;
    size_t *binary_sizes = 0;
    unsigned char **binaries = NULL;
    cl_program program = NULL;
    cl_int* binary_status;

    char *ptr;
    size_t msg_size = sizeof(cl_context) + sizeof(cl_uint);
    if( (msg.size < msg_size) ||
        !(ptr = disassemble_msg(msg.data, sizeof(cl_context), &context,
                                          sizeof(cl_uint), &num_devices,
                                          0, NULL)) )
    {
        send_comm(*clientfd, sizeof(cl_int), sizeof(cl_program), 0,
                             &flag, &program, NULL);
        VERBOSE_OUT(flag);
        return;
    }

    if(!is_context(v, context)) {
        flag = CL_INVALID_CONTEXT;
        send_comm(*clientfd, sizeof(cl_int), sizeof(cl_program), 0,
                             &flag, &program, NULL);
        VERBOSE_OUT(flag);
        return;
    }

    devices = (cl_device_id*)malloc(num_devices * sizeof(cl_device_id));
    binary_sizes = (size_t*)malloc(num_devices * sizeof(size_t));
    binaries = (unsigned char**)malloc(num_devices * sizeof(unsigned char*));
    binary_status = (cl_int*)malloc(num_devices * sizeof(cl_int));
    if(!devices || !binary_sizes || !binaries || !binary_status) {
        send_comm(*clientfd, sizeof(cl_int), sizeof(cl_program), 0,
                             &flag, &program, NULL);
        VERBOSE_OUT(flag);
        free(devices);
        free(binary_sizes);
        free(binaries);
        free(binary_status);
        return;
    }

    msg_size += num_devices * (sizeof(cl_device_id) + sizeof(size_t));
    if( (msg.size < msg_size) ||
        !(ptr = disassemble_msg(ptr,
                                num_devices * sizeof(cl_device_id), devices,
                                num_devices * sizeof(size_t), binary_sizes,
                                0, NULL)) )
    {
        send_comm(*clientfd, sizeof(cl_int), sizeof(cl_program), 0,
                             &flag, &program, NULL);
        VERBOSE_OUT(flag);
        free(devices);
        free(binary_sizes);
        free(binaries);
        free(binary_status);
        return;
    }

    // We are actually making a trick, we are allocating contiguous memory for
    // all the binaries, setting each binary pointer to the appropriate chunk of
    // data
    size_t binary_size = 0;
    for(cl_uint i = 0; i < num_devices; i++) {
        binary_size += binary_sizes[i];
    }
    unsigned char* binary = (unsigned char*)malloc(binary_size);
    if(!binary) {
        send_comm(*clientfd, sizeof(cl_int), sizeof(cl_program), 0,
                             &flag, &program, NULL);
        VERBOSE_OUT(flag);
        free(devices);
        free(binary_sizes);
        free(binaries);
        free(binary_status);
        return;
    }
    for(cl_uint i = 0; i < num_devices; i++) {
        binaries[i] = binary;
        binary += binary_sizes[i];
    }

    msg_size += binary_size;
    if( (msg.size != msg_size) ||
        !(ptr = disassemble_msg(ptr, binary_size, binaries[0],
                                     0, NULL)) )
    {
        send_comm(*clientfd, sizeof(cl_int), sizeof(cl_program), 0,
                             &flag, &program, NULL);
        free(devices);
        free(binary_sizes);
        free(binaries[0]);
        free(binaries);
        free(binary_status);
        VERBOSE_OUT(flag);
        return;
    }

    program = clCreateProgramWithBinary(context,
                                        num_devices,
                                        devices,
                                        binary_sizes,
                                        (const unsigned char**)binaries,
                                        binary_status,
                                        &flag);
    free(devices);
    free(binary_sizes);
    free(binaries[0]);
    free(binaries);
    send_comm(*clientfd, sizeof(cl_int),
                         sizeof(cl_program),
                         num_devices * sizeof(cl_int),
                         0,
                         &flag,
                         &program,
                         binary_status,
                         NULL);
    if(flag != CL_SUCCESS){
        VERBOSE_OUT(flag);
        return;
    }
    validator_add_program(v, program);
    VERBOSE_OUT(flag);
}

void ocland_clRetainProgram(int* clientfd, validator v, recv_pending msg)
{
    VERBOSE_IN();

    cl_int flag = CL_OUT_OF_HOST_MEMORY;

    // The received data shall be directly the program
    if(msg.size != sizeof(cl_program)) {
        send_comm(*clientfd, sizeof(cl_int), 0, &flag, NULL);
        VERBOSE_OUT(flag);
        return;
    }
    const cl_program program = *((cl_program*)(msg.data));
    // Even though we can keep reference counting at this side, actually is the
    // client who is taking care on such task, so we actually never call to
    // clRetainProgram()
    if(!is_program(v, program))
        flag = CL_INVALID_PROGRAM;
    else
        flag = CL_SUCCESS;
    send_comm(*clientfd, sizeof(cl_int), 0, &flag, NULL);
    VERBOSE_OUT(flag);
}

void ocland_clReleaseProgram(int* clientfd, validator v, recv_pending msg)
{
    VERBOSE_IN();

    cl_int flag = CL_OUT_OF_HOST_MEMORY;

    // The received data shall be directly the program
    if(msg.size != sizeof(cl_program)) {
        send_comm(*clientfd, sizeof(cl_int), 0, &flag, NULL);
        VERBOSE_OUT(flag);
        return;
    }
    const cl_program program = *((cl_program*)(msg.data));
    // Even though we can keep reference counting at this side, actually is the
    // client who is taking care on such task, so if we reached this point we
    // can assume that the programory object shall be completelly detroyed
    if(!is_program(v, program))
        flag = CL_INVALID_PROGRAM;
    else {
        flag = clReleaseProgram(program);
        if(flag == CL_SUCCESS)
            validator_remove_program(v, program);
    }

    send_comm(*clientfd, sizeof(cl_int), 0, &flag, NULL);
    VERBOSE_OUT(flag);
}

void ocland_clBuildProgram(int* clientfd, validator v, recv_pending msg)
{
    VERBOSE_IN();
    cl_int flag = CL_OUT_OF_HOST_MEMORY;
    cl_program program;
    cl_uint num_devices;
 	cl_device_id *devices = NULL;
    size_t options_size;
    char* options;
    void* reference;

    char *ptr;
    size_t msg_size = sizeof(cl_program) + sizeof(cl_uint);
    if( (msg.size < msg_size) ||
        !(ptr = disassemble_msg(msg.data, sizeof(cl_program), &program,
                                          sizeof(cl_uint), &num_devices,
                                          0, NULL)) )
    {
        send_comm(*clientfd, sizeof(cl_int), 0, &flag, NULL);
        VERBOSE_OUT(flag);
        return;
    }

    if(!is_program(v, program)) {
        flag = CL_INVALID_PROGRAM;
        send_comm(*clientfd, sizeof(cl_int), 0, &flag, NULL);
        VERBOSE_OUT(flag);
        return;
    }

    devices = (cl_device_id*)malloc(num_devices * sizeof(cl_device_id));
    if(!devices) {
        send_comm(*clientfd, sizeof(cl_int), 0, &flag, NULL);
        VERBOSE_OUT(flag);
        return;
    }

    msg_size += num_devices * sizeof(cl_device_id) + sizeof(size_t);
    if( (msg.size < msg_size) ||
        !(ptr = disassemble_msg(ptr,
                                num_devices * sizeof(cl_device_id), devices,
                                sizeof(size_t), &options_size,
                                0, NULL)) )
    {
        send_comm(*clientfd, sizeof(cl_int), sizeof(cl_program), 0,
                             &flag, &program, NULL);
        VERBOSE_OUT(flag);
        free(devices);
        return;
    }

    options = (char*)malloc(options_size);
    if(!options) {
        send_comm(*clientfd, sizeof(cl_int), 0, &flag, NULL);
        VERBOSE_OUT(flag);
        free(devices);
        return;
    }

    msg_size += options_size + sizeof(void*);
    if( (msg.size != msg_size) ||
        !(ptr = disassemble_msg(ptr,
                                options_size, options,
                                sizeof(void*), &reference,
                                0, NULL)) )
    {
        send_comm(*clientfd, sizeof(cl_int), 0, &flag, NULL);
        VERBOSE_OUT(flag);
        free(devices);
        free(options);
        return;
    }

    if(strnlen(options, options_size) > options_size) {
        send_comm(*clientfd, sizeof(cl_int), 0, &flag, NULL);
        VERBOSE_OUT(flag);
        free(devices);
        free(options);
        return;
    }

    // Special case of NULL options pointer
    if(!strncmp(options, OCLAND_NULL_PTR_STR, OCLAND_NULL_PTR_STR_SIZE)) {
        free(options);
        options = NULL;
    }

    program_notify_data data = new_program_notify_data(
        v, num_devices, devices, reference);
    if(!data) {
        send_comm(*clientfd, sizeof(cl_int), 0, &flag, NULL);
        VERBOSE_OUT(flag);
        free(devices);
        free(options);
        return;
    }

    // Prepare the callback function
    void (CL_CALLBACK *pfn_notify)(cl_program, void*) = NULL;
    void *user_data = NULL;
    if(reference) {
        pfn_notify = program_notify;
        user_data = (void*)data;
    }

    flag = clBuildProgram(program,
                          num_devices,
                          devices,
                          options,
                          pfn_notify,
                          user_data);
    free(devices);
    free(options);

    if(flag != CL_SUCCESS) {
        send_comm(*clientfd, sizeof(cl_int), 0, &flag, NULL);
        if(reference)
            del_program_notify_data(data);
        VERBOSE_OUT(flag);
        return;
    }

    if(!reference) {
        // Synchronously send the building result
        recv_pending build_data = assemble_build_result(program, data);
        send_comm(*clientfd, sizeof(cl_int), build_data.size, 0,
                             &flag, build_data.data, NULL);
        free(build_data.data);
    }

    VERBOSE_OUT(flag);
}

DEFINE_INFO_GETTER(clGetProgramInfo, cl_program, cl_program_info, program)

void ocland_clGetProgramBuildInfo(int* clientfd, validator v, recv_pending msg)
{
    VERBOSE_IN();
    cl_int flag = CL_OUT_OF_HOST_MEMORY;
    cl_program program;
    cl_device_id device;
    cl_program_build_info param_name;
    size_t param_value_size, param_value_size_ret=0;
    void *param_value = NULL;
    const size_t msg_size = sizeof(cl_program) +
                            sizeof(cl_device_id) +
                            sizeof(cl_program_build_info) +
                            sizeof(size_t);
    if( (msg.size != msg_size) ||
        !disassemble_msg(msg.data, sizeof(cl_program), &program,
                                   sizeof(cl_device_id), &device,
                                   sizeof(cl_program_build_info), &param_name,
                                   sizeof(size_t), &param_value_size,
                                   0, NULL) )
    {
        send_comm(*clientfd, sizeof(cl_int), sizeof(size_t), 0,
                             &flag, &param_value_size_ret, NULL);
        VERBOSE_OUT(flag);
        return;
    }
    if(!is_program(v, program)) {
        flag = CL_INVALID_PROGRAM;
        send_comm(*clientfd, sizeof(cl_int), sizeof(size_t), 0,
                             &flag, &param_value_size_ret, NULL);
        VERBOSE_OUT(flag);
        return;
    }
    if(!is_device(v, device)) {
        flag = CL_INVALID_DEVICE;
        send_comm(*clientfd, sizeof(cl_int), sizeof(size_t), 0,
                             &flag, &param_value_size_ret, NULL);
        VERBOSE_OUT(flag);
        return;
    }
    if(param_value_size) {
        param_value = (void*)malloc(param_value_size);
        if(!param_value) {
            send_comm(*clientfd, sizeof(cl_int), sizeof(size_t), 0,
                                 &flag, &param_value_size_ret, NULL);
            VERBOSE_OUT(flag);
            return;
        }
        memset(param_value, '0', param_value_size);
    }
    flag = clGetProgramBuildInfo(program,
                                 device,
                                 param_name,
                                 param_value_size,
                                 param_value,
                                 &param_value_size_ret);
    if( (flag != CL_SUCCESS) || !param_value_size ) {
        free(param_value);
        send_comm(*clientfd, sizeof(cl_int), sizeof(size_t), 0,
                             &flag, &param_value_size_ret, NULL);
        VERBOSE_OUT(flag);
        return;
    }
    send_comm(*clientfd, sizeof(cl_int),
                         sizeof(size_t),
                         param_value_size_ret,
                         0,
                         &flag,
                         &param_value_size_ret,
                         param_value,
                         NULL);
    free(param_value);
    VERBOSE_OUT(flag);
}

// ----------------------------------
// OpenCL 1.2
// ----------------------------------

void ocland_clCreateProgramWithBuiltInKernels(int* clientfd,
                                              validator v,
                                              recv_pending msg)
{
    VERBOSE_IN();

    cl_int flag = CL_OUT_OF_HOST_MEMORY;
    cl_context context;
    cl_uint num_devices;
 	cl_device_id *devices = NULL;
    size_t kernel_names_size = 0;
    char *kernel_names = NULL;
    cl_program program = NULL;

    char *ptr;
    size_t msg_size = sizeof(cl_context) +
                      sizeof(cl_uint);
    if( (msg.size < msg_size) ||
        !(ptr = disassemble_msg(msg.data, sizeof(cl_context), &context,
                                          sizeof(cl_uint), &num_devices,
                                          0, NULL)) )
    {
        send_comm(*clientfd, sizeof(cl_int), sizeof(cl_program), 0,
                             &flag, &program, NULL);
        VERBOSE_OUT(flag);
        return;
    }

    if(!is_context(v, context)) {
        flag = CL_INVALID_CONTEXT;
        send_comm(*clientfd, sizeof(cl_int), sizeof(cl_program), 0,
                             &flag, &program, NULL);
        VERBOSE_OUT(flag);
        return;
    }

    devices = (cl_device_id*)malloc(num_devices * sizeof(cl_device_id));
    if(!devices) {
        send_comm(*clientfd, sizeof(cl_int), sizeof(cl_program), 0,
                             &flag, &program, NULL);
        VERBOSE_OUT(flag);
        return;
    }

    msg_size += num_devices * sizeof(cl_device_id) + sizeof(size_t);
    if( (msg.size < msg_size) ||
        !(ptr = disassemble_msg(ptr,
                                num_devices * sizeof(cl_device_id), devices,
                                sizeof(size_t), &kernel_names_size,
                                0, NULL)) )
    {
        send_comm(*clientfd, sizeof(cl_int), sizeof(cl_program), 0,
                             &flag, &program, NULL);
        VERBOSE_OUT(flag);
        free(devices);
        return;
    }

    kernel_names = (char*)malloc(kernel_names_size);
    if(!kernel_names) {
        send_comm(*clientfd, sizeof(cl_int), sizeof(cl_program), 0,
                             &flag, &program, NULL);
        VERBOSE_OUT(flag);
        free(devices);
        return;
    }

    msg_size += kernel_names_size;
    if( (msg.size != msg_size) ||
        !(ptr = disassemble_msg(ptr, kernel_names_size, kernel_names,
                                     0, NULL)) )
    {
        send_comm(*clientfd, sizeof(cl_int), sizeof(cl_program), 0,
                             &flag, &program, NULL);
        free(devices);
        free(kernel_names);
        VERBOSE_OUT(flag);
        return;
    }

    if(strnlen(kernel_names, kernel_names_size) >= kernel_names_size) {
        send_comm(*clientfd, sizeof(cl_int), sizeof(cl_program), 0,
                             &flag, &program, NULL);
        free(devices);
        free(kernel_names);
        VERBOSE_OUT(flag);
        return;
    }

    program = clCreateProgramWithBuiltInKernels(context,
                                                num_devices,
                                                devices,
                                                kernel_names,
                                                &flag);
    free(devices);
    free(kernel_names);

    send_comm(*clientfd, sizeof(cl_int), sizeof(cl_program), 0,
                         &flag, &program, NULL);

    if(flag == CL_SUCCESS)
        validator_add_program(v, program);

    VERBOSE_OUT(flag);
}

void ocland_clCompileProgram(int* clientfd, validator v, recv_pending msg)
{
    VERBOSE_IN();
    cl_int flag = CL_OUT_OF_HOST_MEMORY;
    cl_program program;
    cl_uint num_devices;
 	cl_device_id *devices = NULL;
    size_t options_size;
    char* options;
    cl_uint num_input_headers;
    cl_program *input_headers = NULL;
    size_t *header_name_sizes = NULL;
    char **header_include_names = NULL;
    void* reference;

    char *ptr;
    size_t msg_size = sizeof(cl_program) + sizeof(cl_uint);
    if( (msg.size < msg_size) ||
        !(ptr = disassemble_msg(msg.data, sizeof(cl_program), &program,
                                          sizeof(cl_uint), &num_devices,
                                          0, NULL)) )
    {
        send_comm(*clientfd, sizeof(cl_int), 0, &flag, NULL);
        VERBOSE_OUT(flag);
        return;
    }

    if(!is_program(v, program)) {
        flag = CL_INVALID_PROGRAM;
        send_comm(*clientfd, sizeof(cl_int), 0, &flag, NULL);
        VERBOSE_OUT(flag);
        return;
    }

    devices = (cl_device_id*)malloc(num_devices * sizeof(cl_device_id));
    if(!devices) {
        send_comm(*clientfd, sizeof(cl_int), 0, &flag, NULL);
        VERBOSE_OUT(flag);
        return;
    }

    msg_size += num_devices * sizeof(cl_device_id) + sizeof(size_t);
    if( (msg.size < msg_size) ||
        !(ptr = disassemble_msg(ptr,
                                num_devices * sizeof(cl_device_id), devices,
                                sizeof(size_t), &options_size,
                                0, NULL)) )
    {
        send_comm(*clientfd, sizeof(cl_int), 0, &flag, NULL);
        VERBOSE_OUT(flag);
        free(devices);
        return;
    }

    options = (char*)malloc(options_size);
    if(!options) {
        send_comm(*clientfd, sizeof(cl_int), 0, &flag, NULL);
        VERBOSE_OUT(flag);
        free(devices);
        return;
    }

    msg_size += options_size + sizeof(void*) + sizeof(cl_uint);
    if( (msg.size < msg_size) ||
        !(ptr = disassemble_msg(ptr,
                                options_size, options,
                                sizeof(void*), &reference,
                                sizeof(cl_uint), &num_input_headers,
                                0, NULL)) )
    {
        send_comm(*clientfd, sizeof(cl_int), 0, &flag, NULL);
        VERBOSE_OUT(flag);
        free(devices);
        free(options);
        return;
    }

    if(strnlen(options, options_size) > options_size) {
        send_comm(*clientfd, sizeof(cl_int), 0, &flag, NULL);
        VERBOSE_OUT(flag);
        free(devices);
        free(options);
        return;
    }

    // Special case of NULL options pointer
    if(!strncmp(options, OCLAND_NULL_PTR_STR, OCLAND_NULL_PTR_STR_SIZE)) {
        free(options);
        options = NULL;
    }

    if(num_input_headers) {
        input_headers = (cl_program*)malloc(
            num_input_headers * sizeof(cl_program));
        header_name_sizes = (size_t*)malloc(
            num_input_headers * sizeof(size_t));
        header_include_names = (char**)malloc(
            num_input_headers * sizeof(char*));
        if(!input_headers || !header_name_sizes || !header_include_names) {
            send_comm(*clientfd, sizeof(cl_int), 0, &flag, NULL);
            VERBOSE_OUT(flag);
            free(devices);
            free(options);
            free(input_headers);
            free(header_name_sizes);
            free(header_include_names);
            return;
        }

        msg_size += num_input_headers * (sizeof(cl_program) + sizeof(size_t));
        if( (msg.size < msg_size) ||
            !(ptr = disassemble_msg(ptr,
                        num_input_headers * sizeof(cl_program), input_headers,
                        num_input_headers * sizeof(size_t), header_name_sizes,
                        0, NULL)) )
        {
            send_comm(*clientfd, sizeof(cl_int), 0, &flag, NULL);
            VERBOSE_OUT(flag);
            free(devices);
            free(options);
            free(input_headers);
            free(header_name_sizes);
            free(header_include_names);
            return;
        }

        // We are actually making a trick, we are allocating contiguous memory for
        // all the headers, setting each header pointer to the appropriate chunk of
        // data
        size_t header_names_size = 0;
        for(cl_uint i = 0; i < num_input_headers; i++) {
            header_names_size += header_name_sizes[i];
        }
        char* header_names = (char*)malloc(header_names_size);
        if(!header_names) {
            send_comm(*clientfd, sizeof(cl_int), 0, &flag, NULL);
            VERBOSE_OUT(flag);
            free(devices);
            free(options);
            free(input_headers);
            free(header_name_sizes);
            free(header_include_names);
            return;
        }
        for(cl_uint i = 0; i < num_input_headers; i++) {
            header_include_names[i] = header_names;
            header_names += header_name_sizes[i];
        }

        msg_size += header_names_size;
        if( (msg.size != msg_size) ||
            !(ptr = disassemble_msg(ptr,
                                    header_names_size, header_include_names[0],
                                    0, NULL)) )
        {
            send_comm(*clientfd, sizeof(cl_int), 0, &flag, NULL);
            VERBOSE_OUT(flag);
            free(devices);
            free(options);
            free(input_headers);
            free(header_name_sizes);
            free(header_include_names[0]);
            free(header_include_names);
            return;
        }

        for(cl_uint i = 0; i < num_input_headers; i++) {
            if(strnlen(header_include_names[i],
                       header_name_sizes[i]) >= header_name_sizes[i])
            {
                send_comm(*clientfd, sizeof(cl_int), 0, &flag, NULL);
                VERBOSE_OUT(flag);
                free(devices);
                free(options);
                free(input_headers);
                free(header_name_sizes);
                free(header_include_names[0]);
                free(header_include_names);
                return;
            }
        }
    } else {
        if(msg.size != msg_size) {
            send_comm(*clientfd, sizeof(cl_int), 0, &flag, NULL);
            VERBOSE_OUT(flag);
            free(devices);
            free(options);
            return;
        }
    }

    program_notify_data data = new_program_notify_data(
        v, num_devices, devices, reference);
    if(!data) {
        send_comm(*clientfd, sizeof(cl_int), 0, &flag, NULL);
        VERBOSE_OUT(flag);
        free(devices);
        free(options);
        free(input_headers);
        free(header_name_sizes);
        if(header_include_names) {
            free(header_include_names[0]);
            free(header_include_names);
        }
        return;
    }

    // Prepare the callback function
    void (CL_CALLBACK *pfn_notify)(cl_program, void*) = NULL;
    void *user_data = NULL;
    if(reference) {
        pfn_notify = program_notify;
        user_data = (void*)data;
    }

    flag = clCompileProgram(program,
                            num_devices,
                            devices,
                            options,
                            num_input_headers,
                            input_headers,
                            (const char**)header_include_names,
                            pfn_notify,
                            user_data);
    free(devices);
    free(options);
    free(input_headers);
    free(header_name_sizes);
    if(header_include_names) {
        free(header_include_names[0]);
        free(header_include_names);
    }

    if(reference) {
        send_comm(*clientfd, sizeof(cl_int), 0, &flag, NULL);
        if(flag != CL_SUCCESS)
            del_program_notify_data(data);
    } else {
        // Synchronously send the building result
        recv_pending build_data = assemble_build_result(program, data);
        send_comm(*clientfd, sizeof(cl_int), build_data.size, 0,
                             &flag, build_data.data, NULL);
        free(build_data.data);
    }

    VERBOSE_OUT(flag);
}

void ocland_clLinkProgram(int* clientfd, validator v, recv_pending msg)
{
    VERBOSE_IN();
    cl_int flag = CL_OUT_OF_HOST_MEMORY;
    cl_context context;
    cl_uint num_devices;
 	cl_device_id *devices = NULL;
    size_t options_size;
    char* options;
    cl_uint num_input_programs;
    cl_program *input_programs = NULL;
    void* reference;
    cl_program program = NULL;

    char *ptr;
    size_t msg_size = sizeof(cl_context) + sizeof(cl_uint);
    if( (msg.size < msg_size) ||
        !(ptr = disassemble_msg(msg.data, sizeof(cl_context), &context,
                                          sizeof(cl_uint), &num_devices,
                                          0, NULL)) )
    {
        send_comm(*clientfd, sizeof(cl_int), sizeof(cl_program), 0,
                             &flag, &program, NULL);
        VERBOSE_OUT(flag);
        return;
    }

    if(!is_context(v, context)) {
        flag = CL_INVALID_CONTEXT;
        send_comm(*clientfd, sizeof(cl_int), sizeof(cl_program), 0,
                             &flag, &program, NULL);
        VERBOSE_OUT(flag);
        return;
    }

    devices = (cl_device_id*)malloc(num_devices * sizeof(cl_device_id));
    if(!devices) {
        send_comm(*clientfd, sizeof(cl_int), sizeof(cl_program), 0,
                             &flag, &program, NULL);
        VERBOSE_OUT(flag);
        return;
    }

    msg_size += num_devices * sizeof(cl_device_id) + sizeof(size_t);
    if( (msg.size < msg_size) ||
        !(ptr = disassemble_msg(ptr,
                                num_devices * sizeof(cl_device_id), devices,
                                sizeof(size_t), &options_size,
                                0, NULL)) )
    {
        send_comm(*clientfd, sizeof(cl_int), sizeof(cl_program), 0,
                             &flag, &program, NULL);
        VERBOSE_OUT(flag);
        free(devices);
        return;
    }

    options = (char*)malloc(options_size);
    if(!options) {
        send_comm(*clientfd, sizeof(cl_int), sizeof(cl_program), 0,
                             &flag, &program, NULL);
        VERBOSE_OUT(flag);
        free(devices);
        return;
    }

    msg_size += options_size + sizeof(cl_uint);
    if( (msg.size < msg_size) ||
        !(ptr = disassemble_msg(ptr,
                                options_size, options,
                                sizeof(cl_uint), &num_input_programs,
                                0, NULL)) )
    {
        send_comm(*clientfd, sizeof(cl_int), sizeof(cl_program), 0,
                             &flag, &program, NULL);
        VERBOSE_OUT(flag);
        free(devices);
        free(options);
        return;
    }

    if(strnlen(options, options_size) > options_size) {
        send_comm(*clientfd, sizeof(cl_int), sizeof(cl_program), 0,
                             &flag, &program, NULL);
        VERBOSE_OUT(flag);
        free(devices);
        free(options);
        return;
    }

    input_programs = (cl_program*)malloc(num_input_programs * sizeof(cl_program));
    if(!input_programs) {
        send_comm(*clientfd, sizeof(cl_int), sizeof(cl_program), 0,
                             &flag, &program, NULL);
        VERBOSE_OUT(flag);
        free(devices);
        free(options);
        return;
    }

    msg_size += num_input_programs * sizeof(cl_program) + sizeof(void*);
    if( (msg.size != msg_size) ||
        !(ptr = disassemble_msg(ptr,
                    num_input_programs * sizeof(cl_program), input_programs,
                    sizeof(void*), &reference,
                    0, NULL)) )
    {
        send_comm(*clientfd, sizeof(cl_int), sizeof(cl_program), 0,
                             &flag, &program, NULL);
        VERBOSE_OUT(flag);
        free(devices);
        free(options);
        free(input_programs);
        return;
    }

    program_notify_data data = new_program_notify_data(
        v, num_devices, devices, reference);
    if(!data) {
        send_comm(*clientfd, sizeof(cl_int), sizeof(cl_program), 0,
                             &flag, &program, NULL);
        VERBOSE_OUT(flag);
        free(devices);
        free(options);
        free(input_programs);
        return;
    }

    // Prepare the callback function
    void (CL_CALLBACK *pfn_notify)(cl_program, void*) = NULL;
    void *user_data = NULL;
    if(reference) {
        pfn_notify = program_notify;
        user_data = (void*)data;
    }

    program = clLinkProgram(context,
                            num_devices,
                            devices,
                            options,
                            num_input_programs,
                            input_programs,
                            pfn_notify,
                            user_data,
                            &flag);
    free(devices);
    free(options);
    free(input_programs);

    validator_add_program(v, program);

    if(reference) {
        send_comm(*clientfd, sizeof(cl_int), sizeof(cl_program), 0,
                             &flag, &program, NULL);
        if(flag != CL_SUCCESS)
            del_program_notify_data(data);
    } else {
        // Synchronously send the building result
        recv_pending build_data = assemble_build_result(program, data);
        send_comm(*clientfd, sizeof(cl_int),
                             sizeof(cl_program),
                             build_data.size,
                             0,
                             &flag,
                             &program,
                             build_data.data,
                             NULL);
        free(build_data.data);
    }

    VERBOSE_OUT(flag);
}

void ocland_clUnloadPlatformCompiler(int* clientfd,
                                     validator v,
                                     recv_pending msg)
{
    VERBOSE_IN();
    cl_int flag = CL_OUT_OF_HOST_MEMORY;
    cl_platform_id platform;

    const size_t msg_size = sizeof(cl_platform_id);
    if( (msg.size != msg_size) ||
        !disassemble_msg(msg.data, sizeof(cl_platform_id), &platform,
                                   0, NULL) )
    {
        send_comm(*clientfd, sizeof(cl_int), 0, &flag, NULL);
        VERBOSE_OUT(flag);
        return;
    }

    if(!is_platform(v, platform)) {
        flag = CL_INVALID_PLATFORM;
        send_comm(*clientfd, sizeof(cl_int), 0, &flag, NULL);
        VERBOSE_OUT(flag);
        return;
    }

    flag = clUnloadPlatformCompiler(platform);

    send_comm(*clientfd, sizeof(cl_int), 0, &flag, NULL);
    VERBOSE_OUT(flag);
}
