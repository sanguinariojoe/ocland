/*
 *  This file is part of ocland, a free cloud OpenCL interface.
 *  Copyright (C) 2012  Jose Luis Cercos Pita <jl.cercos@upm.es>
 *
 *  ocland is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  ocland is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with ocland.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <stdlib.h>
#include <string.h>

#include <ocland/server/ocland_event.h>

/** @struct _ocland_event
 * Events are a paradigm shift, for the sake of performance. In the rest of
 * OpenCL entities, the reference which is used to exchange data with the client
 * is the locally generated one. Conversely, in events such reference is the one
 * provided by the client.
 *
 * The main benefit is clients may ask for events operations (including
 * command enqueing) in a streaming way, that is, without waiting for an answer.
 */
struct _ocland_event
{
    /// Internally OpenCL managed event
    cl_event local;
    /// Client event reference
    cl_event peer;
};

typedef struct _ocland_event* ocland_event;

DEFINE_LIST(ocland_event, linkers)

ocland_events_linker new_ocland_events_linker()
{
    return (ocland_events_linker)linkers_new();
}

void del_ocland_events_linker(ocland_events_linker linker)
{
    for(linkers_node node = linkers_front(linker);
        !linkers_is_null(node);
        node = linkers_next(node))
    {
        clReleaseEvent(linkers_data(node)->local);
        free(linkers_data(node));
    }
    linkers_delete((List*)linker);
}

void add_ocland_event_linker(ocland_events_linker linker,
                             cl_event local,
                             cl_event peer)
{
    ocland_event link = (ocland_event)malloc(sizeof(struct _ocland_event));
    if(!link)
        return;
    link->local = local;
    link->peer = peer;
    linkers_push_back(linker, link);
}

void remove_ocland_event_linker(ocland_events_linker linker, cl_event local)
{
    unsigned int i = 0;
    for(linkers_node node = linkers_front(linker);
        !linkers_is_null(node);
        node = linkers_next(node))
    {
        if(linkers_data(node)->local == local) {
            free(linkers_data(node));
            linkers_remove_at(linker, i);
            return;
        }
        i++;
    }
}

cl_event event_peer2local(ocland_events_linker linker, cl_event peer)
{
    for(linkers_node node = linkers_front(linker);
        !linkers_is_null(node);
        node = linkers_next(node))
    {
        if(linkers_data(node)->peer == peer)
            return linkers_data(node)->local;
    }
    return NULL;
}

cl_event event_local2peer(ocland_events_linker linker, cl_event local)
{
    for(linkers_node node = linkers_front(linker);
        !linkers_is_null(node);
        node = linkers_next(node))
    {
        if(linkers_data(node)->local == local)
            return linkers_data(node)->peer;
    }
    return NULL;
}
