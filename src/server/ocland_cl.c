/*
 *  This file is part of ocland, a free cloud OpenCL interface.
 *  Copyright (C) 2012  Jose Luis Cercos Pita <jl.cercos@upm.es>
 *
 *  ocland is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  ocland is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with ocland.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <pthread.h>
#include <signal.h>

#include <ocland/common/dataExchange.h>
#include <ocland/server/ocland_cl.h>
#include <ocland/server/log.h>

// Exiting shortcuts
#define COMM_FINISH(flag) { VERBOSE_OUT(flag); return; }
#define COMM_FINISH_CB(v, e, flag) {                                 \
    const cl_int err_code = flag;                                    \
    async_send_cb(v, (void*)e, sizeof(cl_int), (void*)(&err_code));  \
    COMM_FINISH(flag)                                                \
}

/**
 * Data passing structure for clEnqueueReadBuffer and clEnqueueWriteBuffer
 * callbacks
 */
struct _exchange_buffer_cb_data {
    /// Validator associated with the data exchange
    validator v;
    /// Data exchanging reference
    void* ref;
    /// Event associated to the communication
    cl_event event;
    /// Data size
    size_t size;
    /// Data
    void* ptr;
    /** If the command finish before we can set the callbacks, then the callback
     * will be called several times, but eventually with the same
     * event_command_exec_status (i.e. CL_COMPLETE or error code). So we are
     * just saving the command execution status
     */
    cl_int command_exec_status;
    /// Thread (if any)
    pthread_t thread;
};

typedef struct _exchange_buffer_cb_data* exchange_buffer_cb_data;

/**
 * Carry out a shallow copy of the clEnqueueReadBuffer/clEnqueueWriteBuffer
 * structure. Useful to register callbacks with different execution status
 * @param  in Input data structure
 * @return Output data structure
 */
exchange_buffer_cb_data shallow_copy_buffer_cb_data(exchange_buffer_cb_data in)
{
    exchange_buffer_cb_data out = (exchange_buffer_cb_data)malloc(
        sizeof(struct _exchange_buffer_cb_data));
    if(!out)
        return out;
    out->v = in->v;
    out->ref = in->ref;
    out->event = in->event;
    out->size = in->size;
    out->ptr = in->ptr;
    out->command_exec_status = in->command_exec_status;
    out->thread = in->thread;
    return out;
}

/**
 * clEnqueueReadBuffer callback to send the data asynchronously to the client
 * @param event                     Local event reference
 * @param event_command_exec_status Command execution status
 * @param user_data                 casted exchange_buffer_cb_data structure
 */
void (CL_CALLBACK read_buffer_cb) (cl_event event,
                                   cl_int event_command_exec_status,
                                   void *user_data)
{
    exchange_buffer_cb_data data = (exchange_buffer_cb_data)user_data;
    // We want to report the client for the event progress up to command finish.
    // When the command finishes, we have 2 alternatives:
    // * Command successfully finished, so we want to send the data (BUT NOT
    //   UPDATE THE EVENT STATUS)
    // * Error happened, so we just want to update the event status, without
    //   sending data
    if(event_command_exec_status != CL_COMPLETE) {
        async_send_cb(data->v,
                      data->ref,
                      sizeof(cl_int),
                      &event_command_exec_status);
    }
    else {
        async_send_data(data->v, data->ref, data->size, data->ptr);
    }

    if(event_command_exec_status <= CL_COMPLETE) {
        clReleaseEvent(event);
        free(data->ptr);
    }
    free(data);
}

void ocland_clEnqueueReadBuffer(int* clientfd, validator v, recv_pending msg)
{
    VERBOSE_IN();
    cl_command_queue queue;
    cl_mem mem;
    size_t offset, size;
    cl_event event_peer;
    cl_uint num_events_in_wait_list;
    cl_event *event_wait_list = NULL;

    char *ptr;
    const size_t msg_size = sizeof(cl_command_queue) +
                            sizeof(cl_mem) +
                            sizeof(size_t) +
                            sizeof(size_t) +
                            sizeof(cl_event) +
                            sizeof(cl_uint);
    if( (msg.size < msg_size) ||
        !(ptr = disassemble_msg(msg.data, sizeof(cl_command_queue), &queue,
                                          sizeof(cl_mem), &mem,
                                          sizeof(size_t), &offset,
                                          sizeof(size_t), &size,
                                          sizeof(cl_event), &event_peer,
                                          sizeof(cl_uint), &num_events_in_wait_list,
                                          0, NULL)) )
    {
        COMM_FINISH(CL_OUT_OF_HOST_MEMORY);
    }

    if(!event_peer)
        COMM_FINISH(CL_OUT_OF_HOST_MEMORY);
    if(!is_queue(v, queue))
        COMM_FINISH_CB(v, event_peer, CL_INVALID_COMMAND_QUEUE);
    if(!is_mem(v, mem))
        COMM_FINISH_CB(v, event_peer, CL_INVALID_MEM_OBJECT);
    if(!size)
        COMM_FINISH_CB(v, event_peer, CL_INVALID_VALUE);

    const size_t wait_list_size = num_events_in_wait_list * sizeof(cl_event);
    if(msg.size != msg_size + wait_list_size)
        COMM_FINISH_CB(v, event_peer, CL_OUT_OF_HOST_MEMORY);

    if(num_events_in_wait_list) {
        event_wait_list = (cl_event*)malloc(wait_list_size);
        if(!event_wait_list ||
           !disassemble_msg(ptr, wait_list_size, event_wait_list,
                                 0, NULL))
        {
            COMM_FINISH_CB(v, event_peer, CL_OUT_OF_HOST_MEMORY);
        }
        // convert the remote events in  local ones
        for(cl_uint i = 0; i < num_events_in_wait_list; i++) {
            event_wait_list[i] = from_remote_event(v, event_wait_list[i]);
            if(!is_event(v, event_wait_list[i]))
                COMM_FINISH_CB(v, event_peer, CL_INVALID_EVENT_WAIT_LIST);
        }
    }

    void *data = malloc(size);
    if(!data)
        COMM_FINISH_CB(v, event_peer, CL_OUT_OF_HOST_MEMORY);

    cl_event event;
    cl_int flag = clEnqueueReadBuffer(queue,
                                      mem,
                                      CL_FALSE,
                                      offset,
                                      size,
                                      data,
                                      num_events_in_wait_list,
                                      event_wait_list,
                                      &event);
    free(event_wait_list);
    if(flag != CL_SUCCESS)
        COMM_FINISH_CB(v, event_peer, flag);

    clRetainEvent(event);
    validator_add_event(v, event, event_peer);

    // Register callbacks to send the data to the client
    exchange_buffer_cb_data cb_data = (exchange_buffer_cb_data)malloc(
        sizeof(struct _exchange_buffer_cb_data));
    if(!cb_data)
        COMM_FINISH_CB(v, event_peer, CL_OUT_OF_HOST_MEMORY);
    cb_data->v = v;
    cb_data->ref = (void*)event_peer;
    cb_data->event = NULL;
    cb_data->size = size;
    cb_data->ptr = data;
    cb_data->command_exec_status = CL_QUEUED;

    cl_int cb_status[3] = {CL_SUBMITTED, CL_RUNNING, CL_COMPLETE};
    for(cl_uint i = 0; i < 3; i++) {
        exchange_buffer_cb_data cb_copy = shallow_copy_buffer_cb_data(cb_data);
        if(!cb_copy)
            COMM_FINISH_CB(v, event_peer, flag);
        cb_copy->command_exec_status = cb_status[i];
        flag = clSetEventCallback(event, cb_status[i],
                                  &read_buffer_cb, cb_copy);
        if(flag != CL_SUCCESS)
            COMM_FINISH_CB(v, event_peer, flag);
    }
    free(cb_data);

    VERBOSE_OUT(CL_SUCCESS);
}

/**
 * clEnqueueWriteBuffer parallel thread to receive the data from the client
 * @param  user_data casted exchange_buffer_cb_data structure
 * @return NULL;
 */
void *write_buffer_thread(void *user_data)
{
    cl_int flag;
    exchange_buffer_cb_data data = (exchange_buffer_cb_data)user_data;

    recv_pending msg = async_recv_data(data->v, data->ref);

    if(!msg.data || (data->size != msg.size)) {
        eprintf("Invalid received data (received %lu bytes, %lu expected)\n",
                msg.size, data->size);
        flag = CL_OUT_OF_HOST_MEMORY;
    } else {
        memcpy(data->ptr, msg.data, msg.size);
        free(msg.data);
        flag = CL_COMPLETE;
    }
    clSetUserEventStatus(data->event, flag);
    clReleaseEvent(data->event);
    free(data);
    pthread_exit(NULL);
    return NULL;
}

/**
 * clEnqueueWriteBuffer callback to asynchronously update the client event
 * @param event                     Local event reference
 * @param event_command_exec_status Command execution status
 * @param user_data                 casted exchange_buffer_cb_data structure
 */
void (CL_CALLBACK write_buffer_cb) (cl_event event,
                                    cl_int event_command_exec_status,
                                    void *user_data)
{
    exchange_buffer_cb_data data = (exchange_buffer_cb_data)user_data;

    // We always updcate the event status
    async_send_cb(data->v,
                  data->ref,
                  sizeof(cl_int),
                  &(data->command_exec_status));

    if(data->command_exec_status <= CL_COMPLETE) {
        validator_remove_thread(data->v, data->thread);
        clReleaseEvent(data->event);
        clReleaseEvent(event);
        free(data->ptr);
    }
    free(data);
}

void ocland_clEnqueueWriteBuffer(int* clientfd, validator v, recv_pending msg)
{
    VERBOSE_IN();
    cl_int flag;
    cl_command_queue queue;
    cl_mem mem;
    size_t offset, size;
    cl_event event_peer;
    cl_uint num_events_in_wait_list;
    cl_event *event_wait_list = NULL;

    char *ptr;
    const size_t msg_size = sizeof(cl_command_queue) +
                            sizeof(cl_mem) +
                            sizeof(size_t) +
                            sizeof(size_t) +
                            sizeof(cl_event) +
                            sizeof(cl_uint);
    if( (msg.size < msg_size) ||
        !(ptr = disassemble_msg(msg.data, sizeof(cl_command_queue), &queue,
                                          sizeof(cl_mem), &mem,
                                          sizeof(size_t), &offset,
                                          sizeof(size_t), &size,
                                          sizeof(cl_event), &event_peer,
                                          sizeof(cl_uint), &num_events_in_wait_list,
                                          0, NULL)) )
    {
        COMM_FINISH(CL_OUT_OF_HOST_MEMORY);
    }

    if(!event_peer)
        COMM_FINISH(CL_OUT_OF_HOST_MEMORY);
    if(!is_queue(v, queue))
        COMM_FINISH_CB(v, event_peer, CL_INVALID_COMMAND_QUEUE);
    if(!is_mem(v, mem))
        COMM_FINISH_CB(v, event_peer, CL_INVALID_MEM_OBJECT);
    if(!size)
        COMM_FINISH_CB(v, event_peer, CL_INVALID_VALUE);

    // We need to add an additional event to wait, the one synchronized with the
    // parallel data reception
    const size_t wait_list_size =
        num_events_in_wait_list * sizeof(cl_event);
    if(msg.size != msg_size + wait_list_size)
        COMM_FINISH_CB(v, event_peer, CL_OUT_OF_HOST_MEMORY);

    event_wait_list = (cl_event*)malloc(wait_list_size + sizeof(cl_event));
    if(!event_wait_list)
        COMM_FINISH_CB(v, event_peer, CL_OUT_OF_HOST_MEMORY);
    if(wait_list_size &&
       !disassemble_msg(ptr, wait_list_size, event_wait_list,
                             0, NULL))
    {
        COMM_FINISH_CB(v, event_peer, CL_OUT_OF_HOST_MEMORY);
    }
    // convert the remote events in  local ones
    for(cl_uint i = 0; i < num_events_in_wait_list; i++) {
        event_wait_list[i] = from_remote_event(v, event_wait_list[i]);
        if(!is_event(v, event_wait_list[i]))
            COMM_FINISH_CB(v, event_peer, CL_INVALID_EVENT_WAIT_LIST);
    }

    void *data = malloc(size);
    if(!data)
        COMM_FINISH_CB(v, event_peer, CL_OUT_OF_HOST_MEMORY);

    // Create an event to hang the write command waiting for the data reception
    cl_context context;
    cl_event recv_event;
    flag = clGetCommandQueueInfo(queue,
  	                             CL_QUEUE_CONTEXT,
  	                             sizeof(cl_context),
  	                             &context,
  	                             NULL);
    if(flag != CL_SUCCESS)
        COMM_FINISH_CB(v, event_peer, CL_OUT_OF_HOST_MEMORY);
    recv_event = clCreateUserEvent(context, &flag);
    event_wait_list[num_events_in_wait_list] = recv_event;
    if(flag != CL_SUCCESS)
        COMM_FINISH_CB(v, event_peer, CL_OUT_OF_HOST_MEMORY);
    clRetainEvent(recv_event);  // For write_buffer_thread
    clRetainEvent(recv_event);  // For write_buffer_cb

    // Enqueue the data reception
    cl_event event;
    flag = clEnqueueWriteBuffer(queue,
                                mem,
                                CL_FALSE,
                                offset,
                                size,
                                data,
                                num_events_in_wait_list + 1,
                                event_wait_list,
                                &event);
    free(event_wait_list);
    if(flag != CL_SUCCESS) {
        free(data);
        clReleaseEvent(recv_event);
        COMM_FINISH_CB(v, event_peer, flag);
    }

    clRetainEvent(event);
    validator_add_event(v, event, event_peer);

    // Fire up a parallel thread to receive the data, which also acts as
    // triggering for the hanged write event
    exchange_buffer_cb_data cb_data = (exchange_buffer_cb_data)malloc(
        sizeof(struct _exchange_buffer_cb_data));
    if(!cb_data)
        COMM_FINISH_CB(v, event_peer, CL_OUT_OF_HOST_MEMORY);
    cb_data->v = v;
    cb_data->ref = (void*)event_peer;
    cb_data->event = recv_event;
    cb_data->size = size;
    cb_data->ptr = data;
    cb_data->command_exec_status = CL_QUEUED;
    pthread_t thread;
    flag = pthread_create(&thread, NULL, write_buffer_thread, (void*)cb_data);
    if(flag) {
        free(data);
        free(cb_data);
        clReleaseEvent(recv_event);
        COMM_FINISH_CB(v, event_peer, CL_OUT_OF_HOST_MEMORY);
    }
    validator_add_thread(v, thread);
    cb_data->thread = thread;

    // We don't need the data reception event anymore
    clReleaseEvent(recv_event);

    // Register callbacks to update the client event
    cl_int cb_status[3] = {CL_SUBMITTED, CL_RUNNING, CL_COMPLETE};
    for(cl_uint i = 0; i < 3; i++) {
        exchange_buffer_cb_data cb_copy = shallow_copy_buffer_cb_data(cb_data);
        if(!cb_copy)
            COMM_FINISH_CB(v, event_peer, flag);
        cb_copy->command_exec_status = cb_status[i];
        flag = clSetEventCallback(event, cb_status[i],
                                  &write_buffer_cb, cb_copy);
        if(flag != CL_SUCCESS)
            COMM_FINISH_CB(v, event_peer, flag);
    }

    VERBOSE_OUT(CL_SUCCESS);
}

int ocland_clEnqueueCopyBuffer(int* clientfd, char* buffer, validator v, void* data)
{
    return 1;
}

int ocland_clEnqueueCopyImage(int* clientfd, char* buffer, validator v, void* data)
{
    return 1;
}

int ocland_clEnqueueCopyImageToBuffer(int* clientfd, char* buffer, validator v, void* data)
{
    return 1;
}

int ocland_clEnqueueCopyBufferToImage(int* clientfd, char* buffer, validator v, void* data)
{
    return 1;
}

int ocland_clEnqueueReadImage(int* clientfd, char* buffer, validator v, void* data)
{
    return 1;
}

int ocland_clEnqueueWriteImage(int* clientfd, char* buffer, validator v, void* data)
{
    return 1;
}

// ----------------------------------
// OpenCL 1.1
// ----------------------------------
int ocland_clEnqueueReadBufferRect(int* clientfd, char* buffer, validator v)
{
    return 1;
}

int ocland_clEnqueueWriteBufferRect(int* clientfd, char* buffer, validator v)
{
    return 1;
}

int ocland_clEnqueueCopyBufferRect(int* clientfd, char* buffer, validator v)
{
    return 1;
}

// ----------------------------------
// OpenCL 1.2
// ----------------------------------

int ocland_clEnqueueFillBuffer(int* clientfd, char* buffer, validator v)
{
    return 1;
}

int ocland_clEnqueueFillImage(int* clientfd, char* buffer, validator v)
{
    return 1;
}

int ocland_clEnqueueMigrateMemObjects(int* clientfd, char* buffer, validator v)
{
    return 1;
}

int ocland_clEnqueueMarkerWithWaitList(int* clientfd, char* buffer, validator v)
{
    return 1;
}

int ocland_clEnqueueBarrierWithWaitList(int* clientfd, char* buffer, validator v)
{
    return 1;
}
