/*
 *  This file is part of ocland, a free cloud OpenCL interface.
 *  Copyright (C) 2012-2019  Jose Luis Cercos Pita <jlcercos@gmail.com>
 *
 *  ocland is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  ocland is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with ocland.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <ocland/server/context.h>
#include <ocland/server/mem.h>
#include <ocland/server/log.h>
#include <ocland/server/ocland_version.h>

void ocland_clCreateBuffer(int* clientfd, validator v, recv_pending msg)
{
    VERBOSE_IN();

    cl_int flag = CL_OUT_OF_HOST_MEMORY;
    cl_context context;
    cl_mem_flags flags;
 	size_t size;
    void *host_ptr = NULL;
    cl_mem mem = NULL;

    char *ptr;
    size_t msg_size = sizeof(cl_context) +
                      sizeof(cl_mem_flags) +
                      sizeof(size_t);
    if( (msg.size < msg_size) ||
        !(ptr = disassemble_msg(msg.data, sizeof(cl_context), &context,
                                          sizeof(cl_mem_flags), &flags,
                                          sizeof(size_t), &size,
                                          0, NULL)) )
    {
        send_comm(*clientfd, sizeof(cl_int), sizeof(cl_mem), 0,
                             &flag, &mem, NULL);
        VERBOSE_OUT(flag);
        return;
    }

    if(!is_context(v, context)) {
        flag = CL_INVALID_CONTEXT;
        send_comm(*clientfd, sizeof(cl_int), sizeof(cl_mem), 0,
                             &flag, &mem, NULL);
        VERBOSE_OUT(flag);
        return;
    }

    if(!size) {
        flag = CL_INVALID_BUFFER_SIZE;
        send_comm(*clientfd, sizeof(cl_int), sizeof(cl_mem), 0,
                             &flag, &mem, NULL);
        VERBOSE_OUT(flag);
        return;
    }

    // We never want to use pinned memory in the server
    if(flags & CL_MEM_USE_HOST_PTR)
        flags ^= CL_MEM_USE_HOST_PTR;

    if(flags & CL_MEM_COPY_HOST_PTR) {
        if(msg.size != msg_size + size) {
            send_comm(*clientfd, sizeof(cl_int), sizeof(cl_mem), 0,
                                 &flag, &mem, NULL);
            VERBOSE_OUT(flag);
            return;
        }
        host_ptr = (void*)ptr;
    }

    mem = clCreateBuffer(context,
                         flags,
                         size,
                         host_ptr,
                         &flag);
    send_comm(*clientfd, sizeof(cl_int), sizeof(cl_mem), 0,
                         &flag, &mem, NULL);
    if(flag != CL_SUCCESS){
        VERBOSE_OUT(flag);
        return;
    }
    validator_add_mem(v, mem);
    VERBOSE_OUT(flag);
}

void ocland_clRetainMemObject(int* clientfd, validator v, recv_pending msg)
{
    VERBOSE_IN();

    cl_int flag = CL_OUT_OF_HOST_MEMORY;

    // The received data shall be directly the context
    if(msg.size != sizeof(cl_mem)) {
        send_comm(*clientfd, sizeof(cl_int), 0, &flag, NULL);
        VERBOSE_OUT(flag);
        return;
    }
    const cl_mem mem = *((cl_mem*)(msg.data));
    // Even though we can keep reference counting at this side, actually is the
    // client who is taking care on such task, so we actually never call to
    // clRetainMemObject()
    if(!is_mem(v, mem))
        flag = CL_INVALID_MEM_OBJECT;
    else
        flag = CL_SUCCESS;
    send_comm(*clientfd, sizeof(cl_int), 0, &flag, NULL);
    VERBOSE_OUT(flag);
}

void ocland_clReleaseMemObject(int* clientfd, validator v, recv_pending msg)
{
    VERBOSE_IN();

    cl_int flag = CL_OUT_OF_HOST_MEMORY;

    // The received data shall be directly the mem
    if(msg.size != sizeof(cl_mem)) {
        send_comm(*clientfd, sizeof(cl_int), 0, &flag, NULL);
        VERBOSE_OUT(flag);
        return;
    }
    const cl_mem mem = *((cl_mem*)(msg.data));
    // Even though we can keep reference counting at this side, actually is the
    // client who is taking care on such task, so if we reached this point we
    // can assume that the memory object shall be completelly detroyed
    if(!is_mem(v, mem))
        flag = CL_INVALID_MEM_OBJECT;
    else {
        flag = clReleaseMemObject(mem);
        if(flag == CL_SUCCESS)
            validator_remove_mem(v, mem);
    }

    send_comm(*clientfd, sizeof(cl_int), 0, &flag, NULL);
    VERBOSE_OUT(flag);
}

void ocland_clGetSupportedImageFormats(int* clientfd,
                                       validator v,
                                       recv_pending msg)
{
    VERBOSE_IN();
    cl_int flag = CL_OUT_OF_HOST_MEMORY;
    cl_context context;
    cl_mem_flags flags;
    cl_mem_object_type image_type;
    cl_uint num_entries, num_image_formats=0;
    void *image_formats = NULL;

    const size_t msg_size = sizeof(cl_mem) +
                            sizeof(cl_mem_flags) +
                            sizeof(cl_mem_object_type) +
                            sizeof(cl_uint);
    if( (msg.size != msg_size) ||
        !disassemble_msg(msg.data, sizeof(cl_context), &context,
                                   sizeof(cl_mem_flags), &flags,
                                   sizeof(cl_mem_object_type), &image_type,
                                   sizeof(cl_uint), &num_entries,
                                   0, NULL) )
    {
        send_comm(*clientfd, sizeof(cl_int), sizeof(cl_uint), 0,
                             &flag, &num_image_formats, NULL);
        VERBOSE_OUT(flag);
        return;
    }

    if(!is_context(v, context)) {
        flag = CL_INVALID_CONTEXT;
        send_comm(*clientfd, sizeof(cl_int), sizeof(cl_uint), 0,
                             &flag, &num_image_formats, NULL);
        VERBOSE_OUT(flag);
        return;
    }

    if(num_entries) {
        image_formats = (void*)malloc(num_entries * sizeof(cl_image_format));
        if(!image_formats) {
            send_comm(*clientfd, sizeof(cl_int), sizeof(cl_uint), 0,
                                 &flag, &num_image_formats, NULL);
            VERBOSE_OUT(flag);
            return;
        }
        memset(image_formats, '\0', num_entries * sizeof(cl_image_format));
    }

    flag = clGetSupportedImageFormats(context,
                                      flags,
                                      image_type,
                                      num_entries,
                                      image_formats,
                                      &num_image_formats);
    if( (flag != CL_SUCCESS) || !num_entries ) {
        free(image_formats);
        send_comm(*clientfd, sizeof(cl_int), sizeof(cl_uint), 0,
                             &flag, &num_image_formats, NULL);
        VERBOSE_OUT(flag);
        return;
    }

    send_comm(*clientfd, sizeof(cl_int),
                         sizeof(size_t),
                         num_entries * sizeof(cl_image_format),
                         0,
                         &flag,
                         &num_image_formats,
                         image_formats,
                         NULL);
    free(image_formats);
    VERBOSE_OUT(flag);
}

DEFINE_INFO_GETTER(clGetMemObjectInfo, cl_mem, cl_mem_info, mem)

DEFINE_INFO_GETTER(clGetImageInfo, cl_mem, cl_image_info, mem)

// ----------------------------------
// OpenCL 1.1
// ----------------------------------

void ocland_clCreateSubBuffer(int* clientfd, validator v, recv_pending msg)
{
    VERBOSE_IN();

    cl_int flag = CL_OUT_OF_HOST_MEMORY;
    cl_mem buffer;
    cl_mem_flags flags;
    cl_buffer_create_type buffer_create_type;
 	size_t size;
    void *buffer_create_info = NULL;
    cl_mem mem = NULL;

    char *ptr;
    size_t msg_size = sizeof(cl_context) +
                      sizeof(cl_mem_flags) +
                      sizeof(size_t);
    if( (msg.size < msg_size) ||
        !(ptr = disassemble_msg(msg.data, sizeof(cl_mem), &buffer,
                                          sizeof(cl_mem_flags), &flags,
                                          sizeof(cl_buffer_create_type), &buffer_create_type,
                                          sizeof(size_t), &size,
                                          0, NULL)) )
    {
        send_comm(*clientfd, sizeof(cl_int), sizeof(cl_mem), 0,
                             &flag, &mem, NULL);
        VERBOSE_OUT(flag);
        return;
    }

    if(!is_mem(v, buffer)) {
        flag = CL_INVALID_MEM_OBJECT;
        send_comm(*clientfd, sizeof(cl_int), sizeof(cl_mem), 0,
                             &flag, &mem, NULL);
        VERBOSE_OUT(flag);
        return;
    }

    if(!size) {
        flag = CL_INVALID_VALUE;
        send_comm(*clientfd, sizeof(cl_int), sizeof(cl_mem), 0,
                             &flag, &mem, NULL);
        VERBOSE_OUT(flag);
        return;
    }
    buffer_create_info = malloc(size);
    if(!buffer_create_info) {
        send_comm(*clientfd, sizeof(cl_int), sizeof(cl_mem), 0,
                             &flag, &mem, NULL);
        VERBOSE_OUT(flag);
        return;
    }

    // We never want to use pinned memory in the server
    if(flags & CL_MEM_USE_HOST_PTR)
        flags ^= CL_MEM_USE_HOST_PTR;

    mem = clCreateSubBuffer(buffer,
                            flags,
                            buffer_create_type,
                            buffer_create_info,
                            &flag);
    send_comm(*clientfd, sizeof(cl_int), sizeof(cl_mem), 0,
                         &flag, &mem, NULL);
    if(flag != CL_SUCCESS){
        VERBOSE_OUT(flag);
        return;
    }
    validator_add_mem(v, mem);
    VERBOSE_OUT(flag);
}

// ----------------------------------
// OpenCL 1.2
// ----------------------------------

void ocland_clCreateImage(int* clientfd, validator v, recv_pending msg)
{
    VERBOSE_IN();

    cl_int flag = CL_OUT_OF_HOST_MEMORY;
    cl_context context;
    cl_mem_flags flags;
    cl_image_format image_format;
    cl_image_desc image_desc;
 	size_t size;
    void *host_ptr = NULL;
    cl_mem mem = NULL;

    char *ptr;
    size_t msg_size = sizeof(cl_context) +
                      sizeof(cl_mem_flags) +
                      sizeof(cl_image_format) +
                      sizeof(cl_image_desc) +
                      sizeof(size_t);
    if( (msg.size < msg_size) ||
        !(ptr = disassemble_msg(msg.data, sizeof(cl_context), &context,
                                          sizeof(cl_mem_flags), &flags,
                                          sizeof(cl_image_format), &image_format,
                                          sizeof(cl_image_desc), &image_desc,
                                          sizeof(size_t), &size,
                                          0, NULL)) )
    {
        send_comm(*clientfd, sizeof(cl_int), sizeof(cl_mem), 0,
                             &flag, &mem, NULL);
        VERBOSE_OUT(flag);
        return;
    }

    if(!is_context(v, context)) {
        flag = CL_INVALID_CONTEXT;
        send_comm(*clientfd, sizeof(cl_int), sizeof(cl_mem), 0,
                             &flag, &mem, NULL);
        VERBOSE_OUT(flag);
        return;
    }

    if(!size) {
        flag = CL_INVALID_BUFFER_SIZE;
        send_comm(*clientfd, sizeof(cl_int), sizeof(cl_mem), 0,
                             &flag, &mem, NULL);
        VERBOSE_OUT(flag);
        return;
    }

    // We never want to use pinned memory in the server
    if(flags & CL_MEM_USE_HOST_PTR)
        flags ^= CL_MEM_USE_HOST_PTR;

    if(flags & CL_MEM_COPY_HOST_PTR) {
        if(msg.size != msg_size + size) {
            send_comm(*clientfd, sizeof(cl_int), sizeof(cl_mem), 0,
                                 &flag, &mem, NULL);
            VERBOSE_OUT(flag);
            return;
        }
        host_ptr = (void*)ptr;
    }

    mem = clCreateImage(context,
                        flags,
                        &image_format,
                        &image_desc,
                        host_ptr,
                        &flag);
    send_comm(*clientfd, sizeof(cl_int), sizeof(cl_mem), 0,
                         &flag, &mem, NULL);
    if(flag != CL_SUCCESS){
        VERBOSE_OUT(flag);
        return;
    }
    validator_add_mem(v, mem);
    VERBOSE_OUT(flag);
}
