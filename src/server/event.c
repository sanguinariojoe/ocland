/*
 *  This file is part of ocland, a free cloud OpenCL interface.
 *  Copyright (C) 2012-2019  Jose Luis Cercos Pita <jlcercos@gmail.com>
 *
 *  ocland is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  ocland is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with ocland.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <ocland/server/event.h>
#include <ocland/server/log.h>
#include <ocland/server/ocland_version.h>

/**
 * Process the events status change
 * @param event                     Changed event
 * @param event_command_exec_status Resulting status
 * @param user_data                 The casted client validator
 */
void CL_CALLBACK event_notify(cl_event event,
                              cl_int event_command_exec_status,
                              void *user_data)
{
    validator v = (validator)user_data;
    size_t msg_size = sizeof(cl_int);
    cl_event id = to_remote_event(v, event);

    if(async_send_cb(v, (void*)id, msg_size, &event_command_exec_status)) {
        printf("Failure reporting event change to %s: ",
               get_client_addr_str(v));
        fflush(stdout);
    }
}

void report_event_error(validator v,
                        cl_context context,
                        cl_int error,
                        const char* funcname)
{
    char error_str[129];
    error_str[128] = '\0';
    snprintf(error_str, 128, "%s() returned %s (%d)",
             funcname, get_opencl_error_name(error), error);
}

void install_event(validator v, cl_event event, cl_event peer)
{
    validator_add_event(v, event, peer);
    cl_int status[3] = {CL_SUBMITTED, CL_RUNNING, CL_COMPLETE};
    for(cl_uint i = 0; i < 3; i++){
        cl_int flag;
        flag = clSetEventCallback(event, status[i], &event_notify, (void*)v);
        if(flag != CL_SUCCESS) {
            cl_context context;
            clGetEventInfo(event, CL_EVENT_CONTEXT,
                           sizeof(cl_context), &context, NULL);
            report_event_error(v, context, flag, "clSetEventCallback");
        }
        flag = clRetainEvent(event);
        if(flag != CL_SUCCESS) {
            cl_context context;
            clGetEventInfo(event, CL_EVENT_CONTEXT,
                           sizeof(cl_context), &context, NULL);
            report_event_error(v, context, flag, "clSetEventCallback");
        }
    }
}

DEFINE_INFO_GETTER(clGetEventInfo, cl_event, cl_event_info, event)

void ocland_clRetainEvent(int* clientfd, validator v, recv_pending msg)
{
    VERBOSE_IN();

    cl_int flag = CL_OUT_OF_HOST_MEMORY;

    // The received data shall be directly the event
    if(msg.size != sizeof(cl_event)) {
        send_comm(*clientfd, sizeof(cl_int), 0, &flag, NULL);
        VERBOSE_OUT(flag);
        return;
    }
    const cl_event event = *((cl_event*)(msg.data));
    // Even though we can keep reference counting at this side, actually is the
    // client who is taking care on such task, so we actually never call to
    // clRetainEvent()
    if(!from_remote_event(v, event))
        flag = CL_INVALID_EVENT;
    else
        flag = CL_SUCCESS;
    send_comm(*clientfd, sizeof(cl_int), 0, &flag, NULL);
    VERBOSE_OUT(flag);
}

void ocland_clReleaseEvent(int* clientfd, validator v, recv_pending msg)
{
    VERBOSE_IN();

    cl_int flag = CL_OUT_OF_HOST_MEMORY;

    // The received data shall be directly the event
    if(msg.size != sizeof(cl_event)) {
        send_comm(*clientfd, sizeof(cl_int), 0, &flag, NULL);
        VERBOSE_OUT(flag);
        return;
    }
    cl_event event = *((cl_event*)(msg.data));
    // Even though we can keep reference counting at this side, actually is the
    // client who is taking care on such task, so if we reached this point we
    // can assume that the event can be safely released, and the linker shall
    // be detroyed
    event = from_remote_event(v, event);
    if(!event)
        flag = CL_INVALID_EVENT;
    else {
        flag = clReleaseEvent(event);
        if(flag == CL_SUCCESS)
            validator_remove_event(v, event);
    }

    send_comm(*clientfd, sizeof(cl_int), 0, &flag, NULL);
    VERBOSE_OUT(flag);
}

DEFINE_INFO_GETTER( clGetEventProfilingInfo, cl_event, cl_profiling_info, event)

// -------------------------------------------- //
//                                              //
// OpenCL 1.1 methods                           //
//                                              //
// -------------------------------------------- //

void ocland_clCreateUserEvent(int* clientfd, validator v, recv_pending msg)
{
    VERBOSE_IN();

    cl_int flag = CL_OUT_OF_HOST_MEMORY;
    cl_context context;
    cl_event ref;
    cl_event event = NULL;

    size_t msg_size = sizeof(cl_context) + sizeof(cl_event);
    if( (msg.size != msg_size) ||
        !disassemble_msg(msg.data, sizeof(cl_context), &context,
                                   sizeof(cl_event), &ref,
                                   0, NULL) )
    {
        // We cannot report to anyone...
        VERBOSE_OUT(flag);
        return;
    }

    if(!is_context(v, context)) {
        // We still cannot report to anyone...
        VERBOSE_OUT(CL_INVALID_CONTEXT);
        return;
    }

    event = clCreateUserEvent(context, &flag);
    if(flag != CL_SUCCESS) {
        report_event_error(v, context, flag, "clCreateUserEvent");
        VERBOSE_OUT(flag);
        return;
    }

    // The user events don't require a callback, since the client itself is
    // changing the event status
    validator_add_event(v, event, ref);
    VERBOSE_OUT(flag);
}

void ocland_clSetUserEventStatus(int* clientfd, validator v, recv_pending msg)
{

    VERBOSE_IN();

    cl_int flag = CL_OUT_OF_HOST_MEMORY;
    cl_event ref;
    cl_int status;

    size_t msg_size = sizeof(cl_event) + sizeof(cl_int);
    if( (msg.size != msg_size) ||
        !disassemble_msg(msg.data, sizeof(cl_event), &ref,
                                   sizeof(cl_int), &status,
                                   0, NULL) )
    {
        // We cannot report to anyone...
        VERBOSE_OUT(flag);
        return;
    }

    cl_event event = from_remote_event(v, ref);
    if(!event) {
        // We still cannot report to anyone...
        VERBOSE_OUT(CL_INVALID_EVENT);
        return;
    }

    flag = clSetUserEventStatus(event, status);
    if(flag != CL_SUCCESS) {
        cl_context c;
        clGetEventInfo(event, CL_EVENT_CONTEXT, sizeof(cl_context), &c, NULL);
        report_event_error(v, c, flag, "clSetUserEventStatus");
        VERBOSE_OUT(flag);
        return;
    }

    VERBOSE_OUT(flag);
}
