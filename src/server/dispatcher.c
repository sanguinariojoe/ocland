/*
 *  This file is part of ocland, a free cloud OpenCL interface.
 *  Copyright (C) 2012  Jose Luis Cercos Pita <jl.cercos@upm.es>
 *
 *  ocland is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  ocland is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with ocland.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <pthread.h>

#include <ocland/common/dataExchange.h>
#include <ocland/server/dispatcher.h>
#include <ocland/server/platform.h>
#include <ocland/server/device.h>
#include <ocland/server/context.h>
#include <ocland/server/command_queue.h>
#include <ocland/server/mem.h>
#include <ocland/server/sampler.h>
#include <ocland/server/program.h>
#include <ocland/server/kernel.h>
#include <ocland/server/event.h>
#include <ocland/server/ocland_cl.h>

typedef void(*func)(int* clientfd, validator v, recv_pending msg);

/// Number of OpenCL FUNCTIONS
#define CL_N_FUNCTIONS 147u

/// List of functions to dispatch request from client
static func dispatchFunctions[CL_N_FUNCTIONS] =
{
    &ocland_clGetPlatformIDs,
    &ocland_clGetPlatformInfo,
    &ocland_clGetDeviceIDs,
    &ocland_clGetDeviceInfo,
    &ocland_clCreateContext,
    &ocland_clCreateContextFromType,
    &ocland_clRetainContext,
    &ocland_clReleaseContext,
    &ocland_clGetContextInfo,
    &ocland_clCreateCommandQueue,
    &ocland_clRetainCommandQueue,
    &ocland_clReleaseCommandQueue,
    &ocland_clGetCommandQueueInfo,
    NULL, // &ocland_clSetCommandQueueProperty, // (NO ICD)
    &ocland_clCreateBuffer,
    NULL, // &ocland_clCreateImage2D, // Redirected to clCreateImage
    NULL, // &ocland_clCreateImage3D, // Redirected to clCreateImage
    &ocland_clRetainMemObject,
    &ocland_clReleaseMemObject,
    &ocland_clGetSupportedImageFormats,
    &ocland_clGetMemObjectInfo,
    &ocland_clGetImageInfo,
    &ocland_clCreateSampler,
    &ocland_clRetainSampler,
    &ocland_clReleaseSampler,
    &ocland_clGetSamplerInfo,
    &ocland_clCreateProgramWithSource,
    &ocland_clCreateProgramWithBinary,
    &ocland_clRetainProgram,
    &ocland_clReleaseProgram,
    &ocland_clBuildProgram,
    NULL, // &ocland_clUnloadCompiler, // (NO ICD)
    &ocland_clGetProgramInfo,
    &ocland_clGetProgramBuildInfo,
    &ocland_clCreateKernel,
    &ocland_clCreateKernelsInProgram,
    &ocland_clRetainKernel,
    &ocland_clReleaseKernel,
    &ocland_clSetKernelArg,
    &ocland_clGetKernelInfo,
    &ocland_clGetKernelWorkGroupInfo,
    NULL, // &ocland_clWaitForEvents,  // Carried out by client
    &ocland_clGetEventInfo,
    &ocland_clRetainEvent,
    &ocland_clReleaseEvent,
    NULL, // &ocland_clGetEventProfilingInfo,
    NULL, // &ocland_clFlush,
    NULL, // &ocland_clFinish,
    &ocland_clEnqueueReadBuffer,
    &ocland_clEnqueueWriteBuffer,
    NULL, // &ocland_clEnqueueCopyBuffer,
    NULL, // &ocland_clEnqueueReadImage,
    NULL, // &ocland_clEnqueueWriteImage,
    NULL, // &ocland_clEnqueueCopyImage,
    NULL, // &ocland_clEnqueueCopyImageToBuffer,
    NULL, // &ocland_clEnqueueCopyBufferToImage,
    NULL, // &ocland_clEnqueueMapBuffer,
    NULL, // &ocland_clEnqueueMapImage,
    NULL, // &ocland_clEnqueueUnmapMemObject,
    NULL, // &ocland_clEnqueueNDRangeKernel,
    NULL, // &ocland_clEnqueueTask,
    NULL, // &ocland_clEnqueueNativeKernel,
    NULL, // &ocland_clEnqueueMarker,
    NULL, // &ocland_clEnqueueWaitForEvents,
    NULL, // &ocland_clEnqueueBarrier,
    NULL, // &ocland_clGetExtensionFunctionAddress // (NO ICD)
    NULL, // &ocland_clCreateFromGLBuffer,
    NULL, // &ocland_clCreateFromGLTexture2D
    NULL, // &ocland_clCreateFromGLTexture3D,
    NULL, // &ocland_clCreateFromGLRenderbuffer,
    NULL, // &ocland_clGetGLObjectInfo,
    NULL, // &ocland_clGetGLTextureInfo,
    NULL, // &ocland_clEnqueueAcquireGLObjects,
    NULL, // &ocland_clEnqueueReleaseGLObjects,
    NULL, // &ocland_clGetGLContextInfoKHR,
    NULL, // &ocland_clGetDeviceIDsFromD3D10KHR, // (NO ICD)
    NULL, // &ocland_clCreateFromD3D10BufferKHR, // (NO ICD)
    NULL, // &ocland_clCreateFromD3D10Texture2DKHR, // (NO ICD)
    NULL, // &ocland_clCreateFromD3D10Texture3DKHR, // (NO ICD)
    NULL, // &ocland_clEnqueueAcquireD3D10ObjectsKHR, // (NO ICD)
    NULL, // &ocland_clEnqueueReleaseD3D10ObjectsKHR, // (NO ICD)
    NULL, // &ocland_clSetEventCallback,
    &ocland_clCreateSubBuffer,
    NULL, // &ocland_clSetMemObjectDestructorCallback,
    NULL, // &ocland_clCreateUserEvent,
    NULL, // &ocland_clSetUserEventStatus,
    NULL, // &ocland_clEnqueueReadBufferRect,
    NULL, // &ocland_clEnqueueWriteBufferRect,
    NULL, // &ocland_clEnqueueCopyBufferRect,
    NULL, // &ocland_clCreateSubDevicesEXT
    NULL, // &ocland_clRetainDeviceEXT
    NULL, // &ocland_clReleaseDeviceEXT
    NULL, // &ocland_clCreateEventFromGLsyncKHR,
    &ocland_clCreateSubDevices,
    &ocland_clRetainDevice,
    &ocland_clReleaseDevice,
    &ocland_clCreateImage,
    &ocland_clCreateProgramWithBuiltInKernels,
    &ocland_clCompileProgram,
    &ocland_clLinkProgram,
    &ocland_clUnloadPlatformCompiler,
    &ocland_clGetKernelArgInfo,
    NULL, // &ocland_clEnqueueFillBuffer,
    NULL, // &ocland_clEnqueueFillImage,
    NULL, // &ocland_clEnqueueMigrateMemObjects,
    NULL, // &ocland_clEnqueueMarkerWithWaitList,
    NULL, // &ocland_clEnqueueBarrierWithWaitList,
    NULL, // &ocland_clGetExtensionFunctionAddressForPlatform // (NO ICD)
    NULL, // &ocland_clCreateFromGLTexture,
    NULL, // &ocland_clGetDeviceIDsFromD3D11KHR, // (NO ICD)
    NULL, // &ocland_clCreateFromD3D11BufferKHR, // (NO ICD)
    NULL, // &ocland_clCreateFromD3D11Texture2DKHR, // (NO ICD)
    NULL, // &ocland_clCreateFromD3D11Texture3DKHR, // (NO ICD)
    NULL, // &ocland_clCreateFromDX9MediaSurfaceKHR, // (NO ICD)
    NULL, // &ocland_clEnqueueAcquireD3D11ObjectsKHR, // (NO ICD)
    NULL, // &ocland_clEnqueueReleaseD3D11ObjectsKHR, // (NO ICD)
    NULL, // &ocland_clGetDeviceIDsFromDX9MediaAdapterKHR, // (NO ICD)
    NULL, // &ocland_clEnqueueAcquireDX9MediaSurfacesKHR, // (NO ICD)
    NULL, // &ocland_clEnqueueReleaseDX9MediaSurfacesKHR, // (NO ICD)
    NULL, // &ocland_clCreateFromEGLImageKHR,
    NULL, // &ocland_clEnqueueAcquireEGLObjectsKHR,
    NULL, // &ocland_clEnqueueReleaseEGLObjectsKHR,
    NULL, // &ocland_clCreateEventFromEGLSyncKHR,
    &ocland_clCreateCommandQueueWithProperties,
    NULL, // &ocland_clCreatePipe,
    NULL, // &ocland_clGetPipeInfo,
    NULL, // &ocland_clSVMAlloc,
    NULL, // &ocland_clSVMFree,
    NULL, // &ocland_clEnqueueSVMFree,
    NULL, // &ocland_clEnqueueSVMMemcpy,
    NULL, // &ocland_clEnqueueSVMMemFill,
    NULL, // &ocland_clEnqueueSVMMap,
    NULL, // &ocland_clEnqueueSVMUnmap,
    NULL, // &ocland_clCreateSamplerWithProperties,
    NULL, // &ocland_clSetKernelArgSVMPointer,
    NULL, // &ocland_clSetKernelExecInfo,
    NULL, // &ocland_clGetKernelSubGroupInfoKHR,
    NULL, // &ocland_clCloneKernel,
    NULL, // &ocland_clCreateProgramWithIL,
    NULL, // &ocland_clEnqueueSVMMigrateMem,
    NULL, // &ocland_clGetDeviceAndHostTimer,
    NULL, // &ocland_clGetHostTimer,
    NULL, // &ocland_clGetKernelSubGroupInfo,
    NULL, // &ocland_clSetDefaultDeviceCommandQueue,
    NULL, // &ocland_clSetProgramReleaseCallback,
    NULL, // &ocland_clSetProgramSpecializationConstant,
};



void dispatch(validator v)
{
    int clientfd = get_client_socket(v);
    size_t command_size = 0;
    unsigned int command;
    int flag = Recv(&clientfd, &command_size, sizeof(size_t),
                    MSG_DONTWAIT | MSG_PEEK);
    if(flag < 0) {
        return;
    }
    if(!flag) {
        printf("%s disconnected, goodbye ;-)\n", get_client_addr_str(v));
        fflush(stdout);
        disconnect(v);
        return;
    }

    recv_pending rest;
    flag = recv_comm(clientfd, &rest, sizeof(unsigned int),
                                      0,
                                      &command,
                                      NULL);
    if(flag) {
        printf("%s disconnected while operating\n", get_client_addr_str(v));
        fflush(stdout);
        disconnect(v);
        return;
    }

    if(command >= CL_N_FUNCTIONS) {
        printf("%s asked for function %u, but just %u are available\n",
               get_client_addr_str(v), command, CL_N_FUNCTIONS);
        return;
    }
    else if(dispatchFunctions[command] == NULL) {
        printf("%s asked for unknown function %u\n",
               get_client_addr_str(v), command);
        return;
    }

    // Call the command
    dispatchFunctions[command] (&clientfd, v, rest);
    free(rest.data);
}
