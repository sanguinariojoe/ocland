/*
 *  This file is part of ocland, a free cloud OpenCL interface.
 *  Copyright (C) 2012-2019  Jose Luis Cercos Pita <jlcercos@gmail.com>
 *
 *  ocland is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  ocland is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with ocland.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <ocland/common/opencl.h>

const char* get_opencl_error_name(cl_int error)
{
#ifdef CASE
#undef CASE
#endif
#define CASE(x) case x: return #x;

    switch (error) {
    	CASE(CL_SUCCESS)                                   // 0
    	CASE(CL_DEVICE_NOT_FOUND)                          // -1
    	CASE(CL_DEVICE_NOT_AVAILABLE)                      // -2
    	CASE(CL_COMPILER_NOT_AVAILABLE)                    // -3
    	CASE(CL_MEM_OBJECT_ALLOCATION_FAILURE)             // -4
    	CASE(CL_OUT_OF_RESOURCES)                          // -5
    	CASE(CL_OUT_OF_HOST_MEMORY)                        // -6
    	CASE(CL_PROFILING_INFO_NOT_AVAILABLE)              // -7
    	CASE(CL_MEM_COPY_OVERLAP)                          // -8
    	CASE(CL_IMAGE_FORMAT_MISMATCH)                     // -9
    	CASE(CL_IMAGE_FORMAT_NOT_SUPPORTED)                // -10
    	CASE(CL_BUILD_PROGRAM_FAILURE)                     // -11
    	CASE(CL_MAP_FAILURE)                               // -12
#ifdef CL_VERSION_1_1
    	CASE(CL_MISALIGNED_SUB_BUFFER_OFFSET)              // -13
    	CASE(CL_EXEC_STATUS_ERROR_FOR_EVENTS_IN_WAIT_LIST) // -14
#endif //CL_VERSION_1_1
#ifdef CL_VERSION_1_2
    	CASE(CL_COMPILE_PROGRAM_FAILURE)                   // -15
    	CASE(CL_LINKER_NOT_AVAILABLE)                      // -16
    	CASE(CL_LINK_PROGRAM_FAILURE)                      // -17
    	CASE(CL_DEVICE_PARTITION_FAILED)                   // -18
    	CASE(CL_KERNEL_ARG_INFO_NOT_AVAILABLE)             // -19
#endif //CL_VERSION_1_2
    	CASE(CL_INVALID_VALUE)                             // -30
    	CASE(CL_INVALID_DEVICE_TYPE)                       // -31
    	CASE(CL_INVALID_PLATFORM)                          // -32
    	CASE(CL_INVALID_DEVICE)                            // -33
    	CASE(CL_INVALID_CONTEXT)                           // -34
    	CASE(CL_INVALID_QUEUE_PROPERTIES)                  // -35
    	CASE(CL_INVALID_COMMAND_QUEUE)                     // -36
    	CASE(CL_INVALID_HOST_PTR)                          // -37
    	CASE(CL_INVALID_MEM_OBJECT)                        // -38
    	CASE(CL_INVALID_IMAGE_FORMAT_DESCRIPTOR)           // -39
    	CASE(CL_INVALID_IMAGE_SIZE)                        // -40
    	CASE(CL_INVALID_SAMPLER)                           // -41
    	CASE(CL_INVALID_BINARY)                            // -42
    	CASE(CL_INVALID_BUILD_OPTIONS)                     // -43
    	CASE(CL_INVALID_PROGRAM)                           // -44
    	CASE(CL_INVALID_PROGRAM_EXECUTABLE)                // -45
    	CASE(CL_INVALID_KERNEL_NAME)                       // -46
    	CASE(CL_INVALID_KERNEL_DEFINITION)                 // -47
    	CASE(CL_INVALID_KERNEL)                            // -48
    	CASE(CL_INVALID_ARG_INDEX)                         // -49
    	CASE(CL_INVALID_ARG_VALUE)                         // -50
    	CASE(CL_INVALID_ARG_SIZE)                          // -51
    	CASE(CL_INVALID_KERNEL_ARGS)                       // -52
    	CASE(CL_INVALID_WORK_DIMENSION)                    // -53
    	CASE(CL_INVALID_WORK_GROUP_SIZE)                   // -54
    	CASE(CL_INVALID_WORK_ITEM_SIZE)                    // -55
    	CASE(CL_INVALID_GLOBAL_OFFSET)                     // -56
    	CASE(CL_INVALID_EVENT_WAIT_LIST)                   // -57
    	CASE(CL_INVALID_EVENT)                             // -58
    	CASE(CL_INVALID_OPERATION)                         // -59
    	CASE(CL_INVALID_GL_OBJECT)                         // -60
    	CASE(CL_INVALID_BUFFER_SIZE)                       // -61
    	CASE(CL_INVALID_MIP_LEVEL)                         // -62
    	CASE(CL_INVALID_GLOBAL_WORK_SIZE)                  // -63
#ifdef CL_VERSION_1_1
	   CASE(CL_INVALID_PROPERTY)                           // -64
#endif //CL_VERSION_1_1
#ifdef CL_VERSION_1_2
    	CASE(CL_INVALID_IMAGE_DESCRIPTOR)                  // -65
    	CASE(CL_INVALID_COMPILER_OPTIONS)                  // -66
    	CASE(CL_INVALID_LINKER_OPTIONS)                    // -67
    	CASE(CL_INVALID_DEVICE_PARTITION_COUNT)            // -68
#endif //CL_VERSION_1_2

        default:
            return "(unrecognized error)";
    }

#undef CASE
}
