/*
 *  This file is part of ocland, a free cloud OpenCL interface.
 *  Copyright (C) 2012-2019  Jose Luis Cercos Pita <jlcercos@gmail.com>
 *
 *  ocland is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  ocland is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with ocland.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>
#include <pthread.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>

#include <ocland/common/dataExchange.h>
#include <ocland/common/typed_list.h>
#include <ocland/common/notifier.h>

/**
 * Notifier endpoint data
 */
struct _endpoint
{
    /// Identifier
    void *id;
    /// Function to be trigggered
    void (*fn) (void *, size_t, void*, void *);
    /// User data to be passed to the function
    void *user_data;
    /// Whether the endpoint shall be destroyed after running or not
    bool once;
};

typedef struct _endpoint* endpoint;

DEFINE_LIST(endpoint, endpoints)

struct _notifier
{
    /// Listening socket
    int socket;
    /// Flag to let know the parallel thread that it should still be listening
    volatile bool keep_alive;
    /// Listening thread
    pthread_t thread;
    /** Mutex to avoid main thread try to modify notifier content while parallel
     * one is using it
     */
    pthread_mutex_t lock;
    /// List of Endpoints
    List* endpoints;
    /// Default endpoint
    endpoint default_endpoint;
};

void *notifier_thread(void *arg)
{
    notifier n = (notifier)arg;

    while(n->keep_alive) {
        void *id;
        size_t msg_size;
        ssize_t exchanged = Recv(&(n->socket), &id, sizeof(void*),
                                 MSG_DONTWAIT | MSG_PEEK);
        if(exchanged < 0) {
            usleep(1000);
            continue;
        }
        if(!exchanged) {
            // Client asked for disconnection
            n->keep_alive = false;
            return NULL;
        }
        // We have a message
        exchanged = Recv(&(n->socket), &id, sizeof(void*), MSG_WAITALL);
        if(exchanged <= 0) {
            // ???
            n->keep_alive = false;
            return NULL;
        }
        exchanged = Recv(&(n->socket), &msg_size, sizeof(size_t), MSG_WAITALL);
        if(exchanged <= 0) {
            // ???
            n->keep_alive = false;
            return NULL;
        }
        void *msg = NULL;
        if(msg_size > 0) {
            msg = malloc(msg_size);
            if(!msg) {
                // ???
                n->keep_alive = false;
                return NULL;
            }
            exchanged = Recv(&(n->socket), msg, msg_size, MSG_WAITALL);
            if(exchanged <= 0) {
                // ???
                n->keep_alive = false;
                return NULL;
            }
        }

        // Time to locate the notifier and trigger the function (if possible)
        pthread_mutex_lock(&(n->lock));
        unsigned int i = 0;
        endpoint default_endpoint = n->default_endpoint;
        for(endpoints_node endpoint_node = endpoints_front(n->endpoints);
            !endpoints_is_null(endpoint_node);
            endpoint_node = endpoints_next(endpoint_node))
        {
            endpoint e = endpoints_data(endpoint_node);
            if(e->id != id)
                continue;
            default_endpoint = NULL;
            e->fn(id, msg_size, msg, e->user_data);
            if(e->once) {
                free(e);
                endpoints_remove_at(n->endpoints, i);
                continue;
            }
            i++;
        }
        if(default_endpoint)
            default_endpoint->fn(id,
                                 msg_size,
                                 msg,
                                 default_endpoint->user_data);
        pthread_mutex_unlock(&(n->lock));

        free(msg);
    }

    return NULL;
}

notifier notifier_new(int socket)
{
    if (socket < 0)
        return NULL;
    notifier n = (notifier)malloc(sizeof(struct _notifier));
    if (!n)
        return NULL;
    n->socket = socket;
    n->keep_alive = true;
    n->default_endpoint = NULL;

    if (pthread_mutex_init(&(n->lock), NULL) != 0) {
        free(n);
        return NULL;
    }

    n->endpoints = endpoints_new();
    if (!n->endpoints) {
        pthread_mutex_destroy(&(n->lock));
        free(n);
        return NULL;
    }

    int err = pthread_create(&(n->thread), NULL, notifier_thread, (void*)n);
    if(err) {
        endpoints_delete(n->endpoints);
        pthread_mutex_destroy(&(n->lock));
        free(n);
        return NULL;
    }

    return n;
}

void notifier_delete(notifier n)
{
    pthread_mutex_lock(&(n->lock));
    n->keep_alive = false;
    pthread_mutex_unlock(&(n->lock));
    pthread_join(n->thread, NULL);

    for(endpoints_node endpoint_node = endpoints_front(n->endpoints);
        !endpoints_is_null(endpoint_node);
        endpoint_node = endpoints_next(endpoint_node))
    {
        free(endpoints_data(endpoint_node));
    }
    endpoints_delete(n->endpoints);

    notifier_unset_default(n);

    pthread_mutex_destroy(&(n->lock));

    free(n);
}

void notifier_append(notifier n,
                     void* id,
                     void (*fn) (void *, size_t, void *, void*),
                     void* user_data,
                     bool once)
{
    endpoint e = (endpoint)malloc(sizeof(struct _endpoint));
    if(!e)
        return;
    e->id = id;
    e->fn = fn;
    e->user_data = user_data;
    e->once = once;

    pthread_mutex_lock(&(n->lock));
    endpoints_push_back(n->endpoints, e);
    pthread_mutex_unlock(&(n->lock));
}

void notifier_remove(notifier n, void* identifier)
{
    unsigned int i=0;
    pthread_mutex_lock(&(n->lock));
    for(endpoints_node endpoint_node = endpoints_front(n->endpoints);
        !endpoints_is_null(endpoint_node);
        endpoint_node = endpoints_next(endpoint_node))
    {
        endpoint e = endpoints_data(endpoint_node);
        if(e->id == identifier) {
            free(e);
            endpoints_remove_at(n->endpoints, i);
            continue;
        }
        i++;
    }
    pthread_mutex_unlock(&(n->lock));
}

void notifier_set_default(notifier n,
                          void (*fn) (void *, size_t, void *, void*),
                          void* user_data)
{
    endpoint e = (endpoint)malloc(sizeof(struct _endpoint));
    if(!e)
        return;
    e->id = NULL;
    e->fn = fn;
    e->user_data = user_data;
    e->once = false;

    pthread_mutex_lock(&(n->lock));
    n->default_endpoint = e;
    pthread_mutex_unlock(&(n->lock));
}

void notifier_unset_default(notifier n)
{
    pthread_mutex_lock(&(n->lock));
    free(n->default_endpoint);
    n->default_endpoint = NULL;
    pthread_mutex_unlock(&(n->lock));
}
