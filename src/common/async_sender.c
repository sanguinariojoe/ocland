/*
 *  This file is part of ocland, a free cloud OpenCL interface.
 *  Copyright (C) 2012-2019  Jose Luis Cercos Pita <jlcercos@gmail.com>
 *
 *  ocland is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  ocland is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with ocland.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>
#include <pthread.h>
#include <sys/types.h>
#include <sys/socket.h>

#include <ocland/common/dataExchange.h>
#include <ocland/common/async_sender.h>

struct _async_sender
{
    /// Listening socket
    int socket;
    /// Whether the sender accepts new tasks or not
    bool active;
    /** Mutex to avoid several threads try to send data at the same time, mixing
     * the packages
     */
    pthread_mutex_t lock;
};

async_sender async_sender_new(int socket)
{
    if (socket < 0)
        return NULL;
    async_sender sender = (async_sender)malloc(sizeof(struct _async_sender));
    if (!sender)
        return NULL;
    sender->socket = socket;
    sender->active = true;

    if (pthread_mutex_init(&(sender->lock), NULL) != 0) {
        free(sender);
        return NULL;
    }

    return sender;
}

void async_sender_delete(async_sender sender)
{
    if(!sender)
        return;

    // Ensure no-one is still trying to send data
    pthread_mutex_lock(&(sender->lock));
    sender->active = false;
    pthread_mutex_unlock(&(sender->lock));

    pthread_mutex_destroy(&(sender->lock));

    free(sender);
}

ssize_t async_send(async_sender sender, void* id, size_t size, void* data)
{
    pthread_mutex_lock(&(sender->lock));
    if(!sender->active) {
        pthread_mutex_unlock(&(sender->lock));
        return -1;
    }

    ssize_t sent = 0;
    sent += Send(&(sender->socket), &id, sizeof(void*), 0);
    sent += Send(&(sender->socket), &size, sizeof(size_t), 0);
    sent += Send(&(sender->socket), data, size, 0);
    pthread_mutex_unlock(&(sender->lock));

    return sent;
}
