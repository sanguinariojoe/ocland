/*
 *  This file is part of ocland, a free cloud OpenCL interface.
 *  Copyright (C) 2012-2019  Jose Luis Cercos Pita <jlcercos@gmail.com>
 *
 *  ocland is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  ocland is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with ocland.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <stdio.h>
#include <string.h>
#include <ocland/common/arch.h>

static char arch_str[256];

/** @brief Get endian
 * @return '<' if little-endian is applied, '>' otherwise
 */
char endian()
{
    unsigned int i = 1;
    char *c = (char*)&i;
    if (*c)
        return '<';
    return '>';
}

char* architecture_string()
{
    memset(arch_str, '\0', 256 * sizeof(char));

    sprintf(arch_str,
            "%c%lu%lu%lu%lu%lu%lu",
            endian(),
            sizeof(void*),
            sizeof(size_t),
            sizeof(int),
            sizeof(long int),
            sizeof(float),
            sizeof(double));

    return arch_str;
}
