/*
 *  This file is part of ocland, a free cloud OpenCL interface.
 *  Copyright (C) 2012-2019  Jose Luis Cercos Pita <jlcercos@gmail.com>
 *
 *  ocland is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  ocland is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with ocland.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <ocland/common/list.h>

struct _List
{
    /// Head of the chain
    Node *head;
    /// Tail of the chain
    Node *tail;
    /// Lenght of the list
    size_t len;
};

List* list_new()
{
    List *list = (List*)malloc(sizeof(List));
    list->head = NULL;
    list->tail = NULL;
    list->len = 0;
    return list;
}

/** @brief Generate a list node from the data
 * @param data Node data
 * @param data_size Node data size
 * @return List node
 */
Node* list_new_node(void* data, size_t data_size)
{
    Node *node = (Node*)malloc(sizeof(Node));
    node->data = malloc(data_size);
    memcpy(node->data, data, data_size);
    node->size = data_size;
    node->next = NULL;
    node->prev = NULL;
    node->is_null = false;
    return node;
}

/// Null node to return when there is not more information available
static Node null_node = {NULL, 0, NULL, NULL, true};

Node list_push_front(List* list, void* data, size_t data_size)
{
    Node *node = list_new_node(data, data_size);

    if(list->head != NULL) {
        list->head->prev = node;
        node->next = list->head;
    }

    list->head = node;
    if(!list->len)
        list->tail = node;
    list->len++;

    return *node;
}

Node list_push_back(List* list, void* data, size_t data_size)
{
    Node *node = list_new_node(data, data_size);

    if(list->tail != NULL) {
        list->tail->next = node;
        node->prev = list->tail;
    }

    list->tail = node;
    if(!list->len)
        list->head = node;
    list->len++;

    return *node;
}

Node list_front(List* list)
{
    if(!list->head)
        return null_node;
    return *(list->head);
}

Node list_back(List* list)
{
    if(!list->tail)
        return null_node;
    return *(list->tail);
}

Node list_pop_front(List* list)
{
    // Get a copy of the node
    Node node = list_front(list);

    // Remove the node
    Node *second = list->head->next;
    if(second)
        second->prev = NULL;
    else
        list->tail = NULL;
    free(list->head);
    list->head = second;
    list->len--;

    return node;
}

Node list_pop_back(List* list)
{
    // Get a copy of the node
    Node node = list_back(list);

    // Remove the node
    Node *second = list->tail->prev;
    if(second)
        second->next = NULL;
    else
        list->head = NULL;
    free(list->tail);
    list->tail = second;
    list->len--;

    return node;
}

size_t list_len(List* list)
{
    return list->len;
}

void list_delete(List* list)
{
    Node *node = list->head;
    while(node) {
        Node *next = node->next;
        free(node->data);
        free(node);
        node = next;
    }
    free(list);
}

Node list_next(Node node)
{
    if(!node.next)
        return null_node;
    return *(node.next);
}

Node list_prev(Node node)
{
    if(!node.prev)
        return null_node;
    return *(node.prev);
}

void* list_data(Node node)
{
    return node.data;
}

bool list_is_first(Node node)
{
    return !node.is_null && (node.prev == NULL);
}

bool list_is_last(Node node)
{
    return !node.is_null && (node.next == NULL);
}

bool list_is_null(Node node)
{
    return node.is_null;
}

/** @brief Get a node by its index in straight direction
 * @param list Considered list
 * @param index Node to get extracted
 * @return The node
 */
Node* list_at_from_front(List* list, size_t index)
{
    Node *node = list->head;
    for(size_t i = 0; i < index; i++) {
        node = node->next;
    }
    return node;
}

/** @brief Get a node by its index in reverse direction
 * @param list Considered list
 * @param index Node to get extracted
 * @return The node
 */
Node* list_at_from_back(List* list, size_t index)
{
    Node *node = list->tail;
    for(size_t i = 0; i < index; i++) {
        node = node->prev;
    }
    return node;
}

Node list_at(List* list, size_t index)
{
    size_t rindex = list->len - index - 1;
    if(rindex < index)
        return *(list_at_from_back(list, rindex));
    return *(list_at_from_front(list, rindex));
}

Node list_insert(List* list, void* data, size_t data_size, size_t index)
{
    if(index == 0)
        return list_push_front(list, data, data_size);
    else if(index == list->len - 1)
        return list_push_back(list, data, data_size);

    Node* prev_node;
    size_t rindex = list->len - index - 1;
    if(rindex < index)
        prev_node = list_at_from_back(list, rindex);
    else
        prev_node = list_at_from_front(list, index);
    Node* next_node = prev_node->next;

    Node* node = list_new_node(data, data_size);
    node->next = next_node;
    node->prev = prev_node;
    prev_node->next = node;
    next_node->prev = node;

    list->len++;

    return *node;
}

Node list_remove_at(List* list, size_t index)
{
    if(index == 0)
        return list_pop_front(list);
    else if(index == list->len - 1)
        return list_pop_back(list);

    // Locate the node
    Node* node;
    size_t rindex = list->len - index - 1;
    if(rindex < index)
        node = list_at_from_back(list, rindex);
    else
        node = list_at_from_front(list, index);

    // Create the bridge
    node->next->prev = node->prev;
    node->prev->next = node->next;

    // Copy the node and release it
    Node ret_node = *node;
    free(node);

    list->len--;

    return ret_node;
}

size_t list_find(List* list, void* data, size_t data_size)
{
    size_t i = 0;
    Node *node = list->head;
    while(node) {
        if((node->size == data_size) && !memcmp(node->data, data, data_size))
            break;
        node = node->next;
        i++;
    }
    return i;
}
