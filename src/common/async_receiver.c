/*
 *  This file is part of ocland, a free cloud OpenCL interface.
 *  Copyright (C) 2012-2019  Jose Luis Cercos Pita <jlcercos@gmail.com>
 *
 *  ocland is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  ocland is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with ocland.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>
#include <pthread.h>
#include <sys/types.h>
#include <sys/socket.h>

#include <ocland/common/async_receiver.h>
#include <ocland/common/notifier.h>
#include <ocland/common/typed_list.h>

struct _recv_datum
{
    /// Identifier
    void *id;
    /// Received data
    recv_pending data;
};

typedef struct _recv_datum* recv_datum;

DEFINE_LIST(recv_datum, recv_data)

struct _async_receiver
{
    /// Listening socket
    int socket;
    /// Whether the receiver accepts new packets or not
    bool active;
    /** Mutex to avoid several threads try to set the stack and consume from it
     * at the same time
     */
    pthread_mutex_t lock;
    /// Whether the receiver accepts new tasks or not
    notifier n;
    /// List of already received data
    List* data;
};

/**
 * Function called when new data arrives
 * @param id       Identifier sent by the counterpart
 * @param msg_size Message size
 * @param msg      Message received
 * @param r        Casted receiver instance
 */
void async_recv_notifier(void *id,
                         size_t msg_size,
                         void* msg,
                         void* r)
{
    async_receiver receiver = (async_receiver)r;
    // Set up the data structure
    recv_datum datum = (recv_datum)malloc(sizeof(struct _recv_datum));
    if(!datum)
        return;
    datum->id = id;
    datum->data.size = msg_size;
    // msg will be removed by notifier when this function finish, so we shall
    // copy it
    datum->data.data = malloc(msg_size);
    if(!datum->data.data) {
        free(datum);
        return;
    }
    memcpy(datum->data.data, msg, msg_size);

    pthread_mutex_lock(&(receiver->lock));
    recv_data_push_back(receiver->data, datum);
    pthread_mutex_unlock(&(receiver->lock));
}

async_receiver async_receiver_new(int socket)
{
    if (socket < 0)
        return NULL;
    async_receiver receiver = (async_receiver)malloc(
        sizeof(struct _async_receiver));
    if (!receiver)
        return NULL;
    receiver->socket = socket;
    receiver->active = true;

    if (pthread_mutex_init(&(receiver->lock), NULL) != 0) {
        free(receiver);
        return NULL;
    }

    receiver->n = notifier_new(socket);
    if(!receiver->n) {
        pthread_mutex_destroy(&(receiver->lock));
        free(receiver);
    }

    notifier_set_default(receiver->n,
                         &async_recv_notifier,
                         (void*)receiver);

    receiver->data = recv_data_new();
    if (!receiver->data) {
        notifier_delete(receiver->n);
        pthread_mutex_destroy(&(receiver->lock));
        free(receiver);
        return NULL;
    }

    return receiver;
}

void async_receiver_delete(async_receiver receiver)
{
    if(!receiver)
        return;

    pthread_mutex_lock(&(receiver->lock));
    receiver->active = false;
    pthread_mutex_unlock(&(receiver->lock));

    pthread_mutex_lock(&(receiver->lock));
    // We mark the object as unactive twice so even in the worst case all
    // async_recv() methods will quit. The unsafe part is the lock itself
    receiver->active = false;
    notifier_delete(receiver->n);
    for(recv_data_node datum_node = recv_data_front(receiver->data);
        !recv_data_is_null(datum_node);
        datum_node = recv_data_next(datum_node))
    {
        recv_datum datum = recv_data_data(datum_node);
        free(datum->data.data);
    }
    recv_data_delete(receiver->data);
    pthread_mutex_unlock(&(receiver->lock));

    pthread_mutex_destroy(&(receiver->lock));

    free(receiver);
}

bool async_has_msg(async_receiver receiver, void* id)
{
    pthread_mutex_lock(&(receiver->lock));
    if(!receiver->active)
        return false;
    for(recv_data_node datum_node = recv_data_front(receiver->data);
        !recv_data_is_null(datum_node);
        datum_node = recv_data_next(datum_node))
    {
        recv_datum datum = recv_data_data(datum_node);
        if(datum->id != id) {
            pthread_mutex_unlock(&(receiver->lock));
            return true;
        }
    }
    pthread_mutex_unlock(&(receiver->lock));
    return false;
}

recv_pending async_recv(async_receiver receiver, void* id)
{
    recv_pending result = {0, NULL};
    while(!result.data && receiver->active) {
        unsigned int i=0;
        pthread_mutex_lock(&(receiver->lock));
        if(!receiver->active)
            return result;
        for(recv_data_node datum_node = recv_data_front(receiver->data);
            !recv_data_is_null(datum_node);
            datum_node = recv_data_next(datum_node))
        {
            recv_datum datum = recv_data_data(datum_node);
            if(datum->id != id) {
                i++;
                continue;
            }

            result = datum->data;
            recv_data_remove_at(receiver->data, i);
            if(!result.data) {
                /// WTF???
                pthread_mutex_unlock(&(receiver->lock));
                return result;
            }
            break;
        }
        pthread_mutex_unlock(&(receiver->lock));
    }

    return result;
}
